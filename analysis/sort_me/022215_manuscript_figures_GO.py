# -*- coding: utf-8 -*-
"""
Created on Wed Feb 25 15:29:45 2015

@author: gstupp


022215_manuscript_figures_GO.py


For a locus:
* Get domain annotations for each hash for all possible protdb IDs
* Generate hash_go_dict, which is the set of all go annotations for any and all domains within each hash
  by combining GO annotations for all domains within that hash:
    {'hash1': [list of GO terms], 'hash2': [another list of GO terms], ...}
* Take the most common set of GO annotations (across all hashes in that locus) as the annotation for that locus

See functional_analysis.get_go_set for details

"""

from itertools import chain
from collections import defaultdict

from analysis import functional_analysis
from file_processing.dta import build_proteins_from_peptides

HOST = 'wl-cmadmin'
from pymongo import MongoClient
sclient = MongoClient(HOST, 27018)
protDB = sclient.ProtDB_072114.ProtDB_072114


DTASelect_filter_51db = '/home/gstupp/01_2015_mass_spec/022215_manuscript_figures/microbiome_51db_human_search_noProtDB_30ppm_50ppmfrag/DTASelect_p2_oldDTA/DTASelect-filter.txt'
DTASelect_filter_HMPRef = '/home/gstupp/01_2015_mass_spec/022215_manuscript_figures/microbiome_HMPRef_search_noProtDB_30ppm_50ppmfrag/DTASelect_p2_oldDTA/DTASelect-filter.txt'
DTASelect_filter_indexdb = '/home/gstupp/01_2015_mass_spec/022215_manuscript_figures/microbiome_indexDB_search_noProtDB_30ppm_50ppmfrag/DTASelect-filter.txt'

dta_51db_human_search = build_proteins_from_peptides.main(DTASelect_filter_51db, mongo_host='wl-cmadmin', mongo_port=27017, seqdb_name='SeqDB_51db_human', seqdb_coll='SeqDB_51db_human')
dta_HMPRef_search = build_proteins_from_peptides.main(DTASelect_filter_HMPRef, mongo_host='wl-cmadmin', mongo_port=27017, seqdb_name='SeqDB_HMPRef', seqdb_coll='SeqDB_HMPRef')
dta_indexdb_search = build_proteins_from_peptides.main(DTASelect_filter_indexdb)

# Remove reverse loci
dta_51db_human_search = [locus for locus in dta_51db_human_search if not locus['Reverse']]
dta_HMPRef_search = [locus for locus in dta_HMPRef_search if not locus['Reverse']]
dta_indexdb_search = [locus for locus in dta_indexdb_search if not locus['Reverse']]

samples = [dta_51db_human_search, dta_HMPRef_search, dta_indexdb_search]
for idx,p in enumerate(chain(*samples)):
    p['go_terms'] = functional_analysis.get_db_annotations(p, from_db = ['pfam'])

# return the set of all go_terms for all domains in all hashes in all possible protIDs, parents and sub

# Number of loci that have any GO terms:
print('51db: ' + str(len([p['go_terms'] for p in dta_51db_human_search if p['go_terms']])) + ' / ' + str(len(dta_51db_human_search)))
print('HMPRef: ' + str(len([p['go_terms'] for p in dta_HMPRef_search if p['go_terms']])) + ' / ' + str(len(dta_HMPRef_search)))
print('indexdb: ' + str(len([p['go_terms'] for p in dta_indexdb_search if p['go_terms']])) + ' / ' + str(len(dta_indexdb_search)))

go_dta_51db_human_search = set(chain(*[p['go_terms'] for p in dta_51db_human_search if p['go_terms']]))
go_dta_HMPRef_search = set(chain(*[p['go_terms'] for p in dta_HMPRef_search if p['go_terms']]))
go_dta_indexdb_search = set(chain(*[p['go_terms'] for p in dta_indexdb_search if p['go_terms']]))

# Number of unique GO terms
print(len(go_dta_51db_human_search))
print(len(go_dta_HMPRef_search))
print(len(go_dta_indexdb_search))
# How many in common?
len(go_dta_indexdb_search & go_dta_HMPRef_search)
len(go_dta_indexdb_search & go_dta_51db_human_search)

# Split into MF, BP, CC
def parse_go_ontology():
    from goatools.obo_parser import GODag
    g = GODag('/home/gstupp/go/go-basic.obo')
    return g

def go_namespace(set_of_go_terms):
    goq = [go_ontology[x] for x in set_of_go_terms] #lookup go terms in ontology
    goq_ns = defaultdict(list) #dict of namespace:list of go terms in that namespace
    for g in goq:
        goq_ns[g.namespace].append(g)
    return {key:len(x) for key,x in goq_ns.items()}

go_ontology = functional_analysis.ontology
print(go_namespace(go_dta_51db_human_search))
print(go_namespace(go_dta_HMPRef_search))
print(go_namespace(go_dta_indexdb_search))







