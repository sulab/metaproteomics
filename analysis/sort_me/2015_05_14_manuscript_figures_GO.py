# -*- coding: utf-8 -*-
"""
Created on Wed Feb 25 15:29:45 2015

@author: gstupp


022215_manuscript_figures_GO.py


"""

from analysis import functional_analysis
from analysis.build_loci import build_loci_from_all_peptides
functional_analysis.init()
from itertools import chain
from collections import defaultdict
from file_processing import blazmass_tools

from pymongo import MongoClient
HOST = 'wl-cmadmin'
sclient = MongoClient(HOST, 27018)
protDB = sclient.ProtDB_072114.ProtDB_072114

from goatools import obo_parser
go_ontology = obo_parser.GODag()

from interproscan import pfam_sets
pfam_dict = pfam_sets.parse_pfam_clans()
root_path = "/mongoc/gstupp/DTASelect/unenriched/121614_SC_sampleH1sol_pepstd_HCD35/"
DTASelect_filter_51db = root_path + '51db_human_search_noProtDB_10ppm_50ppmfrag/DTASelect-filter.txt'
DTASelect_filter_HMPRef = root_path + 'HMPRef_search_noProtDB_10ppm_50ppmfrag/DTASelect-filter.txt'
DTASelect_filter_indexdb = root_path + 'indexDB_search_noProtDB_10ppm_50ppmfrag/DTASelect-filter.txt'

samples = ['db51','hmpref','compil']
sample_paths = [DTASelect_filter_51db,DTASelect_filter_HMPRef,DTASelect_filter_indexdb]
paths = {sample: sample_path for (sample, sample_path) in zip(samples, sample_paths)}

#%% Build loci, using peptides from dta select. Don't group subsets, make them each individual loci

sample_peptides = {sample: set(chain(*[x['peptide_seq'] for x in blazmass_tools.dta_select_parser(sample_path, small = True)])) for (sample, sample_path) in zip(samples, sample_paths)}

loci_51db_human = build_loci_from_all_peptides(sample_peptides['db51'], mongo_host = 'wl-cmadmin', mongo_port = 27017,
                                               seqdb_name = 'SeqDB_51db_human', seqdb_coll = 'SeqDB_51db_human', group_subsets=False)
loci_51db_human = [{'protID': v, 'peptide_seq': set(k)} for (k,v) in loci_51db_human.items()]
loci_HMPRef_human = build_loci_from_all_peptides(sample_peptides['hmpref'], mongo_host = 'wl-cmadmin', mongo_port = 27017,
                                               seqdb_name = 'SeqDB_HMPRef', seqdb_coll = 'SeqDB_HMPRef', group_subsets=False)
loci_HMPRef_human = [{'protID': v, 'peptide_seq': set(k)} for (k,v) in loci_HMPRef_human.items()]
loci_indexdb = build_loci_from_all_peptides(sample_peptides['compil'], group_subsets=False)
loci_indexdb = [{'protID': v, 'peptide_seq': set(k)} for (k,v) in loci_indexdb.items()]

#%%
from matplotlib_venn import venn3

venn3([sample_peptides['db51'], sample_peptides['hmpref'], sample_peptides['compil']], set_labels=('51db','HMPRef','ComPIL'))
#title('Peptides')

#%% go_terms for all domains

for idx,p in enumerate(chain(*[loci_51db_human, loci_HMPRef_human, loci_indexdb])):
    p['go_terms'] = functional_analysis.get_annotations_from_protIDs(p['protID'], return_go = True)

# name them
for p in chain(*[loci_51db_human, loci_HMPRef_human, loci_indexdb]):
    p['go'] = {go:go_ontology[go].name for go in p['go_terms']} if p['go_terms'] else None


# Number of loci that have any GO terms:
print('51db: ' + str(len([p['go_terms'] for p in loci_51db_human if p['go_terms']])) + ' / ' + str(len(loci_51db_human)))
print('HMPRef: ' + str(len([p['go_terms'] for p in loci_HMPRef_human if p['go_terms']])) + ' / ' + str(len(loci_HMPRef_human)))
print('indexdb: ' + str(len([p['go_terms'] for p in loci_indexdb if p['go_terms']])) + ' / ' + str(len(loci_indexdb)))

go_dta_51db_human_search = set(chain(*[p['go_terms'] for p in loci_51db_human if p['go_terms']]))
go_dta_HMPRef_search = set(chain(*[p['go_terms'] for p in loci_HMPRef_human if p['go_terms']]))
go_dta_indexdb_search = set(chain(*[p['go_terms'] for p in loci_indexdb if p['go_terms']]))

print(len(go_dta_HMPRef_search & go_dta_indexdb_search)) # hmpref is a subset of indexdb, 51db has a couple extra things in it..
print(len(go_dta_51db_human_search - go_dta_indexdb_search)) 

# Number of unique GO terms
print(len(go_dta_51db_human_search))
print(len(go_dta_HMPRef_search))
print(len(go_dta_indexdb_search))


# How many in common?
len(go_dta_indexdb_search & go_dta_HMPRef_search)
len(go_dta_indexdb_search & go_dta_51db_human_search)

# Split into MF, BP, CC
def go_namespace(set_of_go_terms):
    goq = [go_ontology[x] for x in set_of_go_terms] #lookup go terms in ontology
    goq_ns = defaultdict(list) #dict of namespace:list of go terms in that namespace
    for g in goq:
        goq_ns[g.namespace].append(g)
    return {key:len(x) for key,x in goq_ns.items()}

print(go_namespace(go_dta_51db_human_search))
print(go_namespace(go_dta_HMPRef_search))
print(go_namespace(go_dta_indexdb_search))

#%% Keep only "Leaf" go terms. Remove parents of all go terms in set
def get_leaves(go_terms):
    go_leaves = copy.copy(go_terms)
    for go in go_terms:
        go_leaves.difference_update(go_ontology[go].get_all_parents())
    return go_leaves
go_dta_51db_human_leaf = get_leaves(go_dta_51db_human_search)
go_dta_HMPRef_leaf = get_leaves(go_dta_HMPRef_search)
go_dta_indexdb_leaf = get_leaves(go_dta_indexdb_search)
print(len(go_dta_51db_human_leaf))
print(len(go_dta_HMPRef_leaf))
print(len(go_dta_indexdb_leaf))

#%% Peptide quantification per sample
SPC = dict() # ex: {'gly1': {'AAA': 43, 'ADB': 5}, 'gly2: {'AAA':23', 'TDSSD': 2}}
for (sample, sample_path) in paths.items():
    SPC[sample] = blazmass_tools.build_pep_quant_dict(sample_path, field = 'Redundancy')

#%% Map go terms to peptides
loci_dbs = [loci_51db_human, loci_HMPRef_human, loci_indexdb]
pep_go = dict()
go_quant = dict()
for sample, loci in zip(samples, loci_dbs):
    pep_go[sample] = defaultdict(set)
    for p in loci:
        if p['go_terms']:
            for go in p['go_terms']:
                pep_go[sample][go].update(p['peptide_seq'])

for sample, loci in zip(samples, loci_dbs):
    go_quant[sample] = dict()
    # for each go term, sum up all spectral counts for associated peptides
    for k,peptides in pep_go[sample].items():
        go_quant[sample][k] = sum(SPC[sample].get(pep, 0) for pep in peptides)

sorted([(go_ontology[go].name, v) for (go,v) in go_quant[samples[0]].items()], key = lambda x: x[1], reverse = True)[:10]
sorted([(go_ontology[go].name, v) for (go,v) in go_quant[samples[1]].items()], key = lambda x: x[1], reverse = True)[:10]
sorted([(go_ontology[go].name, v) for (go,v) in go_quant[samples[2]].items()], key = lambda x: x[1], reverse = True)[:10]


#%% Expand down children. Quantification of a term is the sum of counts for all children
import copy
go_quant_anc = copy.copy(go_quant)
for sample in go_quant.keys():
    for go_term in go_quant[sample].keys():
        children = go_ontology[go_term].get_all_children()
        go_quant_anc[sample][go_term] = sum([go_quant[sample].get(x,0) for x in children] + [go_quant[sample][go_term]])




