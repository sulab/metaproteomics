#!/usr/bin/env python3

# usage:
#
# peptides with 3 parents:
# cat seqSorted_renumbered_071414_indexDB_reversed.json | parallel -j+0 --block 50M --pipe python3 parent_repo_lookup.py 3 > parent_repos_3.out
#
# peptides with 2 parents:
# cat seqSorted_renumbered_071414_indexDB_reversed.json | parallel -j+0 --block 50M --pipe python3 parent_repo_lookup.py 2 > parent_repos_2.out
#
# peptides with 1 parent:
# cat seqSorted_renumbered_071414_indexDB_reversed.json | parallel -j+0 --block 50M --pipe python3 parent_repo_lookup.py 1 > parent_repos_1.out

import sys
import json
from collections import Counter

from pymongo import MongoClient


def main():

    ProtDB = MongoClient('localhost',27018)['ProtDB_072114']['ProtDB_072114']

    num_parents = int(sys.argv[1])

    repo_counter = Counter(parse_file_chunk(ProtDB, num_parents))

    # print(repo_counter)
    for key, value in repo_counter.items():
        print(key, end='\t')
        print(value)

def parse_file_chunk(ProtDB_coll, num_parents):
    for line in sys.stdin:
        obj = json.loads(line.rstrip('\n'))
        parents_list = obj['p']
        if len(parents_list) == num_parents:
            for parent_repo in lookup_parent_repos(parents_list, ProtDB_coll):
                yield parent_repo



def lookup_parent_repos(parents_list, ProtDB_coll):

    query = ProtDB_coll.find({'_id':{'$in':[parent['i'] for parent in parents_list]}})

    for protdb_entry in query:
        yield protdb_entry['r']

if __name__ == '__main__':
    main()