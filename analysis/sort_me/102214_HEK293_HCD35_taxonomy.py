# -*- coding: utf-8 -*-
"""
Last updated Feb 18 2015

@author: gstupp


Tools for generating taxonomy pie charts/stats from a DTA-select file

Work in progress

"""
ipy = get_ipython()
ipy.magic("pylab")
from pymongo import MongoClient

import taxonomy

t = taxonomy.Taxonomy(host = 'wl-cmadmin', port=27017)
client = MongoClient('wl-cmadmin', 27017)
taxDB = client.TaxDB_072114.TaxDB_072114

from file_processing.dta import build_proteins_from_peptides
from itertools import chain
from collections import Counter

from plot_tools import *
import numpy as np

def locus_intersection(a,b):
    # Loci intersect if a locus shares 1 or more ProtDB IDs with another locus
    # this is NOT reciprocal however, because a locus in 'a' can match more than one in 'b', while doing the reverse search may match one
    # Therefore, for convention, define 'a' as the longer list of loci
    (a,b) = (a,b) if len(a)>len(b) else (b,a)
    for locus in chain(a,b):
        locus['forward_loci'] = set(locus['forward_loci'])
    out = sum([any([len(locus['forward_loci'].intersection(other_locus['forward_loci']))>1 for other_locus in b]) for locus in a])
    for locus in chain(a,b):
        locus['forward_loci'] = list(locus['forward_loci'])
    return out

def locus_match(a,b):
    # Loci intersect if a locus shares 1 or more ProtDB IDs with another locus
    # If a locus in 'a' can matches more than one in 'b', count multiple matches. This is reciprocal
    # I'm not sure what this number actually means though. I'd say don't use it
    for locus in chain(a,b):
        locus['forward_loci'] = set(locus['forward_loci'])
    return sum([sum([len(locus['forward_loci'].intersection(other_locus['forward_loci'])) for other_locus in b]) for locus in a])
        
def lookup_tax(locus, subset = True):
    if locus['Reverse']:
        locus['LCA'] = None
        return locus
    if subset:
        field = 'forward_loci'
    else:
        field = 'parent_forward_loci'
    #Get taxonomy information about each protein.
    # All annotated taxIDs, for this set of proteins
    locus['taxIDs'] = taxDB.aggregate([{'$match':{'_id':{'$in':locus[field]}}},
                      {'$group': {'_id': None, 'taxid': {'$addToSet': '$taxid'}}}])['result'][0]['taxid']
    if locus['taxIDs']:
        locus['LCA'] = t.LCA(locus['taxIDs'])
    else:
        locus['LCA'] = None
    return locus

def tax_counter(loci, group_below = 5):
    c = Counter([str(p['LCA']) for p in loci])
    c = dict(c)
    other=0
    for (key, value) in list(c.items()):
        if value<=group_below:
            other += c.pop(key)
    c['Other'] = other
    return c

def plot_tax_pie(c, title):
    items = list(c.items())
    items.sort(key = lambda x: x[1])
    keys, values = list(zip(*items))
    labels = [t.taxid_to_taxonomy(int(x))[0]['scientific_name'] if x not in ['Other','None'] else x for x in keys ]
    
    cmap = plt.cm.rainbow
    pie_colors = cmap(np.linspace(0., 1., len(keys))) 
    
    fig = plt.figure(figsize=(8,8))
    ax = plt.subplot(111)
    patches = ax.pie(values, autopct='%1.1f', colors = pie_colors, labels = labels)
    ax.set_title(title)
    savefig(title + '.png', bbox_inches='tight')
    savefig(title + '.pdf', bbox_inches='tight')
    
    
# From: /mongoa/DTASelect/10_2014_mass_spec/102214_HEK293_HCD35
# Human, humanDB --quiet --sfp 0.01 -p 2
DTASelect_filter = '/home/gstupp/01_2015_mass_spec/102214_HEK293_HCD35/humanDB_search_noProtDB/DTASelect-filter.txt'
ps_humanDB = build_proteins_from_peptides.main(DTASelect_filter, mongo_host = 'wl-cmadmin', mongo_port = 27017,
                               seqdb_name = 'SeqDB_HsapiensDB_110214', seqdb_coll = 'SeqDB_HsapiensDB_110214', 
                               protdb_name = 'ProtDB_HsapiensDB_110214', protdb_coll = 'ProtDB_HsapiensDB_110214', coverage = False)
psm_humanDB = set(chain(*[p['psm'] for p in ps_humanDB]))
all_loci_humanDB = set(chain(*[p['forward_loci'] for p in ps_humanDB]))
parent_loci_humanDB = set(chain(*[p['parent_forward_loci'] for p in ps_humanDB]))

# Human, indexDB --quiet --sfp 0.01 -p 2
DTASelect_filter = '/home/gstupp/01_2015_mass_spec/102214_HEK293_HCD35/indexDB_search_noProtDB/DTASelect-filter.txt'
ps_indexDB = build_proteins_from_peptides.main(DTASelect_filter, mongo_host = 'wl-cmadmin', mongo_port = 27018, coverage = True)
psm_indexDB = set(chain(*[p['psm'] for p in ps_indexDB]))
all_loci_indexDB = set(chain(*[p['forward_loci'] for p in ps_indexDB]))
parent_loci_indexDB = set(chain(*[p['parent_forward_loci'] for p in ps_indexDB]))
loci_in_both = locus_intersection(ps_humanDB,ps_indexDB)
print('---HumanDB---')
print('# of loci: ' + str(len(ps_humanDB)))
print('# of PSMs: ' + str(len(psm_humanDB)))
print('# of pIDs (include subset): ' + str(len(all_loci_humanDB)))
print('# of parent pIDs: ' + str(len(parent_loci_humanDB)))
print('')
print('---IndexDB---')
print('# of loci: ' + str(len(ps_indexDB)))
print('# of PSMs: ' + str(len(psm_indexDB)))
print('# of pIDs (include subset): ' + str(len(all_loci_indexDB)))
print('# of parent pIDs: ' + str(len(parent_loci_indexDB)))
print('')
print('---HumanDB / IndexDB- Intersection--')
print('# of loci in both: ' + str(loci_in_both))
print('# of PSMs in both: ' + str(len(psm_humanDB.intersection(psm_indexDB))))
print('# of pIDs (include subset) in both: ' + str(len(all_loci_humanDB.intersection(all_loci_indexDB))))
print('# of parent pIDs in both: ' + str(len(parent_loci_humanDB.intersection(parent_loci_indexDB))))

# Taxonomy, include subset
ps_humanDB = [lookup_tax(locus, subset = True) for locus in ps_humanDB]
ps_indexDB = [lookup_tax(locus, subset = True) for locus in ps_indexDB]
c_humanDB = tax_counter(ps_humanDB, group_below = 5)
c_indexDB = tax_counter(ps_indexDB, group_below = 5)
#plot_tax_pie(c_humanDB, 'LCA for each locus, HEK293, humanDB, include subset')
#plot_tax_pie(c_indexDB, 'LCA for each locus, HEK293, indexDB, include subset')

# Taxonomy, parent only (no subset)
parent_ps_humanDB = [lookup_tax(locus, subset = False) for locus in ps_humanDB]
parent_ps_indexDB = [lookup_tax(locus, subset = False) for locus in ps_indexDB]
parent_c_humanDB = tax_counter(parent_ps_humanDB, group_below = 5)
parent_c_indexDB = tax_counter(parent_ps_indexDB, group_below = 5)
#plot_tax_pie(parent_c_humanDB, 'LCA for each locus, HEK293, humanDB, parent only (no subset)')
#plot_tax_pie(parent_c_indexDB, 'LCA for each locus, HEK293, indexDB, parent only (no subset)')


# are the nonhumans reverse??
len([p for p in ps_indexDB  if p['LCA'] != None and p['LCA'] != 9606 and p['Reverse']])
# No, they aren't. But maybe they are lower quality. What about Make 3 ppp?

ps_indexDB_3 = [x for x in ps_indexDB if len(x['psm'])>=3]
c_indexDB_3 = tax_counter(ps_indexDB_3, group_below = 3)
#plot_tax_pie(c_indexDB_3, 'LCA for each locus, HEK293, indexDB, include subset, 3ppp')



# MAke histogram of median coverage per locus
coverages_h = []
coverages_nh = []
for p in ps_indexDB:
    loci = iter([p] + [x for x in p['subset']])
    if p['LCA'] == 9606:
        coverages_h.extend([median(list(x['coverage'].values())) for x in loci])
    else:
        coverages_nh.extend([median(list(x['coverage'].values())) for x in loci])
plt.hist(coverages_h)
plt.hist(coverages_nh)
print(median(coverages_h))
print(median(coverages_nh))

# Toss any locus in which the median coverage is <10%
ps_indexDB_hc = [x for x in ps_indexDB if median(list(x['coverage'].values()))>.1]
c_indexDB_hc = tax_counter(ps_indexDB_hc, group_below = 3)
plot_tax_pie(c_indexDB_hc, 'LCA for each locus, HEK293, indexDB, include subset, 3ppp, hc')



        