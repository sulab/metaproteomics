# http://www.bioconductor.org/packages/release/bioc/vignettes/plgem/inst/doc/plgem.pdf
# http://bioconductor.org/packages/release/bioc/vignettes/Biobase/inst/doc/ExpressionSetIntroduction.pdf

library(plgem)
library(Biobase)

exprsFile = '/home/gstupp/projects/Wolan/ana_probe_cmkGLP/2015_04_17_clustering/gly_phe.csv'
m = as.matrix(read.table(exprsFile, sep=',', row.names = 1, header = TRUE, as.is = TRUE))

#normalize columsn
#m = m / t(replicate(nrow(m), colMeans(m)))

minimalSet <- ExpressionSet(assayData=m)

pData = data.frame(sample = 1:length(colnames(m)),
           sample.name = colnames(m),
           probe = sapply(colnames(m), function(x) substring(x,1,nchar(x)-1)),
           #probe = rep(c("gly","phe"),c(3,3)),
           all = rep(1,length(colnames(m))))
row.names(pData) <- pData$sample.name
phenoData <- new("AnnotatedDataFrame",data=pData)
exampleSet <- ExpressionSet(assayData=m, phenoData=phenoData)

LPSfit <- plgem.fit(data=exampleSet, covariate='probe', fitCondition='gly', p=8, q=0.5,
                    plot.file=FALSE, fittingEval=TRUE, verbose=TRUE,
                    trimAllZeroRows = TRUE, zeroMeanOrSD = "trim")

LPSobsStn <- plgem.obsStn(data=exampleSet, covariate='probe', baselineCondition='phe', plgemFit=LPSfit, verbose=TRUE)

LPSresampledStn <- plgem.resampledStn(data=exampleSet, plgemFit=LPSfit, covariate='probe', baselineCondition='phe', 
                                      iterations="automatic", verbose=TRUE)

#LPSpValues <- plgem.pValue2(observedStn=LPSobsStn, plgemResampledStn=LPSresampledStn, verbose=TRUE, alt.hyp = 'greater')
LPSpValues <- plgem.pValue(observedStn=LPSobsStn, plgemResampledStn=LPSresampledStn, verbose=TRUE)

LPSdegList <- plgem.deg(observedStn=LPSobsStn, plgemPval=LPSpValues, delta=0.01, verbose=TRUE)
STN = LPSobsStn$PLGEM.STN
colnames(STN)[1] = 'STN'
m2 = cbind(m, STN, LPSpValues)
sig_m = m2[match(LPSdegList$significant$`0.01`$gly_vs_phe,row.names(m2)),]
m2_sort = m2[order(m2[,'gly_vs_phe']),]

match(STN,row.names(sig_m))
write.csv(LPSpValues, file = '/home/gstupp/projects/Wolan/ana_probe_cmkGLP/2015_04_17_clustering/LPSpValues.csv')
write.csv(m2_sort, file = '/home/gstupp/projects/Wolan/ana_probe_cmkGLP/2015_04_17_clustering/m2_sort.csv')

