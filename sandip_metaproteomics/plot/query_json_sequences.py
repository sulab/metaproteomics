#!/usr/bin/env python

# query_json_sequences.py
# script to query refseq MongoDB databases (indexDBs) using a pregenerated JSON file
#
# usage: $ cat sorted_renumbered_RefSeq44.json | python query_json_sequences.py 43 > output.txt
# (this exact command would extract peptide sequences from RefSeq44 and query them against RefSeq 43)
#
# using GNU Parallel:
# $ cat sorted_renumbered_RefSeq44.json | parallel -j12 --block 5M --tmpdir /mongob/create_indexDB/tmp --pipe python query_json_sequences.py 43 > output.txt
#
# Sandip Chatterjee, 3/28/14


import re
import sys
from pymongo import MongoClient

def main():
	
	refseq_DB_to_query = sys.argv[1]
	JSON_file = sys.stdin
	query_sequence = ''

	client = MongoClient('localhost', 27017)
	db = client['refseqs']
	collection = db['refseq'+refseq_DB_to_query]

	for line in JSON_file:
		query_sequence = re.findall('(\SEQ": ")(\S+)("},)',line)[0][1]
		query_result = collection.find_one({"SEQ": query_sequence},{"MASS":1}) # will only return MASS value
		if query_result:
			# print query_result['MASS']
			pass
		else:
			print re.findall('(\MASS": )(\d+)',line)[0][1]


	client.close()

if __name__ == '__main__':
	main()