#!/usr/local/bin/python

#	pfam_family_stats.py
#	Sandip Chatterjee
#	v1, August 29, 2013
#	For generating simple plots from an input HMMER tabular results file and an input multi-protein FASTA file
#	Usage: 
#	$ python pfam_family_stats.py HMMER_results_file_tblout.txt Gastrointestinal_tract_proteins.fsa

import sys
import re
import matplotlib as plt

def main():
	
	print "Pfam family stats"
	print "Given a HMMER3 tabular output file, reads protein matches, removes duplicates, and generates various plots"

	hmmer_results_file = sys.argv[1]
	fasta_file = sys.argv[2]

	hmmer_results_dict = read_hmm(hmmer_results_file)
	
	print "Number of unique HMMER entries in hmmer_results_file:", len(hmmer_results_dict)

	proteins_per_species_dict = count_proteins_per_species(fasta_file)

	print "Number of unique species entries in fasta_file:", len(proteins_per_species_dict)
	##	Probably good to make a plot of this dictionary!

	proteins_per_genus_dict = count_proteins_per_genus(proteins_per_species_dict)

	from code import interact
	interact(local=locals())

	print "Finished"

##	returns a dictionary of protein IDs
##	{key:value} = {HMP_protein_ID:number_of_occurrences}
def read_hmm(hmmer_results_file):

	hmmer_output = []
	with open(hmmer_results_file,'rb') as f:
		hmmer_output = f.readlines()

	hmmer_output = [line.strip('\n') for line in hmmer_output if line[0] != '#']
	print "Number of HMMER matches in file '"+hmmer_results_file+"': "+str(len(hmmer_output))

	hmmer_dict = {}
	for line in hmmer_output:
		HMP_ID = line.split()[0].strip()
		if HMP_ID in hmmer_dict:
			hmmer_dict[HMP_ID] += 1
		else:
			hmmer_dict[HMP_ID] = 1

	return hmmer_dict

def count_proteins_per_species(fasta_file):
	
	proteins_per_species_dict = {}

	with open(fasta_file) as f:
		for line in f:
			if line[0] == '>':
				organism_ID = re.findall(r"(?<=\[)[^\[]+(?=\])",line)[-1]
				if organism_ID in proteins_per_species_dict:
					proteins_per_species_dict[organism_ID] += 1
				else:
					proteins_per_species_dict[organism_ID] = 1
				
	return proteins_per_species_dict

def count_proteins_per_genus(proteins_per_species_dict):
	
	proteins_per_genus_dict = {}

	for species in proteins_per_species_dict:
		genus = species.split()[0].strip()
		print genus
		if genus in proteins_per_genus_dict:
			proteins_per_genus_dict[genus] += proteins_per_species_dict[species]
		else:
			proteins_per_genus_dict[genus] = proteins_per_species_dict[species]

	return proteins_per_genus_dict

if __name__ == '__main__':
	main()
