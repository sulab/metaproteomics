#!/opt/applications/python/2.7.1/gnu/bin/python

import re
import sys
json_file = sys.argv[1]
with open(json_file,'rb') as f, open(json_file+'_MASS','wb') as f2, open(json_file+'_COUNT','wb') as f3:
    for line in f:
        f2.write(re.findall('(\MASS": )(\d+)',line)[0][1]+'\n')
        f3.write(re.findall('(\COUNT": )(\d+)',line)[0][1]+'\n')

print "finished"