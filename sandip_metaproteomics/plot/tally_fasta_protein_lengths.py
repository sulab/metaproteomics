#!/usr/bin/env python

# tally_fasta_protein_lengths.py
# 
# script for tallying protein lengths in a FASTA file
# requires Python 2.7 (due to Counter class)

import sys
from collections import Counter

def main():
	
	DB_file = sys.argv[1]
	count = Counter()
	len_list = []

	with open(DB_file) as f:

		line = f.readline()
		
		while True:
			defline = ''
			sequence_lines = []
			# protein_dicts = [] ##	list of dicts (one per peptide), to be unpacked before appending to current_chunk
			if not line:
				break
			if line[0] == '>':
				defline = line[1:].rstrip('\n')		##	remove leading '>' and trailing newline
				while True:
					line = f.readline()
					if not line or line[0] == '>':
						break
					sequence_lines.append(line.rstrip('\n'))

			full_sequence = ''.join(sequence_lines).replace('\n','')
			count[str(len(full_sequence))] += 1
			len_list.append(len(full_sequence))
	
	len_list.sort()
	for x in len_list:
		print x

	# sorted_count_keys = [int(x) for x in count.keys()]
	# sorted_count_keys.sort()
	# for key in sorted_count_keys:
	# 	print str(key)+','+str(count[str(key)])

if __name__ == '__main__':
	main()