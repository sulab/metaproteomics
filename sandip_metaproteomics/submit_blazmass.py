#!/usr/bin/env python3

# submit_blazmass.py
#
# For splitting an input MS2 file and submitting to the Garibaldi cluster
# (need to load Python3 module using 'module load python/3.3.2')
# 
# Sandip Chatterjee
# v4.1, December 1, 2014
#
# usage: submit_blazmass.py [-h] [-s] [-c] [-p SPLITPREFIX] MS2_input split_n

# positional arguments:
#   MS2_input             Path to input MS2 file
#   split_n               Split input file into n subfiles

# optional arguments:
#   -h, --help            show this help message and exit
#   -s, --submit          Submit jobs to Garibaldi queue
#   -c, --cleanup         Run final cleanup job after all search jobs complete
#   -p SPLITPREFIX, --splitprefix SPLITPREFIX
#                         Prefix for split MS2 files (default is input filename)

# Some typical arguments for indexDB search on MicroCloud sharded MongoDB:
# python3 submit_blazmass.py -s -n 4 -m 12 all 25
# "-n 4"  -- requests 4 cores (since mongos is being started, runs Blazmass with n-1 = 3 search threads)
# "-m 12" -- requests 12GB of RAM (allocating 4GB per search thread -- probably necessary for indexDB search since all candidates are scored and stored in RAM)
# "all"   -- MS2 filename to split and submit. "all" means submit all MS2 files in directory (anything matching *.ms2)
# "25"    -- splits each input MS2 file into 25 chunks with roughly equal numbers of scans


import os
import sys
import glob
import socket
import argparse
import subprocess

def main():

	new_file_directory = 'dummy'

	parser = argparse.ArgumentParser()
	parser.add_argument('MS2_input', help='Path to input MS2 file (use "all" for all .ms2 files in current directory)')
	parser.add_argument('split_n', help='Split each input file into n subfiles', type=int)
	parser.add_argument('-s', '--submit', help='Submit jobs to Garibaldi queue', action='store_true')
	parser.add_argument('-c', '--cleanup', help='Run final cleanup job after all search jobs complete', action='store_true')
	parser.add_argument('-n', '--numcores', help='Number of cores per job', type=int)
	parser.add_argument('-t', '--numthreads', help='Number of threads per search job', type=int)	
	parser.add_argument('-m', '--memgb', help='Amount of available RAM per job (in GB)', type=int)
	parser.add_argument('-w', '--walltime', help='Amount of walltime to request per job (in hours)', type=int)
	parser.add_argument('-p', '--splitprefix', help='Prefix for split MS2 files (default is input filename)')
	parser.add_argument('--nomongos', help='Don\'t start a mongos process on localhost', action='store_true')

	args = parser.parse_args()

	MS2_file = args.MS2_input

	if MS2_file == 'all':
		MS2_files = glob.glob('*.ms2')
		for MS2f in MS2_files:
			success = split_and_submit_one_MS2(MS2f, new_file_directory, args)
			if success:
				print('Jobs submitted successfully for MS2 file {}\n'.format(MS2f))
			else:
				print('!! Error submitting jobs for MS2 file {}'.format(MS2f))
	else:
		success = split_and_submit_one_MS2(MS2_file, new_file_directory, args)
		if success:
			print('Jobs submitted successfully for MS2 file {}\n'.format(MS2_file))
		else:
			print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'+len(MS2_file)*'!')
			print('!! Error submitting jobs for MS2 file {} !!'.format(MS2_file))
			print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'+len(MS2_file)*'!')


	# with open(MS2_file) as MS2_input:
	# 	MS2_scans = create_MS2_chunks(MS2_input)

	# if not MS2_scans:
	# 	print("No scans in MS2 file. Exiting")
	# 	sys.exit(1)

	# if args.splitprefix:
	# 	filename_prefix = args.splitprefix
	# else:
	# 	filename_prefix = MS2_file.replace('.ms2','')

	# n = args.split_n

	# MS2_scan_blocks = split_MS2_scans(MS2_scans, n)

	# new_MS2_filenames = create_MS2_files(MS2_scan_blocks, filename_prefix, new_file_directory)

	# num_jobs = len(new_MS2_filenames)

	# if args.submit:
	# 	submit_flag = True
	# else:
	# 	submit_flag = ask_about_submitting_jobs(num_jobs)

	# if submit_flag:
	# 	if 'garibaldi' in socket.gethostname():
	# 		submit_jobs(new_MS2_filenames, new_file_directory, MS2_file, args)
	# 	else:
	# 		print("Jobs need to be submitted from Garibaldi")

def split_and_submit_one_MS2(MS2_file, new_file_directory, args):

	with open(MS2_file) as MS2_input:
		MS2_scans = create_MS2_chunks(MS2_input)

	if not MS2_scans:
		print('No scans in MS2 file {} -- skipping'.format(MS2_file))
		return False

	if args.splitprefix:
		filename_prefix = args.splitprefix
	else:
		filename_prefix = MS2_file.replace('.ms2','')

	n = args.split_n

	MS2_scan_blocks = split_MS2_scans(MS2_scans, n)

	new_MS2_filenames = create_MS2_files(MS2_scan_blocks, filename_prefix, new_file_directory)

	num_jobs = len(new_MS2_filenames)

	# if args.submit:
		# submit_flag = True
	# else:
		# submit_flag = ask_about_submitting_jobs(num_jobs)

	if args.submit:
		if 'garibaldi' in socket.gethostname():
			submit_jobs(new_MS2_filenames, new_file_directory, MS2_file, args)
			return True
		else:
			print("Jobs need to be submitted from Garibaldi")
			return False
	else:
		return True

def ask_about_submitting_jobs(num_jobs):

	''' prompt user about submitting jobs to garibaldi cluster '''

	answer = input("Submit "+str(num_jobs)+" jobs to cluster? (y/n) ")

	if  answer == 'y' or answer == 'yes':
		submit_flag = True
	else:
		submit_flag = False

	return submit_flag

def extract_H_lines(MS2_input):

	''' extracts the first set of H lines from input and returns as a list, 1 item per line '''

	H_lines = []

	while True:
		line = MS2_input.readline()
		if line[:2] == 'H\t':
			H_lines.append(line)
		if not line:
			break
		if H_lines and line[:2] != 'H\t':
			break

	return H_lines

def create_MS2_chunks(MS2_input):

	''' returns a list of "chunks" of the MS2 file, with one chunk per scan '''

	ms2_scan_chunks = []
	current_chunk = []

	for line in MS2_input:
		if line[:2] != 'H\t':
			if line[:2] == 'S\t' and current_chunk:
				ms2_scan_chunks.append(current_chunk)
                # do something with scan data block (current_chunk) here!

				current_chunk = []  ## reset current block
				current_chunk.append(line)
			else:
				current_chunk.append(line)
	else:
		ms2_scan_chunks.append(current_chunk) ## add final block to chunk after for loop ends

	print("Parsed", len(ms2_scan_chunks), "scans from MS2 input")

	return ms2_scan_chunks

def split_MS2_scans(MS2_scans, n):

	''' splits a list of MS2 chunks into n sublists '''
	''' (generator function) '''

	return (MS2_scans[i::n] for i in range(n if n < len(MS2_scans) else len(MS2_scans)))

def create_MS2_files(MS2_scan_blocks, filename_prefix, new_file_directory, H_lines=[]):

	''' creates new MS2 files based on filename prefix '''

	if not os.path.isdir(new_file_directory):
		os.makedirs(new_file_directory)

	new_MS2_filenames = []

	for block_num, MS2_block in enumerate(MS2_scan_blocks):
		new_file_name = filename_prefix+'_'+str(block_num+1)+'.ms2'
		with open(new_file_directory+'/'+new_file_name, 'w') as f:
			for chunk in MS2_block:
				f.write(''.join(H_lines))
				f.write(''.join(chunk))
			print(len(MS2_block), "scans written to", new_file_name)
		new_MS2_filenames.append(new_file_name)

	print("Created", len(new_MS2_filenames) ,"MS2 files")

	return new_MS2_filenames

def submit_jobs(MS2_filenames, new_file_directory, MS2_file, args):

	''' creates .job files and submits jobs to Garibaldi cluster (PBS scheduler) '''

	job_filepaths = [new_file_directory+'/'+fname.replace('.ms2','.job') for fname in MS2_filenames]
	sqt_output = [new_file_directory+'/'+fname.replace('.ms2','.sqt') for fname in MS2_filenames]

	# num_cores is the number of cores requested in the PBS job script
	if args.numcores:
		num_cores = args.numcores
	else:
		num_cores = 9

	# num_threads is the number of threads to run Blazmass using (should be <= num_cores)
	if args.numthreads:
		num_threads = args.numthreads
		if num_threads > num_cores:	# don't want 18 threads running on 16 cores, for example
			num_threads = num_cores
	else:
		if args.nomongos:
			num_threads = num_cores
		else:
			num_threads = num_cores - 1 # allowing an extra core to handle mongos CPU usage
	if num_threads < 1:
		num_threads = 1

	if args.memgb:
		mem_gb = args.memgb
	else:
		mem_gb = num_threads * 4 # allowing 4GB RAM per thread

	if args.walltime:
		walltime_hours = args.walltime
	else:
		walltime_hours = 24

	cputime_hours = walltime_hours * num_threads

	job_boilerplate = 			['#!/bin/bash',
								'#PBS -l nodes=1:ppn={}'.format(num_cores),
								'#PBS -l cput={}:00:00'.format(cputime_hours),
								'#PBS -l walltime={}:00:00'.format(walltime_hours),
								# '#PBS -e localhost:/dev/null',
								# '#PBS -o localhost:/dev/null',
								'#PBS -j oe',
								'#PBS -l mem={}gb'.format(mem_gb)
								]

	# mongo_start = 'mongos --configdb wl-cm01:27019,wl-cm02:27019,wl-cm03:27019 --port 27018 > /dev/null &'
	mongo_start = 'mongos --configdb wl-cm01:27019,wl-cm02:27019,wl-cm03:27019 --port 27018 --fork --logpath /dev/null --pidfilepath $PBSTMPDIR/mongod.pid'
	# mongo_proc = 'MONGOS_PID=`echo $!`'
	mongo_proc = 'MONGOS_PID=`cat $PBSTMPDIR/mongod.pid`'
	blazmass_generic_invocation = 'java -jar /gpfs/home/sandip/blazmass_search/blazmass/blazmass.jar . $MS2FILE blazmass.params {} 1> /dev/null'.format(num_threads)
	kill_mongo = 'kill -9 $MONGOS_PID'

	if not os.path.exists('blazmass.params'):
		print("Need a blazmass.params file...")
		sys.exit(1)

	if not os.path.exists(new_file_directory+'/blazmass.params'):
		# create symlink to blazmass.params file in parent directory
		subprocess.Popen(['ln','-s','../blazmass.params','.'],cwd=new_file_directory)

	PBS_job_ids = []
	job_num = 1
	for job_file, ms2_file in zip(job_filepaths, MS2_filenames):
		with open(job_file,'w') as f:
			blazmass_invoc = blazmass_generic_invocation.replace('$MS2FILE',ms2_file)
			for line in job_boilerplate:
				f.write(line+'\n')
			f.write('#PBS -N "blazmass_mongo{}"\n'.format(job_num))
			f.write('module load java/1.7.0_21\n')  # load Java 1.7
			f.write('cd $PBS_O_WORKDIR\n')
			if not args.nomongos:
				f.write(mongo_start+'\n')
				f.write(mongo_proc+'\n')
			f.write(blazmass_invoc+'\n')
			if not args.nomongos:
				f.write(kill_mongo+'\n')
			f.write('echo "Finished"\n')
		job_num += 1
		process = subprocess.Popen(['qsub',job_file.split('/')[1]],stdout=subprocess.PIPE,cwd=new_file_directory)
		stdout = process.communicate()[0].decode('utf-8')
		PBS_job_id = stdout.split('.')[0]
		print("Submitted search job", PBS_job_id)
		PBS_job_ids.append(PBS_job_id)

	if args.cleanup:
		submit_cleanup_job(PBS_job_ids, sqt_output, new_file_directory, MS2_file)
		# submit_cleanupfail_job(PBS_job_ids, sqt_output, new_file_directory, MS2_file)

def submit_cleanup_job(PBS_job_ids, sqt_output, new_file_directory, MS2_file):

	''' submits a "cleanup" job that runs only after all work jobs have successfully completed '''

	cleanup_boilerplate = 		['#!/bin/bash',
								'#PBS -l nodes=1:ppn=1',
								'#PBS -l walltime=1:00:00',
								'#PBS -j oe',
								'#PBS -l mem=1gb'
								]

	first_job_id = PBS_job_ids[0]
	last_job_id = PBS_job_ids[-1]
	job_file_name = 'cleanup{}_{}.job'.format(first_job_id, last_job_id)
	cleanup_jobfile = new_file_directory+'/'+job_file_name

	with open(cleanup_jobfile,'w') as f:
		f.write('\n'.join(cleanup_boilerplate)+'\n')
		f.write('#PBS -N "cleanup{}_{}"\n'.format(first_job_id, last_job_id))
		f.write('#PBS -W depend=afterok:'+':'.join(PBS_job_ids)+'\n')
		f.write('TIMESECONDS=`date +"%s"`\n')
		f.write('cd $PBS_O_WORKDIR\n')
		f.write('cd ..\n')
		f.write('grep "^H" '+sqt_output[0]+' > $TIMESECONDS"_sqt_H_lines"\n')
		f.write('grep -vh "^H" '+' '.join(sqt_output)+' > $TIMESECONDS"_sqt_data"\n')
		f.write('cat $TIMESECONDS"_sqt_H_lines" $TIMESECONDS"_sqt_data" > {}'.format(MS2_file.replace('.ms2','.sqt')+'\n'))
		f.write('rm $TIMESECONDS"_sqt_H_lines" $TIMESECONDS"_sqt_data"\n')

	process = subprocess.Popen(['qsub', job_file_name],stdout=subprocess.PIPE,cwd=new_file_directory)
	print("Submitted cleanup job", process.communicate()[0].decode('utf-8').split('.')[0])

def submit_cleanupfail_job(PBS_job_ids, sqt_output, new_file_directory, MS2_file):

	''' submits a "cleanup failure" job that runs only if jobs in PBS_job_ids fail '''

	cleanup_boilerplate = 		['#!/bin/bash',
								'#PBS -l nodes=1:ppn=1',
								'#PBS -l walltime=1:00:00',
								'#PBS -j oe',
								'#PBS -l mem=1gb'
								]

	first_job_id = PBS_job_ids[0]
	last_job_id = PBS_job_ids[-1]
	job_file_name = 'cleanupfail{}_{}.job'.format(first_job_id, last_job_id)
	cleanup_jobfile = new_file_directory+'/'+job_file_name

	with open(cleanup_jobfile,'w') as f:
		f.write('\n'.join(cleanup_boilerplate)+'\n')
		f.write('#PBS -N "cleanupfail{}_{}"\n'.format(first_job_id, last_job_id))
		f.write('#PBS -W depend=afternotok:'+':'.join(PBS_job_ids)+'\n')
		f.write('TIMESECONDS=`date +"%s"`\n')
		f.write('cd $PBS_O_WORKDIR\n')
		f.write('cd ..\n')
		f.write('ERRORFILE="error_"$TIMESECONDS\n')
		f.write('echo "One or more of the following jobs IDs failed to complete searching:" > $ERRORFILE\n')
		f.write('echo "{}" >> $ERRORFILE\n'.format(' '.join(PBS_job_ids)))
		f.write('echo "" >> $ERRORFILE\n')
		f.write('echo "One or more of the following MS2 files failed to complete searching:" >> $ERRORFILE\n')
		f.write('echo "{}" >> $ERRORFILE\n'.format(' '.join([filename.replace('.sqt','.ms2') for filename in sqt_output])))
		f.write('echo "" >> $ERRORFILE\n')
		# f.write('grep "^H" '+sqt_output[0]+' > $TIMESECONDS"_sqt_H_lines"\n')
		# f.write('grep -vh "^H" '+' '.join(sqt_output)+' > $TIMESECONDS"_sqt_data"\n')
		# f.write('cat $TIMESECONDS"_sqt_H_lines" $TIMESECONDS"_sqt_data" > {}'.format(MS2_file.replace('.ms2','.sqt')+'\n'))
		# f.write('rm $TIMESECONDS"_sqt_H_lines" $TIMESECONDS"_sqt_data"\n')

	process = subprocess.Popen(['qsub', job_file_name],stdout=subprocess.PIPE,cwd=new_file_directory)
	print("Submitted cleanup/fail job", process.communicate()[0].decode('utf-8').split('.')[0])

if __name__ == '__main__':
	main()