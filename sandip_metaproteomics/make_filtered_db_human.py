import sys
from pymongo import MongoClient
#import hashlib

def main():
    
    client = MongoClient('wl-cmadmin', port = 27017)
    protDB = client.ProtDB_uniprot_human.ProtDB_uniprot_human

    for line in sys.stdin:
        protein_str = line.strip()
        if protein_str == '':
            continue
        if protein_str.startswith('Reverse_'):
            protein_int=int(protein_str[8:])
        else:
            try:
                protein_int=int(protein_str)
            except:
                print('Bad value: ' + protein_str)
                continue

        protDB_query = protDB.find_one({'_id':protein_int})
        #print('>'+protein_str+'|'+hashlib.md5(protDB_query['s'].encode('utf-8')).hexdigest())
        print('>'+protein_str)
        print(protDB_query['s'])

if __name__ == '__main__':
        main()

'''
# test times
# %timeit -n 200 redunDB.find_one({'pID':protein_int})
# %timeit -n 200 hashlib.md5(protDB_query['s'].encode('utf-8')).hexdigest()

# Result: MUCH faster to hash than to look up the hash
# but it wont search correctly with dta-select now....
'''