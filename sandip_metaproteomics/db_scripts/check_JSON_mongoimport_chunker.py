#!/usr/bin/env python3

# check_JSON_mongoimport_chunker.py
#
# submits check_JSON_mongoimport jobs across cluster

import sys
import glob
import socket
import argparse
import subprocess

def main():
    
    parser = argparse.ArgumentParser(description='script to create and submit check_JSON_mongoimport jobs for all files in a directory')
    parser.add_argument('db', help='MongoDB database name')
    parser.add_argument('collection', help='MongoDB collection name')
    parser.add_argument('prefix', help='prefix for files (to use for glob)')
    parser.add_argument('-i', '--insert', help='insert JSON objects if not found in DB', action='store_true')
    parser.add_argument('-s', '--submit', help='submit all jobs to garibaldi', action='store_true')
    parser.add_argument('--host', help='MongoDB hostname (mongod or mongos) -- default: localhost', default='localhost')
    parser.add_argument('--port', help='MongoDB port (mongod or mongos) -- default: 27017', default=27017, type=int)
    parser.add_argument('--nomongos', help='Don\'t start a mongos process on localhost', action='store_true')
    parser.add_argument('-q', '--quiet', help='Suppress "Document not found" messages', action='store_true')

    args = parser.parse_args()

    if args.submit:
        if 'garibaldi' not in socket.gethostname():
            print("Jobs need to be submitted from Garibaldi. Exiting. (remove -s option)")
            sys.exit(1)

    filenames = glob.glob('{}*'.format(args.prefix))

    filenum = 0
    for filenum, filename in enumerate(filenames):
        job_filename = create_jobfile(filenum+1, filename, args)
        if args.submit:
            submit_job(job_filename)

    print('Done creating {} jobs'.format(filenum+1))

def create_jobfile(filenum, filename, args):

    ''' creates job file for Garibaldi cluster (PBS scheduler) '''


    num_cores = 4   ## num_cores is the number of cores requested in the PBS job script
    mem_gb = 8
    walltime_hours = 24
    cputime_hours = walltime_hours * num_cores

    host = args.host
    port = args.port

    job_boilerplate =           ['#!/bin/bash',
                                '#PBS -l nodes=1:ppn={}'.format(num_cores),
                                '#PBS -l cput={}:00:00'.format(cputime_hours),
                                '#PBS -l walltime={}:00:00'.format(walltime_hours),
                                '#PBS -o {}.out'.format(filename),
                                '#PBS -j oe',
                                '#PBS -l mem={}gb'.format(mem_gb)
                                ]

    if 'garibaldi' in socket.gethostname():
        cluster = 'garibaldi'
        mongos_node_prefix = 'node'
        home_directory = '/gpfs/home/sandip'
    elif 'ims' in socket.gethostname():
        cluster = 'ims'
        mongos_node_prefix = 'imsb'
        home_directory = '/gpfs/ims/home/sandip'
    else:
        print('Unrecognized cluster... exiting') # shouldn't ever get here...
        sys.exit(1)

    if not args.nomongos:
        host = '$MONGOHOST'
        mongos_choice_code = '''MONGOSNODES_PATH={home_directory}/mongosnodes

if ! test -n "$(find $MONGOSNODES_PATH -maxdepth 1 -name '{mongos_node_prefix}*' -print -quit)"; then
    echo "No mongos node information found at $MONGOSNODES_PATH -- exiting"
    exit 1
fi

# mongo database and collection names for testing mongos connection
DBNAME="SeqDB_072114"
COLLNAME="SeqDB_072114"

function remove_job_running_file {{
    if ! [ -z ${{JOB_RUNNING_FILE+x}} ] && [ -f $JOB_RUNNING_FILE ]; then 
        rm -rf $JOB_RUNNING_FILE;
    fi
}}

function pick_nearby_mongos {{
    for i in `ls $MONGOSNODES_PATH/{mongos_node_prefix}* | shuf`; do
        if grep -q `hostname` $i; then
            MONGOHOST=`head -1 $i`;
            break;
        fi;
    done
}}

function pick_any_mongos {{
    RANDOM_MONGOS=`ls $MONGOSNODES_PATH/{{imsb,node}}* | shuf | head -1`;
    MONGOHOST=`head -1 $RANDOM_MONGOS`;
}}

echo
echo "Trying to connect to nearby Garibaldi mongos"
j="1"
while [ $j -lt 4 ]
do
    echo -n "Attempt #$j of 3"
    pick_nearby_mongos

    # test if MONGOHOST is set, and then test if a successful connection can be made to mongos on MONGOHOST
    # (if connection fails, unset MONGOHOST and run pick_nearby_mongos again)
    if [ ! -z ${{MONGOHOST+x}} ] && mongo $DBNAME --quiet --host $MONGOHOST --port 27018 --eval "db.$COLLNAME.findOne()" 2> /dev/null 1> /dev/null; then
        echo "... successfully connected to mongos on "$MONGOHOST;
        break;
    else
        echo "... failed"
        unset MONGOHOST
    fi
    j=$[$j+1]
done

# if unable to connect to a MONGOHOST from nearby nodes, try to pick any Garibaldi mongos...
if [ -z ${{MONGOHOST+x}} ]; then
    echo
    echo "Could not connect to nearby mongos. Trying to connect to any available Garibaldi mongos..."
    j="1"
    while [ $j -lt 4 ]; do
        echo -n "Attempt #$j of 3"
        pick_any_mongos
        
        # test if MONGOHOST is set, and then test if a successful connection can be made to mongos on MONGOHOST
        # (if connection fails, unset MONGOHOST and run pick_any_mongos again)
        if [ ! -z ${{MONGOHOST+x}} ] && mongo $DBNAME --quiet --host $MONGOHOST --port 27018 --eval "db.$COLLNAME.findOne()" 2> /dev/null 1> /dev/null; then
            echo "... successfully connected to mongos on "$MONGOHOST;
            break;
        else
            echo "... failed"
            unset MONGOHOST
        fi
        j=$[$j+1]
    done
fi

# if still unable to connect to a MONGOHOST, exit...
if [ -z ${{MONGOHOST+x}} ]; then
    echo "Unable to connect to mongos. Exiting";
    exit 1;
fi'''
        customization_dict = {
                    'mongos_node_prefix':mongos_node_prefix,
                    'home_directory':home_directory
                    }

    if args.insert:
        if args.quiet:
           check_JSON_mongoimport_invocation = 'python3 check_JSON_mongoimport.py -i --quiet --host {} --port {} {} {} {}'.format(host, port, '$PBSTMPDIR/{}'.format(filename), args.db, args.collection)
        else:
           check_JSON_mongoimport_invocation = 'python3 check_JSON_mongoimport.py -i --host {} --port {} {} {} {}'.format(host, port, '$PBSTMPDIR/{}'.format(filename), args.db, args.collection)
    else:
        if args.quiet:
            check_JSON_mongoimport_invocation = 'python3 check_JSON_mongoimport.py --quiet --host {} --port {} {} {} {}'.format(host, port, '$PBSTMPDIR/{}'.format(filename), args.db, args.collection)
        else:
            check_JSON_mongoimport_invocation = 'python3 check_JSON_mongoimport.py --host {} --port {} {} {} {}'.format(host, port, '$PBSTMPDIR/{}'.format(filename), args.db, args.collection)

    cleanup_mongo = 'function cleanup_mongo {\n\techo "killing mongos process..."\n\tkill $MONGOS_PID\n\tif ! ps -e | grep -q mongos; then echo "mongos process no longer running"; else echo "mongos process still running!"; echo $(ps -e | grep mongos); kill -9 $MONGOS_PID; fi\n}'
    trap_sigterm = 'trap \'echo "! Received SIGTERM"; cleanup_mongo; exit\' TERM'
    trap_err = 'trap \'echo "! An error occurred"; cleanup_mongo; exit\' ERR'

    job_filename = '{}.job'.format(filename)
    with open(job_filename, 'w') as f:
        for line in job_boilerplate:
            f.write(line+'\n')
        f.write('#PBS -N "mongocheck{}"\n'.format(filenum))
        f.write('set -e\n')
        f.write('module load python/3.3.2\n')
        f.write('echo "################################################################################"\n')
        f.write('echo "Processing JSON chunk: {}"\n'.format(filename))
        f.write('echo "PBS job script file: {}"\n'.format(job_filename))
        f.write('echo "Running job on host `hostname`"\n')
        f.write('echo "################################################################################"\n')

        f.write('cd $PBS_O_WORKDIR\n')
        f.write('rsync -av {} $PBSTMPDIR\n'.format(filename))
        if not args.nomongos:
            # f.write(trap_err+'\n')          ## run cleanup_mongo bash function if ERR signal received
            # f.write(trap_sigterm+'\n')      ## run cleanup_mongo bash function if SIGTERM signal received
            f.write(mongos_choice_code.format(**customization_dict)+'\n')
            f.write('echo\n')
            f.write('mongo --host {} --port {} --eval "sh.status()"\n'.format(host, port))
        f.write(check_JSON_mongoimport_invocation+'\n')
        f.write('echo "Finished"\n')

    return job_filename


def submit_job(job_filename):

    ''' submits job to Garibaldi cluster (PBS scheduler) '''

    process = subprocess.Popen(['qsub',job_filename],stdout=subprocess.PIPE)
    stdout = process.communicate()[0].decode('utf-8')
    PBS_job_id = stdout.split('.')[0]

    print("Submitted search job", PBS_job_id)

if __name__ == '__main__':
    main()