#!/usr/local/bin/python

#	mongomass.py
#	Sandip Chatterjee
#	v4.1, October 16, 2013
#	
#	For generating a MongoDB representation of a large (larger than memory) protein database
#	"indexDB"
#	
#	Usage:
#	$ python mongomass.py protein_DB.fasta [blazmass.params]

##################################  TO DO:  ##################################

# rewrite trypsin_digest() function to allow for half-tryptic peptides and missed cleavages (see Robin's cutseq.java code)
	#
	# more information to read from blazmass.params file:
	#
	# enzyme_residues = KR
	# enzyme_cut = c  : this means it cuts in c terminus
	# miscleavage = 1 :  1 is half , and 2 is fully tryptic
	# max_num_internal_cleavage_sites = 3 : max internal miscleavage is 3
	# enzyme_nocut_residues = P : means no cut if it is followed by P.  This is common for tryptic cut.  This should be good option for your project, too.

##############################################################################

import sys
import re
from pymongo import MongoClient
try:
	from pyteomics import parser	##	may have to remove this dependency in the future
except:
	print "Requires the Pyteomics package, which can be installed using `pip install pyteomics`"
	sys.exit()

def main():
	
	try:
		DB_file = sys.argv[1]
	except:
		print "Requires a FASTA file"
		print "Correct usage: $ python mongomass.py protein_DB.fasta [blazmass.params]"
		sys.exit()

	blazmass_params = ''
	try:
		blazmass_params = sys.argv[2]
		aa_mass_dict = read_blazmass_params(blazmass_params)
	except:
		print "\nblazmass.params file not found"
		print "Using default amino acid masses\n"
		aa_mass_dict = make_aa_mass_dict()

	############################################################################
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION:   #########
	############################################################################

	host_name = 'hadoop00-adm'
	db_name = 'sandipTest'
	collection_name = 'refseq_0'

	############################################################################
	########    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    #########
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION    #########
	############################################################################

	client = MongoClient(host_name)
	db = client[db_name]
	collection = db[collection_name]
	collection.ensure_index('MASS')
	collection.ensure_index('SEQ')
	collection.ensure_index('COUNT')

	print '\nConnecting to MongoDB host: '+host_name
	print 'Database name: '+db_name
	print 'Collection name: '+collection_name+'\n'

	read_fasta(DB_file,aa_mass_dict,collection)	##might not need to pass in db

	print "Finished"

def read_blazmass_params(blazmass_params):

	aa_mass_dict = {}

	with open(blazmass_params,'rb') as f:
		params_file  = f.readlines()

	params_file = [line.rstrip('\n') for line in params_file if line[0:4] == 'add_']	## restrict to static mod lines of params file
	termini_lines = [line for line in params_file if line[0:14] == 'add_C_terminus' or line[0:14] == 'add_N_terminus'] 	##	restrict to N- and C-terminus static mods
	aa_lines = [line for line in params_file if 'terminus' not in line]	##	restrict to amino acid mods

	for line in termini_lines:
		if line[0:5] == 'add_C':
			aa_mass_dict['C-term'] = float(line.split()[2])
		elif line[0:5] == 'add_N':
			aa_mass_dict['N-term'] = float(line.split()[2])

	aa = ''
	base_mass = 0.0
	mod_mass = 0.0

	for line in aa_lines:
		aa = line[4]
		base_mass = float(line.split()[-1])	##	using monoisotopic mass
		# base_mass = float(re.findall(r'(^.+mono.)(\s+\d+.\d+)',line)[0][1])
		mod_mass = float(line.split('=')[1].split()[0])
		# mod_mass = float(re.findall(r'(^add_[A-Z]_[A-Za-z]+\s=\s)(\d.\d+)(.+)',line)[0][1])
		aa_mass_dict[aa] = base_mass + mod_mass

	return aa_mass_dict

def make_aa_mass_dict():

	aa_mass_dict = {}
	##	From Robin and from http://www.ddbj.nig.ac.jp/sub/ref2-e.html

	# Abbreviation	1 letter abbreviation	Amino acid name
	# Ala	A	Alanine
	# Arg	R	Arginine
	# Asn	N	Asparagine
	# Asp	D	Aspartic acid
	# Cys	C	Cysteine
	# Gln	Q	Glutamine
	# Glu	E	Glutamic acid
	# Gly	G	Glycine
	# His	H	Histidine
	# Ile	I	Isoleucine
	# Leu	L	Leucine
	# Lys	K	Lysine
	# Met	M	Methionine
	# Phe	F	Phenylalanine
	# Pro	P	Proline
	# Pyl	O	Pyrrolysine
	# Ser	S	Serine
	# Sec	U	Selenocysteine
	# Thr	T	Threonine
	# Trp	W	Tryptophan
	# Tyr	Y	Tyrosine
	# Val	V	Valine
	# Asx	B	Aspartic acid or Asparagine
	# Glx	Z	Gultamic acid or Glutamine
	# Xaa	X	Any amino acid
	# Xle	J	Leucine or Isoleucine
	# TERM		termination codon

	#############################################################################
	##############    MODIFY THESE VALUES FOR STATIC MODIFICATIONS ##############
	#############################################################################

	static_mod_G = 0.0
	static_mod_A = 0.0
	static_mod_S = 0.0
	static_mod_P = 0.0
	static_mod_V = 0.0
	static_mod_T = 0.0
	static_mod_C = 57.02146
	static_mod_L = 0.0
	static_mod_I = 0.0
	static_mod_J = 0.0
	static_mod_X = 0.0
	static_mod_N = 0.0
	static_mod_O = 0.0
	static_mod_B = 0.0
	static_mod_D = 0.0
	static_mod_Q = 0.0
	static_mod_K = 0.0
	static_mod_Z = 0.0
	static_mod_E = 0.0
	static_mod_M = 0.0
	static_mod_H = 0.0
	static_mod_F = 0.0
	static_mod_R = 0.0
	static_mod_Y = 0.0
	static_mod_W = 0.0
	static_mod_U = 0.0

	#############################################################################
	##############    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ##############
	##############    MODIFY THESE VALUES FOR STATIC MODIFICATIONS ##############
	#############################################################################

	aa_mass_dict['G'] = 57.0214636 + static_mod_G
	aa_mass_dict['A'] = 71.0371136 + static_mod_A
	aa_mass_dict['S'] = 87.0320282 + static_mod_S
	aa_mass_dict['P'] = 97.0527636 + static_mod_P
	aa_mass_dict['V'] = 99.0684136 + static_mod_V
	aa_mass_dict['T'] = 101.0476782 + static_mod_T
	aa_mass_dict['C'] = 103.0091854 + static_mod_C
	aa_mass_dict['L'] = 113.0840636 + static_mod_L
	aa_mass_dict['I'] = 113.0840636 + static_mod_I
	aa_mass_dict['J'] = 113.0840636 + static_mod_J
	aa_mass_dict['X'] = 113.0840636 + static_mod_X
	aa_mass_dict['N'] = 114.0429272 + static_mod_N
	aa_mass_dict['O'] = 114.0793126 + static_mod_O
	aa_mass_dict['B'] = 114.5349350 + static_mod_B
	aa_mass_dict['D'] = 115.0269428 + static_mod_D
	aa_mass_dict['Q'] = 128.0585772 + static_mod_Q
	aa_mass_dict['K'] = 128.0949626 + static_mod_K
	aa_mass_dict['Z'] = 128.5505850 + static_mod_Z
	aa_mass_dict['E'] = 129.0425928 + static_mod_E
	aa_mass_dict['M'] = 131.0404854 + static_mod_M
	aa_mass_dict['H'] = 137.0589116 + static_mod_H
	aa_mass_dict['F'] = 147.0684136 + static_mod_F
	aa_mass_dict['R'] = 156.1011106 + static_mod_R
	aa_mass_dict['Y'] = 163.0633282 + static_mod_Y
	aa_mass_dict['W'] = 186.0793126 + static_mod_W
	# aa_mass_dict['U'] = 0.0 + static_mod_U ## find out exact mass of selenocysteine...

	aa_mass_dict['N-term'] = 0.0 	##	N-terminal static mod
	aa_mass_dict['C-term'] = 0.0 	##	C-terminal static mod

	return aa_mass_dict

def check_alphabet(pep, aa_mass_dict):
	
	regex = '^['+''.join([x for x in aa_mass_dict.keys() if len(x) == 1])+']*$'

	if re.match(regex,pep):
		return True
	else:
		return False

def peptide_mass(peptide_sequence, aa_mass_dict):
	
	mass = 0.0
	for amino_acid in peptide_sequence:
		mass += aa_mass_dict[amino_acid]

	mass += 19.0183897	##	account for water and hydrogen (N- and C-terminus of peptide, and peptide is charged/protonated)

	##	account for N- and C-terminal static mods
	mass += aa_mass_dict['N-term']
	mass += aa_mass_dict['C-term']

	return mass

##	function trypsin_digest
##	input: protein sequence (string, case-insensitive)
##	output: tryptic peptides from input protein sequence (list of peptides, upper-case)
##	
##	Info from Expasy Peptide Cutter (http://web.expasy.org/peptide_cutter/peptidecutter_enzymes.html#Tryps):
##	Preferentially cleaves at Arg and Lys in position P1 with higher rates for Arg (Keil, 1992), especially at high pH (but treated equally in the program). 
##	
##	SPECIAL CASES FOR PROLINE AT P1':
##	Pro usually blocks the action when found in position P1', 
##	but not when Lys is in position P1 and Trp is in position P2 at the same time. 
##	This blocking of cleavage exerted by Pro in position P1' is also negligible when Arg is in position P1 and Met is in position P2 at the same time 
##	
##	SPECIAL CASES FOR LYSINE AT P1:
##	Furthermore, if Lys is found in position P1 the following situation considerably block the action of trypsin: 
##	Either Asp in position P2 and Asp in position P1' or 
##	Cys in position P2 and Asp in position P1' or 
##	Cys in position P2 and His in position P1' or 
##	Cys in position P2 and Tyr in position P1'. 
##	
##	SPECIAL CASES FOR ARGININE AT P1:
##	A likewise considerable block of trypsin action is seen when Arg is in P1 and the following situations are found: 
##	Either Arg in position P2 and His in position P1' or 
##	Cys in position P2 and Lys in position P1' or 
##	Arg in position P2 and Arg in position P1'.
def trypsin_digest(protein_sequence,missed_cleavages = 0):
	
	# tryptic_protein_sequence = protein_sequence.upper().replace('K','K_').replace('R','R_')	##	define all Lys, Arg potential cleavage sites as '_'

	# ##	special cases for trypsin cleavage with proline at P1'
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('_P','P')		##	if P1' residue (C-terminal to cleavage site) is Proline, remove cleavage site
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('WKP','WK_P')	##	if P1 is K and P2 is W, Proline at P1' will not block cleavage
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('MRP','MR_P')	##	if P1 is R and P2 is M, Proline at P1' will not block cleavage

	# ##	special cases for trypsin cleavage with lysine at P1
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('DK_D','DKD')		##	if P1 is K, P2 is D, and P1' is D, remove cleavage site
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('CK_D','CKD')		##	if P1 is K, P2 is C, and P1' is D, remove cleavage site
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('CK_H','CKH')		##	if P1 is K, P2 is C, and P1' is H, remove cleavage site
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('CK_Y','CKY')		##	if P1 is K, P2 is C, and P1' is Y, remove cleavage site

	# ##	special cases for trypsin cleavage with arginine at P1
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('R_R_H','R_RH')		##	if P1 is R, P2 is R, and P1' is H, remove cleavage site
	# ##	want to keep R_RH but not RR_H
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('CR_K','CRK')		##	if P1 is R, P2 is C, and P1' is K, remove cleavage site
	# tryptic_protein_sequence = tryptic_protein_sequence.replace('R_R_R','R_RR')		##	if P1 is R, P2 is R, and P1' is R, remove cleavage site
	# ##	want to keep R_RR but not RR_R

	# tryptic_peptides = tryptic_protein_sequence.split('_')		##	cleave at all sites in protein_sequence marked by '_'

	# ##	if protein_sequence can't be digested according to these rules,
	# ##	just return the full sequence
	# if not tryptic_peptides:
	# 	tryptic_peptides = protein_sequence

	tryptic_peptides = list(parser.cleave(protein_sequence,'([KR](?=[^P]))|((?<=W)K(?=P))|((?<=M)R(?=P))',missed_cleavages))

	return tryptic_peptides

def process_trypsin_digest(protein_sequence,aa_mass_dict):
	
	tryptic_peptides = trypsin_digest(protein_sequence)

	##	exclude peptides that don't use the standard dictionary (excluding selenocysteine too)
	##	exclude short peptides (6 or fewer amino acids long)
	tryptic_peptides = [pep for pep in tryptic_peptides if check_alphabet(pep, aa_mass_dict) and len(pep) > 6]

	##	create dictionaries with format: 
	##	{'PEPTIDE_SEQUENCE1':startposition1,'PEPTIDE_SEQUENCE2':startposition2,'PEPTIDE_SEQUENCE3':startposition3}
	peptide_position_dict, peptide_mass_dict, peptide_LR_dict = {}, {}, {}
	
	for peptide in tryptic_peptides:
		
		##	peptide start position in protein sequence (integer)
		start_position = protein_sequence.find(peptide)
		peptide_length = len(peptide)
		end_position = start_position+peptide_length
		protein_length = len(protein_sequence)
		peptide_position_dict[peptide] = start_position
		left_residues = ''
		right_residues = ''

		##	multiple mass by 1000 and keep only integer portion	
		peptide_mass_dict[peptide] = int(1000*round(peptide_mass(peptide,aa_mass_dict),3))

		##	find 3 residues to the left of peptide start position
		##	(if too close to protein N-terminus, use '-' in place of any of 3 residues)
		if start_position >= 3:
			left_residues = protein_sequence[start_position-3:start_position]
		elif start_position == 2:
			left_residues = '-' + protein_sequence[:start_position]
		elif start_position == 1:
			left_residues = '--' + protein_sequence[:start_position]
		else: 	##	if start_position == 0:
			left_residues = '---'

		##	find 3 residues to the right of peptide start position
		if end_position <= protein_length-3:
			right_residues = protein_sequence[end_position:end_position+3]
		elif end_position == protein_length-2:
			right_residues = protein_sequence[-2:] + '-'
		elif end_position == protein_length-1:
			right_residues = protein_sequence[-1:] + '--'
		else: 	##	if end_position == protein_length:
			right_residues = '---'

		peptide_LR_dict[peptide] = (left_residues,right_residues)

	return (peptide_position_dict,peptide_mass_dict,peptide_LR_dict)

def read_fasta(DB_file,aa_mass_dict,collection):

	##	might want to account for multiple instances of a tryptic peptide in a single protein (probably very rare)

	print 'Reading proteins from', DB_file+'...'
	count = 0
	reporting_step_size = 10000

	with open(DB_file,'rb') as f:
		
		line = f.readline()
		
		while True:
			defline = ''
			sequence_lines = []
			if not line:
				break
			if line[0] == '>':
				defline = line[1:].rstrip('\n')		##	remove leading '>' and trailing newline
				while True:
					line = f.readline()
					if not line or line[0] == '>':
						break
					sequence_lines.append(line.rstrip('\n'))

			full_sequence = ''.join(sequence_lines).replace('\n','')

			count += 1
			if count % reporting_step_size == 0:
				print "Finished processing protein #"+str(count)+"..."

			peptide_position_dict, peptide_mass_dict, peptide_LR_dict = process_trypsin_digest(full_sequence,aa_mass_dict)

			##	enter into DB
			## assemble dictionary ("document") for each peptide mass, assemble dictionaries into list (dictionary_list)

			dictionary_list = []
			for peptide in peptide_position_dict:
				dictionary_list.append({
					'MASS':peptide_mass_dict[peptide],	##	MASS is an integer, (float value rounded to 3 decimal places, multiplied by 1000)
					'SEQ':peptide,		##	peptide sequence (string)
					# 'PROT_ID':[defline.split('|')[0].strip('>')],	##	protein ID for numbered FASTA file (for example, protein115)
					'PROT_ID':defline[:40]+';',		##	first 40 characters of FASTA record defline
					'OFFSET':str(peptide_position_dict[peptide])+';',	##	OFFSET converted to string to be consistent
					'LEN':len(peptide),
					'LR':peptide_LR_dict[peptide][0]+';',	##	3 residues to the left of this peptide in this protein sequence
					'RR':peptide_LR_dict[peptide][1]+';',	##	3 residues to the right of this peptide in this protein sequence
					'COUNT':1 	##	how many times this exact peptide sequence appears in proteins in this FASTA file
					})

			update_mongo(collection,dictionary_list)

def update_mongo(collection,dictionary_list):
	
	dictionary_list = [document for document in dictionary_list if document] 	##	remove empty dictionaries (would be empty MongoDB documents...)

	##	for loop ensures that empty dictionary_list will not be inserted into MongoDB collection
	for document in dictionary_list:
		
		query = collection.find_one({"SEQ":document['SEQ']})	##	search collection for existing records with same mass
	
		if query:
			protein_id = query['PROT_ID'] + document['PROT_ID']	##	multiple protein IDs are semicolon-delimited
			offset = query['OFFSET'] + document['OFFSET']	##	multiple protein offset values are semicolon-delimited
			lr = query['LR'] + document['LR']
			rr = query['RR'] + document['RR']
			count = query['COUNT'] + 1

			##	if peptide already exists, update document in MongoDB collection
			collection.update({"SEQ":document['SEQ']},{'$set':{'PROT_ID':protein_id,'OFFSET':offset,'LR':lr,'RR':rr,'COUNT':count}})
		else:
			collection.insert(document)	##	if document with SEQ value doesn't exist, create new document in MongoDB collection

if __name__ == '__main__':
	main()