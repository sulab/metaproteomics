#!/usr/local/bin/python

#	make_uniprot_trembl_go_mongodb.py
#	Sandip Chatterjee
#	v1, October 8, 2013
#	
#	For generating a MongoDB representation of the large uniprot uniprot_trembl.dat
#	(mapping UniProt accession numbers to GO terms)
#	
#	Usage: 
#	$ python make_uniprot_trembl_go_mongodb.py uniprot_trembl.dat

import sys
from pymongo import MongoClient

def main():

	uniprot_trembl_flatfile = sys.argv[1]

	############################################################################
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION:   #########
	############################################################################

	host_name = 'hadoop00-adm'
	db_name = 'sandip'
	collection_name = 'uniprot_trembl_go_mapping'

	############################################################################
	########    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    #########
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION    #########
	############################################################################

	client = MongoClient(host_name)
	db = client[db_name]
	collection = db[collection_name]

	print '\nConnecting to MongoDB host: '+host_name
	print 'Database name: '+db_name
	print 'Collection name: '+collection_name+'\n'


	read_db(uniprot_trembl_flatfile,collection)

	print "Finished"

def read_db(uniprot_trembl_flatfile,collection):
	

	######COUNTERS TO REMOVE
	record_counter = 0
	no_go_term_counter = 0
	many_acc_number_counter = 0
	list_of_many_acc_number = []
	###############

	with open(uniprot_trembl_flatfile,'rb') as f:
		weird_recs = []
		while True:
			temp_line = ''
			record_list = []
			record_doc = {}
			while temp_line.strip('\n') != '//':
				temp_line = f.readline()
				if not temp_line:
					break
				record_list.append(temp_line)
			if not temp_line:
				break

			## do something with the current uniprot (swissprot) record

			## extract accession numbers ('AC' lines) - record_AC is a list of strings of all accession numbers found in a record
			record_AC = [line.strip('\n').strip(';') for line in record_list if line[0:2] == 'AC']
			record_AC = ''.join(''.join([x.replace('AC ','') for x in record_AC]).split(';')).split()

			# if len(''.join(record_AC)) != 6:	##remove
				# many_acc_number_counter += 1 	##Remove
				# list_of_many_acc_number.append(record_AC)	##Remove
			record_counter += 1
			if record_counter % 5000000 == 0:
				print "Finished processing record #",record_counter

			## extract 'DR' lines
			record_DR = [line.strip('\n') for line in record_list if line[0:2] == 'DR']

			## extract DR GO lines
			record_DR = [DR_line for DR_line in record_DR if DR_line[5:7] == 'GO']

			if record_DR:				## if record has GO terms associated...
				record_DR = [GO_line.split(';') for GO_line in record_DR]
				list_of_GO_terms = [''.join([term.strip() for term in x if term.strip()[0:2] == 'GO' and len(term.strip()) == 10]) for x in record_DR]

				for accession_number in record_AC:	##	there may be more than one UniProt accession number for a record (some are secondary numbers, retired, etc)
					for GO_term in list_of_GO_terms:
						collection.insert({
						'uid':accession_number,		##	'uid' is the UniProt accession number (unique, one per line)
						'go':GO_term				##	e.g., GO:0016491
					})
			else:
				no_go_term_counter += 1

	print "Number of UniProt records parsed:", record_counter
	print "Number of records without associated GO terms:", no_go_term_counter

	# from code import interact
	# interact(local=locals())

if __name__ == '__main__':
	main()