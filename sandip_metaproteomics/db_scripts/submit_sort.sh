#!/bin/csh

#PBS -l walltime=10:00:00
#PBS -l mem=16gb
#PBS -l nodes=1:ppn=8
#PBS -N 'gnu_sort'
#PBS -j oe

cd $PBSTMPDIR
cp ~/db/numbered_071213_155213_refseq_microbial_reversed.flatdb .

echo
echo 'start time for gnu sort:'
echo `date`

/usr/local/bin/sort -k1 -T $PBSTMPDIR numbered_071213_155213_refseq_microbial_reversed.flatdb > sorted_file

echo 'end time for gnu sort:'
echo `date`

echo
echo 'start of sorted file:'
echo `head -100 sorted_file`
echo
echo `ls -lh`
echo "Finished"