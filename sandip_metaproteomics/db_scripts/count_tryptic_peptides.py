#!/usr/local/bin/python

#	count_tryptic_peptides.py
#	Sandip Chatterjee
#	v1, September 20, 2013
#	
#	Simple script to count number of tryptic peptides in a multi-protein FASTA file
#	Requires mongomass.py and Biopython

import sys
from Bio import SeqIO
from mongomass import trypsin_digest

def main():
	
	db_file = sys.argv[1]
	protein_counter = 0
	peptide_counter = 0

	for seq_record in SeqIO.parse(db_file,'fasta'):
		protein_counter += 1
		peptide_counter += len(trypsin_digest(str(seq_record.seq)))

	print "Total number of proteins in file:", protein_counter
	print "Total number of tryptic peptides in file:", peptide_counter

if __name__ == '__main__':
	main()