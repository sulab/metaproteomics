#!/usr/bin/env python

## multiprocess_DB_queries.py
##
## script for running many mass range queries of a MongoDB "indexDB"
## usage: 
## python multiprocess_DB_queries.py MS2_file.ms2
## 4/16/14

from pymongo import MongoClient
from multiprocessing import Pool
from time import time,strftime
import sys

try:
	client = MongoClient()
	# db = client['indexDB']
	db = client['MassDB']
	# collection = db['indexDB_012714']
	collection = db['012714MassDB']

except:
	print "Couldn't connect to mongod, exiting"
	sys.exit()

def main():

	try:
		MS2_file = sys.argv[1]
		pool_size = sys.argv[2]
	except:
		print "Requires an input MS2 file"
		sys.exit()
	if '.ms2' not in MS2_file:
		sys.exit()

	ppm_mass_tolerance = 20.0 ## in ppm
	ppm_mass_tolerance = ppm_mass_tolerance/100
	DIFFMASSC12C13 = 1.0033548

	massranges = []  ## list of tuples
	## generate massranges list...

	with open(MS2_file,'rb') as f:
		for line in f:
			
			if line[0] == 'Z':
				line_list = line.rstrip('\n').split('\t')
				charge_state = int(line_list[1])
				isotope_num = charge_state * 2 + 1
				mass = float(line_list[2])

				for i in range(isotope_num):
					massranges.append(calculate_mass_range_from_mass(mass-i*DIFFMASSC12C13,ppm_mass_tolerance))

	print 'Performing '+str(len(massranges))+' mass queries using '+str(pool_size)+' processes...'

	start_time = time()

	pool = Pool(int(pool_size))
	query_results = pool.map(perform_query,massranges)
	total_query_time = "%.1f" % (time()-start_time)
	pool.close()
	pool.join()

	output_file_name = 'mongodb_query_results_poolsize'+str(pool_size)+'_'+strftime("%m%d%y_%H-%M")
	with open(output_file_name,'wb') as f:
		for result in query_results:
			f.write(str(result[0])+'\t'+result[1]+'\n')

	print "Total query time took "+total_query_time
	print "(using multiprocessing pool size of "+str(pool_size)+")"
	print "Finished"

def calculate_mass_range_from_mass(mass,ppm_mass_tolerance):
	
	ppm = ppm_mass_tolerance / 1000
	ppm += 1
	ppm = mass - mass/ppm

	low_mass = int('%.0f' % ((mass-ppm)*1000))
	high_mass = int('%.0f' % ((mass+ppm)*1000))

	return (low_mass,high_mass)

def perform_query(massrange_tuple):
	
	full_query_start = time()

	low_mass, high_mass = massrange_tuple[0], massrange_tuple[1]

	count = 0

	for record in collection.find({"_id":{"$gte":low_mass,"$lte":high_mass}}):
		
		# iteration_start = time()

		pass ## query processing logic, if any
		
		# print "1_it\t%.1f" % (time()-iteration_start)
		count += 1

	# print "Full query iteration over "+str(count)+" records took %.1f" % (time()-full_query_start)
	full_query_time = '%.1f' % (time()-full_query_start)

	return (count,full_query_time)

if __name__ == '__main__':
	main()