#!/usr/bin/env python

# test_seqDB_queries.py
#
# quick script to test out MongoDB query performance
# takes an SQT file as input, splits M lines and queries SeqDB by peptide sequence (records all parent protein IDs)
#
# 6/17/14

from time import time
import sys
from pymongo import MongoClient

def main():
	
	client = MongoClient()
	db = client.SeqDB
	coll = db['SeqDB_012714']

	sqt_file = sys.argv[1]
	M_lines = []
	with open(sqt_file,'rb') as f:
		for line in f:
			if line[0] == 'M' and len(line.split()) > 2:
				#M_lines.append(line.rstrip('\n'))
				M_lines.append(line.rstrip('\n').split()[-2].split('.')[1])

	start_time = time()
	
	file_lines = []
	for line in M_lines:
		query_start = time()
		temp_dict = coll.find_one({'_id':line})
		if temp_dict:
			temp_parents = temp_dict['p']
		query_total = time()-query_start
		line_to_write = line
		for parent in temp_parents:
			line_to_write += '\t'+str(parent['i'])
		line_to_write += '\t'+str(query_total)
		file_lines.append(line_to_write)
	
	with open('db_output.out','wb') as f:
		for line in file_lines:
			f.write(line+'\n')

	print "Finished"
	print time()-start_time, "seconds"

if __name__ == '__main__':
	main()
