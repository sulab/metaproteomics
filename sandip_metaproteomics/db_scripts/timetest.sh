#!/bin/bash

# START=$(date +%s)

# echo "Java Blazmass, GNU parallel j+4 5M block size"
# touch empty.fasta
# cat /tmp_mongodb/create_indexdb/renumbered_071213_155213_refseq_microbial_reversed.fasta | parallel -j+4 --block 5M --recstart '>' --tmpdir /tmp_mongodb/create_indexdb/tmp --pipe java -jar java/blazmass.jar -i /tmp_mongodb/create_indexdb/java blazmass.params 2> /dev/null 1> /tmp_mongodb/create_indexdb/renumbered_flatdb.flatdb

# DIFF=$(( $END - $START ))
# echo "...took $DIFF seconds"

DB="indexDB"
COLL="refseq0713"

echo "db.refseq0713.find({'PARENTS.0': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.0': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.1': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.1': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.2': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.2': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.3': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.3': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.4': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.4': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.5': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.5': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.6': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.6': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.7': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.7': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.8': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.8': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.9': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.9': {\$exists: true}}).count()"

echo "db.refseq0713.find({'PARENTS.10': {$exists: true}}).count()"
mongo $DB --eval "db."$COLL".find({'PARENTS.10': {\$exists: true}}).count()"

echo
echo "Finished"