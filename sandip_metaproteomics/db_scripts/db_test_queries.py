#!/usr/bin/env python

#	db_test_queries.py
#
#	script for testing MongoDB query/seek performance
#	Sandip Chatterjee
#	2/12/14

import sys
import timeit
from pymongo import MongoClient
from time import time

def main():
	
	############################################################################
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION:   #########
	############################################################################

	host_name = 'localhost'
	db_name = 'indexDB2'
	collection_name = '012714indexDB'

	############################################################################
	########    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    #########
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION    #########
	############################################################################

	client = MongoClient(host_name)
	db = client[db_name]
	collection = db[collection_name]

	print '\nConnecting to MongoDB host: '+host_name
	print 'Database name: '+db_name
	print 'Collection name: '+collection_name+'\n'

	# print 'Finding mass 3075586'
	# find_mass(3075586,collection)

	# print 'Finding mass 1988136'
	# find_mass(1988136,collection)

	# print 'Finding mass 1669989'
	# find_mass(1669989,collection)

	# print 'Finding mass 678382'
	# find_mass(678382,collection)

	# print 'Finding mass 1986157'
	# find_mass(1986157,collection)

	# print 'Finding mass range 1944100,1944120'
	# find_mass_range([1944100,1944120],collection)

	# print 'Finding mass range 4455280,4455299'
	# find_mass_range([4455280,4455299],collection)

	# print 'Finding mass range 1873012,1873032'
	# find_mass_range([1873012,1873032],collection)

	# print 'Finding mass range 1157651,1157691'
	# find_mass_range([1157651,1157691],collection)

	# print 'Finding peptide sequence AAFEIKPLAPATSIIKTENQSYPLCSLDITGNEIASR...'
	# find_peptide_sequence('AAFEIKPLAPATSIIKTENQSYPLCSLDITGNEIASR', collection)

	# print 'Finding peptide sequence AADCVAAIPPIIHFHGTADQTFPLSGRAIGTR...'
	# find_peptide_sequence('AADCVAAIPPIIHFHGTADQTFPLSGRAIGTR', collection)

	# print 'Finding peptide sequence AAMKALQAEQKKK...'
	# find_peptide_sequence('AAMKALQAEQKKK', collection)

	# print 'Finding peptide sequence YVYTITGSQNNAGSISNFKAR...'
	# find_peptide_sequence('YVYTITGSQNNAGSISNFKAR', collection)

	# print 'Finding peptide sequence YYKLNNLFPALDESGYNTLVFIVEQVDELDNMFGSYTK...'
	# find_peptide_sequence('YYKLNNLFPALDESGYNTLVFIVEQVDELDNMFGSYTK', collection)

	print 'Doing full read of database...'
	db_full_read(collection)

	print 'Finished'

def get_time():
	return time()

def find_mass(mass,collection):
	s = time()
	query = collection.find_one({"MASS":mass})
	if query:
		print "done"
	print time()-s, 'seconds'
	print ''

def find_mass_range(mass_range,collection):
	s = time()
	query = collection.find({"MASS":{"$gt":mass_range[0],"$lt":mass_range[1]}})
	if query:
		print "done"
	print time()-s, 'seconds'
	print ''

def find_peptide_sequence(seq, collection):
	s = time()
	query = collection.find_one({"SEQ":seq})
	if query:
		print "done"
	print time()-s, 'seconds'
	print ''

def db_full_read(collection):
	s = time()
	counter = 0
	with open('012714_indexDB_peptidelengths_masses_and_counts.txt','wb') as f:
		f.write('len_SEQ,MASS,COUNT\n')
		for i in collection.find():
			counter += 1
			f.write(str(len(i['SEQ']))+','+str(i['MASS'])+','+str(i['COUNT'])+'\n')
	print counter, "documents read"
	print time()-s, 'seconds'
	print ''

if __name__ == '__main__':
	main()

