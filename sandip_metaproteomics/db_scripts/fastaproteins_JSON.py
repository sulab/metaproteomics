#!/usr/bin/env python

#	fastaproteins_JSON.py
#
#	script generating JSON objects from a multiprotein FASTA file (reads FASTA file via STDIN)
#	Sandip Chatterjee
#	3/3/14

import sys
import json

def main():

	DB_file = sys.stdin

	read_flatfile(DB_file)

def read_flatfile(DB_file):

	# current_chunk = []

	f = DB_file
	line = DB_file.readline()

	while True:
		defline = ''
		sequence_lines = []
		# protein_dicts = [] ##	list of dicts (one per peptide)
		if not line:
			break
		if line[0] == '>':
			defline = line[1:].rstrip('\n')		##	remove leading '>' and trailing newline
			while True:
				line = f.readline()
				if not line or line[0] == '>':
					break
				sequence_lines.append(line.rstrip('\n'))

		full_sequence = ''.join(sequence_lines).replace('\n','')
		print_json_obj(defline,full_sequence)

def print_json_obj(defline,full_sequence):

	# current_chunk = [line.rstrip('\n') for line in current_chunk]

	# json_dict = {}
	accession = defline.split('|')[3]
	json_dict = {
					'def':defline,
					'accv':accession,	# accession number including version
					'acc':accession.split('.')[0],	# accession number without version
					'seq':full_sequence	# full protein sequence
	}

	print json.dumps(json_dict)+','	##	print json string and comma to screen

if __name__ == '__main__':
	main()