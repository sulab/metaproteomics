#!/usr/local/bin/python

#	make_gi_uniprot_mongodb.py
#	Sandip Chatterjee
#	v1, October 12, 2013
#	
#	For generating a MongoDB representation of the large uniprot idmapping.dat file
#	(mapping NCBI/GenBank gi numbers to UniProt accession numbers)
#
#	Downloaded updated id_mapping file from UniProt:
#	ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping.dat.gz
#	(14GB ungzipped)
#	
#	Usage: 
#	$ python make_gi_uniprot_mongodb.py idmapping.dat

import sys
from pymongo import MongoClient

def main():

	try:
		uniprot_idmapping_db = sys.argv[1]
	except:
		print "Requires 1 argument (UniProt ID Mapping file idmapping.dat)"
		print "\nPROPER USAGE:"
		print "$ python make_gi_uniprot_mongodb.py idmapping.dat"

	############################################################################
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION:   #########
	############################################################################

	host_name = 'hadoop00-adm'
	db_name = 'sandip'
	collection_name = 'gi_uniprot_mapping'

	############################################################################
	########    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    #########
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION    #########
	############################################################################

	client = MongoClient(host_name)
	db = client[db_name]
	collection = db[collection_name]

	read_db(uniprot_idmapping_db,collection)

	print "Finished"

def read_db(uniprot_idmapping_db,collection):
	
	counter = 0
	with open(uniprot_idmapping_db,'rb') as f:
		for line in f:
			if line.split()[1] == 'GI':
				counter += 1
				uniprot_id = line.split()[0]
				GI_id = line.split()[2]
				collection.insert({
					'uid':uniprot_id,	##	'uid' is the UniProt accession number
					'gi':GI_id	##	this is the GI ID
				})

	print "Inserted", counter, "documents into MongoDB collection"

if __name__ == '__main__':
	main()