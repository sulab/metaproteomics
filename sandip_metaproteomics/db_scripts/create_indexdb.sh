#!/bin/bash

echo "create_indexdb protein_fasta_file.fasta mongodb_database mongodb_collection"
echo
echo "Dependencies:"
echo "Python 2.7"
echo "GNU Sort, preferably v8.1+ for built-in parallel processing"
echo "GNU Parallel, v20130622 or newer"
echo "MongoDB v2.4+ with 'mongoimport' and 'mongo' available in PATH"
echo
echo "Requires Java1.7/Blazmass, number_fasta_db.py, fasta_peptides_json.py, and a blazmass.params file in the same directory"
echo "Will probably use 10-20x disk space of original FASTA file..."

echo
echo "Start date/time:"
echo `date`

ORIGFASTADB=$1
FASTADB="renumbered_"$ORIGFASTADB
DB=$2
COLL=$3

START=$(date +%s)
echo
echo "Numbering proteins in "$ORIGFASTADB

cat $ORIGFASTADB | python ex/python/number_fasta_db.py > $FASTADB
echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Numbering proteins took $DIFF seconds"

echo
echo "Generating flattened peptide file using Blazmass"

FLATDB=${FASTADB/.fasta/.flatdb}
MSFLATDB="massSorted_"$FLATDB
SSFLATDB="seqSorted_"$FLATDB

START=$(date +%s)

## FOR NODEA1309:
touch empty.fasta
cat $FASTADB | parallel -j+4 --block 5M --recstart '>' --tmpdir . --pipe java -jar ex/java/blazmass.jar -i . blazmass.params 2> /dev/null 1> $FLATDB
rm empty.fasta

echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "flatdb peptide file $FLATDB generated in $DIFF seconds"



START=$(date +%s)
echo "-------------------------------"
echo "Running GNU Sort 8.21 to generate MassDB flatdb"
ex/sort -nk1 -T . $FLATDB > $MSFLATDB
echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "flatdb numeric sort took $DIFF seconds"

START=$(date +%s)
echo "-------------------------------"
echo "Running GNU Sort 8.21 to generate SeqDB flatdb"
ex/sort -k2 -T . $FLATDB > $SSFLATDB
echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "flatdb peptide sequence sort took $DIFF seconds"



# START=$(date +%s)
# echo "-------------------------------"
# echo "Generating grouped JSON file"

# JSONFILE="sorted_"${FLATDB/.flatdb/.json}

# ./flatdb_parse.py $SFLATDB | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe ./flatdb_json.py > $JSONFILE
# echo `ls -l $JSONFILE`
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "json file generation took $DIFF seconds"

# START=$(date +%s)
# echo "-------------------------------"
# echo "Running mongoimport"

# # mongoimport -d $DB -c $COLL --file $JSONFILE
# cat $JSONFILE | parallel -j2 --block 500M --tmpdir /scratch/sandip/create_indexdb/tmp --pipe mongoimport -d $DB -c $COLL

# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "mongoimport took $DIFF seconds"





# echo "-------------------------------"
# echo "Creating index on 'COUNT' field"
# mongo $DB --eval "db."$COLL".ensureIndex({'COUNT':1})"
# echo "...done"

# START=$(date +%s)
# echo "-------------------------------"
# echo "Creating index on 'MASS' field"
# mongo $DB --eval "db."$COLL".ensureIndex({'MASS':1})"
# echo "...done"
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "ensureIndex on MASS took $DIFF seconds"

# START=$(date +%s)
# echo "-------------------------------"
# echo "Creating index on 'SEQ' field"
# mongo $DB --eval "db['"$COLL"'].ensureIndex({'SEQ':1})"
# echo "...done"
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "ensureIndex on SEQ took $DIFF seconds"

# echo
# echo "Finish date/time:"
# echo `date`
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "IndexDB generation took $DIFF seconds"