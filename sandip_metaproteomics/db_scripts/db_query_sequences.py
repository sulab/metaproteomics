#!/usr/bin/env python

#	db_query_sequences.py
#
#	script for querying/counting peptide sequence strings in a MongoDB database 'forwardDB'
#	Sandip Chatterjee
#	v2, simplified for use with GNU Parallel
#	2/20/14

import sys
from time import strftime
# try:
from pymongo import MongoClient
# except:
	# print "ERROR - Requires pymongo module. Exiting."
	# sys.exit()

def main():

	# try:
	# 	DB_filename = sys.argv[1]
	# except:
	# 	print "Need one argument, a properly-formatted JSON DB file"
	# 	print "Proper usage: python db_query_sequences protein_file.json"
	# 	sys.exit()
	
	############################################################################
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION:   #########
	############################################################################

	host_name = 'localhost'
	db_name = 'forwardDB'
	collection_name = '012714_indexDB'

	############################################################################
	########    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    #########
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION    #########
	############################################################################

	client = MongoClient(host_name)
	db = client[db_name]
	collection = db[collection_name]

	# print '\nConnecting to MongoDB host: '+host_name
	# print 'Database name: '+db_name
	# print 'Collection name: '+collection_name+'\n'

	# with open(DB_filename,'rb') as DB_file:

	DB_file = sys.stdin

##	test_str has 20 peptide sequences that should be in the database and one that should not
# 		test_str = '''{ "_id" : ObjectId("52fd8cc2e96b0e03f7bb01e7"), "COUNT" : 1, "PARENTS" : [ { "LR" : "QYR", "RR" : "RTP", "PROT_ID" : "p28970409", "OFFSET" : 17 } ], "MASS" : 2429217, "LEN" : 30, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAAADYAER" }
# { "_id" : ObjectId("52fd8cc2e96b0e03f7bb01e8"), "COUNT" : 1, "PARENTS" : [ { "LR" : "QYR", "RR" : "TPN", "PROT_ID" : "p28970409", "OFFSET" : 17 } ], "MASS" : 2585318, "LEN" : 31, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAAADYAERR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01e9"), "COUNT" : 1, "PARENTS" : [ { "LR" : "QYR", "RR" : "RTP", "PROT_ID" : "p28970409", "OFFSET" : 17 } ], "MASS" : 3253642, "LEN" : 37, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAAADYAERRTPNAER" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01ea"), "COUNT" : 1, "PARENTS" : [ { "LR" : "QYR", "RR" : "TPN", "PROT_ID" : "p28970409", "OFFSET" : 17 } ], "MASS" : 3409743, "LEN" : 38, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAAADYAERRTPNAERR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01eb"), "COUNT" : 1, "PARENTS" : [ { "LR" : "REK", "RR" : "NDL", "PROT_ID" : "p1539354", "OFFSET" : 885 } ], "MASS" : 2597452, "LEN" : 31, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAKFVKKADK" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01ec"), "COUNT" : 1, "PARENTS" : [ { "LR" : "REK", "RR" : "ADK", "PROT_ID" : "p1539354", "OFFSET" : 885 } ], "MASS" : 2283293, "LEN" : 28, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAKFVKK" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01ed"), "COUNT" : 1, "PARENTS" : [ { "LR" : "REK", "RR" : "KAD", "PROT_ID" : "p1539354", "OFFSET" : 885 } ], "MASS" : 2155198, "LEN" : 27, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAKFVK" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01ee"), "COUNT" : 1, "PARENTS" : [ { "LR" : "REK", "RR" : "FVK", "PROT_ID" : "p1539354", "OFFSET" : 885 } ], "MASS" : 1780966, "LEN" : 24, "SEQ": "AAAAAAAAAAAAAAAAAAAAAAAK" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01ef"), "COUNT" : 1, "PARENTS" : [ { "LR" : "AVR", "RR" : "YCL", "PROT_ID" : "p1513081", "OFFSET" : 309 } ], "MASS" : 2879391, "LEN" : 34, "SEQ": "AAAAAAAAAAAAAAAAAAAAAPPPSEDGEGEAFR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f0"), "COUNT" : 1, "PARENTS" : [ { "LR" : "AVR", "RR" : "RGS", "PROT_ID" : "p1513081", "OFFSET" : 309 } ], "MASS" : 4301115, "LEN" : 45, "SEQ": "AAAAAAAAAAAAAAAAAAAAAPPPSEDGEGEAFRYCLGFVQERLR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f1"), "COUNT" : 1, "PARENTS" : [ { "LR" : "AVR", "RR" : "GSR", "PROT_ID" : "p1513081", "OFFSET" : 309 } ], "MASS" : 4457216, "LEN" : 46, "SEQ": "AAAAAAAAAAAAAAAAAAAAAPPPSEDGEGEAFRYCLGFVQERLRR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f2"), "COUNT" : 1, "PARENTS" : [ { "LR" : "AVR", "RR" : "LRR", "PROT_ID" : "p1513081", "OFFSET" : 309 } ], "MASS" : 4031930, "LEN" : 43, "SEQ": "AAAAAAAAAAAAAAAAAAAAAPPPSEDGEGEAFRYCLGFVQER" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f3"), "COUNT" : 2, "PARENTS" : [ 	{ "LR" : "SDR", "RR" : "G--", "PROT_ID" : "p1342293", "OFFSET" : 534 }, 	{ "LR" : "SDR", "RR" : "G--", "PROT_ID" : "p1342310", "OFFSET" : 525 } ], "MASS" : 1524824, "LEN" : 20, "SEQ": "AAAAAAAAAAAAAAAAAAAR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f4"), "COUNT" : 1, "PARENTS" : [ { "LR" : "KGK", "RR" : "RLS", "PROT_ID" : "p1351077", "OFFSET" : 700 } ], "MASS" : 1924995, "LEN" : 25, "SEQ": "AAAAAAAAAAAAAAAAAAGGTGAQR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f5"), "COUNT" : 1, "PARENTS" : [ { "LR" : "KGK", "RR" : "SHC", "PROT_ID" : "p1351077", "OFFSET" : 700 } ], "MASS" : 2550397, "LEN" : 30, "SEQ": "AAAAAAAAAAAAAAAAAAGGTGAQRRLSLR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f6"), "COUNT" : 1, "PARENTS" : [ { "LR" : "KGK", "RR" : "KGE", "PROT_ID" : "p1351077", "OFFSET" : 700 } ], "MASS" : 3090620, "LEN" : 34, "SEQ": "AAAAAAAAAAAAAAAAAAGGTGAQRRLSLRSHCR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f7"), "COUNT" : 1, "PARENTS" : [ { "LR" : "KGK", "RR" : "LSL", "PROT_ID" : "p1351077", "OFFSET" : 700 } ], "MASS" : 2081096, "LEN" : 26, "SEQ": "AAAAAAAAAAAAAAAAAAGGTGAQRR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f8"), "COUNT" : 2, "PARENTS" : [ 	{ "LR" : "AEK", "RR" : "VAS", "PROT_ID" : "p1337486", "OFFSET" : 249 }, 	{ "LR" : "AEK", "RR" : "VAS", "PROT_ID" : "p1337486", "OFFSET" : 451 } ], "MASS" : 1913950, "LEN" : 23, "SEQ": "AAAAAAAAAAAAAAAAAEMEAAR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01f9"), "COUNT" : 2, "PARENTS" : [ 	{ "LR" : "AEK", "RR" : "SAS", "PROT_ID" : "p1337486", "OFFSET" : 249 }, 	{ "LR" : "AEK", "RR" : "SAS", "PROT_ID" : "p1337486", "OFFSET" : 451 } ], "MASS" : 2513253, "LEN" : 29, "SEQ": "AAAAAAAAAAAAAAAAAEMEAARVASEGR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01fa"), "COUNT" : 2, "PARENTS" : [ 	{ "LR" : "AEK", "RR" : "QGR", "PROT_ID" : "p1337486", "OFFSET" : 249 }, 	{ "LR" : "AEK", "RR" : "QGR", "PROT_ID" : "p1337486", "OFFSET" : 451 } ], "MASS" : 3737921, "LEN" : 41, "SEQ": "AAAAAAAAAAAAAAAAAEMEAARVASEGRSASQLQAVARGR" }
# { "_id" : ObjectId("52fd8cc3e96b0e03f7bb01fa"), "COUNT" : 2, "PARENTS" : [ 	{ "LR" : "AEK", "RR" : "QGR", "PROT_ID" : "p1337486", "OFFSET" : 249 }, 	{ "LR" : "AEK", "RR" : "QGR", "PROT_ID" : "p1337486", "OFFSET" : 451 } ], "MASS" : 3737921, "LEN" : 41, "SEQ": "AAAAAAAAAAAAAAAEVARGR" }'''

		# test_list = test_str.split('\n')
		
		# print "Running 21 test queries - should see 20 in database, 1 not found..."
		# read_json_db(test_list,collection)

		# print "Running queries from JSON file..."
		# read_json_db(DB_file,collection)
	read_json_db(DB_file,collection)

	# print ''
	# print 'Finished'

def exists_in_collection(query_string,collection):
	
	if collection.find_one({"SEQ":query_string}):
		return True
	else:
		return False

def read_json_db(DB_file,collection):
	peptide_sequence = ''
	found_count = 0
	notfound_count = 0
	done_count = 0
	# with open('matched_peptide_lengths.txt','wb') as f:
	for line in DB_file:
		peptide_sequence = line.split('"SEQ":')[-1].split('"')[1]
		if exists_in_collection(peptide_sequence,collection):
			# found_count += 1
			# f.write(str(len(peptide_sequence))+'\n')
			print len(peptide_sequence)
		# else:
			# notfound_count += 1

		# done_count += 1

		# if done_count % 1000000 == 0:
		# 	print strftime("%Y-%m-%d %H:%M:%S"), "-- finished", done_count, "queries..."

	# print ''
	# print "Searched for "+str(done_count)+" peptide sequences"
	# print "Found "+str(found_count)+" peptide sequences in database"
	# print "Did not find "+str(notfound_count)+" peptide sequences in database"

if __name__ == '__main__':
	main()