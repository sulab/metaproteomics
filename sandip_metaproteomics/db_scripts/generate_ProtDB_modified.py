#!/usr/bin/env python

import sys
import re
# from pymongo import MongoClient

# mongo_hostname = 'localhost'	# 'localhost' is default
# mongo_port = 27018		# 27017 is default for mongod, 27018 is default for mongos
repositories = [
				'RefSeq',
				'HMP_Reference_Genomes',
				'HMP_metagenomics',
				'UniProt_Human',
				'Integrated_Gene_Catalog',
				'UniProt_Archaea',
				'UniProt_Bacteria',
				'UniProt_Viruses'
				]

def main():

	# client = MongoClient(mongo_hostname,mongo_port) # 27018 is for mongos router
	# db = client[sys.argv[1]]
	# coll = db[sys.argv[2]]

	f = sys.stdin
	# line = f.readline()

	# while True:
	# 	defline = ''
	# 	sequence_lines = []
	# 	protein_dict = {}
	# 	if not line:
	# 		break
	# 	if line[0] == '>':
	# 		defline = line[1:].rstrip('\n')		##	remove leading '>' and trailing newline
	# 		while True:
	# 			line = f.readline()
	# 			if not line or line[0] == '>':
	# 				break
	# 			sequence_lines.append(line.rstrip('\n'))
	for line in f:
		protein_dict = {}
		defline = line[1:].rstrip('\n')

		# full_sequence = ''.join(sequence_lines).replace('\n','')
		# linesplit = defline.split('||')
		linesplit = [None,defline]
		# protein_dict['_id'] = int(linesplit[0])
		annotated_defline = linesplit[1]				## full defline after protein ID (including repository, GO terms)
		go_array = annotated_defline.split('|')[-1]
		g = go_array.lstrip('[').rstrip(']')			## array of GO terms ([GO:123456,GO:123929])
		if g:
			protein_dict['g'] = g.split(',')
		protein_dict['r'] = annotated_defline.split('|')[-2]	## source repository (RefSeq, UniProt, etc.)
		# protein_dict['s'] = full_sequence
		d = '|'.join(annotated_defline.split('|')[:-2])
		protein_dict['d'] = d

		if protein_dict['r'] == 'RefSeq':
			org = parse_refseq_HMP_defline_for_organism(d)
			if org:
				protein_dict['o'] = org

		elif protein_dict['r'] == 'HMP_Reference_Genomes':
			org = parse_refseq_HMP_defline_for_organism(d)
			if org:
				protein_dict['o'] = org

		elif protein_dict['r'] == 'HMP_metagenomics':
			pass

		elif protein_dict['r'] == 'UniProt_Human':
			protein_dict['o'] = 'Homo sapiens'

		elif protein_dict['r'] == 'Integrated_Gene_Catalog':
			pass

		elif protein_dict['r'] == 'UniProt_Archaea':
			org = parse_uniprot_complete_proteomes_for_organism(d)
			if org:
				protein_dict['o'] = org

		elif protein_dict['r'] == 'UniProt_Bacteria':
			org = parse_uniprot_complete_proteomes_for_organism(d)
			if org:
				protein_dict['o'] = org

		elif protein_dict['r'] == 'UniProt_Viruses':
			org = parse_uniprot_complete_proteomes_for_organism(d)
			if org:
				protein_dict['o'] = org

		print defline
		print protein_dict['d'] 	## truncated defline
		print protein_dict['r'] 	## repository
		if 'g' in protein_dict:
			print protein_dict['g'] ## list of GO terms, if any
		if 'o' in protein_dict:
			print protein_dict['o'] ## parent organism, if available
		print ''

		# coll.insert(protein_dict)


def parse_refseq_HMP_defline_for_organism(line):

	o = re.findall(r"(?<=\[)[^\[]+(?=\])",line)

	if o:
		return o[-1]
	else:
		return None

def parse_uniprot_complete_proteomes_for_organism(line):
	pattern = re.compile(r"(.+)(OS=)(.+)(GN=)")
	matched = pattern.match(line)
	if matched:
		if matched.groups()[1] == 'OS=':
			return matched.groups()[2].strip()
		else:
			return None
	else:
		pattern = re.compile(r"(.+)(OS=)(.+)(PE=)")
		matched = pattern.match(line)
		if matched:
			if matched.groups()[1] == 'OS=':
				return matched.groups()[2].strip()
			else:
				return None
		else:
			return None

if __name__ == '__main__':
	main()
