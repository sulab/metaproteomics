#!/usr/bin/env python3

# check_JSON_mongoimport.py
# simple script to check to see if the objects in an input JSON file (on STDIN) are in a MongoDB database
# (if not, insert them)
#
# Usage:
# python3 check_JSON_mongoimport.py JSON_file.json db_name collection_name
#
# Sandip Chatterjee
# 1/8/2015

import sys
import json
import argparse
from multiprocessing import Pool
from pymongo import MongoClient

def main():

    with open(args.filename) as f:
        pool = Pool(args.processes)
        results = pool.imap_unordered(process_dict,read_dict_from_json(f),10)
        pool.close()
        pool.join()

    found_document_count = 0
    not_found_document_count = 0
    for result in results:
        if result:
            found_document_count += 1
        else:
            not_found_document_count += 1

    print('! {} documents in {}.{}'.format(found_document_count, args.db, args.collection))
    print('! {} documents not found in {}.{}'.format(not_found_document_count, args.db, args.collection))

def read_dict_from_json(file_handle):

    for line in file_handle:
        JSON_obj = json.loads(line.rstrip('\n'))
        yield JSON_obj

def process_dict(JSON_obj):

    query = check_JSON_obj(JSON_obj, coll)
    if not query:
        if not args.quiet:
            print('Document not found:')
            print(JSON_obj)
            print()
        if args.insert:
            if not args.quiet:
                print('Inserting into database...\n')
            insert_JSON_obj(JSON_obj, coll)
        return False
    else:
        return True

def check_JSON_obj(JSON_obj, coll):

    query = coll.find_one({'_id':JSON_obj['_id']})

    return query

def insert_JSON_obj(JSON_obj, coll):

    coll.insert(JSON_obj)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='simple script to check to see if the objects in an input JSON file (on STDIN) are in a MongoDB database')
    parser.add_argument('filename', help='JSON filename')
    parser.add_argument('db', help='MongoDB database name')
    parser.add_argument('collection', help='MongoDB collection name')
    parser.add_argument('-i', '--insert', help='insert JSON objects if not found in DB', action='store_true')
    parser.add_argument('--host', help='MongoDB hostname (mongod or mongos) -- default: localhost', default='localhost')
    parser.add_argument('--port', help='MongoDB port (mongod or mongos) -- default: 27017', default=27017, type=int)
    parser.add_argument('-p', '--processes', help='Number of processes to start up in process pool', default=4, type=int)
    parser.add_argument('-q', '--quiet', help='Suppress "Document not found" messages', action='store_true')

    args = parser.parse_args()

    client = MongoClient(args.host, args.port, w=0)   ## write concern 0
    db = client[args.db]
    coll = db[args.collection]
    main()