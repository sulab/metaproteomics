#!/bin/bash

echo "create_indexdb protein_fasta_file.fasta mongodb_database mongodb_collection"
echo
echo "Dependencies:"
echo "Python 2.7"
echo "GNU Parallel, v20130622 or newer"
echo "MongoDB v2.4+ with 'mongoimport' and 'mongo' available in PATH"
echo
echo "Start date/time:"
echo `date`

ORIGFASTADB=$1
# FASTADB="renumbered_"$ORIGFASTADB
JSONFILE=${ORIGFASTADB/.fasta/.json}
DB=$2
COLL=$3

START=$(date +%s)
# echo
# echo "Numbering proteins in "$ORIGFASTADB

# cat $ORIGFASTADB | ./number_fasta_db.py > $FASTADB
# echo "...done"
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "Numbering proteins took $DIFF seconds"

# echo
# echo "Generating FLATDB using Blazmass"

# FLATDB=${FASTADB/.fasta/.flatdb}
# SFLATDB="sorted_"$FLATDB

# START=$(date +%s)

## FOR NODEA1309:
# touch empty.fasta
cat $ORIGFASTADB | parallel -j+4 --block 5M --recstart '>' --tmpdir /tmp_mongodb/create_refseqDB/tmp --pipe python fastaproteins_JSON.py 2> /dev/null 1> $JSONFILE

echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "JSON file generation took $DIFF seconds"



# START=$(date +%s)
# echo "-------------------------------"
# echo "Running GNU Sort 8.21"
# /usr/local/bin/sort -k1 -T . $FLATDB > $SFLATDB
# echo "...done"
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "flatdb sort took $DIFF seconds"



# START=$(date +%s)
# echo "-------------------------------"
# echo "Generating grouped JSON file"

# JSONFILE="sorted_"${FLATDB/.flatdb/.json}

# ./flatdb_parse.py $SFLATDB | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe ./flatdb_json.py > $JSONFILE
# echo `ls -l $JSONFILE`
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "json file generation took $DIFF seconds"

START=$(date +%s)
echo "-------------------------------"
echo "Running mongoimport"

# mongoimport -d $DB -c $COLL --file $JSONFILE
cat $JSONFILE | parallel -j2 --block 500M --tmpdir /tmp_mongodb/create_refseqDB/tmp --pipe mongoimport -d $DB -c $COLL

END=$(date +%s)
DIFF=$(( $END - $START ))
echo "mongoimport took $DIFF seconds"

# echo "-------------------------------"
# echo "Creating index on 'COUNT' field"
# mongo $DB --eval "db."$COLL".ensureIndex({'COUNT':1})"
# echo "...done"

# START=$(date +%s)
# echo "-------------------------------"
# echo "Creating index on 'MASS' field"
# mongo $DB --eval "db."$COLL".ensureIndex({'MASS':1})"
# echo "...done"
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "ensureIndex on MASS took $DIFF seconds"

START=$(date +%s)
echo "-------------------------------"
echo "Creating index on 'acc' field"
mongo $DB --eval "db['"$COLL"'].ensureIndex({'acc':1})"
echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "ensureIndex on acc took $DIFF seconds"

START=$(date +%s)
echo "-------------------------------"
echo "Creating index on 'accv' field"
mongo $DB --eval "db['"$COLL"'].ensureIndex({'accv':1})"
echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "ensureIndex on accv took $DIFF seconds"

# echo
# echo "Finish date/time:"
# echo `date`
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "IndexDB generation took $DIFF seconds"