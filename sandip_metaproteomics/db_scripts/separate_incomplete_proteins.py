#!/usr/bin/env python

# separate_incomplete_proteins.py
#
# short script to read in a FASTA file (like hmgi_aa.fasta - a file of metagenomic sequence reads, translated to proteins)
# and output proteins with missing sequence information (the 'x' or 'X' amino acid residue) to one file and complete proteins to another file

import sys

def main():
	
	fasta_file = sys.argv[1]

	read_fasta(fasta_file)

def read_fasta(fasta_file):

	with open(fasta_file,'rb') as f, open(fasta_file.rstrip('.fasta')+'_incomplete.fasta','wb') as i, open(fasta_file.rstrip('.fasta')+'_complete.fasta','wb') as c:
		line = f.readline()
		
		while True:
			defline = ''
			sequence_lines = []
			protein_dicts = [] ##	list of dicts (one per peptide), to be unpacked before appending to current_chunk
			if not line:
				break
			if line[0] == '>':
				defline = line[1:].rstrip('\n')		##	remove leading '>' and trailing newline
				while True:
					line = f.readline()
					if not line or line[0] == '>':
						break
					sequence_lines.append(line.rstrip('\n'))

			full_sequence = ''.join(sequence_lines).replace('\n','')

			if 'x' in full_sequence or 'X' in full_sequence:
				write_prot_to_file(i,defline,full_sequence)
			else:
				write_prot_to_file(c,defline,full_sequence)

def write_prot_to_file(file_obj,defline,protein_sequence):
	file_obj.write('>'+defline+'\n')
	file_obj.write(protein_sequence+'\n')

if __name__ == '__main__':
	main()