#!/usr/bin/env python

#	make_refseq_fasta.py
#	
#	Script that tries to generate a RefSeq multi-protein FASTA file from a given RefSeq catalog file
#
#	USAGE: $ cat refseq_catalog | python make_refseq_fasta.py > big_fasta_file.fasta
#	- or -
#	
#	cat refseq_catalog | parallel -j+0 --block 50M --tmpdir /tmp_mongodb/create_refseqDB/tmp --pipe python make_refseq_fasta.py 2> /dev/null 1> big_fasta_file.fasta
#
#	v1, 3/3/14
#	Sandip Chatterjee


import sys
from pymongo import MongoClient

def main():
	
	############################################################################
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION:   #########
	############################################################################

	host_name = 'localhost'
	db_name = 'refseqDB'
	collection_name = 'refseq63'

	############################################################################
	########    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    #########
	########    MODIFY THESE PARAMETERS FOR YOUR MONGO INSTALLATION    #########
	############################################################################

	client = MongoClient(host_name)
	db = client[db_name]
	collection = db[collection_name]

	# print '\nConnecting to MongoDB host: '+host_name
	# print 'Database name: '+db_name
	# print 'Collection name: '+collection_name+'\n'

	# with open(DB_filename,'rb') as DB_file:

	catalog_file = sys.stdin

	for line in catalog_file:
		if 'complete|microbial' in line:
			accv = line.split('\t')[2]
			accv_query = query_DB(accv,collection)
			if accv_query:
				print '>'+accv_query['def']
				print accv_query['seq']


def query_DB(accession, collection):
	accv_query = collection.find_one({'accv':accession})
	if accv_query:
		return accv_query
	else:
		acc = accession.split('.')[0]	##	acc is the accession number without a version number (accesion.version)
		acc_query = collection.find_one({'acc':acc})
		if acc_query:
			return acc_query 	##	treats a query for 'acc' or 'accv' the same (protein record with any version number is OK)
		else:
			return False

if __name__ == '__main__':
	main()