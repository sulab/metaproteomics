    /**
     * Cut a Fasta protein sequence according to params spec and index the
     * protein and generated sequences
     *
     * Should be called once per unique Fasta sequence
     *
     * @param fasta fasta to index
     * @throws IOException
     */
    private void cutSeq(final String protAccession, final String protSeq) throws IOException {

        //Enzyme enz = sparam.getEnzyme();
        final int length = protSeq.length();
//
        //AssignMass aMass = AssignMass.getInstance(true);

        final char[] pepSeq = new char[MAX_SEQ_LENGTH]; //max seq length
        int curSeqI = 0;

        final int maxMissedCleavages = sparam.getMaxMissedCleavages();
        int maxIntCleavage = sparam.getMaxInternalCleavageSites();

        try {
            long proteinId = indexStore.addProteinDef(++protNum, protAccession, protSeq);
//	    System.out.println(fasta.getSequestLikeAccession());
            //System.out.println(fasta.getDefline());

            for (int start = 0; start < length; ++start) {
                int end = start;

                //clear the preallocated seq byte array
                //Arrays.fill(seq, 0, curSeqI > 0?curSeqI-1:0, (byte) 0); //no need, we copy up to curSeqI nowu
                curSeqI = 0;

                //float precMass = Constants.H2O_PROTON_SCALED_DOWN;
                float precMass = Constants.H2O_PROTON;
                precMass += AssignMass.getcTerm();
                precMass += AssignMass.getnTerm();
                

                // System.out.println("===>>" + precMass + "\t" + Constants.MAX_PRECURSOR);
                //System.out.println("==" + j + " " + length + " " + (j < length));

                //int testC=0;
                int pepSize = 0;

                int intMisCleavageCount = -1;


                //while (precMass <= Constants.MAX_PRECURSOR_MASS && end < length) {
                while (precMass <= sparam.getMaxPrecursorMass() && end < length) {
                    pepSize++;

                    final char curIon = protSeq.charAt(end);
                    pepSeq[curSeqI++] = curIon;
                    precMass += AssignMass.getMass(curIon);

                    if (Enzyme.isEnzyme(protSeq.charAt(end))) {
                        intMisCleavageCount++;
                    }

                    final int cleavageStatus = Enzyme.checkCleavage(protSeq, start, end);

                    if (intMisCleavageCount > maxIntCleavage) {
                        break;
                    }
                    
                    //if (precMass > Constants.MAX_PRECURSOR_MASS) {
                    if (precMass > sparam.getMaxPrecursorMass()) {
                        break;
                    }
                    
                    if (pepSize >= Constants.MIN_PEP_LENGTH && precMass >= sparam.getMinPrecursorMass() ) { //Constants.MIN_PRECURSOR ) {
                        if (cleavageStatus >= maxMissedCleavages) {
                            //qualifies based on params

                            final String peptideSeqString = String.valueOf(Arrays.copyOf(pepSeq, curSeqI));

                            //check if index will accept it
                            final FilterResult filterResult = indexStore.filterSequence(precMass, peptideSeqString);

                            if (filterResult.equals(FilterResult.SKIP_PROTEIN_START) ) {
                                //bail out earlier as we are no longer interested in this protein starting at start
                                break; //move to new start position
                            }
                            else if (filterResult.equals(FilterResult.INCLUDE) ) {
                                final int resLeftI = start >= Constants.MAX_INDEX_RESIDUE_LEN ? start - Constants.MAX_INDEX_RESIDUE_LEN : 0;
                                final int resLeftLen = Math.min(Constants.MAX_INDEX_RESIDUE_LEN, start);
                                StringBuilder sbLeft = new StringBuilder(Constants.MAX_INDEX_RESIDUE_LEN);
                                for (int ii = 0; ii < resLeftLen; ++ii) {
                                    sbLeft.append(protSeq.charAt(ii + resLeftI));
                                }
                                final int resRightI = end + 1;
                                final int resRightLen = Math.min(Constants.MAX_INDEX_RESIDUE_LEN, length - end - 1);
                                StringBuilder sbRight = new StringBuilder(Constants.MAX_INDEX_RESIDUE_LEN);
                                if (resRightI < length) {
                                    for (int jj = 0; jj < resRightLen; ++jj) {
                                        sbRight.append(protSeq.charAt(jj + resRightI));
                                    }
                                }


                                //add -- markers to fill Constants.MAX_INDEX_RESIDUE_LEN length
                                final int lLen = sbLeft.length();
                                for (int c = 0; c < Constants.MAX_INDEX_RESIDUE_LEN - lLen; ++c) {
                                    sbLeft.insert(0, '-');
                                }
                                final int rLen = sbRight.length();
                                for (int c = 0; c < Constants.MAX_INDEX_RESIDUE_LEN - rLen; ++c) {
                                    sbRight.append('-');
                                }

                                final String resLeft = sbLeft.toString();
                                final String resRight = sbRight.toString();

                                indexStore.addSequence(precMass, start, curSeqI, peptideSeqString, resLeft, resRight, proteinId);
                            } //end if add sequence
                        }
                    }                    
        
                    ++end;
                }

            }
        } catch (DBIndexStoreException e) {
            logger.log(Level.SEVERE, "Error writing sequence to db index store, ", e);
        }

    }

