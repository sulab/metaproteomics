#!/bin/bash

# get_all_refseqDB_counts.sh
#
# short bash script to return all 63 refseq databases' document counts from db.collection.stats()

for i in `seq 1 63`;
do
	printf $i"\t" && mongo refseqs --quiet --eval "db.refseq$i.stats().count"
done