#!/bin/bash

# LEN aggregation (count) on forwardDB
# mongo forwardDB --quiet < forwardDB_LEN.js > forwardDB_LEN_aggregation

# COUNT aggregation (count) on forwardDB
# mongo forwardDB --quiet < forwardDB_COUNT.js > forwardDB_COUNT_aggregation

# LEN aggregation (count) on reverseDB
# mongo reverseDB --quiet < reverseDB_LEN.js > reverseDB_LEN_aggregation

# COUNT aggregation (count) on reverseDB
# mongo reverseDB --quiet < reverseDB_COUNT.js > reverseDB_COUNT_aggregation

# MASSFLOOR aggregation (count) on indexDB
mongo indexDB --quiet < indexDB_MASSFLOOR.js > indexDB_MASSFLOOR_aggregation

# MASSFLOOR aggregation (count) on forwardDB
mongo forwardDB --quiet < forwardDB_MASSFLOOR.js > forwardDB_MASSFLOOR_aggregation

# MASSFLOOR aggregation (count) on reverseDB
mongo reverseDB --quiet < reverseDB_MASSFLOOR.js > reverseDB_MASSFLOOR_aggregation

# MASS aggregation (count) on indexDB
mongo indexDB --quiet < indexDB_MASS.js > indexDB_MASS_aggregation

# MASS aggregation (count) on forwardDB
mongo forwardDB --quiet < forwardDB_MASS.js > forwardDB_MASS_aggregation

# MASS aggregation (count) on reverseDB
mongo reverseDB --quiet < reverseDB_MASS.js > reverseDB_MASS_aggregation