#!/usr/bin/env python

import sys
import json
from collections import Counter

def main():

	JSON_file = sys.argv[1]

	cntr = Counter()

	with open(JSON_file,'rb') as f:
		for line in f:
			line_dict = json.loads(line.rstrip(',\n'))
			len_parents = len(line_dict['PARENTS'])
			cntr[len_parents] += 1

	with open('manual_count_aggregation','wb') as f:
		f.write('NUMPARENTS\tCOUNT\n')
		for key in cntr:
			f.write(str(key)+'\t'+str(cntr[key])+'\n')

if __name__ == '__main__':
	main()