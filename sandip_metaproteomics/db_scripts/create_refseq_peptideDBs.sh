#!/bin/bash

# echo "create_refseq_peptideDBs protein_fasta_file.fasta mongodb_database mongodb_collection"
# echo
# echo "Dependencies:"
# echo "Python 2.7"
# echo "GNU Sort, preferably v8.1+ for built-in parallel processing"
# echo "GNU Parallel, v20130622 or newer"
# echo "MongoDB v2.4+ with 'mongoimport' and 'mongo' available in PATH"
# echo
# echo "Requires Java1.7/Blazmass, number_fasta_db.py, fasta_peptides_json.py, and a blazmass.params file in the same directory"
# echo "Will probably use 10-20x disk space of original FASTA file..."

# echo
# echo "Start date/time:"
# echo `date`

# TEMP:


START=$(date +%s)
echo "-------------------------------"
echo "Creating index on 'SEQ' field"
mongo refseqs --eval "db.refseq29.ensureIndex({'SEQ':1})"
echo "...done"
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "ensureIndex on SEQ for Refseq29 took $DIFF seconds"
# /TEMP

# for i in `seq 1 63`;
# for i in `seq 1 40`;
for i in `seq 41 60`;
do
	# ORIGFASTADB=RefSeq$i.fasta
	# FASTADB="renumbered_"$ORIGFASTADB
	# START=$(date +%s)
	# echo "-------------------------------"
	# echo $ORIGFASTADB
	# echo
	# echo "Numbering proteins in "$ORIGFASTADB
	# cat /mongob/refseqs/fasta/$ORIGFASTADB | ./number_fasta_db.py > tmp/$FASTADB

	# END=$(date +%s)
	# DIFF=$(( $END - $START ))
	# echo "Numbering proteins took $DIFF seconds"
	# echo
	# echo "Generating FLATDB using Blazmass"

	# FLATDB=${FASTADB/.fasta/.flatdb}
	# SFLATDB="sorted_"$FLATDB

	# START=$(date +%s)

	# ## FOR NODEA1309:
	# touch empty.fasta
	# cat tmp/$FASTADB | parallel -j+4 --block 5M --recstart '>' --tmpdir /mongob/create_indexdb/tmp --pipe java -jar java/blazmass.jar -i /mongob/create_indexdb/java blazmass.params 2> /dev/null 1> /mongob/refseqs/flatdb/$FLATDB
	# rm tmp/$FASTADB

	# END=$(date +%s)
	# DIFF=$(( $END - $START ))
	# echo "FLATDB peptide file generation took $DIFF seconds"

	# START=$(date +%s)
	# echo
	# echo "Running GNU Sort 8.21"
	# /usr/local/bin/sort -k1 -T . /mongob/refseqs/flatdb/$FLATDB > /mongob/refseqs/sorted_flatdb/$SFLATDB

	# END=$(date +%s)
	# DIFF=$(( $END - $START ))
	# echo "FLATDB sort took $DIFF seconds"
	
	# START=$(date +%s)
	# echo
	# echo "Generating grouped JSON file"

	# JSONFILE="sorted_"${FLATDB/.flatdb/.json}

	# ./flatdb_parse.py /mongob/refseqs/sorted_flatdb/$SFLATDB | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe ./flatdb_json.py > /mongob/refseqs/json/$JSONFILE

	# END=$(date +%s)
	# DIFF=$(( $END - $START ))
	# echo "json file generation took $DIFF seconds"

	START=$(date +%s)
	echo "-------------------------------"
	echo "Running mongoimport"

	cat /mongob/refseqs/json/sorted_renumbered_RefSeq$i.json | parallel -j2 --block 500M --tmpdir /mongoc/tmp --pipe mongoimport -d refseqs -c refseq$i

	END=$(date +%s)
	DIFF=$(( $END - $START ))
	echo "Refseq$i mongoimport took $DIFF seconds"

	START=$(date +%s)
	echo "-------------------------------"
	echo "Creating index on 'SEQ' field"
	mongo refseqs --eval "db.refseq"$i".ensureIndex({'SEQ':1})"
	echo "...done"
	END=$(date +%s)
	DIFF=$(( $END - $START ))
	echo "ensureIndex on SEQ for Refseq$i took $DIFF seconds"

done

# START=$(date +%s)
# echo "-------------------------------"
# echo "Running mongoimport"

# cat $JSONFILE | parallel -j2 --block 500M --tmpdir /tmp_mongodb/create_indexdb/tmp --pipe mongoimport -d $DB -c $COLL

# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "mongoimport took $DIFF seconds"

# echo "-------------------------------"
# echo "Creating index on 'COUNT' field"
# mongo $DB --eval "db."$COLL".ensureIndex({'COUNT':1})"
# echo "...done"

# START=$(date +%s)
# echo "-------------------------------"
# echo "Creating index on 'MASS' field"
# mongo $DB --eval "db."$COLL".ensureIndex({'MASS':1})"
# echo "...done"
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "ensureIndex on MASS took $DIFF seconds"

# START=$(date +%s)
# echo "-------------------------------"
# echo "Creating index on 'SEQ' field"
# mongo $DB --eval "db['"$COLL"'].ensureIndex({'SEQ':1})"
# echo "...done"
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "ensureIndex on SEQ took $DIFF seconds"

# echo
# echo "Finish date/time:"
# echo `date`
# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "IndexDB generation took $DIFF seconds"