#!/bin/bash

# ORIGFASTADB=$1
# FASTADB="renumbered_"$ORIGFASTADB
# # DB=$2
# # COLL=$3

# START=$(date +%s)
# echo "-------------------------------"
# echo $ORIGFASTADB
# echo
# echo "Numbering proteins in "$ORIGFASTADB
# cat $ORIGFASTADB | ./number_fasta_db.py > tmp/$FASTADB

# END=$(date +%s)
# DIFF=$(( $END - $START ))

# echo "Numbering proteins took $DIFF seconds"
# echo
# echo "Generating FLATDB using Blazmass"

# FLATDB=${FASTADB/.fasta/.flatdb}

# START=$(date +%s)



# touch empty.fasta
# cat tmp/$FASTADB | parallel -j12 --block 5M --recstart '>' --tmpdir /mongoa/create_indexdb/tmp --pipe java -jar java/blazmass.jar -i /mongoa/create_indexdb/java blazmass.params 2> /dev/null 1> $FLATDB
# rm tmp/$FASTADB
# END=$(date +%s)
# DIFF=$(( $END - $START ))

# echo "FLATDB generation took $DIFF seconds"

START=$(date +%s)
cat sorted_renumbered_012714_indexDB_reversed.flatdb hmgi_complete.flatdb | /usr/local/bin/sort -k1 -T /mongob/create_indexdb/tmp --parallel=12 > /mongoa/hmp_metagenomics/indexDB_032014_sorted.flatdb
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "FLATDB sort took $DIFF seconds"

echo "Finished"