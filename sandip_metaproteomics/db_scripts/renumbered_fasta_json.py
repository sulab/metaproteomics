#!/opt/applications/python/2.7.1/gnu/bin/python

import sys
import json

def main():

	DB_file = sys.stdin

	read_db_file(DB_file)

def read_db_file(DB_file):
	
	temp_dict = {}

	for line in DB_file:
		if line[0] == '>':
			line_split = line.lstrip('>').rstrip('\n').split('||')
			# temp_dict["_id"] = line_split[0]
			temp_dict["pnum"] = line_split[0]
			temp_dict["defline"] = line_split[1]
			print_json_chunk(temp_dict)

def print_json_chunk(json_dict):
	
	print json.dumps(json_dict)+','	##	print json string and comma to screen

if __name__ == '__main__':
	main()