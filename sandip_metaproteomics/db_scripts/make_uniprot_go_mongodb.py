#!/usr/local/bin/python

#	make_uniprot_go_mongodb.py
#	Sandip Chatterjee
#	v1, October 1, 2013
#	
#	For generating a MongoDB representation of the large uniprot gene_association.goa_uniprot
#	(mapping UniProt accession numbers to GO terms)
#	
#	Usage: 
#	$ python make_uniprot_go_mongodb.py gene_association.goa_uniprot

import sys
from pymongo import MongoClient

def main():

	uniprot_idmapping_db = sys.argv[1]

	client = MongoClient('hadoop00-adm')	##	for use on garibaldi
	db = client.sandip			## database name (default: test)
	collection = db.uniprot_go_mapping		## collection name 

	read_db(uniprot_idmapping_db,collection)
	print "Finished"

def read_db(uniprot_idmapping_db,collection):
	
	linesplit = []
	uniprot_id = ''
	GO_term = ''

	with open(uniprot_idmapping_db,'rb') as f:
		for line in f:
			if line[0] != '!':
				linesplit = line.split('\t')
				# uniprot_id = linesplit[1]
				# GO_term = linesplit[4]	##	this column in the gene_association file only contains a single GO term per line (e.g., GO:0016491)
				collection.insert({
									'uid':linesplit[1],	##	'uid' is the UniProt accession number (unique, one per line)
									'go':linesplit[4]	##	this column in the gene_association file contains a single GO term per line (e.g., GO:0016491)
								})

if __name__ == '__main__':
	main()