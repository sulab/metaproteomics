#!/usr/bin/env python

#	import_json.py
#	
#	for importing a JSON file into MongoDB (2.6+) using unordered bulk import operations

import sys
# import json
from pymongo import MongoClient

def main():

	db_name = 'rnaseqDB'

	client = MongoClient()
	db = client[db_name]
	coll = db['rnaseqColl']

	coll.ensure_index('m')
	# client.admin.command('enablesharding', db_name)

	JSON_file = sys.argv[1]
	JSON_obj = {}
	JSON_obj_list = []

	with open(JSON_file,'rb') as f:
		bulk = coll.initialize_unordered_bulk_op()
		for num, line in enumerate(f):
			JSON_obj = eval(line.rstrip(',\n'))
			# JSON_obj = json.loads(line.rstrip(',\n'))
			# JSON_obj_list.append(JSON_obj)
			bulk.insert(JSON_obj)

			if num % 100000 == 0:
				print "processed", num, "JSON objects"

			if num % 1000000 == 0:
				bulk.execute()
				bulk = coll.initialize_unordered_bulk_op()

if __name__ == '__main__':
	main()