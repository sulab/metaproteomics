#!/usr/bin/env python3

# lookup_onehitwonder_stats.py
# Simple analysis script for peptide sequences that have only one parent protein 
# (sequences that only appear once in the entire protein database)

# Questions to answer using this data:
#
# - Regarding the 'parent proteins for peptides' histogram (where ~2B of 4B peptide sequences have only one parent protein)
#    - How many distinct proteins are represented by these "one hit wonder" peptides?
#        - can simply use the Parent Protein ID for this classification (the ProtDB accession number)
#        - Use SeqDB JSON file
#        - record parent protein ID for all peptides, then count occurrences to make a histogram
#            - potential problem: maybe there are several similar proteins (from different repositories) that have distinct parent protein IDs in ProtDB
#    - How many distinct bacterial organisms are represented by these peptides?
#        - can query ProtDB (with accession #) for 'o' field (if it exists)
#    - What percentage of these peptides are from decoy/Reverse proteins? (i.e., how many of these 'unique' peptides are completely made up?)
#        - can look at SeqDB JSON file, check to see if 'd' field exists in each 'p' object (should be True if it exists)

# Sandip Chatterjee
# 9/20/14


import sys
import json
from pymongo import MongoClient

def main():

    client = MongoClient('localhost',27018)
    db = client['ProtDB_072114']
    coll = db['ProtDB_072114']

    # SeqDB JSON file
    JSON_file = sys.argv[1]
    
    parent_proteinID_counter = {}
    parent_proteinID_counter_forwardpeptidesonly = {}
    organism_counter = {"Unknown":0} # includes 'Unknown' organism name
    organism_counter_forwardpeptidesonly = {"Unknown":0} # includes 'Unknown' organism name

    forward_counter = 0
    reverse_counter = 0

    with open(JSON_file) as f:

        for line in f:

            obj = json.loads(line.rstrip('\n'))
            parents_list = obj['p']
            decoy = False
            
            # only looking at peptides with exactly 1 parent protein
            if len(parents_list) != 1:
                continue

            parent = parents_list[0]
            if 'd' in parent:
                decoy = True
            
            parentID = parent['i']
            ProtDB_query = coll.find_one({"_id":parentID})
            if ProtDB_query:
                if 'o' in ProtDB_query:
                    parentOrg = ProtDB_query['o']
                else:
                    parentOrg = 'Unknown'
            else:
                print("! Protein ID {} not found in ProtDB...".format(parentID))

            if parentID in parent_proteinID_counter:
                parent_proteinID_counter[parentID] = parent_proteinID_counter[parentID] + 1
            else:
                parent_proteinID_counter[parentID] = 1
            
            if parentOrg in organism_counter:
                organism_counter[parentOrg] = organism_counter[parentOrg] + 1
            else:
                organism_counter[parentOrg] = 1

            if not decoy:
                if parentID in parent_proteinID_counter_forwardpeptidesonly:
                    parent_proteinID_counter_forwardpeptidesonly[parentID] = parent_proteinID_counter_forwardpeptidesonly[parentID] + 1
                else:
                    parent_proteinID_counter_forwardpeptidesonly[parentID] = 1
                
                if parentOrg in organism_counter_forwardpeptidesonly:
                    organism_counter_forwardpeptidesonly[parentOrg] = organism_counter_forwardpeptidesonly[parentOrg] + 1
                else:
                    organism_counter_forwardpeptidesonly[parentOrg] = 1

            if decoy:
                reverse_counter += 1
            else:
                forward_counter += 1

    # how many peptides from each parent protein? (these peptides only appear once in the database)
    write_dict_to_file(parent_proteinID_counter, JSON_file+'_parent_proteinID_counter.tab')

    # how many peptides from each FORWARD parent protein? (these peptides only appear once in the database)
    write_dict_to_file(parent_proteinID_counter_forwardpeptidesonly, JSON_file+'_parent_proteinID_counter_forwardpeptidesonly.tab')

    # how many peptides from each organism? (these peptides only appear once in the database, but multiple such peptides may come from one organism)
    write_dict_to_file(organism_counter, JSON_file+'_organism_counter.tab')

    # how many peptides (from FORWARD proteins) from each organism? (these peptides only appear once in the database, but multiple such peptides may come from one organism)
    write_dict_to_file(organism_counter_forwardpeptidesonly, JSON_file+'_organism_counter_forwardpeptidesonly.tab')

    print("Peptides with 1 parent from a FORWARD parent:", forward_counter)
    print("Peptides with 1 parent from a REVERSE parent:", reverse_counter)

def write_dict_to_file(counter_obj, newfilename):
    with open(newfilename,'w') as f:
        for key in sorted(counter_obj):
            f.write(str(key)+'\t'+str(counter_obj[key])+'\n')


if __name__ == '__main__':
    main()
