#!/usr/bin/env python3

# what does the 'parent peptide' histogram look like for peptides from 'Forward' proteins only? ('real' peptides)
# what does the peptide length distribution look like for peptides from 'Forward' proteins only? ('real' peptides)
# (similar to count_stats.py from 7/20/14)
# 
# Sandip Chatterjee
# 9/20/14

import sys
import json

def main():
	JSON_file = sys.argv[1]
	parent_counter = {}
	length_counter = {}

	with open(JSON_file) as f:
		for line in f:
			obj = json.loads(line.rstrip('\n'))
			parents_list = obj['p']
			if reversed_protein_present(parents_list):
				continue
			count = len(parents_list)
			if count in parent_counter:
				parent_counter[count] = 1+parent_counter[count]
			else:
				parent_counter[count] = 1
			length = len(obj['_id'])
			if length in length_counter:
				length_counter[length] += 1
			else:
				length_counter[length] = 1

	with open(JSON_file+'_parentcount_forwardonly_aggregation.tab','w') as f:
		for key in sorted(parent_counter):
			f.write(str(key)+'\t'+str(parent_counter[key])+'\n')

	with open(JSON_file+'_peptidelength_forwardonly_aggregation.tab','w') as f:
                for key in sorted(length_counter):
                        f.write(str(key)+'\t'+str(length_counter[key])+'\n')

def reversed_protein_present(parents_list):

	''' returns True if a parent protein in parents_list is a decoy (Reverse) protein  '''

	for parent in parents_list:
		if 'd' in parent:
			return True
	else:
		return False

if __name__ == '__main__':
	main()
