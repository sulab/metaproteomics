#!/usr/bin/env python3

# count_protein_lengths.py
# 9/4/14, Sandip Chatterjee
# 
# input:    SeqDB JSON file (on STDIN)
# output:   information on peptide redundancy of human peptides
#
#			ex:
#			[fold-redundancy]	[peptide_sequence_from_human_proteome]	<-- one peptide sequence per line
#			
#			1	AAAAAAAAAAAAAAAAAAAAAAAKFVKK
#			4	AAAAAAAAAAANNGGGGSSGGTVNAPQSNGTWIKPTNGRLTSPYGWR
#			...
#
# usage:    python3 count_protein_lengths.py

# import sys
import json
from pymongo import MongoClient

def main():

	# JSON_chunk = sys.stdin
	client = MongoClient('localhost',27018)
	ProtDB = client['ProtDB_072114']
	ProtDBColl = ProtDB['ProtDB_072114']

	repository_list = []
	for record in ProtDBColl.find():
		if record['r'] in 

	# for line in JSON_chunk:
	# 	peptide_dict = json.loads(line.rstrip('\n'))
	# 	if len(peptide_dict['p']) > 1:
	# 		repository_list = []
	# 		for parent in peptide_dict['p']:
	# 			protID = parent['i']
	# 			query_result = ProtDBColl.find_one({"_id":protID})
	# 			if query_result:
	# 				repository_list.append(query_result['r'])
	# 			else:
	# 				print('!!ERROR LOOKING UP PROTEIN ID {}'.format(protID))
	# 		if 'UniProt_Human' in repository_list:
	# 			repository_list_nr = list(set(repository_list))
	# 			if len(repository_list_nr) > 1:
	# 				print(len(repository_list), end='\t')
	# 				print(peptide_dict['_id'])


if __name__ == '__main__':
	main()