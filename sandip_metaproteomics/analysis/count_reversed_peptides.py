#!/usr/bin/env python3

# count_reversed_peptides.py
# 8/11/14, Sandip Chatterjee
# 
# input:    SeqDB JSON file (on STDIN)
# output:   lengths of peptides that are present in both decoy and regular proteins (1 length per line)
#
# usage:    cat huge_SeqDB_JSON_file.json | python3 count_reversed_peptides.py > redundant_peptide_lengths
# altusage: cat huge_SeqDB_JSON_file.json | parallel -j+0 --block 50M --pipe python3 count_reversed_peptides.py > redundant_peptide_lengths

import sys
import json

def main():
	json_file = sys.stdin	
	for line in json_file:
		line_dict = json.loads(line.rstrip('\n'))
		parents = line_dict['p']
		decoy_parent_present = False
		for parent in parents:
			if 'd' in parent:
				decoy_parent_present = True
		if decoy_parent_present and len(parents) != 1:
			# if there is a decoy parent present, and it's not the only parent...
			if not all_parents_are_decoys(parents):
				# if all parent proteins aren't decoy proteins, but there is a decoy parent present
				print(str(len(line_dict['_id'])))
				#print(line,end='')

def all_parents_are_decoys(parents):
	all_decoys = True
	for parent in parents:
		if 'd' not in parent:
			all_decoys = False
	return all_decoys

if __name__ == '__main__':
	main()
