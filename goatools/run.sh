#!/bin/bash

# configuration for Gene Ontology files
GO_OBO_FILE=go-basic.obo
GOSLIM_OBO_FILE=goslim_generic.obo

GO_OBO_DOWNLOAD=http://purl.obolibrary.org/obo/go/go-basic.obo
GOSLIM_OBO_DOWNLOAD=http://www.geneontology.org/ontology/subsets/goslim_generic.obo

# if the gene ontology files don't exist, download them
if [ ! -f $GO_OBO_FILE ]
then
    echo "downloading GO file: $GO_OBO_FILE"
    wget -O $GO_OBO_FILE $GO_OBO_DOWNLOAD
fi

if [ ! -f $GOSLIM_OBO_FILE ]
then
    echo "downlaoding GOslim file: $GOSLIM_OBO_FILE"
    wget -O $GOSLIM_OBO_FILE $GOSLIM_OBO_DOWNLOAD
fi
