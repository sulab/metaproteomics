# -*- coding: utf-8 -*-
"""
Created on Fri May 29 16:49:29 2015

@author: gstupp
"""

"""
Genotypes.vcf:
/gpfs/home/gstupp/1000genomes/phase3/ALL.chr{1..22}.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.gz
/gpfs/home/gstupp/1000genomes/phase3/ALL.chrX.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.gz
/gpfs/home/gstupp/1000genomes/phase3/ALL.chrY.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.gz

# From vcf file:
##reference=ftp://ftp.1000genomes.ebi.ac.uk//vol1/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
from the readme: based on GRCh37p4

Download CCDS, convert to bed file:
ftp://ftp.ncbi.nih.gov/pub/CCDS/archive/15/BuildInfo.20131129.txt



Whole genome fasta file:
/gpfs/group/databases/Homo_sapiens/NCBI/build37.2/Sequence/WholeGenomeFasta/genome.fa
/gpfs/group/databases/Homo_sapiens/Ensembl/GRCh37/Sequence/WholeGenomeFasta/genome.fa

ftp://ftp.ncbi.nih.gov/pub/CCDS/archive/Hs37.3/
or 
ftp://ftp.ncbi.nih.gov/pub/CCDS/archive/15/
CCDS.current.txt

http://biopython.org/wiki/Coordinate_mapping

FastaAlternateReferenceMaker
https://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_gatk_tools_walkers_fasta_FastaAlternateReferenceMaker.php

"""
"""
How to run:

Download genotype VCF files:
$ wget ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/*genotypes.vcf.gz

Download CCDS file:
$ wget ftp://ftp.ncbi.nih.gov/pub/CCDS/archive/15/CCDS.20131129.txt
$ sort -k1 -k4n CCDS.20131129.txt | chanjo convert > CCDS.20131129.bed
$ wget ftp://ftp.ncbi.nih.gov/pub/CCDS/archive/15/CCDS_nucleotide.20131129.fna.gz

Split CCDS into chromosomes:
$ grep -i public CCDS.20131129.txt | awk '{print >> $1"_CCDS.txt"; close($1"_CCDS.txt")}'

Generate coding VCF:
./run_intersectBed.sh
ln -s ALL.chrY.phase3_integrated.20130502.genotypes.vcf.CDS.gz ALL.chrY.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.CDS.gz
ln -s ALL.chrX.phase3_shapeit2_mvncall_integrated.20130502.genotypes.vcf.CDS.gz ALL.chrX.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.CDS.gz
ln -s ALL.chrY.phase3_integrated.20130502.genotypes.vcf.CDS.gz.tbi ALL.chrY.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.CDS.gz.tbi
ln -s ALL.chrX.phase3_shapeit2_mvncall_integrated.20130502.genotypes.vcf.CDS.gz.tbi ALL.chrX.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.CDS.gz.tbi

Run tabix
$ for i in $(ls *.gz); do tabix -p vcf -f $i; done



"""
#%%
import pickle
import gzip
def gsave(obj, filename, protocol=3):
    # should check if file exists first...
    with gzip.GzipFile(filename, 'wb') as f:
        pickle.dump(obj, f, protocol)

def gload(filename):
    f = gzip.open(filename,'rb')
    myNewObject = pickle.load(f)
    f.close()
    return myNewObject
#%%
import os
from collections import Counter
from itertools import chain
import numpy as np
import pandas as pd
import vcf
from vcf import model as vcf_model
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna

class CCD(object):
    """Class defining a CCD.
    
    chrom (str): chromosome

    interval (list of lists)
        Start & stop positions of exons
        example: [[948953, 948955], [949363, 949857]]

    position_idx (list): size = length
        list of positions for each base
        
    orientation (boolean)
        + = True
        - = False
        
    ID (str): CCD_ID
    
    length (int): length of transcript
    
    variants (list of vcf.model._Record)
        
    """
    def __init__(self, chrom = None, interval = None, position_idx = None, orientation = None, ID = None, length = None, variants = None, line = None):
        self.chrom = chrom
        self.interval = interval
        self.position_idx = position_idx
        self.orientation = orientation
        self.ID = ID
        self.length = length
        self.variants = variants
        self._line = None
        self.line = line
        
    def __str__(self):
        return str((self.chrom, self.ID, '+' if self.orientation else '-', self.length, str(self.interval[0][0]) + ' - ' + str(self.interval[-1][-1])))
    
    def __repr__(self):
        return str((self.chrom, self.ID, '+' if self.orientation else '-', self.length, str(self.interval[0][0]) + ' - ' + str(self.interval[-1][-1])))
    
    @property
    def line(self):
        return self._line
        
    @line.setter
    def line(self, line):
        self._line = line
        if line:
            self.interval = [[int(y) for y in x.strip().split('-')] for x in line[9][1:-1].split(',')]
            self.position_idx = list(np.array(list(chain(*[list(range(x[0],x[1]+1)) for x in self.interval]))) + 1)
            self.orientation = line[6] == '+' # positive = True, negative = False
            self.chrom = line[0]
            self.ID = line[4]
            self.length = sum([x[1]+1-x[0] for x in self.interval])
        
    def uniquify_variants(self):
        # Uniquify a list of vcf_records based on POS
        seen = set()
        self.variants = [ x for x in self.variants if not (x['POS'] in seen or seen.add(x['POS']))]

def genotype_dict_to_list(d):
    """
    >>> {0: 1, 2: 1, 4: 2, 6: 1, 9: 1}
    [1, 0, 1, 0, 2, 0, 1, 0, 0, 1]
    """
    return [d.get(x, 0) for x in range(2504)]

def genotype_list_to_dict(s):
    """
    >>> [1, 0, 1, 0, 2, 0, 1, 0, 0, 1]
    {0: 1, 2: 1, 4: 2, 6: 1, 9: 1}
    """
    return {idx: value for idx,value in enumerate(s) if value != 0}
    
def vcf_model_to_dict(model):
    # Convert the non-pickleable vcf.model.record into a dictionary containing info I need
    # NOTE: some genotypes looks like ".|0"
    #df = pd.DataFrame([list(map(int, model.samples[sample].gt_alleles)) for sample in range(len(model.samples))])

    df = pd.DataFrame([model.samples[sample].gt_alleles for sample in range(len(model.samples))])
    genotype_str = ''.join([''.join(list(df[0])),''.join(list(df[1]))])
    if not genotype_str.isdigit():
        raise ValueError("genotype contains uncalled values")

    genotypes = [genotype_list_to_dict(map(int,df[0])), genotype_list_to_dict(map(int,df[1]))] # 100x smaller
    return {'CHROM': model.CHROM, 'POS': model.POS, 'ID': model.ID, 'REF': model.REF, 
            'ALT': list(map(str,model.ALT)), 'genotypes': genotypes, 'var_type': model.var_type,
            'end': model.end, 'INFO': model.INFO, 'affected_end': model.affected_end, 
            'affected_start': model.affected_start, 'heterozygosity': model.heterozygosity,
            'start': model.start, 'var_subtype': model.var_subtype}

import traceback
def get_ccd(CCD_line):
    """
    Parse a CCD from the CCD info file.
    Pull out all variants from VCF that are within this CCD
    Returns a pickleable object
    """
    CCD_line = CCD_line.split('\t')
    if CCD_line[5] != 'Public':
        return None
    ccd = CCD(line = CCD_line)
    try:
        vcf_file_path = vcf_path + 'ALL.chr{}.phase3_shapeit2_mvncall_integrated_v4.20130502.genotypes.vcf.CDS.gz'.format(ccd.chrom)
        with open(vcf_file_path, 'rb') as vcf_file:
            vcf_reader = vcf.Reader(vcf_file)
            #ccd.variants = [vcf_model_to_dict(record) for record in vcf_reader.fetch('1', 850000, 875000)]
            variants = list(chain(*[list(vcf_reader.fetch(ccd.chrom, interval[0], interval[1]+1)) for interval in ccd.interval]))
            ccd.variants = [vcf_model_to_dict(record) for record in variants]
            #print([x.POS for x in ccd.variants])
        ccd.uniquify_variants()
    except Exception:
        print(traceback.format_exc())
        return None
    finally:
        print(ccd)
    
    return ccd

#%% 

import sys

if __name__ == "__main__":
    path_1000 = "/mongoc/gstupp/1000genomes/phase3/"
    #path_1000 = "/home/gstupp/1000genomes/"
    CCDS_nucleotide_fna = path_1000 + "CCDS_nucleotide.20131129.fna"
    CCDS_txt = path_1000 + "{}_CCDS.txt".format(sys.argv[1])
    vcf_path = path_1000 + "genotypes_cds_vcf/"
    
    record_dict_CCDS = SeqIO.index(CCDS_nucleotide_fna, "fasta") # CCDS nucleotide sequences
    CCDS_file = open(CCDS_txt) # CCDS info (positions, strand, etc)
    
    multi = True
    if multi:
        from multiprocessing import Pool
        p = Pool(16)
        ccds = p.imap_unordered(get_ccd, CCDS_file, chunksize=50)
        p.close()
        p.join()
    else:
        ccds = [get_ccd(ccd) for ccd in CCDS_file]

    gsave(ccds,"{}_CCDS.pkl.gz".format(sys.argv[1]))

"""
#%% get haplotype. do mutations
def pprint_record(record):
    print((record['POS'],record['REF'],record['ALT']))

def mutate_seq(seq, CCDS_idx, CCDS_vcf, genotype, verbose = True):
    df = pd.DataFrame(list(seq), index = CCDS_idx, columns=['REF'])
    df['ALT'] = df['REF']
    for (record, allele) in zip(CCDS_vcf, genotype):
        if allele == 0:
            continue
        if record['var_type'] in ['snp','indel']:
            df.loc[record['POS']:record['end'], 'ALT'] = ''
            df.loc[record['POS'], 'ALT'] = record['ALT'][allele - 1]
        if verbose:
            pprint_record(record)
            idx_start = int(np.where(df.index==record['POS'])[0])
            idx_end = int(np.where(df.index==record['end'])[0])
            print(df.iloc[idx_start-1: idx_end+2])
            #print(allele)
    #return ''.join(list(newdf[0]))
    return df

hgvs = lambda x,g: x['CHROM']+':g.'+str(x['POS'])+x['REF']+'>'+x['ALT'][g]

import os
os.chdir("/home/gstupp/1000genomes")
ccds = gload("21_CCDS.pkl.gz")
path_1000 = "/home/gstupp/1000genomes/"
CCDS_nucleotide_fna = path_1000 + "CCDS_nucleotide.20131129.fna"
record_dict_CCDS = SeqIO.index(CCDS_nucleotide_fna, "fasta") # CCDS nucleotide sequences
n_samples = 2054
aas=[]

for ccd in ccds:
    if not ccd:
        continue
    CCDS_vcf = ccd.variants
    
    seq = str(record_dict_CCDS[ccd.ID + '|Hs105|chr' + ccd.chrom].seq)
    if not ccd.orientation: # if orientation is negative
        seq = str(Seq(seq, generic_dna).reverse_complement())
    
    geno1 = Counter([tuple([record['genotypes'][0].get(sample,0) for record in ccd.variants]) for sample in range(n_samples)])
    geno2 = Counter([tuple([record['genotypes'][1].get(sample,0) for record in ccd.variants]) for sample in range(n_samples)])
    genotypes = geno1 + geno2

    for genotype, count in genotypes.items():
        freq = count / sum(genotypes.values())
        if freq < 0.01:
            continue
        newdf = mutate_seq(seq, ccd.position_idx, CCDS_vcf, genotype, verbose = False)
        newseq = ''.join(list(newdf['ALT']))
        if not ccd.orientation:
            new_aa = str(Seq(newseq, generic_dna).reverse_complement().translate(to_stop = True))
        else:
            new_aa = str(Seq(newseq, generic_dna).translate(to_stop = True))
        
        hgvs_id = ';'.join([hgvs(variant, g-1) for variant, g in zip(CCDS_vcf, genotype) if g != 0])
        x = {'seq': new_aa, 'hgvs_id': hgvs_id, 'ccd_ID':ccd.ID}
        aas.append(x)
#%% for testing

poss = [93000,93005,93010]
refs = ['A','A','TT']
alts = [['T'], ['TT'], ['A','T']]
allele = [0,1,2]

record = vcf_model._Record('10',93000,'id1','A',[model._Substitution('T')],None,None,{},None,{},None)
'''
seq="ATGATTGATGATTCAACACGCGTTCCTCTTGGAAAAAGCAAGGACTACATCAATGCTAGTTATATTAGAATAGTCAATTGTGGAGAAGAGTATTTTTATATCGCTACTCAAGGACCACTGCTGAGCACCATAGATGACTTTTGGCAAATGGTGTTGGAAAATAATTCAAATGTTATTGCCATGATAACCAGAGAGATAGAAGGTGGAATTATCAAATGCTACCATTACTGGCCCATTTCTCTGAAGAAGCCATTGGAATTGAAACACTTCCGTGTATTCCTGGAGAACTACCAGATACTTCAATATTTCATCATTCGAATGTTTCAAGTTGTGGAGAAGTCCACGGGAACTAGTCACTCTGTAAAACAGTTGCAGTTCACCAAGTGGCCAGACCATGGCACTCCTGCCTCAGCAGATAGCTTCATAAAATATATTCGTTATGCAAGGAAGAGCCACCTTACAGGACCCATGGTTGTTCACTGCAGTGCCGGCATAGGCCGGACAGGGGTGTTCCTATGTGTGGATGTCGTGTTCTGTGCCATCGTAAAGAACTGTTCATTCAACATCATGGATATAGTGGCCCAAATGAGAGAACAACGTTCTGGCATGGTTCAAACGAAGGAGCAGTATCACTTTTGTTACGATATTGTGCTTGAAGTTCTTCGGAAACTTCTGACTTTGGATTAA"
from Bio.Seq import Seq
seq = Seq(seq, generic_dna)
seq = str(seq.reverse_complement())
'''

"""