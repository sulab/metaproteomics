#os.chdir("/home/gstupp/metaproteomics/PHapL/tests")
from PHapL.generate_seq import mutate_seq
from PHapL.protein_haplotypes import vcf_model_to_dict
from vcf import model as vcf_model
import vcf

def fh(fname, mode='rt'):
    #"open file in this directory"
    try:
        return open(os.path.join(os.path.dirname(__file__), fname), mode)
    except NameError:
        return open(fname, mode)

def test_mutate_seq_snp1():
    SEQ = "A" * 20
    CCDS_idx = list(range(1000,1020))
    record = {'POS': 1005, 'REF': 'A', 'ALT': ['G', 'T'], 
              'var_type': 'snp', 'end': 1005}
    genotype = 1,
    df = mutate_seq(SEQ, CCDS_idx, [record], genotype, verbose = False)
    seq_out = ''.join(list(df['ALT']))
    assert(seq_out == 'AAAAAGAAAAAAAAAAAAAA')

def test_mutate_seq_snp2():
    SEQ = "A" * 20
    CCDS_idx = list(range(1000,1020))
    record = {'POS': 1005, 'REF': 'A', 'ALT': ['G', 'T'], 
              'var_type': 'snp', 'end': 1005}
    genotype = 2,
    df = mutate_seq(SEQ, CCDS_idx, [record], genotype, verbose = False)
    seq_out = ''.join(list(df['ALT']))
    assert(seq_out == 'AAAAATAAAAAAAAAAAAAA')
    
def test_mutate_seq_2snps():
    SEQ = "A" * 20
    CCDS_idx = list(range(1000,1020))
    record = {'POS': 1005, 'REF': 'A', 'ALT': ['G', 'T'], 
              'var_type': 'snp', 'end': 1005}
    record2 = {'POS': 1006, 'REF': 'A', 'ALT': ['G', 'T'], 
              'var_type': 'snp', 'end': 1006}
    record3 = {'POS': 1008, 'REF': 'A', 'ALT': ['G', 'T'], 
              'var_type': 'snp', 'end': 1008}
    genotype = (1,1,0)
    df = mutate_seq(SEQ, CCDS_idx, [record, record2, record3], genotype, verbose = False)
    seq_out = ''.join(list(df['ALT']))
    assert(seq_out == 'AAAAAGGAAAAAAAAAAAAA')    
    
def test_mutate_seq_insertion():
    SEQ = "A" * 20
    CCDS_idx = list(range(1000,1020))
    record = {'POS': 1005, 'REF': 'A', 'ALT': ['GG'], 
              'var_type': 'indel', 'end': 1005}
    genotype = 1,
    df = mutate_seq(SEQ, CCDS_idx, [record], genotype, verbose = False)
    seq_out = ''.join(list(df['ALT']))
    assert(seq_out == 'AAAAAGGAAAAAAAAAAAAAA')

def test_mutate_seq_deletion():
    SEQ = "A" * 20
    CCDS_idx = list(range(1000,1020))
    record = {'POS': 1005, 'REF': 'AA', 'ALT': ['G'], 
              'var_type': 'indel', 'end': 1006}
    genotype = 1,
    df = mutate_seq(SEQ, CCDS_idx, [record], genotype, verbose = False)
    seq_out = ''.join(list(df['ALT']))
    assert(seq_out == 'AAAAAGAAAAAAAAAAAAA')
    
def test_mutate_seq_snp_insertion_deletion():
    SEQ = "A" * 20
    CCDS_idx = list(range(1000,1020))
    record1 = {'POS': 1005, 'REF': 'A', 'ALT': ['G'], 
              'var_type': 'snp', 'end': 1005}
    record2 = {'POS': 1008, 'REF': 'AA', 'ALT': ['G'], 
              'var_type': 'indel', 'end': 1009}
    record3 = {'POS': 1015, 'REF': 'A', 'ALT': ['GG'], 
              'var_type': 'indel', 'end': 1015}
    genotype = (1,1,1)
    df = mutate_seq(SEQ, CCDS_idx, [record1, record2, record3], genotype, verbose = False)
    seq_out = ''.join(list(df['ALT']))
    assert(seq_out == 'AAAAAGAAGAAAAAGGAAAA')

def test_vcfmodel_to_dict():
    """
    1	69428	rs140739101	T	G	100	PASS	AC=4;AF=0.189696;AN=22;NS=11	GT	0|0	0|0	1|0	0|0	0|1	0|0	0|0	1|1	0|0	0|0	0|0
    """
    vcf_reader = vcf.Reader(fh("test1.vcf"))
    vcf_record = next(vcf_reader)
    record_dict = vcf_model_to_dict(vcf_record)
    correct_dict = {'ALT': ['G'],
     'CHROM': '1',
     'ID': 'rs140739101',
     'INFO': {'AC': [4], 'AF': [0.189696], 'AN': 22, 'NS': 11},
     'POS': 69428,
     'REF': 'T',
     'affected_end': 69428,
     'affected_start': 69427,
     'end': 69428,
     'genotypes': [{2: 1, 7: 1}, {4: 1, 7: 1}],
     'heterozygosity': 0.2975206611570249,
     'start': 69427,
     'var_subtype': 'tv',
     'var_type': 'snp'}
    assert (record_dict == correct_dict)




"""
POS = 1005
REF = 'A'
ALT = ['G','T']
CCDS_vcf = [vcf_model._Record('1', POS, 'id1', REF, [vcf_model._Substitution(alt) for alt in ALT],
                                  None, None, {}, None, {}, None)]
"""