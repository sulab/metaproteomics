ls *.pkl.gz | parallel -P 8 python3 /local/home/gstupp/metaproteomics/protein_haplotypes.py haplotypes {}
cat chr10_variants.fasta
chr11_variants.fasta
chr12_variants.fasta
chr13_variants.fasta
chr14_variants.fasta
chr15_variants.fasta
chr16_variants.fasta
chr17_variants.fasta
chr18_variants.fasta
chr19_variants.fasta
chr1_variants.fasta
chr20_variants.fasta
chr21_variants.fasta
chr22_variants.fasta
chr2_variants.fasta
chr3_variants.fasta
chr4_variants.fasta
chr5_variants.fasta
chr6_variants.fasta
chr7_variants.fasta
chr8_variants.fasta
chr9_variants.fasta
chrX_variants.fasta
out.fasta
variants.fasta > protein_haplotypes.fasta
