"""
Once we have all of the ccds and their associated haplotypes, 
calculate new sequences


Sonmething is still wrong with the references
Over 1k of the CCDS don't have a reference in phapl

> grep '^>' PHapL.fasta | grep -v 'g' | sort | uniq | cut -f1 -d';' | wc -l
> grep '^>' PHapL.fasta | cut -f1 -d';' | sort | uniq | wc -l

# CCDS1005.1
# CCDS9963.1


"""

from collections import Counter
from itertools import chain

import pandas as pd
import numpy as np
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from Bio import SeqIO

import utils


def pprint_record(record):
    print((record['POS'],record['REF'],record['ALT']))

def mutate_seq(seq, CCDS_idx, CCDS_vcf, genotype, verbose = True):
    """
    seq (str): reference nucleotide sequence
    CCDS_idx (list of ints): genomic position of each nucleotide, 0-based
    CCDS_vcf (list of dicts): dict contains each variant to apply to `seq`.
    genotype (list of ints): allele for each variant in CCDS_vcf    
    
    """
    df = pd.DataFrame(list(seq), index = CCDS_idx, columns=['REF'])
    df['ALT'] = df['REF']
    for (record, allele) in zip(CCDS_vcf, genotype):
        if allele == 0:
            continue
        if record['var_type'] in ['snp','indel']:
            df.loc[record['POS']:record['end'], 'ALT'] = ''
            df.loc[record['POS'], 'ALT'] = record['ALT'][allele - 1]
        if verbose:
            pprint_record(record)
            idx_start = int(np.where(df.index==record['POS'])[0])
            idx_end = int(np.where(df.index==record['end'])[0])
            print(df.iloc[idx_start-1: idx_end+2])
    return df

hgvs = lambda x,g: x['CHROM']+':g.'+str(x['POS'])+x['REF']+'>'+x['ALT'][g]

def generate_seq_from_ccd(ccd):
    if not ccd:
        return None
    CCDS_vcf = ccd.variants
    
    seq = str(record_dict_CCDS[ccd.ID + '|Hs105|chr' + ccd.chrom].seq)
    if not ccd.orientation: # if orientation is negative
        seq = str(Seq(seq, generic_dna).reverse_complement())
    
    geno1 = Counter([tuple([record['genotypes'][0].get(sample,0) for record in ccd.variants]) for sample in range(n_samples)])
    geno2 = Counter([tuple([record['genotypes'][1].get(sample,0) for record in ccd.variants]) for sample in range(n_samples)])
    genotypes = geno1 + geno2
    sorted_genotypes = sorted(genotypes.items(), key=lambda x:x[1], reverse=True)
    
    # always make the reference go first
    ref_genotype = (tuple([0] * len(CCDS_vcf)), genotypes[tuple([0] * len(CCDS_vcf))]) #count can be zero
    if ref_genotype[1]: sorted_genotypes.remove(ref_genotype)
    sorted_genotypes.insert(0, ref_genotype)
    
    seen_seqs = set()
    seqs = []
    for genotype, count in sorted_genotypes:
        freq = count / sum(genotypes.values())
        if freq < 0.01 and freq != 0: #reference may be zero exactly. include it
            continue
        newdf = mutate_seq(seq, ccd.position_idx, CCDS_vcf, genotype, verbose = False)
        newseq = ''.join(list(newdf['ALT']))
        if not ccd.orientation:
            new_aa = str(Seq(newseq, generic_dna).reverse_complement().translate(to_stop = True))
        else:
            new_aa = str(Seq(newseq, generic_dna).translate(to_stop = True))
        #merge identical sequences because not all are nonsynonymous
        if new_aa in seen_seqs:
            continue
        else:
            seen_seqs.add(new_aa)
        hgvs_id = ';'.join([hgvs(variant, g-1) for variant, g in zip(CCDS_vcf, genotype) if g != 0])
        x = {'seq': new_aa, 'hgvs_id': hgvs_id, 'ccd_id':ccd.ID, '_id': ccd.ID + ';' + hgvs_id}
        seqs.append(x)
    return seqs

#%%

import sys

if __name__ == "__main__":
    CHROM = sys.argv[1]
    path_1000 = "/mongoc/gstupp/1000genomes/phase3/"
    #path_1000 = "/home/gstupp/1000genomes/"
    
    CCDS_pkl_gz = path_1000 + "{}_CCDS.pkl.gz".format(CHROM)
    ccds = utils.load(CCDS_pkl_gz)
    
    CCDS_nucleotide_fna = path_1000 + "CCDS_nucleotide.20131129.fna"
    record_dict_CCDS = SeqIO.index(CCDS_nucleotide_fna, "fasta") # CCDS nucleotide sequences
    n_samples = 2054
    
    sequences = list(chain(*[generate_seq_from_ccd(ccd) for ccd in ccds if ccd]))
    #sequences = gutils.uniquify_by_key(sequences, '_id')
    # generate fasta
    with open(CHROM + '.fasta', 'w') as fasta_f:
        for doc in sequences:
            fasta_f.write('>' + doc['_id'] + '\n')
            fasta_f.write(doc['seq'] + '\n')
