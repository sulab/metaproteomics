for VCF in $(ls genotypes_vcf/*.genotypes.vcf.gz); do
    VCF_OUT=${VCF/genotypes_vcf/genotypes_cds_vcf}
    VCF_OUT=${VCF_OUT%.gz}.CDS.gz
    [ -f $VCF_OUT ] || /local/home/gstupp/bedtools-2.17.0/bin/intersectBed -a $VCF -b CCDS.20131129.bed -wa -u -header | bgzip > $VCF_OUT &
    echo $VCF
    echo $VCF_OUT
done
