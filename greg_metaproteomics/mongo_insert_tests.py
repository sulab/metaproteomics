# -*- coding: utf-8 -*-
"""
Created on Thu Nov  6 11:58:38 2014

@author: Greg
"""

from pymongo import MongoClient
ipy = get_ipython()

coll1 = MongoClient().test.test1
coll2 = MongoClient().test.test2
coll3 = MongoClient().test.test3
coll4 = MongoClient().test.test4
coll1.drop()
coll2.drop()
coll3.drop()
coll4.drop()

records = [{'_id':x, 'apple': x} for x in range(10000)]

def test1(records, coll):
    coll.insert(records)
    
def test2(records, coll):
    for record in records:
        coll.insert(record)
    
def test3(records, coll):
    bulk = coll.initialize_unordered_bulk_op()
    for record in records:
        bulk.insert(record)
    bulk.execute({'w':0})

def test4(records, coll):
    bulk = coll.initialize_unordered_bulk_op()
    for record in records:
        bulk.insert(record)
    bulk.execute()
    
ipy.magic("time test1(records, coll1)")
ipy.magic("time test2(records, coll2)")
ipy.magic("time test3(records, coll3)")
ipy.magic("time test4(records, coll4)")
