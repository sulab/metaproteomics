# -*- coding: utf-8 -*-
"""
Created on Wed Nov 19 09:56:29 2014

@author: Greg
DUF Counter
"""

from pymongo import MongoClient
domainDB = client('wl-cmadmin').domainDB.domainDB

# Number or proteins containing one or more DUFs % 1318771
domainDB.find({'d.ipd': {'$regex':'DUF'}}).count()

# Number or proteins containing only one DUF # 746109
domainDB.find({'d.ipd': {'$regex':'DUF'}, 'd': {'$size': 1}}).count()

# Of proteins that have at least one DUFs, Count all DUF domains (including multiple domains within a protein)
match = domainDB.aggregate([{'$match': {'d.ipd': {'$regex':'DUF'}}},
                        {'$unwind': '$d'}, 
                        {'$match': {'d.ipd': {'$regex':'DUF'}}}, 
                        {'$group': {'_id': '$d.ipa', 'count': {'$sum': 1}}}])

match = match['result']
match.sort(key = lambda x: x['count'], reverse=True)


# Go Terms
domainDB.find({'d.g': '0008233'}).count()
