"""
For building pride/proteomeXchange file relationship mapping

"""
import os
import glob

BASE = "/home/gstupp/unenriched"
os.chdir(BASE)

class File():
    n = 0
    def __init__(self, path=None,type=None,mapping=[]):
        self.path = path
        self.type = type
        self.mapping = mapping
        self.n = File.n
        File.n += 1

    def name(self):
        return os.path.basename(os.path.splitext(self.path)[0])
    
    def name_minus_num(self):
        if "_" in self.path:
            return os.path.basename(os.path.splitext(self.path)[0]).rsplit("_",1)[0]
        elif "-" in self.path:
            return os.path.basename(os.path.splitext(self.path)[0]).rsplit("-",1)[0]
        else:
            return None
    
    def __str__(self):
        if self.mapping:
            return "FME\t{n}\t{type}\t{path}\t{mapping}".format(n=self.n, type=self.type,
                                path=self.path, mapping=",".join(map(str,self.mapping)))
        else:
            return "FME\t{n}\t{type}\t{path}".format(n=self.n, type=self.type,path=self.path)
            
    def __repr__(self):
        return self.__str__()

files = []
File.n = 0

for f in sorted(glob.glob("*.ms2")):
    files.append(File(os.path.join(BASE,f),"PEAK"))
for f in sorted(glob.glob("*.sqt")):
    files.append(File(os.path.join(BASE,f),"SEARCH"))
for f in sorted(glob.glob("*.raw")):
    files.append(File(os.path.join(BASE,f),"RAW"))
for f in sorted(glob.glob("*.txt")):
    files.append(File(os.path.join(BASE,f),"OTHER"))
for file in [f for f in files if f.path.endswith(".sqt")]:
    name = file.name()
    name_minus_num = file.name_minus_num()
    file.mapping = [x.n for x in files if x.name() == name and not x.path.endswith(".sqt")]
    # get the dtaselect file
    dtas = [x.n for x in files if name_minus_num in x.name() and x.path.endswith(".txt")]
    assert(len(dtas) == 1)
    file.mapping.append(dtas[0])
    
#%%
with open("/home/gstupp/unenriched/out",'w') as out:
    for file in files:
        print(file, file=out)