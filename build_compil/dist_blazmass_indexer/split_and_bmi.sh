#!/bin/bash

python3 ~/metaproteomics/file_processing/fasta/fasta_splitter.py renumbered_072114_indexDB_hq.fasta 2
#cat *.fasta | parallel --block 2G --recstart '\n>' --regexp --pipe 'cat > renumbered.fasta.{#}'

INCOMP_FLAG=$(ls | grep *.incomp | wc -l)

NUM=$(ls *.fasta.* | wc -l)
NUM=$((NUM-1))

FIRST=$(qsub -t 0-${NUM} run_block.job)
echo $FIRST
SECOND=$(qsub -W depend=afteranyarray:$FIRST merge_gzipped_chunks.job)
echo $SECOND
