## Collection of software to build ComPIL databases (ProtDB, MassDB, SeqDB)

### by Sandip Chatterjee and Greg Stupp

Download ComPIL fasta file [here](https://hpccloud.scripps.edu/index.php/s/HpqxSKvUhZF5Atr)

* The primary script to run is the bash script **create_compil**
* this workflow has been tested on machines running CentOS 6.5 with the following additional dependencies:
 * Python 2.7+ ('python')
 * Python 3.3+ ('python3')
 * Java 1.7 (OpenJDK or Oracle)
 * GNU Parallel 2014_0622
 * MongoDB 2.6.x running locally or elsewhere (can specify mongod or mongos connection information in create_compil script)
 * Pymongo Python package (MongoDB connection library)
* the **dist** directory contains a *mostly untested*, distributed version of build_compil 

### To run **create_compil**:
 * Accepts 3 arguments: `path_to_fasta_file` | `DB_name` | `coll_name`
 * `path_to_fasta_file`: full path to fasta file. If fasta file does not contain reverse proteins (an equal number of deflines that do and don't begin with "Reverse_"). If the fasta file is not numbered, it will be numbered.
 * `DB_name`: Name of mongo database to create
 * `coll_name`: Name of mongo collection to create
 * **Other variables to change (within script)**:
  * `MONGO_HOST` 
  * `MONGO_PORT`
  * `WRITE_PROTDB_METADATA`: parse extra metadata from defline (repository, go terms, organism)
  * `FLATDB_DIRECTIMPORT`: Write a json file to directly import from flat-file
  * `BINPATH`: relative path to python and java scripts
  * In blazmass.params: Be sure to set miscleavage to `2` for fully tryptic digestion. You can also set the number of possible missed cleavages


### Notes:
['RefSeq','HMP_Reference_Genomes','HMP_metagenomics','Integrated_Gene_Catalog','UniProt_Human','UniProt_Bacteria','UniProt_Archaea']


### To DO
Look into getting EC numbers for proteins in compil
Should be doable for refseq, uniprot, and HMP_reference proteins
http://www.uniprot.org/mapping/?query=WP_015265029.1&from=P_REFSEQ_AC&to=ACC&format=tab
http://www.uniprot.org/uniprot/?query=D1PL59&columns=id,entry%20name,ec&format=tab
For all other, look into GO->EC mapping
