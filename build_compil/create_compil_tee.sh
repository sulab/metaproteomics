FASTADB=$1
DB=$2
PROTDB_NAME="ProtDB_"$DB
MASSDB_NAME="MassDB_"$DB
SEQDB_NAME="SeqDB_"$DB
COLL=$3
PROTDBCOLL_NAME="ProtDB_"$COLL
MASSDBCOLL_NAME="MassDB_"$COLL
SEQDBCOLL_NAME="SeqDB_"$COLL
FLATDB=${FASTADB%.fasta}.flatdb
MASS_FLATDB=${FLATDB%.flatdb}.mass.flatdb
SEQ_FLATDB=${FLATDB%.flatdb}.seq.flatdb
MASS_JSON=${FLATDB%.flatdb}.mass.json
SEQ_JSON=${FLATDB%.flatdb}.seq.json

BINPATH=ex
MONGO_HOST='wl-cmadmin'
MONGO_PORT=27017

set -e
mkdir -p tmp


cat $FASTADB | parallel -j+0 --block 1M --recstart '\n>' --regexp --tmpdir tmp --pipe  python3 ex/python/generate_peptides_pyteomics.py | tee \
    >(sort -S 1000m -bk 2,2 -T tmp | pigz > ${SEQ_FLATDB}.gz ) \
    >(sort -S 1000m -nbk 1,1 -T tmp | pigz > ${MASS_FLATDB}.gz) \
    >(pigz -b 4096 > $FLATDB.gz) > /dev/null

jobs

sleep 120

jobs

zcat ${MASS_FLATDB}.gz | awk '{ if ($3 == "---") print $0}' | tee >(pigz > ${MASS_FLATDB%.flatdb}_Nterm.flatdb.gz) |\
    python3 ex/python/flatdb_parse.py mass | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe ex/python/flatdb_json.py mass | tee >(pigz > ${MASS_JSON%.json}_Nterm.json.gz) | \
    mongoimport --host=$MONGO_HOST --port=$MONGO_PORT -d ${MASSDB_NAME}_Nterm -c ${MASSDBCOLL_NAME}_Nterm -j 8 2> mongo_Nterm.out & 

zcat ${MASS_FLATDB}.gz | awk '{ if ($4 == "---") print $0}' | tee >(pigz > ${MASS_FLATDB%.flatdb}_Cterm.flatdb.gz) |\
    python3 ex/python/flatdb_parse.py mass | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe ex/python/flatdb_json.py mass | tee >(pigz > ${MASS_JSON%.json}_Cterm.json.gz) | \
    mongoimport --host=$MONGO_HOST --port=$MONGO_PORT -d ${MASSDB_NAME}_Cterm -c ${MASSDBCOLL_NAME}_Cterm -j 8 2> mongo_Cterm.out &


wait
exit



