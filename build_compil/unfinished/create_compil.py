#!/usr/bin/env python3

# python-only version of build_compil
# tested on Python 3.3+
# (only dependencies will be GNU Sort and Pymongo)

# uses a params file ('params.json') in the same directory that specifies:
# - mongo host
# - mongo port
# - future database name
# - future collection name

import os
import sys
import json
import argparse
import warnings
import subprocess
import multiprocessing as mp
try:
    from pymongo import MongoClient
except ImportError:
    print('pymongo module required for MongoDB database connection')
    sys.exit(1)
from build_compil import paramsreader, fastareader, blazmassflatdb
from build_compil.fastareader import OpenFasta

def main():

    print(params)
    if not args.yes and input('Continue? y/n\n> ') != 'y': # escape hatch, if params don't look right...
        sys.exit(1)

    fasta_file_name = params.get('fasta_file')
    tmp_directory = params.get('temp_dir')
    flatdb_file_name = 'output.flatdb'
    flatdb_gen = blazmassflatdb.BlazmassFlatdb(params, presort=False)

    with OpenFasta(fasta_file_name, include_reversed=True, blazmass_defline_format=True) as ff, \
        open('{}/{}'.format(tmp_directory, flatdb_file_name), 'w') as f:
        pool = mp.Pool()
        fasta_chunks = yield_fasta_chunk(ff, chunksize=5000)
        result = pool.imap_unordered(flatdb_gen.convert_fasta_chunk, fasta_chunks)
        for item in result:
            f.write(item)
        pool.close()
        pool.join()

    subprocess.call(['sort', '-nbk', '1,1', '-T', tmp_directory, '{}/{}'.format(tmp_directory, flatdb_file_name), '-o', 'tmp/massSorted.flatdb'])

    print()
    print('!'*25+'\nFinished running main() !')

def yield_fasta_chunk(fasta_parser, chunksize=5000):

    ''' 

    take an input fasta parser (OpenFasta object) and return chunks of FASTA sequences

    generator function to allow Blazmass Indexer (Java) to operate without spawning
    a new subprocess for each FASTA record

    '''

    records_chunk = []
    for record in fasta_parser:
        minimal_blazmass_record = '>{}||{}\n{}\n'.format(record['number'], record['defline'][:10], record['sequence'])
        records_chunk.append(minimal_blazmass_record)
        if len(records_chunk) % chunksize == 0:
            yield ''.join(records_chunk)
            records_chunk = []
    if records_chunk:
        yield ''.join(records_chunk)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Builds a set of ComPIL databases (MassDB, SeqDB, ProtDB)')
    parser.add_argument('-p', '--params', action='store_true', help='Create a "default" params file')
    parser.add_argument('-y', '--yes', action='store_true', help='Answer "yes" to any user prompt')
    args = parser.parse_args()

    params_file_name = 'params.json'
    required_params = ['fasta_file', 'mongo_host', 'mongo_port', 'database_suffix', 'half_tryptic']
    params = paramsreader.ParamsReader(required_params)

    if args.params or not params.file_found:
        params.make_params_file()
        sys.exit()

    if not params.read():
        print('Error reading params. Exiting.')
        sys.exit(1)

    main()