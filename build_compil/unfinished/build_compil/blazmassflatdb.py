#!/usr/bin/env python3

import io
import os
import subprocess

class BlazmassFlatdb(object):

    def __init__(self, params, presort=False):

        self.blazmass_path = 'build_compil/java/blazmass_orig_indexer.jar'
        self.blazmass_params_file = 'blazmass.params'
        self.params = params
        self._presort = presort

        if not os.path.isfile(self.blazmass_path):
            raise OSError('Blazmass indexer jar file not found')

    def convert_fasta_chunk(self, fasta_chunk):

        if self.params.get('half_tryptic'):
            blazmass_invocation = ['java', '-jar', self.blazmass_path, '-i', '.', 'blazmass.params_ht'] # half-tryptic blazmass.params
        else:
            blazmass_invocation = ['java', '-jar', self.blazmass_path, '-i', '.', 'blazmass.params']

        try:
            flatdb_output = subprocess.check_output(blazmass_invocation, universal_newlines=True, input=fasta_chunk)
        except subprocess.CalledProcessError:
            print('Issue with Java/Blazmass!')
            raise

        if self._presort:
            flatdb_output_list = flatdb_output.split('\n')
            flatdb_output_list.sort(key=lambda x: int(x.split('\t')[0]) if x else 0) # else statement takes care of blank lines
            return '\n'.join(flatdb_output_list)
        else:
            return flatdb_output