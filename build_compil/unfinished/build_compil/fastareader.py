#!/usr/bin/env python3

class FastaReader(object):

    '''
    Class that parses an input FASTA file
    '''

    def __init__(self, fasta_filename, split_sequence=None, blazmass_defline_format=False):
        self.file_name = fasta_filename
        self._blazmass_defline_format = blazmass_defline_format
        self._split_sequence_by = split_sequence

    def parser(self, include_reversed=False):

        '''
        Generator that returns FASTA defline, 
        sequence in a 2-key dictionary.

        If include_reversed is True, returns each 'forward'
        protein immediately followed by its 'reversed' protein sequence
        (forward will be one iteration over the generator, reverse will
        be a second iteration)
        '''

        self._protein_number = 0

        with open(self.file_name) as fasta_file_handle:
            defline, sequence = '', []
            for line in fasta_file_handle:
                if line[0] == '>':
                    if defline:
                        self.increment_counter()
                        self.defline = defline
                        self.sequence = ''.join(sequence)
                        yield {'defline': self.defline, 'sequence': self.sequence} ## should probably verify that defline doesn't contain '||' already
                        if include_reversed:
                            self.increment_counter()
                            self.defline = 'Reverse_'+defline
                            self.sequence = ''.join(sequence)[::-1]
                            yield {'defline': self.defline, 'sequence': self.sequence}
                    defline, sequence = line[1:].rstrip(), []
                else:
                    sequence.append(line.rstrip())

            if defline:
                self.increment_counter()
                self.defline = defline
                self.sequence = ''.join(sequence)
                yield {'defline': self.defline, 'sequence': self.sequence}
                if include_reversed:
                    self.increment_counter()
                    self.defline = 'Reverse_'+defline
                    self.sequence = ''.join(sequence)[::-1]
                    yield {'defline': self.defline, 'sequence': self.sequence}

    def fasta_record_iterator(self, include_reversed=False):
        for record in self.parser(include_reversed=include_reversed):
            yield self.fasta_record

    def increment_counter(self):
        self._protein_number += 1

    @property
    def protein_number(self):
        return self._protein_number

    @property
    def fasta_defline(self):
        if self._blazmass_defline_format:
            return '{}||{}'.format(self._protein_number, self.defline)
        else:
            return '{}'.format(self.defline)

    @property
    def fasta_sequence(self):
        if self._split_sequence_by:
            return '\n'.join(self.split_string_by_n(self.sequence, self._split_sequence_by))
        else:
            return self.sequence

    @property
    def fasta_record(self):
        return '>{}\n{}'.format(self.fasta_defline, self.fasta_sequence)

    @property
    def blazmass_defline_format(self):
        return _blazmass_defline_format

    @blazmass_defline_format.setter
    def blazmass_defline_format(self, value):
        if isinstance(value, bool):
            self._blazmass_defline_format = value
        else:
            raise TypeError('blazmass_format must be set to True or False')

    @property
    def split_sequence(self):

        ''' return split_sequence property '''

        return _split_sequence_by

    @split_sequence.setter
    def split_sequence(self, value):
        if isinstance(value, int) or value is None:
            self._split_sequence_by = value
        else:
            raise TypeError('split_sequence must either be set to an integer or None')

    def split_string_by_n(self,long_string,n):
        while long_string:
            yield long_string[:n]
            long_string = long_string[n:]


class OpenFasta(object):

    '''
    class that can be used similarly to a file open() object 
    
    for example, using 'with open() as f' statement

    example:
    with OpenFasta(filename) as f:
        for fasta_record in f:
            pass
    '''

    def __init__(self, fasta_filename, include_reversed=False, blazmass_defline_format=False, split_sequence=80):
        self.file_name = fasta_filename
        self._fileobj = None
        self._protein_number = 0
        self._include_reversed = include_reversed
        self._blazmass_defline_format = blazmass_defline_format
        self._split_sequence_by = split_sequence

    def __enter__(self):
        self._fileobj = open(self.file_name)
        return self

    def __exit__(self, type, value, traceback):
        self._fileobj.close()

    def __iter__(self):
        defline, sequence = '', []
        for line in self._fileobj:
            if line[0] == '>':
                if defline:
                    self._increment_counter()
                    self._defline = defline
                    self._sequence = ''.join(sequence)
                    yield {'defline': self._defline, 'sequence': self._sequence, 'number': self._protein_number} ## should probably verify that defline doesn't contain '||' already
                    if self._include_reversed:
                        self._increment_counter()
                        self._defline = 'Reverse_'+defline
                        self._sequence = ''.join(sequence)[::-1]
                        yield {'defline': self._defline, 'sequence': self._sequence, 'number': self._protein_number}
                defline, sequence = line[1:].rstrip(), []
            else:
                sequence.append(line.rstrip())

        if defline:
            self._increment_counter()
            self._defline = defline
            self._sequence = ''.join(sequence)
            yield {'defline': self._defline, 'sequence': self._sequence, 'number': self._protein_number}
            if self._include_reversed:
                self._increment_counter()
                self._defline = 'Reverse_'+defline
                self._sequence = ''.join(sequence)[::-1]
                yield {'defline': self._defline, 'sequence': self._sequence, 'number': self._protein_number}

    def __str__(self):
        ''' returns CURRENT FASTA record as string '''
        return '>{}\n{}'.format(self.fasta_defline, self.fasta_sequence)

    def __repr__(self):
        return 'OpenFasta(\'{}\', include_reversed={}, blazmass_defline_format={}, split_sequence={})'.format(self.file_name, self._include_reversed, self._blazmass_defline_format, self._split_sequence_by)

    @property
    def fasta_defline(self):
        ''' property returns formatted current fasta defline '''
        if self._blazmass_defline_format:
            return '{}||{}'.format(self._protein_number, self._defline)
        else:
            return '{}'.format(self._defline)

    @property
    def fasta_sequence(self):
        ''' property returns formatted current fasta sequence '''
        if self._split_sequence_by:
            return '\n'.join(self._split_string_by_n(self._sequence, self._split_sequence_by))
        else:
            return self._sequence

    def _increment_counter(self):
        ''' increments protein number '''
        self._protein_number += 1

    def _split_string_by_n(self,long_string,n):
        ''' splits a long string by n characters, returning a list (iterable) '''
        while long_string:
            yield long_string[:n]
            long_string = long_string[n:]