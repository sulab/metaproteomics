import os
from collections import deque

class FastaFlatDB(object):

    ''' class that converts FASTA format to a flattened format
        suitable for sorting with GNU Sort

        flattened format: ('flatdb')
        intMass [tab] peptideSeq [tab] resLeft [tab] resRight [tab] proteinID [tab] peptideStartPositionInProtein
        
        for example:
        2681471 [tab] AAAAAAAAAAAAAAAAAAAAAAAKERVGFRR [tab] VFK [tab] LHH [tab] 37226444 [tab] 216
    '''

    def __init__(self, params):

        if not os.path.isdir(new_file_directory):
            os.makedirs(new_file_directory)
        self._halftryptic = params.get('half_tryptic')

        max_internal_missed_cleavage_sites = 2

        self._aaMassAvg = {}
        self._aaMassAvg['G'] =  57.05192
        self._aaMassAvg['A'] =  71.07880
        self._aaMassAvg['S'] =  87.07820
        self._aaMassAvg['P'] =  97.11668
        self._aaMassAvg['V'] =  99.13256
        self._aaMassAvg['T'] = 101.10508
        self._aaMassAvg['C'] = 103.13880
        self._aaMassAvg['L'] = 113.15944
        self._aaMassAvg['I'] = 113.15944
        self._aaMassAvg['X'] = 113.15944
        self._aaMassAvg['N'] = 114.10384
        self._aaMassAvg['O'] = 114.14720
        self._aaMassAvg['B'] = 114.59622
        self._aaMassAvg['D'] = 115.08860
        self._aaMassAvg['Q'] = 128.13072
        self._aaMassAvg['K'] = 128.17408
        self._aaMassAvg['Z'] = 128.62310
        self._aaMassAvg['E'] = 129.11548
        self._aaMassAvg['M'] = 131.19256
        self._aaMassAvg['H'] = 137.14108
        self._aaMassAvg['F'] = 147.17656
        self._aaMassAvg['R'] = 156.18748
        self._aaMassAvg['Y'] = 163.17596
        self._aaMassAvg['W'] = 186.21320

        self._aaMassMono = {}
        self._aaMassMono['G'] =  57.0214636
        self._aaMassMono['A'] =  71.0371136
        self._aaMassMono['S'] =  87.0320282
        self._aaMassMono['P'] =  97.0527636
        self._aaMassMono['V'] =  99.0684136
        self._aaMassMono['T'] = 101.0476782
        self._aaMassMono['C'] = 103.0091854
        self._aaMassMono['L'] = 113.0840636
        self._aaMassMono['I'] = 113.0840636
        self._aaMassMono['X'] = 113.0840636
        self._aaMassMono['N'] = 114.0429272
        self._aaMassMono['O'] = 114.0793126
        self._aaMassMono['B'] = 114.5349350
        self._aaMassMono['D'] = 115.0269428
        self._aaMassMono['Q'] = 128.0585772
        self._aaMassMono['K'] = 128.0949626
        self._aaMassMono['Z'] = 128.5505850
        self._aaMassMono['E'] = 129.0425928
        self._aaMassMono['M'] = 131.0404854
        self._aaMassMono['H'] = 137.0589116
        self._aaMassMono['F'] = 147.0684136
        self._aaMassMono['R'] = 156.1011106
        self._aaMassMono['Y'] = 163.0633282
        self._aaMassMono['W'] = 186.0793126

        self._aaStaticMod = {}
        self._aaStaticMod['C'] = 57.02146

        self._aaTotalMass = {}
        for aa in self._aaMassMono:
            self._aaTotalMass[aa] = self._aaMassMono[aa] + self._aaStaticMod.get(aa, 0)

        self._other_masses = {}
        self._other_masses['proton'] = 1.007276466
        self._other_masses['H'] = 1.007825
        self._other_masses['H2O'] = 15.9949145 + 2*self._other_masses['H']
        self._other_masses['H2O_PROTON'] = self._other_masses['H2O'] + self._other_masses['proton']
        self._other_masses['cterm'] = 0.0
        self._other_masses['nterm'] = 0.0

    def cutSeq(protSeq, protID=12345):

        ''' python implementation of Blazmass cutSeq method
            (variable names are in camelCase to stay consistent between Python and Java versions)
        '''

        def isEnzyme(char):

            # may need to account for proline residues...

            if char in ('K', 'R'):
                return True
            else:
                return False

        def checkCleavage(seq, start, end):

            ''' Enzyme.checkCleavage 

                final int cleavageStatus = Enzyme.checkCleavage(protSeq, start, end);    
            '''

            enzymeArr = []
            for character in seq:
                if character in ('K', 'R'): ## how to account for proline?
                    enzymeArr.append(True)
                else:
                    enzymeArr.append(False)

            status = 0

            if start <= 0:
                status += 1
            else:
                if seq[start-1] in ('K','R'):
                    status += 1
            if seq[end] in ('K','R'):
                status += 1

            return status

        max_deque_length = 43

        length = len(protSeq)

        # pepSeq = '' ## character array
        pepSeq = []
        pepSeq = deque(['']*max_deque_length,maxlen=max_deque_length)

        curSeqI = 0 # int

        maxMissedCleavages = 1 ## get from params -- this is 1 for half-tryptic, 2 for fully-tryptic

        maxIntCleavage = max_internal_missed_cleavage_sites ## get from params -- max internal missed cleavages

        for start in range(length):

            end = start

            curSeqI = 0

            precMass = other_masses['H2O_PROTON'] # PLACEHOLDER float precMass = Constants.H2O_PROTON;
            precMass += other_masses['cterm'] # PLACEHOLDER precMass += AssignMass.getcTerm();
            precMass += other_masses['nterm'] # PLACEHOLDER precMass += AssignMass.getnTerm();

            pepSize = 0

            intMisCleavageCount = -1

            while precMass <= 6000.0 and end < length: # while (precMass <= sparam.getMaxPrecursorMass() && end < length) 

                pepSize += 1
                curIon = protSeq[end]
                curSeqI += 1
                pepSeq[curSeqI-1] = curIon # pepSeq[curSeqI++] = curIon;

                precMass += aaTotalMass[curIon] # PLACEHOLDER precMass += AssignMass.getMass(curIon)

                if isEnzyme(protSeq[end]): ## if (Enzyme.isEnzyme(protSeq.charAt(end))) {  -- I think this works...
                    intMisCleavageCount += 1

                cleavageStatus = checkCleavage(protSeq, start, end)

                if intMisCleavageCount > maxIntCleavage:
                    break

                if precMass > 6000.0:   # sparam.getMaxPrecursorMass() == 6000.0
                    break

                if pepSize >= 6 and precMass >= 500.0: # Constants.MIN_PEP_LENGTH == 6 && sparam.getMinPrecursorMass() == 500.0)

                    if cleavageStatus >= maxMissedCleavages:

                        peptideSeqString = ''.join(pepSeq)[:curSeqI] # final String peptideSeqString = String.valueOf(Arrays.copyOf(pepSeq, curSeqI));  ------ not sure about this...

                        truncMass = '{0:.0f}'.format(precMass*1000)

                        if start < 3:
                            resLeft = '{:->3}'.format(protSeq[:start])
                        else:
                            resLeft = protSeq[start-3:start]

                        if end > (len(protSeq)-4):
                            resRight = '{:-<3}'.format(protSeq[end+1:])
                        else:
                            resRight = protSeq[end+1:end+4]

                        print(truncMass, peptideSeqString, resLeft, resRight, protID, start, sep='\t')

                end += 1

        return None