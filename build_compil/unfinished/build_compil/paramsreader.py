#!/usr/bin/env python3

import os
import sys
import json
import warnings
warnings.formatwarning = lambda message, category, filename, lineno, _: 'WARNING: {}\n'.format(str(message))

class ParamsReader(object):

    ''' 
    Class that handles a parameters file "params.json", including 
    validation and creation of a sample params file
    '''

    def __init__(self, required_params, params_filename='params.json'):

        if not os.path.isfile(params_filename):
            warnings.warn('No parameters file found (params.json should be in base directory)')
            self.file_found = False
            print('Run with "-p" flag to generate sample params file')
        else:
            self.file_found = True

        self.reqs = required_params
        self.file_name = params_filename

    def __str__(self):

        '''
        Pretty-printable parameters
        '''

        to_print = []

        to_print.append('*'*60)
        to_print.append('Parameters from {}\n'.format(self.file_name))

        titles = ''.join(('PARAMETER'.ljust(20,' '), 'VALUE'.ljust(20,' '), 'DESCRIPTION'.ljust(20,' ')))

        to_print.append(titles)

        for parameter in self.params_dict:
            info = self.params_info_dict.get(parameter, '-')
            to_print.append('{}{}{}'.format(parameter.ljust(20,' '), str(self.params_dict[parameter]).ljust(20,' '), str(info).ljust(20,' ')))

        to_print.append('*'*60)

        return '\n'.join(to_print)

    def read(self):

        '''
        Reads in JSON file using json module.
        
        Returns True if successful (and if it validates).
        '''

        if self.file_found:
            with open(self.file_name) as j:
                try:
                    self.params_dict = json.load(j)
                except ValueError:
                    print('Possible invalid JSON in params file')
                    return False
        else:
            print('No params file found!')
            raise OSError


        self.valid = self.validate()
        if self.valid:
            self.set_param_info()

        return self.valid

    def set_param_info(self):

        '''
        Sets information for each parameter
        '''

        self.params_info_dict = {}

        self.params_info_dict['mongo_host'] = 'MongoDB connection host'
        self.params_info_dict['mongo_port'] = 'MongoDB connection port (integer)'
        self.params_info_dict['half_tryptic'] = 'Generate half tryptic peptides (False will generate fully tryptic only)'
        self.params_info_dict['database_suffix'] = 'Suffix to use for ComPIL database names -- MassDB_{0}, ProtDB_{0}, SeqDB_{0}'.format(self.get('database_suffix'))
        self.params_info_dict['collection_suffix'] = 'Suffix to use for ComPIL DB Collection names -- MassDB_[suffix], ProtDB_[suffix], SeqDB_[suffix]'
        self.params_info_dict['fasta_file'] = 'Path to FASTA protein sequence file used for ComPIL generation'
        self.params_info_dict['temp_dir'] = 'Directory for temp files (may be large...)'

    def validate(self):

        '''
        Ensures that all required parameters are in file and
        have correct types.

        Returns True if successful.
        '''

        for req in self.reqs:
            if req not in self.params_dict:
                print('Required parameter {} not found in parameters file'.format(req))
                return False
            elif self.get(req) is None:
                print('Required parameter {} not properly defined'.format(req))
                return False

        if 'collection_suffix' not in self.params_dict:
            warnings.warn('Using same MongoDB collection name as database name (i.e., MassDB_{})'.format(self.get('database_suffix')))
            self.params_dict['collection_suffix'] = self.params_dict['database_suffix']

        tmp_directory = self.get('temp_dir')
        if not tmp_directory:
            message = 'temp directory not set. Using {}/tmp'.format(os.getcwd())
            warnings.warn(message)
            self.params_dict['temp_dir'] = 'tmp'

        mongo_port = self.get('mongo_port')
        if not mongo_port:
            print('MongoDB connection port not set')
            raise ValueError
        elif not isinstance(mongo_port, int):
            try:
                self.set('mongo_port', int(mongo_port))
            except ValueError:
                print('MongoDB connection port ("mongo_port") must be an integer')
                return False

        fasta_file_name = self.get('fasta_file')
        if not fasta_file_name.endswith('.fasta') and not fasta_file_name.endswith('.fsa'):
            print('Requires a FASTA file ending with .fasta or .fsa')
            return False

        return True

    def get(self, parameter_name):

        '''
        Returns a parameter named 'parameter_name'
        (or None if that parameter doesn't exist)
        '''

        return self.params_dict.get(parameter_name, None)

    def set(self, parameter_name, value):

        '''
        Sets 'parameter_name' to 'value'
        '''

        self.params_dict[parameter_name] = value

    def make_params_file(self, params_filename='params.json'):

        ''' Makes a new params file 

        (contains placeholder values -- must be edited before use)

        '''

        if os.path.isfile(params_filename):
            print('Overwrite existing params.json file? y/n')
            if input('> ') != 'y':
                print('OK, not overwriting params.json file')

        params_dict = {}
        for key in self.reqs:
            params_dict[key] = None

        with open('params.json','w') as f:
            json.dump(params_dict, f, indent=4)

        print('Parameters written to file "params.json"')