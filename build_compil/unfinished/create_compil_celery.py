#!/usr/bin/env python3

# python-only version of build_compil
# tested on Python 3.3+
# (only dependencies will be GNU Sort and Pymongo)

# uses a params file ('params.json') in the same directory that specifies:
# - mongo host
# - mongo port
# - future database name
# - future collection name

import os
import sys
import json
import argparse
import warnings
import subprocess
import multiprocessing as mp
from celery import Celery
try:
    from pymongo import MongoClient
except ImportError:
    print('pymongo module required for MongoDB database connection')
    sys.exit(1)
from build_compil import paramsreader, fastareader, blazmassflatdb
from build_compil.fastareader import OpenFasta

def main():

    print(params)
    if not args.yes and input('Continue? y/n\n> ') != 'y': # escape hatch, if params don't look right...
        sys.exit(1)

    fasta_file_name = params.get('fasta_file')
    tmp_directory = params.get('temp_dir')
    flatdb_file_name = 'output.flatdb'
    flatdb_gen = blazmassflatdb.BlazmassFlatdb(params, presort=False)

    with OpenFasta(fasta_file_name, include_reversed=True, blazmass_defline_format=True) as ff, \
        open('{}/{}'.format(tmp_directory, flatdb_file_name), 'w') as f:
        pool = mp.Pool()
        fasta_chunks = yield_fasta_chunk(ff, chunksize=5000)
        result = pool.imap_unordered(flatdb_gen.convert_fasta_chunk, fasta_chunks)
        for item in result:
            f.write(item)
        pool.close()
        pool.join()

    subprocess.call(['sort', '-nbk', '1,1', '-T', tmp_directory, '{}/{}'.format(tmp_directory, flatdb_file_name), '-o', 'tmp/massSorted.flatdb'])

    print()
    print('!'*25+'\nFinished running main() !')

def yield_fasta_chunk(fasta_parser, chunksize=5000):

    ''' 

    take an input fasta parser (OpenFasta object) and return chunks of FASTA sequences

    generator function to allow Blazmass Indexer (Java) to operate without spawning
    a new subprocess for each FASTA record

    '''

    records_chunk = []
    for record in fasta_parser:
        minimal_blazmass_record = '>{}||{}\n{}\n'.format(record['number'], record['defline'][:10], record['sequence'])
        records_chunk.append(minimal_blazmass_record)
        if len(records_chunk) % chunksize == 0:
            yield ''.join(records_chunk)
            records_chunk = []
    if records_chunk:
        yield ''.join(records_chunk)

if __name__ == '__main__':

    # parser = argparse.ArgumentParser(description='Builds a set of ComPIL databases (MassDB, SeqDB, ProtDB)')
    # parser.add_argument('-p', '--params', action='store_true', help='Create a "default" params file')
    # parser.add_argument('-y', '--yes', action='store_true', help='Answer "yes" to any user prompt')
    # args = parser.parse_args()

    # params_file_name = 'params.json'
    # required_params = ['fasta_file', 'mongo_host', 'mongo_port', 'database_suffix', 'half_tryptic']
    # params = paramsreader.ParamsReader(required_params)

    # if args.params or not params.file_found:
    #     params.make_params_file()
    #     sys.exit()

    # if not params.read():
    #     print('Error reading params. Exiting.')
    #     sys.exit(1)

    main()

########### 
from celery import group, chord
app = Celery('tasks', broker='redis://localhost:6379/0', backend='redis://localhost:6379')

@app.task(serializer='json')
def read_fasta():

    params_file_name = 'params.json'
    required_params = ['fasta_file', 'mongo_host', 'mongo_port', 'database_suffix', 'half_tryptic']
    params = paramsreader.ParamsReader(required_params)
    params.read()
    
    fasta_file_name = params.get('fasta_file')

    with OpenFasta(fasta_file_name, include_reversed=True, blazmass_defline_format=True) as ff:

        fasta_chunks = yield_fasta_chunk(ff, chunksize=5000)

        flatdb_gen = blazmassflatdb.BlazmassFlatdb(params)

        # job = group(make_flatdb.s(flatdb_gen, fasta_chunk, params, file_num) for file_num, fasta_chunk in enumerate(fasta_chunks))
        job = group(make_flatdb.s(flatdb_gen, fasta_chunk, params, file_num) for file_num, fasta_chunk in enumerate(fasta_chunks))
        result = job.apply_async()

    return result

# @app.task(serializer='json')
@app.task()
def make_flatdb(flatdb_gen, fasta_chunk, params, file_num):
    filename = '{}/{}_{}'.format(params.get('temp_dir'), file_num, 'output.flatdb')
    with open(filename, 'w') as f:
        f.write(flatdb_gen.convert_fasta_chunk(fasta_chunk))
    return filename

@app.task()
def sort_flatdb(filenames, params):
    sort_params_dict = {}
    sort_params_dict['sorted_filename'] = 'tmp/massSorted.flatdb'
    sort_params_dict['filenames'] = ' '.join(filenames)
    sort_params_dict['tempdir'] = 'tmp'

    # bad to use shell=True but will change later
    try:
        output = subprocess.check_output('cat {filenames} | sort -nbk 1,1 -T {tempdir} -o {sorted_filename}'.format(**sort_params_dict), shell=True)
    except subprocess.CalledProcessError:
        print('flatdb sort failed')
        raise
    else:
        try:
            subprocess.check_output('rm -rf {}'.format(sort_params_dict['filenames']), shell=True)
        except subprocess.CalledProcessError:
            print('error removing temporary flatdb files')
            raise

    return sort_params_dict['sorted_filename']

''' on wolanlab web server:

>>> from celery.execute import send_task; import celery_app; result = send_task('create_compil_celery.read_fasta', ())
>>> result.successful()
True
>>> result.get()
<GroupResult: 2e8bd390-59e6-4785-b84d-3c20cb0e9e47 [9274b7e5-6162-4b62-8511-471e965ce324]>
>>> result.get().get()
['tmp/0_output.flatdb']
>>> result2 = send_task('create_compil_celery.sort_flatdb', (result.get().get(), 'params'))
>>> result2.get()
'tmp/massSorted.flatdb'

'''