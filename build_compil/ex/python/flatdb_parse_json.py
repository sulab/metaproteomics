#!/usr/bin/env python
"""
For parsing and preparing a flatdb file and generating a JSON representation of a MERGED SORTED peptide flatfile 
(sorted by peptide sequence or mass)

If using a seq flatdb file, each line much represent one json document. Lines may be grouped using:
awk '$2==k{for(i=3;i<=NF;i++){s=s"\t"$i}; next} s{print s} {s=$0;k=$2} END{print s}' | cut -f2-
example: 
zcat seq_sorted.flatdb.gz | awk '$2==k{for(i=3;i<=NF;i++){s=s"\t"$i}; next} s{print s} {s=$0;k=$2} END{print s}' | cut -f2- | gzip > seq_sorted.flatdb.awk.gz

Usage:


## Convert flatdb to json file (print)
python3 ex/python/flatdb_parse_json.py ["mass"|"seq"] massSorted.flatdb > massSorted_JSON_file.json
or
python3 ex/python/flatdb_parse_json.py ["mass"|"seq"] <(zcat massSorted.flatdb.gz) > massSorted_JSON_file.json


## Convert seq flatdb file to json in parallel
zcat seq_sorted.flatdb.awk.gz | parallel --pipe --cat "python3 ~/build_indexdb/ex/python/flatdb_parse_json.py seq {} | gzip" > test.json.gz

## Run while splitting shards and inserting to mongodb
python3 ex/python/flatdb_parse_json.py seq <(zcat seq_sorted.flatdb_cut.awk.gz) --port 27022 --split-shards

## Just mongo_import (no sharding, no splitting chunks) Doesn't run in parallel. Just for testing
python3 ex/python/flatdb_parse_json.py seq <(zcat seq_sorted.flatdb_cut.awk.gz) --port 27022 --mongo-import

"""
import sys
import csv
import json
import argparse
import itertools
import split_shards
import concurrent.futures
from pymongo import MongoClient


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('flatdb_delimiter', help='Delimiter key by which to separate flatdb file ("mass" or "seq")', type=str)
    parser.add_argument('flatdb_file', help='path to flatdb file. If not given read from stdin', type=str, default=None, nargs='?')
    parser.add_argument('--mongo-import', help='Directly import flatdb chunks into MongoDB, no sharding', action='store_true')
    parser.add_argument('--cutoff', type=int, help='Number of obj to insert at a time', default = 5000)
    parser.add_argument('--host', help="mongo host", default = 'localhost')
    parser.add_argument('--port', help="mongo port", type = int, default = 27017)
    parser.add_argument('-d', '--mongo-db', help='Mongo db name', default = 'test')
    parser.add_argument('-c', '--mongo-coll', help='Mongo coll name', default = 'test')
    parser.add_argument('-w', '--write-concern', help='No write concern', action='store_true')
    parser.add_argument('--silent', help="Don't print anything to stdout", action='store_true')
    parser.add_argument('--split-shards', help='split shards and import', action='store_true')
    parser.add_argument('--json-import', help='Read json docs, not flatdb file', action='store_true')

    args = parser.parse_args()
    
    if not args.flatdb_file and args.mongo_import:
        raise ValueError("flatdb_file argument required, cannot read from stdin")
    if not args.flatdb_file:
        args.flatdb_file = sys.stdin
    else:
        args.flatdb_file = open(args.flatdb_file)

    mongo_info = {'host': args.host, 'port': args.port, 'db': args.mongo_db, 'coll': args.mongo_coll}
    
    if args.split_shards:
        sharder = split_shards.Sharder(args.flatdb_delimiter, args.host, args.port, args.mongo_db, args.mongo_coll)
        sharder.setup_mongo()
        
    if args.flatdb_delimiter == 'mass':
        parse = parse_mass
    if args.flatdb_delimiter == 'seq':
        parse = parse_seq
    if args.json_import:
        parse = read_json

    if not args.split_shards:
        if args.mongo_import:
            json_dicts = []
            for json_dict in parse(args):
                json_dicts.append(json_dict)
                if len(json_dicts) >= 10000:
                    mongo_import(json_dicts, mongo_info)
                    json_dicts = []
            mongo_import(json_dicts, mongo_info)
            return
        if not args.silent:
            for json_dict in parse(args):
                print(json.dumps(json_dict))
            return
    
    # Determine shard splits, spawn off insert processes
    max_workers = 8
    futures = []
    executor = concurrent.futures.ProcessPoolExecutor(max_workers=max_workers)
    json_dicts = []
    for json_dict in parse(args):
        json_dicts.append(json_dict)
        for chunk_key in sharder.add_dict(json_dict):
            print(chunk_key)
            print(len(json_dicts))
            
            sharder.do_sharding(chunk_key)
            
            # Submit the job to process json_dicts
            futures.append(executor.submit(mongo_import, json_dicts, mongo_info))
            json_dicts = []
            
            if len(futures) >= max_workers * 2:
                doneAndNotDoneFutures = concurrent.futures.wait(futures, return_when = 'FIRST_COMPLETED')
#                for future in doneAndNotDoneFutures.not_done:
#                    print(future.exception())
                futures = [f for f in futures if f in doneAndNotDoneFutures.not_done]
    
    # Add the last chunk
    sharder.do_sharding(json_dicts[0]['_id'])
    futures.append(executor.submit(mongo_import, json_dicts, mongo_info))
            
    # Cleanup
    if args.mongo_import:
        args.mongo_importer.cleanup()
    
    
def read_json(args):
    for line in args.flatdb_file:
        yield json.loads(line)

def parse_seq(args):
    '''
    time zcat seq_sorted.flatdb_cut.gz | awk '$2==k{for(i=3;i<=NF;i++){s=s"\t"$i}; next} s{print s} {s=$0;k=$2} END{print s}' | cut -f2- | python3 ex/python/flatdb_parse_json.py seq > /dev/null
    
    #     From sort -m
    eval "$cmd" | awk '$2==k{for(i=3;i<=NF;i++){s=s"\t"$i}; next} s{print s} {s=$0;k=$2} END{print s}' | cut -f2- | gzip > seq_sorted.flatdb.awk.gz
    # Then,
    # zcat seq_sorted.flatdb.gz | parallel --pipe "python3 ~/build_indexdb/ex/python/flatdb_parse_json.py seq"
    '''
    f = csv.reader(args.flatdb_file, delimiter = '\t')
    for line in f:
        yield process_protein(line[0], iter(line[1:]))

def process_protein(peptide, c):
    return({'_id': peptide,     'p':        [
                    {'l': protein[0], 'r': protein[1], 'o': int(protein[3]), 'i': int(protein[2][1:]), 'd': True } if 'r' in protein[2] else 
                    {'l': protein[0], 'r': protein[1], 'o': int(protein[3]), 'i': int(protein[2])}
                for protein in zip(c,c,c,c) ] })
    
def parse_mass(args):
    '''
    Each line must be one json document (one mass, lots of peptide sequences)
        
    time zcat mass_sorted.flatdb.gz | awk '$1==k{for(i=2;i<=NF;i++){s=s"\t"$i}; next} s{print s} {s=$0;k=$1} END{print s}' | awk -v OFS='\t' '{ delete u;x=$1; for(n=2;n<=NF;n++){u[$(n)]}; for(f in u){x=x OFS f} print x }' | gzip > mass_sorted.flatdb.awk.uniq.gz
    http://objectmix.com/awk/27161-unique-fields-2.html    
    '''
    f = csv.reader(args.flatdb_file, delimiter = '\t')
    for line in f:
        yield {'_id': int(line[0]), 's': list(set(iter(line[1:])))}

def mongo_import(json_dicts, mongo_info):
    coll = MongoClient(host = mongo_info['host'], port = mongo_info['port'], w=0)[mongo_info['db']][mongo_info['coll']]
    coll.insert(json_dicts)
    print('Inserted ' + str(len(json_dicts)) + ' documents')
    
    ''' initialize_unordered_bulk_op() Same speed as inserting list '''
    
if __name__ == '__main__':
    #pass    
    main()