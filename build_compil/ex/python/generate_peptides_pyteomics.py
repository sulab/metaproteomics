"""
generate_peptides_pyteomics.py

Generate blazmass style flatdb using pyteomics

2777627	CCDCCCCCCCCCCCCCDK	VVK	EEE	1	26

mass peptide left3 right3 protID start_pos

'formic acid cleavage before or after D: "(.(?=D))|(D)"


"""

#from pyteomics import parser
from pyteomics.parser import expasy_rules
from pyteomics import mass
import copy
import sys
aa_mass = copy.copy(mass.std_aa_mass)
aa_mass['C'] += 57.02146
#%%
from collections import deque
import itertools as it
import re
def cleave(sequence, rule, missed_cleavages=0, min_length=6, max_length=42, **kwargs):
    """Stolen from biopython
    Modified to return left and right sequences and pos (ala blazmass)
    """
    peptides = []
    seq_dict = {pos:res for pos,res in enumerate(sequence)}
    f = len(sequence)
    seq_dict.update({-3:'-',-2:'-',-1:'-',f+0:'-',f+1:'-',f+2:'-'})
    cleavage_sites = deque([0], maxlen=missed_cleavages+2)
    for i in it.chain(map(lambda x: x.end(), re.finditer(rule, sequence)),[None]):
        cleavage_sites.append(i)
        for j in range(len(cleavage_sites)-1):
            seq = sequence[cleavage_sites[j]:cleavage_sites[-1]]
            st_pos = cleavage_sites[j]
            end_pos = cleavage_sites[-1] if cleavage_sites[-1] else f
            if seq:
                if len(seq) >= min_length and len(seq) <= max_length:
                    l = ''.join([seq_dict[x] for x in range(st_pos-3,st_pos)])
                    r = ''.join([seq_dict[x] for x in range(end_pos,end_pos+3)])
                    out = (seq, l, r, st_pos)
                    peptides.append(out)
    return uniquify_by(peptides, 0)

def uniquify_by(d, index):
    # uniquify a list of tuples by index # `index`
    seen = set()
    return [ x for x in d if not (x[index] in seen or seen.add(x[index])) ]

#cleave('MAAAAAAAAAAKBBBBBBBBBBBBBBKCCDCCCCCCCCCCCCCDKEEEEEEEEEEEEEEEV', rule = expasy_rules['trypsin'], missed_cleavages=3, min_length = 6)
#%%
def parse_fasta(fasta_file_handle):
    defline, sequence = '', []
    for line in fasta_file_handle:
        if line[0] == '>':
            if defline:
                yield {'defline': defline, 'seq': ''.join(sequence)}
            defline, sequence = line[1:].rstrip(), []
        else:
            sequence.append(line.rstrip())
    if defline:
        yield {'defline': defline, 'seq': ''.join(sequence)}

fasta_file_handle = sys.stdin
for record in parse_fasta(fasta_file_handle):
    peptides = cleave(record['seq'], rule = expasy_rules['trypsin'], missed_cleavages=3, min_length = 6)
    for peptide in peptides:
        print('\t'.join([str(round(mass.fast_mass(peptide[0], charge=1, aa_mass = aa_mass)*1000)), 
                                                  peptide[0], peptide[1], peptide[2], record['defline'].split('||',1)[0], str(peptide[3])]))
        
        
        
        
        