#!/usr/bin/env python

## multiprocess_JSON_import.py
##
## script doing a multiprocess bulk import from a multi-line JSON file
##
## usage: 
## python multiprocess_JSON_import.py JSON_file.json database_name chunk_size pool_size
## python multiprocess_JSON_import.py JSON_file.json rnaseqDB 100000 16
## 5/13/14


# sh.shardCollection("MassDB_indexDB_2mill")
# sh.shardCollection("MassDB_indexDB_2mill.MassDB_indexDB_2mill", {'_id':1})

#%%
from pymongo import MongoClient
from multiprocessing import Pool
from time import time
import json
import sys
import argparse

mongos_uri = "mongodb://node0097:27018,node0129:27018,node0401:27018,node0441:27018," + \
            "node0481:27018,node0521:27018,node0561:27018,node0601:27018," + \
            "node0617:27018,node0649:27018,node0953:27018,node0985:27018," + \
            "node1001:27018,nodea1331:27018,nodea1431:27018," + \
            "node0113:27018,node0145:27018,node0411:27018,node0421:27018,node0431:27018," + \
            "node0451:27018,node0461:27018,node0471:27018,node0491:27018,node0501:27018," + \
            "node0511:27018,node0531:27018,node0541:27018,node0551:27018,node0571:27018," + \
            "node0581:27018,node0591:27018,node0633:27018,node0665:27018,node0681:27018," + \
            "node0921:27018,node0937:27018,node0969:27018,nodea1301:27018,nodea1401:27018"

db_coll = None

def init_sharding(db_name, coll_name):
    db = MongoClient("wl-cmadmin", 27018).admin
    db.command('enablesharding', db_name)
    db.command('shardcollection', db_name + '.' + coll_name, key={'_id': 1})

    
def main(json_file, db_name, chunk_size, pool_size):
    start_time = time()
    
    global db_coll
    coll_name = db_name
    client = MongoClient(mongos_uri)
    db_coll = client[db_name][coll_name]
    
    with open(json_file,'r',encoding='ascii') as f:
        pool = Pool(pool_size)
        pool.imap_unordered(execute_bulk_operation,generate_bulk_object(f, chunk_size))
        pool.close()
        pool.join()

    total_query_time = "%.1f" % (time()-start_time)
    # print "Total import time:", total_query_time, "seconds"
    print("chunk size\t"+str(chunk_size)+"\tpool size\t"+str(pool_size)+"\ttime\t"+str(total_query_time))

# map execute_bulk_operation function on to list of JSON file chunks (obtained from generate_file_chunk) -- use imap
def generate_bulk_object(file_obj, chunk_size):
    
    bulk_list = []

    for num, line in enumerate(file_obj):
        JSON_obj = json.loads(line.rstrip(',\n'))
        bulk_list.append(JSON_obj)

        if (num + 1) % chunk_size == 0:
            # yield bulk
            print("Parsed", num, "JSON objects")
            yield bulk_list
            bulk_list = []
    yield bulk_list

def execute_bulk_operation(bulk_list):
    bulk = db_coll.initialize_unordered_bulk_op()
    for obj in bulk_list:
        # bulk.insert(eval(obj)) ##    ~3-fold slower than json.loads()
        bulk.insert(obj) ## line must have DOUBLE QUOTES, NOT SINGLE QUOTES for json.loads() to work
    result = bulk.execute()
    print(result)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('json_file', help='Path to json file', type=str)
    parser.add_argument('db_name', help='db and coll name (ex: SeqDB_010214)', type=str)
    parser.add_argument('-c','--chunk-size', help='number of docs per process', type=int, default=100000)
    parser.add_argument('-p','--pool-size', help='number of processes', type=int, default=16)
    parser.add_argument('-s','--enable-sharding', help='enable sharding first', action='store_true')
    args = parser.parse_args()

    if args.enable_sharding:
        init_sharding(args.db_name, args.db_name)
        print("Initialized sharding")

    main(args.json_file, args.db_name, args.chunk_size, args.pool_size)
