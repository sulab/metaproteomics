#!/usr/bin/env python

#    flatdb_parse.py
#    Sandip Chatterjee
#    Greg Stupp
#    v3, Dec 19, 2014
#    
#    For parsing and preparing a flatdb file to use with GNU parallel and fasta_peptides_json.py
#    flatdb_parse can also read from stdin.
# 
#    Usage:
#    $ ./flatdb_parse.py sorted_flatdb_file.flatdb ["mass"|"seq"]
#
#    or
#
#    $ cat sorted_flatdb_file.flatdb | python flatdb_parse.py ["mass"|"seq"]
#
#    (prints entire flatdb file to STDOUT)

import sys

def main():
    if len(sys.argv)==3:
        DB_file = sys.argv[1]
        flatdb_delimiter = sys.argv[2]
        f = open(DB_file,'rb')
    elif len(sys.argv)==2:
        # Read from stdin
        f = sys.stdin
        flatdb_delimiter = sys.argv[1]
    else:
        print("Requires a peptide flatfile")
        print("Correct usage: $ python flatdb_parse.py sorted_flatdb_file.flatdb [mass|seq]")
        print('Or with one input arg, read from stdin')
        sys.exit()

    if flatdb_delimiter == 'mass':
        read_flatfile(f, 0)
    elif flatdb_delimiter == 'seq':
        read_flatfile(f, 1)

def read_flatfile(f, delim):

    current_chunk = []

    line = f.readline()
    while True:
        current_chunk = []
        if not line:
            break
        else:
            current_peptide = line.split()[delim]
        while True:
            current_chunk.append(line)
            line = f.readline()
            if not line or line.split()[delim] != current_peptide:
                break

        print_flatdb_chunk(current_chunk)
    
def print_flatdb_chunk(current_chunk):
    
    sys.stdout.write('>')
    for line in current_chunk:
        sys.stdout.write(line)

if __name__ == '__main__':
    main()