#!/usr/bin/env python

## multiprocess_JSON_import.py
##
## script doing a multiprocess bulk import from a multi-line JSON file
##
## usage: 
## python multiprocess_JSON_import.py JSON_file.json database_name chunk_size pool_size
## python multiprocess_JSON_import.py JSON_file.json rnaseqDB 100000 16
## 5/13/14


# sh.shardCollection("MassDB_indexDB_2mill")
# sh.shardCollection("MassDB_indexDB_2mill.MassDB_indexDB_2mill", {'_id':1})

#%%
from pymongo import MongoClient
from multiprocessing import Pool
from time import time
import json
import sys

HOST = 'wl-cmadmin'
PORT = 27018

def init_sharding(HOST, PORT, db_name, coll_name):
    db = MongoClient(HOST, PORT).admin
    db.command('enablesharding', db_name)
    db.command('shardcollection', db_name + '.' + coll_name, key={'_id': 1})

    
JSON_file = sys.argv[1]
db_name = sys.argv[2]
coll_name = db_name
client = MongoClient(HOST,PORT)
db_coll = client[db_name][coll_name]
chunk_size = int(sys.argv[3])
pool_size = int(sys.argv[4])

def main():
    start_time = time()

    #init_sharding(HOST, PORT, db_name, coll_name)
    #print("Initialized sharding")
    print(db_coll)
    
    with open(JSON_file,'rb') as f:
        pool = Pool(pool_size)
        pool.imap_unordered(execute_bulk_operation,generate_bulk_object(f, chunk_size))
        pool.close()
        pool.join()

    total_query_time = "%.1f" % (time()-start_time)
    # print "Total import time:", total_query_time, "seconds"
    print("chunk size\t"+str(chunk_size)+"\tpool size\t"+str(pool_size)+"\ttime\t"+str(total_query_time))

# map execute_bulk_operation function on to list of JSON file chunks (obtained from generate_file_chunk) -- use imap
def generate_bulk_object(file_obj, chunk_size):
    
    bulk_list = []

    for num, line in enumerate(file_obj):
        JSON_obj = json.loads(line.rstrip(',\n'))
        bulk_list.append(JSON_obj)

        if (num + 1) % chunk_size == 0:
            # yield bulk
            print("Parsed", num, "JSON objects")
            yield bulk_list
            bulk_list = []
    yield bulk_list

def execute_bulk_operation(bulk_list):
    bulk = db_coll.initialize_unordered_bulk_op()
    for obj in bulk_list:
        # bulk.insert(eval(obj)) ##    ~3-fold slower than json.loads()
        bulk.insert(obj) ## line must have DOUBLE QUOTES, NOT SINGLE QUOTES for json.loads() to work
    result = bulk.execute()
    print(result)

if __name__ == '__main__':
    main()
