# -*- coding: utf-8 -*-
"""
Created on Fri Oct 10 09:18:53 2014

@author: Greg
"""
import argparse
import sys
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('flatdb_delimiter', help='Delimiter key by which to separate flatdb file ("mass" or "seq")', type=str)
parser.add_argument('size', type = float, help="Size in GB")
parser.add_argument('-s','--submit', action='store_true')
args = parser.parse_args()

if args.submit:
    import drmaa

if args.flatdb_delimiter == 'mass':
    filename = 'mass_sorted.'
elif args.flatdb_delimiter == 'seq':
    filename = 'seq_sorted.'

chunk_num = 0
chunk_handle = open(filename + str(chunk_num),'w')
f = sys.stdin
f = open('/home/gstupp/build_indexdb/seq_sorted.flatdb')
f = map(lambda x: x.split(), f)
if args.flatdb_delimiter == 'mass':
    chunk_iter = itertools.groupby(f, lambda x: x[0])
elif args.flatdb_delimiter == 'seq':
    chunk_iter = itertools.groupby(f, lambda x: x[1])
for (key,subiter) in chunk_iter:
    chunk_handle.write('\n'.join(['\t'.join(x) for x in subiter]) + '\n')
    if chunk_handle.tell()>1000000000*args.size:
        chunk_handle.close()
        if args.submit:
            spawn_job(chunk_handle.name)
        chunk_num+=1
        chunk_handle = open(filename + str(chunk_num),'w')

chunk_handle.close()

def spawn_job(file_name):
    with drmaa.Session() as s:
           print('Spawning job for ' + file_name)
           jt = s.createJobTemplate()
           jt.remoteCommand = os.path.join(os.getcwd(), 'run_json.job')
           jt.args = ['mass', file_name]
           jt.joinFiles=True
    
           jobid = s.runJob(jt)
           print('Your job has been submitted with ID %s' % jobid)
    
           print('Cleaning up')
           s.deleteJobTemplate(jt)