"""
Download an organism's gff file from entrez by its taxonomy id
Help from https://www.biostars.org/p/17715/
"""

from Bio import Entrez
import sys

def get_gff(ncbiTaxId, explode = False):
    entrezDbName = 'protein'
    Entrez.email = 'gstupp@scripps.edu'

    if explode:
        # explode tax
        entrezQuery = 'txid%s[Organism:exp]'%(ncbiTaxId)
    else:
        # just single tax
        entrezQuery = "txid%s[Organism]"%(ncbiTaxId)
        
    # Find entries matching the query
    searchResultHandle = Entrez.esearch(db=entrezDbName, term=entrezQuery, retMax = 1000000)
    searchResult = Entrez.read(searchResultHandle)
    searchResultHandle.close()
    
    # Get the data.
    uidList = ','.join(searchResult['IdList'])
    entryData = Entrez.efetch(db=entrezDbName, id=uidList, rettype='gp').read()
    return entryData


if __name__ == "__main__":
    ncbiTaxId = sys.argv[1]
    explode = False if len(sys.argv)==2 else sys.argv[2] == '1'
    #ncbiTaxId = '4496'
    
    entryData = get_gff(ncbiTaxId, explode = False)
    
    print(entryData)
    
'''   
#http://wilke.openwetware.org/Parsing_Genbank_files_with_Biopython.html
from Bio import SeqIO
gpff = '/home/gstupp/test/bacteria.202.protein.gpff'
parser = SeqIO.parse(gpff,'genbank')
feat = next(parser)
s = feat.features[0]
s.qualifiers['db_xref']
str(feat.seq)
'''