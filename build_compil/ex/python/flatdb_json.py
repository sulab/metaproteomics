#!/usr/bin/env python3

#	flatdb_json.py
#	Sandip Chatterjee
#	v3, December 17, 2014
#	
#	For generating a JSON (peptide) representation of a SORTED peptide flatfile (sorted by peptide sequence or mass)
#	READS FROM STDIN
#	OUTPUTS TO STDOUT
#	
#	Usage:
#	$ ./flatdb_parse.py seqSorted_flatdb_file.flatdb "seq" | ./flatdb_json.py "seq" > seqSorted_JSON_file.json
#	
#	or
#
#	$ ./flatdb_parse.py massSorted_flatdb_file.flatdb "mass" | ./flatdb_json.py "mass" > massSorted_JSON_file.json

import os
import sys
import json
from pymongo import MongoClient
import argparse

def main():

	try:
		DB_file = sys.stdin
	except:
		print("Requires a DB file on sys.stdin and 'seq' or 'mass' argument")
		sys.exit(1)

	parser = argparse.ArgumentParser()
	parser.add_argument('flatdb_delimiter', help='Delimiter key by which to separate flatdb file ("mass" or "seq")', type=str)
	parser.add_argument('--directimport', help='Directly import flatdb chunks into MongoDB', action='store_true')

	args = parser.parse_args()

	flatdb_delimiter = args.flatdb_delimiter

	if args.directimport:
		mongo_host = os.environ.get('MONGO_HOST', None)
		mongo_port = os.environ.get('MONGO_PORT', None)
		if not mongo_host:
			print('Environment variable $MONGO_HOST not set. Exiting.')
			sys.exit(1)
		if not mongo_port:
			print('Environment variable $MONGO_PORT not set. Exiting.')
			sys.exit(1)

		client = MongoClient(mongo_host, int(mongo_port), w=0)	## writeconcern off... may or may not be a good idea.

		if flatdb_delimiter == 'mass':
			massdb_name = os.environ.get('MASSDB_NAME', None)
			massdbcoll_name = os.environ.get('MASSDBCOLL_NAME', None)
			if massdb_name and massdbcoll_name:
				db = client[massdb_name]
				coll = db[massdbcoll_name]
			else:
				print('Environment variable $MASSDB_NAME / $MASSDBCOLL_NAME not set. Exiting.')
				sys.exit(1)
			read_massflatfile(DB_file, args, coll)

		elif flatdb_delimiter == 'seq':
			seqdb_name = os.environ.get('SEQDB_NAME', None)
			seqdbcoll_name = os.environ.get('SEQDBCOLL_NAME', None)
			if seqdb_name and seqdbcoll_name:
				db = client[seqdb_name]
				coll = db[seqdbcoll_name]
			else:
				print('Environment variable $SEQDB_NAME / $SEQDBCOLL_NAME not set. Exiting.')
				sys.exit(1)
			read_seqflatfile(DB_file, args, coll)
	else:
		if flatdb_delimiter == 'mass':
			read_massflatfile(DB_file, args)
		elif flatdb_delimiter == 'seq':
			read_seqflatfile(DB_file, args)


def read_massflatfile(DB_file, args, mongo_coll=None):

	current_chunk = []

	line = DB_file.readline()

	while True:
		defline = ''
		current_chunk = []
		protein_dicts = [] ##	list of dicts (one per peptide), to be unpacked before appending to current_chunk
		if not line:
			break
		if line[0] == '>':
			current_chunk.append(line[1:])		##	remove leading '>'
			while True:
				line = DB_file.readline()
				if not line or line[0] == '>':
					break
				current_chunk.append(line)
		json_obj = create_massjson_chunk(current_chunk)
		if mongo_coll:
			mongo_coll.insert(json_obj)	## shouldn't be inserting one document at a time... will create chunks to insert in bulk
		print(json.dumps(json_obj))

def read_seqflatfile(DB_file, args, mongo_coll=None):

	current_chunk = []

	line = DB_file.readline()

	while True:
		defline = ''
		current_chunk = []
		protein_dicts = [] ##	list of dicts (one per peptide), to be unpacked before appending to current_chunk
		if not line:
			break
		if line[0] == '>':
			current_chunk.append(line[1:])		##	remove leading '>'
			while True:
				line = DB_file.readline()
				if not line or line[0] == '>':
					break
				current_chunk.append(line)
		json_obj = create_seqjson_chunk(current_chunk)
		if mongo_coll:
			mongo_coll.insert(json_obj)	## shouldn't be inserting one document at a time... will create chunks to insert in bulk
		print(json.dumps(json_obj))

def create_massjson_chunk(current_chunk):

	current_chunk = [line.rstrip('\n') for line in current_chunk]

	json_dict = {}
	chunk_peptides = []
	json_dict['_id'] = int(current_chunk[0].split()[0])			## mass value for all peptides in this document

	for line in current_chunk:
		chunk_peptides.append(line.split()[1])

	json_dict['s'] = list(set(chunk_peptides))					## list of peptide sequences for given mass value

	return json_dict	##	return dict

def create_seqjson_chunk(current_chunk):

	current_chunk = [line.rstrip('\n') for line in current_chunk]

	json_dict = {}
	parent_proteins = []
	json_dict['_id'] = current_chunk[0].split()[1]				## unique peptide sequence (in SeqDB)

	for line in current_chunk:
		line_split = line.split()
		if len(line_split) < 6:
			print(line, file=sys.stderr)
		parent_dict = {}
		parent_dict['l'] = line_split[2]						## resLeft (3 residues to left of peptide in protein sequence)
		parent_dict['r'] = line_split[3]						## resRight (3 residues to right of peptide in protein sequence)
		parent_dict['o'] = int(line_split[5])					## offset (peptide start position in protein sequence)
		if 'r' in line_split[4]:
			parent_dict['d'] = True 							## decoy (True or False -- is this protein a 'decoy' protein? ('Reverse'))
			parent_dict['i'] = int(line_split[4].lstrip('r')) 	## strip 'r' from ProtDB ID for decoy parent protein
		else:
			parent_dict['i'] = int(line_split[4])				## ProtDB ID for parent protein
		parent_proteins.append(parent_dict)

	json_dict['p'] = parent_proteins							## list of parent proteins for peptide sequence

	return json_dict	##	return dict

if __name__ == '__main__':
	main()
