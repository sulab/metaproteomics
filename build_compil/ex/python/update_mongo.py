# -*- coding: utf-8 -*-
"""
massdb
current:
{"_id": 500258, "s": ["GPGGGR", "PGGGGR", "GGPGGR", "GGGGPR", "GGGPGR"]}
new:
{"_id": 500258, "s": ["GPGGGR", "PGGGGR", "ABCD"]}

seqdb
{"p": [{"d": true, "r": "VNR", "l": "-GR", "i": 3956565, "o": 2}, 
       {"d": true, "r": "VNR", "l": "-GR", "i": 3956582, "o": 2}], 
 "_id": "AAAAAAAAAAAAAAAAAAARDSPAAAPVAAR"}
 
new:
{"p": [{"d": true, "r": "AAA", "l": "BBB", "i": 123, "o": 2}, 
       {"d": true, "r": "AAA", "l": "BBB", "i": 125, "o": 2}], 
 "_id": "AAAAAAAAAAAAAAAAAAARDSPAAAPVAAR"}

"""

from pymongo import MongoClient
#%% mass test
masstest = MongoClient().masstest.masstest
doc = {"_id": 500258, "s": ["GPGGGR", "PGGGGR", "ABCD"]}
doc = {"_id": 500259, "s": ["GPGGGR", "PGGGGR", "ABCD"]}
masstest.update({"_id":doc['_id']}, { "$addToSet": {"s": {"$each": doc["s"]} }}, upsert = True)

#%% seq test
seqtest = MongoClient().seqtest.seqtest
doc = {"p": [{"d": True, "r": "AAA", "l": "BBB", "i": 123, "o": 2}, {"d": True, "r": "AAA", "l": "BBB", "i": 125, "o": 2}], "_id": "A"}
doc = {"p": [{"d": True, "r": "AAA", "l": "BBB", "i": 123, "o": 2}, {"d": True, "r": "AAA", "l": "BBB", "i": 125, "o": 2}], "_id": "AAAAAAAAAAAAAAAAAAARDSPAAAPVAAR"}
seqtest.update({"_id":doc["_id"]}, {"$push": {"p": {"$each": doc["p"]}}}, upsert = True)
