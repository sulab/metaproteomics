# -*- coding: utf-8 -*-
"""
Created on Sun Dec 28 14:22:48 2014

@author: gstupp


'''
Example code for sharding administration:
https://github.com/ajdavis/cluster-profile/blob/master/cluster_setup.py
'''

"""

#import bson
import os
import sys
import math
import paramiko
import threading
import pymongo.errors
from random import shuffle
from itertools import cycle
from pymongo import MongoClient


def approx_seq_size(x):
    return (len(x['p'])-1)*44 + 45 + len(x['_id']) + 22 + sum(['d' in d.keys() for d in x['p']])*4 + list_size_adjuster(len(x['p']))

def approx_mass_size(x):
    return sum(len(d) for d in x['s']) + 8*(len(x['s'])-1) + 30 + list_size_adjuster(len(x['s']))

def list_size_adjuster(num):
    #accepts an int which is the size of the list
    total = 0
    for i in range(1,int(math.log10(num)) + 1):
        total += (num-int(math.pow(10,i))) if num>int(math.pow(10,i)) else 0
    return total

class Sharder():
    
    def __init__(self, flatdb_delimiter, host, port, mongo_db, mongo_coll, chunk_size = 40, min_space = 8, user = 'admin'):
        #chunk_size is size in MB of each split (for mongodb sharding)
        #min_space is in GB. Minimum space left on a shard before it is not used
    
        # Initialize variables
        self.s = 0
        self.counter = 0
        self.previous_key = None
        self.user = user ## user name for shardsvr
        self.splits = []
        self.previous_key = None
        self.chunk_size = chunk_size
        self.min_space = min_space * 1000000

        if flatdb_delimiter == 'seq':
            self.approx_size = approx_seq_size
        elif flatdb_delimiter == 'mass':
            self.approx_size = approx_mass_size
        
        # set up mongo stuff
        self.client = MongoClient(host, port)
        self.db = mongo_db
        self.coll = mongo_coll
        self.db_coll = '.'.join([self.db,self.coll])
        
        # Get list of hosts and shards
        # This will fail if not connected to a mongos
        shard_dict = self.client.admin.command('listShards',1)
        self.hosts = [x['host'].split(':')[0] for x in shard_dict['shards']]
        self.shards = [x['_id'] for x in shard_dict['shards']]
        
        # Set up ssh connections to shard hosts
        self.setup_ssh()
        
        # Get dbpath for each shard
        self.dbpaths = self.run_ssh_command(self.run_db_path)

        # Get space (df) for each shard, determine if any are too full. 
        # Setup iterator that cycles through available shards
        self.usage = self.run_ssh_command(self.run_get_shard_space)
        self.shards = [x for x in self.usage.keys() if self.usage[x]['avail'] > self.min_space]
        self.shard_iter = cycle(self.shards)
        
        # Recheck free space after every round of splits
        self.check_space_every = len(self.shards)
    
    def run_ssh_command(self, run_fcn):
        # Run run_fcn on each client in clients in parallel
        # Store output in dictionary 'd' with keys from clients.keys()
        threads = []
        d = {}
        for host in self.clients.keys():
            t = threading.Thread(target=run_fcn, args=(self.clients[host], d, host))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()
        return d
    
    def run_db_path(self, client, d, idx):
        _, stdout, _ = client.exec_command('grep dbpath /etc/mongod*.conf')
        d[idx] = list(stdout)[0].split('=')[1].rstrip()
    
    def run_get_shard_space(self, client, d, idx):
        _, stdout, _ = client.exec_command('df ' + self.dbpaths[idx])
        df = list(stdout)[1].rstrip().split()
        d[idx] = {'used': int(df[2]), 'size': int(df[1]), 'avail': int(df[3])}
        
    def setup_ssh(self):
        # Set up ssh connections to shard hosts
        self.clients = dict()
        for host in self.hosts:
            self.clients[host] = paramiko.SSHClient()
            self.clients[host].load_system_host_keys()
            self.clients[host].connect(host, username='admin')

    def closeall_ssh(self):
        for host in self.clients.keys():
            self.clients[host].close()
        
    def check_if_over_df(self):
        print('checking space')
        # Refresh usage stats
        self.usage = self.run_ssh_command(self.run_get_shard_space)
        if any((self.usage[host]['avail'] <= self.min_space for host in self.hosts)):
            self.shards = [x for x in self.usage.keys() if self.usage[x]['avail'] > self.min_space]
            self.shard_iter = cycle(self.shards)
            self.check_space_every = len(self.shards)
            print('updated shards space')
    
    def setup_mongo(self):
        '''        
        Only run this once. Enables sharding on collection, 
        disables balancer for the collection and changes global chunk size.
        Global chunk size is changed to prevent mongo from splitting chunks by itself
        '''
        
        # Enable sharding on the collection
        admin = self.client.admin # admin database.
        try:
            admin.command('enableSharding', self.db) # Shard the 'test' db.
            # fails if already enabled...
        except pymongo.errors.OperationFailure as e:
            sys.stderr.write(str(e))
        try:
            admin.command('shardCollection', self.db_coll, key={'_id': 1})
            # fails if already sharded...
        except pymongo.errors.OperationFailure as e:
            sys.stderr.write(str(e))
            
        # Set primary
        # the first one. first split will move to next shard
        # This causes the first chunk to always fail. Fix later?
        '''
        try:
            admin.command('movePrimary', self.db, to=next(self.shards))
        except pymongo.errors.OperationFailure as e:
            if str(e).count('it is already the primary'):
                pass
            else:
                raise e
        '''
        
        # Disable balancer for this specific collection
        self.client.config.collections.update({'_id': self.db_coll},{'$set': {'noBalance': True}}, upsert=True)
        
        #Change chunk size
        self.default_chunk_size = self.client.config.settings.find_one({'_id': 'chunksize'})['value']
        self.client.config.settings.update({'_id':'chunksize'}, {'$set': {'value': self.chunk_size * 8}}, upsert=True)
        
    def add_dict(self, d):
        self.s += self.approx_size(d)
        if self.s > (self.chunk_size * 1000000):
            self.counter += 1
            self.splits.append(self.previous_key)
            if self.counter%self.check_space_every == 0:
                self.check_if_over_df()
            yield self.previous_key
        self.previous_key = d['_id']
        
    def cleanup_mongo(self):
        # Close ssh connections to shards
        self.closeall_ssh()
        # Enable balancer for this specific collection
        self.client.config.collections.update({'_id': self.db_coll},{'$unset': {'noBalance': True}})
        # change chunk size back
        self.client.config.settings.update({'_id':'chunksize'}, {'$set': {'value': self.default_chunk_size}}, upsert=True)
        
    def do_sharding(self, key):
        admin = self.client.admin
        admin.command('split', self.db_coll, middle = { '_id' : key})
        to_shard = next(self.shard_iter)
        try:
            admin.command('moveChunk', self.db_coll, find={'_id' : key}, to = to_shard)
        except pymongo.errors.OperationFailure as e:
            sys.stderr.write(str(e))
        
    def __str__(self):
        s = []
        s.append(str(self.client))
        s.append(str(self.db_coll))
        s.append('Using shards: ' + ','.join(self.hosts))
        s.append('Size in current chunk: ' + str(self.s))
        s.append('Previous key: ' + str(self.previous_key))
        return '\n'.join(s)
    def __repr__(self):
        return self.__str__()
        
        

'''
hosts = ['wl-cm01','wl-cm02','wl-cm03','wl-cm04','wl-cm05','wl-cm06','wl-cm07','wl-cm08']
shards = ['microcloud01','microcloud02','microcloud03','microcloud04',
          'microcloud05', 'microcloud06','microcloud07','microcloud08']

'''
if __name__ == '__main__':
    import sys
    #f = open('/home/gstupp/build_indexdb/seq_sorted.json')
    f = sys.stdin
    print(next(f))
    s = Sharder('seq','localhost',27017, 'test','test', user = 'gstupp', min_space = 8, chunk_size = 5)
    print(s.usage)
    #s.setup_mongo()
    
    for line in f:
        d = eval(line)
        print(d)
        #s.add_dict(d)
    s.cleanup_mongo()
        
