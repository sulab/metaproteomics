#!/usr/bin/env python3

import sys

def main():
    with open(sys.argv[1]) as f:
        for record in parse(f):
            print(record)

def parse(fasta_file_handle):

    defline, sequence = '', []
    for line in fasta_file_handle:
        if line[0] == '>':
            if defline:
                yield {'defline': defline, 'seq': ''.join(sequence)}
            defline, sequence = line[1:].rstrip(), []
        else:
            sequence.append(line.rstrip())

    if defline:
        yield {'defline': defline, 'seq': ''.join(sequence)}

if __name__ == '__main__':
    main()