# -*- coding: utf-8 -*-
"""
Created on Thu Dec 25 11:47:41 2014

@author: gstupp
"""

#Determine distribution of mass values
import sys
import itertools
f=sys.stdin
#f = open('/home/gstupp/build_indexdb/mass_sorted.flatdb')
f = map(lambda x: x.split(), f)
chunk_iter = itertools.groupby(f, lambda x: x[0])
for (mass,subiter) in chunk_iter:
    print(mass + '\t' + str(len(''.join(list(set(itertools.chain(*[x[1:] for x in subiter])))))))