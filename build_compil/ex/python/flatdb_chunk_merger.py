# -*- coding: utf-8 -*-
"""
Merge sorted flatdb chunks
After digesting a chunk of fasta proteins with blazmass, sort, then 
run a merge step to speed up sort-merge

@author: gstupp
"""
#f = open('/home/gstupp/build_indexdb/massSorted_renumbered_test.flatdb')

import sys
from itertools import groupby

def main():
    flatdb_delimiter = sys.argv[1]
    f = sys.stdin
    f = map(lambda x: x.split(), f)
    
    if flatdb_delimiter == 'mass':
        merge_flatdb_mass_chunk(f)
    elif flatdb_delimiter == 'seq':
        merge_flatdb_seq_chunk(f)

def merge_flatdb_mass_chunk(f):
    # print mass *tab* [peptides], tab-separated
    # ex: 547320  ASGKGK  GKGSAK
    chunk_iter = groupby(f, lambda x: x[0])
    for (mass,subiter) in chunk_iter:
        peptides = set([x[1] for x in subiter])
        print(mass + '\t' + '\t'.join(peptides))

def merge_flatdb_seq_chunk(f):
    # print mass *tab* peptide_seq *tab* 
    # [left, right, ID, pos], tab-separated, tab-delimited
    # ex: 715410	AAAAAAAAK	AKR	KKA	r1798000	131	AKR	KKA	r1803038	131	AKR	KKA	r1834662	131
    chunk_iter = groupby(f, lambda x: (x[0],x[1]))
    for (mass_peptide,subiter) in chunk_iter:
        print('\t'.join(mass_peptide) + '\t' + '\t'.join(['\t'.join(x[2:]) for x in subiter]))
        
if __name__ == '__main__':
	main()
