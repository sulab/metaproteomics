#!/usr/bin/env python

# fasta_reverse.py
# reverses all FASTA protein sequences in input and prints to screen (STDOUT)
#
# usage: cat FASTA1.fasta FASTA2.fasta ... | python fasta_reverse.py > Forward_Reverse_concat_fasta_filename.fasta
#
# Sandip Chatterjee
# v2, 7/14/14


# ProtDB_072114, last forward: 82817736

import sys

def main():

	DB_file = sys.stdin
	parse_fasta(DB_file)

def parse_fasta(DB_file):

	# current_chunk = []

	f = DB_file
	line = DB_file.readline()

	while True:
		defline = ''
		sequence_lines = []
		# protein_dicts = [] ##	list of dicts (one per peptide)
		if not line:
			break
		if line[0] == '>':
			defline = line[1:].rstrip('\n')		##	remove leading '>' and trailing newline
			while True:
				line = f.readline()
				if not line or line[0] == '>':
					break
				sequence_lines.append(line.rstrip('\n'))

		full_sequence = ''.join(sequence_lines).replace('\n','')
		rev_defline = 'Reverse_'+defline
		rev_full_sequence = full_sequence[::-1]
		print_fasta_record(rev_defline,rev_full_sequence)

def split_string_by_n(long_string,n):
	while long_string:
		yield long_string[:n]
		long_string = long_string[n:]

def print_fasta_record(defline,full_sequence):
	full_sequence = '\n'.join(split_string_by_n(full_sequence,80))  ## inserts a newline after every 80 characters of sequence
	print '>'+defline
	print full_sequence

if __name__ == '__main__':
	main()
