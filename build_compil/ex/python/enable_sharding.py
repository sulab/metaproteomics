import sys
from pymongo import MongoClient

HOST = sys.argv[1]
PORT = int(sys.argv[2])
db_name = sys.argv[3]
coll_name = sys.argv[4]

db = MongoClient(HOST, PORT).admin
try:
    db.command('enablesharding', db_name)
    db.command('shardcollection', db_name + '.' + coll_name, key={'_id': 1})
except:
    print("Failed to enable sharding")
