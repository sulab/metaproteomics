#!/usr/bin/env python3

from pymongo import MongoClient

import pymongo

if pymongo.version_tuple[0] < 3:
    raise ImportError("Requires pymongo version 3.x or higher")

def count_collection(host, port, db, coll):
    """
    http://docs.mongodb.org/manual/reference/method/cursor.count/
    On a sharded cluster, count() can result in an inaccurate count if orphaned documents exist or if a chunk migration is in progress.
    To avoid these situations, on a sharded cluster, use the $group stage of the db.collection.aggregate() method to $sum the documents.
    """
    mongo = MongoClient(host = host, port = port)[db][coll]
    result = list(mongo.aggregate([{"$group": {"_id": None, "count": {"$sum": 1}}}]))
    
    if len(result) == 0:
        return 0
    return result[0]['count']

if __name__ == "__main__":
    """
    Example usage: python3 count_collection.py wl-cmadmin 27018 ProtDB_indexDB_2mill ProtDB_indexDB_2mill
    """
    import sys
    host = sys.argv[1]
    port = int(sys.argv[2])
    db = sys.argv[3]
    coll = sys.argv[4]
    print(count_collection(host, port, db, coll))