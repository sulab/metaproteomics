#!/usr/bin/env python3

'''
Read fasta file from std input. Parse into protDB and generate mongo collection

'''


import sys
import re
import argparse
from pymongo import MongoClient
import fasta_parser

repositories = [
                'RefSeq',
                'HMP_Reference_Genomes',
                'HMP_metagenomics',
                'UniProt_Human',
                'Integrated_Gene_Catalog',
                'UniProt_Archaea',
                'UniProt_Bacteria',
                'UniProt_Viruses',
                'UniProt_Mouse',
                'mgm' #mouse gut microbiome 4661127
                ]

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument('db', type = str)
    parser.add_argument('coll', type = str)
    parser.add_argument('-m','--no-meta', help="Don't parse extra metadata from defline (repository, go terms, organism)", action='store_true')
    parser.add_argument('-mh','--mongo-host', type=str, default="localhost")
    parser.add_argument('-mp','--mongo-port', type = int, default = 27017)
    parser.add_argument('-t','--test', help= "Don't do any mongo insertion, be verbose", action = 'store_true')
    parser.add_argument('-w','--write-concern', help= "Set write concern to zero", action = 'store_true')
    args = parser.parse_args()
    
    if args.write_concern:
        client = MongoClient(args.mongo_host, args.mongo_port, w=0)
    else:
        client = MongoClient(args.mongo_host, args.mongo_port)
    db = client[args.db]
    coll = db[args.coll]
    if args.test:
        print(coll)

    records = [] # list of records to insert
    for record in fasta_parser.parse(sys.stdin):
        linesplit = record['defline'].split('||')
        protein_dict = dict()
        protein_dict['_id'] = int(linesplit[0])
        annotated_defline = ''.join(linesplit[1:])        ## full defline after protein ID (including repository, GO terms)
        protein_dict['s'] = record['seq']
        
        if args.no_meta:
            protein_dict['d'] = annotated_defline
            if args.test:
                print(protein_dict)
            else:
                records.append(protein_dict)
            continue            
        
        go_array = annotated_defline.split('|')[-1]
        g = go_array.lstrip('[').rstrip(']')            ## array of GO terms ([GO:123456,GO:123929])
        if g:
            protein_dict['g'] = g.split(',')
        protein_dict['r'] = annotated_defline.split('|')[-2]    ## source repository (RefSeq, UniProt, etc.)
        
        d = '|'.join(annotated_defline.split('|')[:-2])
        protein_dict['d'] = d

        if protein_dict['r'] == 'RefSeq':
            org = parse_refseq_HMP_defline_for_organism(d)
            if org:
                protein_dict['o'] = org

        elif protein_dict['r'] == 'HMP_Reference_Genomes':
            org = parse_refseq_HMP_defline_for_organism(d)
            if org:
                protein_dict['o'] = org

        elif protein_dict['r'] == 'HMP_metagenomics':
            pass

        elif protein_dict['r'] == 'UniProt_Human':
            protein_dict['o'] = 'Homo sapiens'

        elif protein_dict['r'] == 'Integrated_Gene_Catalog':
            pass

        elif protein_dict['r'].startswith('UniProt'):
            org = parse_uniprot_complete_proteomes_for_organism(d)
            if org:
                protein_dict['o'] = org
        
        if args.test:
            print(protein_dict)
        else:
            records.append(protein_dict)
    
    if not args.test:
        coll.insert_many(records, ordered=False)

def parse_refseq_HMP_defline_for_organism(line):

    o = re.findall(r"(?<=\[)[^\[]+(?=\])",line)

    if o:
        return o[-1]
    else:
        return None

def parse_uniprot_complete_proteomes_for_organism(line):
    pattern = re.compile(r"(.+)(OS=)(.+)(GN=)")
    matched = pattern.match(line)
    if matched:
        if matched.groups()[1] == 'OS=':
            return matched.groups()[2].strip()
        else:
            return None
    else:
        pattern = re.compile(r"(.+)(OS=)(.+)(PE=)")
        matched = pattern.match(line)
        if matched:
            if matched.groups()[1] == 'OS=':
                return matched.groups()[2].strip()
            else:
                return None
        else:
            return None

if __name__ == '__main__':
    main()
