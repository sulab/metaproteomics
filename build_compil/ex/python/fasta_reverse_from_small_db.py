#!/usr/bin/env python
"""
# fasta_reverse_from_small_db.py

Make a new fasta file for building a mongodb
from output.fasta, which is the first match from sqt files
For the purpose of making a small search db

Read in proteins, if it a forward, generate reverse
If reverse, generate forward.

Make sure IDs match up with compil_072114

Make sure proteins are only printed once 

Proteins should already be numbered with their Compil ID

# ProtDB_072114, last forward: 82817736


"""
import sys

LAST = 82817736

def main():

    DB_file = sys.stdin
    parse_fasta(DB_file)

def parse_fasta(DB_file):

    protein_ids = set()

    f = DB_file
    line = DB_file.readline()

    while True:
        defline = ''
        sequence_lines = []
        # protein_dicts = [] ##    list of dicts (one per peptide)
        if not line:
            break
        if line[0] == '>':
            defline = line[1:].rstrip('\n')        ##    remove leading '>' and trailing newline
            while True:
                line = f.readline()
                if not line or line[0] == '>':
                    break
                sequence_lines.append(line.rstrip('\n'))
        
        full_sequence = ''.join(sequence_lines).replace('\n','')
        prot_id = defline.split()[0]
        if prot_id in protein_ids:
            continue
        else:
            protein_ids.add(prot_id)
            print_fasta_record(defline,full_sequence)

        if defline.startswith("Reverse_"):
            rev_defline = str(int(defline.replace("Reverse_","",1)) - LAST)
            prot_id = rev_defline.split()[0]
            if prot_id in protein_ids:
                continue
            else:
                rev_full_sequence = full_sequence[::-1]
                print_fasta_record(rev_defline,rev_full_sequence)
        else:
            rev_defline = 'Reverse_' + str(int(defline) + LAST)
            prot_id = rev_defline.split()[0]
            if prot_id in protein_ids:
                continue
            else:
                rev_full_sequence = full_sequence[::-1]
                print_fasta_record(rev_defline,rev_full_sequence)

def split_string_by_n(long_string,n):
    while long_string:
        yield long_string[:n]
        long_string = long_string[n:]

def print_fasta_record(defline,full_sequence):
    full_sequence = '\n'.join(split_string_by_n(full_sequence,80))  ## inserts a newline after every 80 characters of sequence
    print('>'+defline)
    print(full_sequence)

if __name__ == '__main__':
    main()
