"""
Add json file to existing db

# protdb doesn't need anything fancy. no updates. Just insert the new proteins
"""

#from pymongo import ReturnDocument # for debug add return_document=ReturnDocument.AFTER arg to find_one_and_update

from pymongo import MongoClient
import json
import sys
import argparse

mongos_uri = "mongodb://wl-cmadmin:27018,node0097:27018,node0129:27018,node0401:27018,node0441:27018," + \
            "node0481:27018,node0521:27018,node0561:27018,node0601:27018," + \
            "node0617:27018,node0649:27018,node0953:27018,node0985:27018," + \
            "node1001:27018,nodea1331:27018,nodea1431:27018," + \
            "node0113:27018,node0145:27018,node0411:27018,node0421:27018,node0431:27018," + \
            "node0451:27018,node0461:27018,node0471:27018,node0491:27018,node0501:27018," + \
            "node0511:27018,node0531:27018,node0541:27018,node0551:27018,node0571:27018," + \
            "node0581:27018,node0591:27018,node0633:27018,node0665:27018,node0681:27018," + \
            "node0921:27018,node0937:27018,node0969:27018,nodea1301:27018,nodea1401:27018"

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('db_name', help='db and coll name (ex: SeqDB_010214)', type=str)
    parser.add_argument('db_type', help='can be mass or seq', type=str)
    
    args = parser.parse_args()
    db_name = args.db_name
    coll_name = db_name
    client = MongoClient(mongos_uri)
    db_coll = client[db_name][coll_name]
    bulk = db_coll.initialize_unordered_bulk_op()
    
    if args.db_type == "mass":
        field = 's'
    elif args.db_type == "seq":
        field = 'p'
    else:
        print("invalid db_type")
        sys.exit(1)
    
    for count,line in enumerate(sys.stdin):
        doc = json.loads(line)
        bulk.find({'_id': doc['_id']}).upsert().update_one({'$addToSet': {field: {"$each": doc[field]}}})
        #db_coll.find_one_and_update({'_id': doc['_id']}, {'$addToSet': {field: {"$each": doc[field]}}}, upsert = True)
    print(bulk.execute())

