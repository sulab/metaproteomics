# If the line starts with a '>', append "$DB|[]"
awk -v db="$1" '{if ($1 ~ /^>/) print $0"|"db"|[]"; else print $0}'
