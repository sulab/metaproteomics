#!/bin/bash
set -e
mkdir -p tmp

BINPATH=../ex
FASTADB=$1

# Set some variables
FLATDB=${FASTADB/.fasta/.flatdb}
MSFLATDB="massSorted_"$FLATDB
SSFLATDB="seqSorted_"$FLATDB
MSJSONFILE=${MSFLATDB/.flatdb/.json}
SSJSONFILE=${SSFLATDB/.flatdb/.json}

cat $FASTADB | parallel -j+4 --block 5M --recstart '\n>' --regexp --tmpdir tmp --pipe java -jar $BINPATH/java/blazmass_orig_indexer.jar -i . blazmass.params 2> bm.out 1> $FLATDB
$BINPATH/sort -nbk 1,1 -T . $FLATDB > $MSFLATDB
$BINPATH/sort -bk 2,2 -T . $MSFLATDB > $SSFLATDB
$BINPATH/python/flatdb_parse.py $MSFLATDB "mass" | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe $BINPATH/python/flatdb_json.py "mass" > $MSJSONFILE
$BINPATH/python/flatdb_parse.py $SSFLATDB "seq" | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe $BINPATH/python/flatdb_json.py "seq" > $SSJSONFILE
