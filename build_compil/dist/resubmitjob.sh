#!/bin/bash

#Check for existence of failed files, every 10 seconds forever. If a chunk fails, resubmit it



while true; do
    CHUNKS=$(ls | grep incomp\.)
    for i in $CHUNKS; do
        NUM=${i/.incomp/}
        echo "resubmitting $NUM"
        rm $i
        qsub -t ${NUM} run_block.job
    done
    sleep 5
done
