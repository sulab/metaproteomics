
# Main script to run build_indexdb
# Parameters: 
#        1: path to fasta file
#        2: name of blazmass.params file. (default: "blazmass.params")

if [ -z $1 ]; then echo "Must call with an input fasta file"; exit 1; fi
FASTA_IN=$1

if [ ! -z $2 ]; then
    B_PARAMS="blazmass.params"
fi

function check_reverse {
    if [ $(grep '^>' $FASTA_IN | grep -cv '^>Reverse_') != $(grep '^>' $FASTA_IN | grep -c '^>Reverse_') ]; then
        MAKE_REVERSE=true
    else
        MAKE_REVERSE=false
    fi
}

echo "Checking for reverse proteins in $FASTA_IN"
check_reverse
if [ $MAKE_REVERSE = true ]; then
    echo "Something is wrong with your reverse proteins! Remaking forward/reverse fasta file."
    REV_FASTA="${FASTA_IN%.*}_reverse.fasta"
    cp $FASTA_IN $REV_FASTA
    cat $FASTA_IN | python3 ~/metaproteomics/file_processing/fasta/fasta_reverse.py >> $REV_FASTA
    FASTA_IN=$REV_FASTA
    check_reverse
    if [ $MAKE_REVERSE = true ]; then
        echo "Something is seriously wrong with your reverse proteins! Exiting..."
        exit 1
    else
        echo "Reverse proteins OK!"
        echo "Using: $FASTA_IN"
    fi
else
    echo "Reverse proteins OK!"
fi

# Split fasta file into chunks, count number of proteins, submit processing jobs for each chunk
qsub -v FASTA_IN=$FASTA_IN -v B_PARAMS=$B_PARAMS run.job

# If any incomp.# files appear, resubmit those jobs
# Once all .fasta.# files have a massDB.#.gz and seqDB.#.gz and no incomp.# files, move on to the next step
KEEP_GOING=true
while [ $KEEP_GOING == true ]; do
    # Check if there are any fasta chunks yet
    if [ $( ls -l | grep ${FASTA_IN}\. | wc -l) == 0 ]; then sleep 30; echo "Haven't split fasta yet"; continue; fi
    # Check if any jobs failed
    CHUNKS=$(ls | grep incomp\.)
    for i in $CHUNKS; do
        NUM=${i/incomp./}
        echo "resubmitting $i"
        rm $i
        qsub -t ${NUM} run_block.job
    done
    # Check if all fasta chunks have a massDB and seqDB
    KEEP_GOING=false
    for i in $(ls ${FASTA_IN}.*); do
        NUM=${i/${FASTA_IN}./}
        if ! [ -s massDB.$NUM.gz ]; then KEEP_GOING=true; echo "Missing massDB.$NUM.gz"; fi
        if ! [ -s seqDB.$NUM.gz ]; then KEEP_GOING=true; echo "Missing seqDB.$NUM.gz"; fi
    done
    if [ $KEEP_GOING == true ]; then sleep 20; fi
done

echo "All chunks complete"

echo "Submitting chunk mergers"
SEQ_JOB=$(qsub merge_seq_chunks.job)
MASS_JOB=$(qsub merge_mass_chunks.job)

echo "Submitting import jobs"
qsub -W depend=afterok:$SEQ_JOB:$MASS_JOB -v DBNAME=$DBNAME,COLLNAME=$COLLNAME import.job

