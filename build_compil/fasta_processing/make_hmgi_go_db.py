#!/usr/bin/env python

#	make_hmgi_go_db.py
#	Sandip Chatterjee
#	v1, July 9, 2014
#	
#	For parsing a hmgi gff3 ontologyterm file and 
#	
#	Usage:
#	$ python make_hmgi_go_db.py hmgi_gff3_ontologytermfile

import sys
from pymongo import MongoClient

def main():
	try:
		DB_file = sys.argv[1]
	except:
		print "Requires a gene association flatfile"
		print "Correct usage: $ python make_uniprot_goa_db.py gene_association.uniprot_goa"
		sys.exit()

	#read_flatfile(DB_file,1)
	with open(DB_file) as f:
		read_hmgi_flatfile(f)
	print "Finished"

def read_hmgi_flatfile(DB_fileobj):
	
	client = MongoClient()
	db = client['HMGIDB']
	coll = db['HMGIDB_061314']
#	insert_list = []

	bulk = coll.initialize_unordered_bulk_op()

	for num, line in enumerate(DB_fileobj):
		linesplit = line.rstrip('\n').split('\t')
		infolist = linesplit[8].split(';')
		GO_info = [x for x in infolist if 'Ontology_term=' in x]
		if len(GO_info) > 1:
			print "too many GO terms"
			print GO_info
		ID_info = [x for x in infolist if 'ID=' in x]
		if len(ID_info) > 1:
			print "too many ID terms"
			print ID_info
		parent_info = [x for x in infolist if 'Parent=' in x]
		if len(parent_info) > 1:
                        print "too many parent terms"
                        print parent_info

		JSON_obj = {}
		if ID_info:
			JSON_obj['hmgiID'] = ID_info[0].replace('ID=','')
		if GO_info:
			JSON_obj['go'] = GO_info[0].replace('Ontology_term=','').split(',') ## need check the split on this...
		if parent_info:
			JSON_obj['hmgiParent'] = parent_info[0].replace('Parent=','')
		bulk.insert(JSON_obj)
		if num % 50000 == 0:
			bulk.execute()
			bulk = coll.initialize_unordered_bulk_op()

	bulk.execute()
	coll.ensure_index([('hmgiID',1)])
	coll.ensure_index([('hmgiParent',1)])

def read_flatfile_old(DB_file,delim):

	client = MongoClient()
	db = client['UniProtGOA']
	coll = db['UniProtGOA_061314']

	current_chunk = []

	with open(DB_file,'rb') as f:
		
		while True:
			line = f.readline()
			if line[0] != '!':
				break
		
		chunk_counter = 0
		bulk = coll.initialize_unordered_bulk_op()
		while True:
			current_chunk = []
			if not line:
				break

			else:
				current_acc = line.split('\t')[delim]
			while True:
				current_chunk.append(line)
				line = f.readline()
				if not line or line.split('\t')[delim] != current_acc:
					break

			chunk_counter += 1
			bulk.insert(insert_flatfile_chunk(current_chunk,coll))
			if chunk_counter % 5000 == 0:
				bulk.execute()
				bulk = coll.initialize_unordered_bulk_op()
	bulk.execute()
	coll.ensure_index([('uac',1)])
	
def insert_flatfile_chunk(current_chunk,coll):
	
	temp_dict = {}
	GO_list = []
	uac = current_chunk[0].split('\t')[1]
	for line in current_chunk:
		GO_list.append(line.split('\t')[4])
	GO_list = list(set(GO_list))
	temp_dict['uac'] = uac
	temp_dict['go'] = GO_list
	return temp_dict
#	coll.insert(temp_dict)

if __name__ == '__main__':
	main()
