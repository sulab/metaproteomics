#!/usr/bin/env python

import sys
import urllib2
from multiprocessing import Pool

def main():

	#input_file = sys.stdin
	pool = Pool(16)
	
	input_file = sys.argv[1]
	with open(input_file) as f:
		all_IDs = [x.rstrip('\n') for x in f.readlines() if x[0:5] != 'Taxon'] ## filters out first line, removes newlines
	all_IDs = [x.split('\t')[0] for x in all_IDs]
	results_array = pool.map(lookup_info,all_IDs)
	pool.close()
	pool.join()

	with open(input_file+'.out','wb') as f:
		for result in results_array:
			if result:
				[f.write(x+'\n') for x in result if x]

def lookup_info(ID):
	try:
		mydata = urllib2.urlopen('http://www.uniprot.org/uniprot/?query=organism%3a'+ID+'+keyword%3a181&force=yes&format=fasta&include=yes').read()
		#mydata_parsed = mydata.split('\n')[1]
		return mydata.split('\n')
	except:
		print ID ## IDs that were unable to be retrieved, printed to STDOUT
		return ''

if __name__ == '__main__':
	main()
