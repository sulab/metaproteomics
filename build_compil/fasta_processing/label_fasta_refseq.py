#!/usr/bin/env python

# label_fasta_refseq.py
#
# For use with GNU Parallel
# cat $FASTAFILE | parallel -j+0 --block 50M --recstart '>' --tmpdir . --pipe python label_fasta_refseq.py > $LABELEDFASTAFILE
#
# Labels FASTA deflines with repository source and list of gene ontology terms
# BEFORE: gi|526245011|ref|YP_008320337.1| terminase small subunit [Paenibacillus phage phiIBB_Pl23]
# AFTER:  gi|526245011|ref|YP_008320337.1| terminase small subunit [Paenibacillus phage phiIBB_Pl23]||RefSeq||[GO:12345,GO:23924,GO:123835,GO:924831]
#
# Requires MongoDB ID Mapping database (UniProtIDMapping.UniProtIDMapping_061314 for example)
# 
# July 5, 2014

# Remember MongoDB UniProtIDMapping schema:
#
# (list index) (abbrev. key name) (full key name)
# 0) uac --> 1. UniProtKB-AC
# 1) uid --> 2. UniProtKB-ID
# 2) gid --> 3. GeneID (EntrezGene)
# 3) rs --> 4. RefSeq
# 4) gi --> 5. GI
# 5) pdb --> 6. PDB
# 6) go --> 7. GO
# 7) u100 --> 8. UniRef100
# 8) u90 --> 9. UniRef90
# 9) u50 --> 10. UniRef50
# 10) up --> 11. UniParc
# 11) pir --> 12. PIR
# 12) ncbi --> 13. NCBI-taxon
# 13) mim --> 14. MIM
# 14) ug --> 15. UniGene
# 15) pm --> 16. PubMed
# 16) embl --> 17. EMBL
# 17) emblc --> 18. EMBL-CDS
# 18) ens --> 19. Ensembl
# 19) enst --> 20. Ensembl_TRS
# 20) ensp --> 21. Ensembl_PRO
# 21) pm2 --> 22. Additional PubMed

import sys
from pymongo import MongoClient

def main():

	fasta_file = sys.stdin
	client = MongoClient()

	IDMappingDB = client['UniProtIDMapping']
	IDMappingColl = IDMappingDB['UniProtIDMapping_061314']

	GOADB = client['UniProtGOA']
	GOAColl = GOADB['UniProtGOA_061314']

	repository_name = 'RefSeq'

	for line in fasta_file:
		if line[0] == '>':
			line_dict = parse_defline_refseq(line.rstrip('\n'))
			GO_terms = get_GO_terms(line_dict,IDMappingColl,GOAColl)
			sys.stdout.write(line.rstrip('\n')+'|'+repository_name+'|[')
			sys.stdout.write(','.join(GO_terms))
			sys.stdout.write(']\n')
		else:
			print line.rstrip('\n')

def parse_defline_refseq(defline):
	line_dict = {}
	line_split = defline.lstrip('>').split('|')
	line_dict['gi'] = line_split[1]
	line_dict['rs'] = line_split[3]
	return line_dict

def get_GO_terms(line_dict, IDMappingColl, GOAColl):
	GO_term_list = []
	for key in line_dict:
		result = IDMappingColl.find_one({key:line_dict[key]})
		if result:
			if 'go' in result:
				if hasattr(result['go'],'lower'): # if type is string -- need to change database to only have GO lists, even if length=1...
					GO_term_list.append(result['go'])
				else:
					GO_term_list.extend(result['go'])

			if 'uac' in result:	# UniProt Accession should be present, but just a check
				uniprot_acc = result['uac']
				result2 = GOAColl.find_one({'uac':uniprot_acc})	# query gene_annotation database for additional GO terms
				if result2:
					if 'go' in result2:
						GO_term_list.extend(result2['go'])
	GO_term_list = list(set(GO_term_list))	# remove duplicates
	return GO_term_list

if __name__ == '__main__':
	main()
