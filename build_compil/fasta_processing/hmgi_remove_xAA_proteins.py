#!/usr/bin/env python

import sys

def main():
	
	with open(sys.argv[1]) as f, open(sys.argv[1]+'_x_removed','wb') as f2:
		line = f.readline()
		while True:
			defline = ''
			sequence_lines = []
			# protein_dicts = [] ##	list of dicts (one per peptide)
			if not line:
				break
			if line[0] == '>':
				defline = line[1:].rstrip('\n')		##	remove leading '>' and trailing newline
				while True:
					line = f.readline()
					if not line or line[0] == '>':
						break
					sequence_lines.append(line.rstrip('\n'))

			full_sequence = ''.join(sequence_lines).replace('\n','')
			#print_json_obj(defline,full_sequence)
			if 'x' in full_sequence or 'X' in full_sequence:
				pass
			else:
				f2.write('>'+defline+'\n'+full_sequence+'\n')

if __name__ == '__main__':
	main()
