#!/usr/bin/env python

import sys
from pymongo import MongoClient

def main():
	
	idmapping_file = sys.argv[1]
	client = MongoClient()
	db = client['UniProtIDMapping']
	coll = db['UniProtIDMapping_061314']
	
	key_list = {}
	key_list[0] = 'uac'
	key_list[1] = 'uid'
	key_list[2] = 'gid'
	key_list[3] = 'rs'
	key_list[4] = 'gi'
	key_list[5] = 'pdb'
	key_list[6] = 'go'
	key_list[7] = 'u100'
	key_list[8] = 'u90'
	key_list[9] = 'u50'
	key_list[10] = 'up'
	key_list[11] = 'pir'
	key_list[12] = 'ncbi'
	key_list[13] = 'mim'
	key_list[14] = 'ug'
	key_list[15] = 'pm'
	key_list[16] = 'embl'
	key_list[17] = 'emblc'
	key_list[18] = 'ens'
	key_list[19] = 'enst'
	key_list[20] = 'ensp'
	key_list[21] = 'pm2'

	with open(idmapping_file) as f:
		for line in f:
			temp_dict = {}
			line_list = line.rstrip('\n').split('\t')
			for index, item in enumerate(line_list):
				entry = format_entry(key_list,index,item)
				if entry:
					temp_dict[entry[0]] = entry[1]
			coll.insert(temp_dict)
	
	print "Finished"

def format_entry(key_list,index,line_list_item):
	
	if not line_list_item:
		return None
	key = key_list.get(index)
	if not key:
		return None
	if ';' in line_list_item:
		line_list_item_split = line_list_item.split(';')
		return (key,[x.strip() for x in line_list_item_split])
	else:
		return (key,line_list_item.strip())

if __name__ == '__main__':
	main()
