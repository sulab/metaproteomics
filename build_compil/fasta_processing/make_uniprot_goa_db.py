#!/usr/bin/env python

#	make_uniprot_goa_db.py
#	Sandip Chatterjee
#	v1, July 5, 2014
#	
#	For parsing a gene_association.uniprot_goa file and 
#	
#	Usage:
#	$ python make_uniprot_goa_db.py gene_association.uniprot_goa

import sys
from pymongo import MongoClient

def main():
	try:
		DB_file = sys.argv[1]
	except:
		print "Requires a gene association flatfile"
		print "Correct usage: $ python make_uniprot_goa_db.py gene_association.uniprot_goa"
		sys.exit()

	read_flatfile(DB_file,1)

	print "Finished"

def read_flatfile(DB_file,delim):

	client = MongoClient()
	db = client['UniProtGOA']
	coll = db['UniProtGOA_061314']

	current_chunk = []

	with open(DB_file,'rb') as f:
		
		while True:
			line = f.readline()
			if line[0] != '!':
				break
		
		chunk_counter = 0
		bulk = coll.initialize_unordered_bulk_op()
		while True:
			current_chunk = []
			if not line:
				break

			else:
				current_acc = line.split('\t')[delim]
			while True:
				current_chunk.append(line)
				line = f.readline()
				if not line or line.split('\t')[delim] != current_acc:
					break

			chunk_counter += 1
			bulk.insert(insert_flatfile_chunk(current_chunk,coll))
			if chunk_counter % 5000 == 0:
				bulk.execute()
				bulk = coll.initialize_unordered_bulk_op()
	bulk.execute()
	coll.ensure_index([('uac',1)])
	
def insert_flatfile_chunk(current_chunk,coll):
	
	temp_dict = {}
	GO_list = []
	uac = current_chunk[0].split('\t')[1]
	for line in current_chunk:
		GO_list.append(line.split('\t')[4])
	GO_list = list(set(GO_list))
	temp_dict['uac'] = uac
	temp_dict['go'] = GO_list
	return temp_dict
#	coll.insert(temp_dict)

if __name__ == '__main__':
	main()
