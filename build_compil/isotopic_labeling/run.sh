#!/bin/bash
set -e
mkdir -p tmp

FASTADB=compil_mouse_mgm.fasta
B_PARAMS=blazmass_Nterm30_K30.params
MASSDB=MassDB_20151009_compil_mgm_NT30_K30

FLATDB=${FASTADB/.fasta/.flatdb}
MSFLATDB="massSorted_"$FLATDB
MSJSONFILE=${MSFLATDB/.flatdb/.json}
BINPATH=../ex

cat $FASTADB | parallel --block 10M --recstart '\n>' --regexp --tmpdir tmp --pipe java -jar $BINPATH/java/blazmass_orig_indexer.jar -i . $B_PARAMS | cut -f1-2 | pigz -2 > $FLATDB
echo "Finished blazmass"
zcat $FLATDB | $BINPATH/sort -nbk 1,1 -T . | uniq >  $MSFLATDB
echo "Finished sorting"
$BINPATH/python/flatdb_parse.py $MSFLATDB "mass" | parallel -j+0 --block 10M --recstart '>' --tmpdir tmp --pipe $BINPATH/python/flatdb_json.py "mass" > $MSJSONFILE
echo "Finished json"
python3 $BINPATH/python/enable_sharding.py wl-cmadmin 27018 $MASSDB $MASSDB
cat $MSJSONFILE | parallel -j16 --block 100M --pipe "python3 $BINPATH/python/JSON_import_uri.py $MASSDB" > mongo_import.out
echo "Finished import"

