#!/usr/bin/env python3

# generate_fasta_chunks.py
# for generating subset FASTA files of ProtDB

# run with -h (or --help) flag to see argparse options

# 10/7/14
# Sandip Chatterjee

import sys
import random
import argparse
import warnings
import multiprocessing as mp
from pymongo import MongoClient

def main():

	if args.limitrecords:
		protein_IDs = get_protein_IDs(protdb_coll, num_records=args.limitrecords)
	else:
		protein_IDs = get_protein_IDs(protdb_coll)

	protein_ID_batches = split_protein_IDs(protein_IDs)

	pool = mp.Pool()
	pool.map(generate_subset_fasta, protein_ID_batches, chunksize=1)
	print('Closing multiprocessing pool...')
	pool.close()
	pool.join()

	print("Finished")

def generate_subset_fasta(protein_batch_tuple, file_prefix=''):

	batch_number = protein_batch_tuple[0]
	protein_batch = protein_batch_tuple[1]

	if args.fileprefix:
		file_prefix = args.fileprefix

	with open(file_prefix+str(batch_number)+'.fasta','w') as f:
		for protein_ID in protein_batch:
			FASTA_record = lookup_protein(protein_ID)
			if FASTA_record:
				f.write(FASTA_record)
			else:
				pass # ProtDB lookup failed

def lookup_protein(protein_ID):
	query = protdb_coll.find_one({'_id':protein_ID})
	if query:
		defline = query['d']
		sequence = query['s']
		return assemble_fasta_record(defline, sequence, protein_ID)
	else:
		print('ProtDB Query for {} failed'.format(protein_ID))
		warnings.warn('ProtDB Query for {} failed'.format(protein_ID))
		return None

def get_protein_IDs(mongo_collection, num_records=None, accurate=True):

	'''
	gets the number of protein records in mongo_collection (which is a pymongo/collection object for ProtDB).
	'''
	if not num_records:
		if accurate:
			# Number of records in ProtDB -- always accurate, but takes (much) longer to run
			num_records = mongo_collection.aggregate({ '$group': { '_id': None, 'count': { '$sum': 1 } } })['result'][0]['count']
		else:
			# Number of records in ProtDB -- may be inaccurate for a sharded cluster if chunks are not fully migrated
			num_records = mongo_collection.count()

	protein_IDs = range(1, num_records+1)

	# random.shuffle(protein_IDs) #slow for large lists, requires entire list to be stored in memory (not generator)

	return protein_IDs

def split_protein_IDs(protein_IDs, n=100):

	''' 
	splits a list of protein_IDs into n sublists.
	takes every nth protein_ID and groups into chunks, then returns a list of those chunks.

	Format of return value is (chunk_num, chunk) where chunk_num begins at 1 and continues to n

	(returns a generator function)
	'''

	if args.numchunks:
		n = args.numchunks

	return ((i+1,protein_IDs[i::n]) for i in range(n if n < len(protein_IDs) else len(protein_IDs)))

def split_string_by_n(long_string,n):

	'''
	splits a (long) string into n parts, returning one part at a time.

	(generator function)
	'''

	while long_string:
		yield long_string[:n]
		long_string = long_string[n:]

def assemble_fasta_record(defline, full_sequence, protein_ID, split_num=80):

	'''
	returns a FASTA record as a single string (with newline characters embedded).
	FASTA sequence can be split on to multiple lines using split_num (number of characters to include on each line)
	'''

	split_sequence = '\n'.join(split_string_by_n(full_sequence,split_num))  ## inserts a newline after every (split_num) characters of sequence

	return ''.join(['>', str(protein_ID), '||', defline, '\n', split_sequence, '\n'])

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-s', '--host', help='MongoDB hostname (mongod or mongos)', type=str)
	parser.add_argument('-p', '--port', help='MongoDB port number (mongod or mongos) -- if present, requires --hostname', type=int)
	parser.add_argument('-d', '--database', help='MongoDB database name (ProtDB)', type=str)
	parser.add_argument('-c', '--collection', help='MongoDB collection name (ProtDB)', type=str)
	parser.add_argument('-f', '--fileprefix', help='Prefix for subset FASTA chunks', type=str)
	parser.add_argument('-n', '--numchunks', help='Number of FASTA chunks to create', type=int)
	parser.add_argument('-l', '--limitrecords', help='Limit total number of FASTA records to obtain from ProtDB', type=int)
	args = parser.parse_args()

	if args.host:
		if args.port:
			host = args.host
			port = args.port
		else:
			host = args.host
			port = 27017
	else:
		host = 'localhost'
		port = 27017

	print('Connecting to MongoDB server at {}:{}'.format(host, port))
	client = MongoClient(host, port)

	if args.database and args.collection:
		protdb_name = args.database
		protdb_collname = args.collection
	else:
		protdb_name = 'ProtDB_072114'
		protdb_collname = 'ProtDB_072114'

	print('Connecting to MongoDB collection {}:{} (ProtDB)'.format(protdb_name, protdb_collname))
	protdb = client[protdb_name]
	protdb_coll = protdb[protdb_collname]

	main()