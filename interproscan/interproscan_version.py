#!/usr/bin/env python
"""
Parse version information from interproscan

usage:
java -jar interproscan-5.jar | python3 ~/metaproteomics/interproscan/interproscan_version.py

"""
import sys
import json
adict = {}
txt = sys.stdin
#%%
line = next(txt)
ipsv = line[line.index("InterProScan"):].strip()
adict.update({ipsv.split('-',1)[0]:ipsv.split('-',1)[1]})

#%%
for line in txt:
    if "Available analyses" in line:
        break
#%%
line = next(txt)
for line in txt:
    if not line.strip():
        break
    analyses = line.strip().split(":")[0]
    k = analyses.split('-')[0]
    v = analyses.split('-')[1]
    adict.update({k:v})

json.dumps(adict)