#%%

from itertools import chain
import pickle
import gzip
import os
def gsave(obj, filename, protocol=3):
    # should check if file exists first...
    with gzip.GzipFile(filename, 'wb') as f:
        pickle.dump(obj, f, protocol)

def gload(filename):
    f = gzip.open(filename,'rb')
    myNewObject = pickle.load(f)
    f.close()
    return myNewObject


#%% multi threaded
from collections import defaultdict
from itertools import chain
from pymongo import MongoClient
HOST = 'wl-cmadmin'
PORT = 27018
client = MongoClient(HOST, PORT)
hashDB = client.HashDB_072114.HashDB_072114
domainDB = client.DomainDB_072114.DomainDB_072114

ipa_sets = defaultdict(set)
def process_range(r):
    ipa_sets = defaultdict(set)
    ids = {doc['_id']:doc['pID'] for doc in hashDB.find({'pID':{'$in': list(range(r[0],r[1]))}})}
    for doc in domainDB.find({'_id':{'$in': list(ids.keys())}}):
        domain_list = list(chain(*doc['d'].values()))
        ipa_annotations = frozenset([domain['ipa'] for domain in domain_list if 'ipa' in domain])
        ipa_sets[ipa_annotations].update(set(ids[doc['_id']]))
    return ipa_sets

#%% 
from concurrent.futures import ProcessPoolExecutor
executor = ProcessPoolExecutor(max_workers=8)
by = 10000
m = 1000000
e = executor.map(process_range, zip(range(0, m, by),range(by, m+by, by)))
executor.shutdown()
print('done1')
def merge(dicts):
    merged = defaultdict(set)
    for d in dicts:
        for k in d:
            merged[k] |= d[k]
    return merged

ipa_sets = merge(e)
print(len(ipa_sets))
#gsave(ipa_sets,'ipa_sets.pkl.gz')

#%% ArchDB_IPA_071414

from pymongo import MongoClient
db = MongoClient('wl-cmadmin', 27017).ArchDB_IPA_071414.ArchDB_IPA_071414
db.insert(map(lambda item: {'_id': min(item[1]), 'pID': list(item[1]), 'IPR': sorted(list(item[0]))}, [x for x in ipa_sets.items() if len(x[0])])) # skip the doc with no IPR annotations
db.ensure_index('pID')
db.ensure_index('IPR')

#%%
db.find()