## Steps to generate domain annotation db (DomainDB_072114)
Format: Described in IPSParser.py

1. Submit fasta file for processing using interproscan in batches of 5k-20k
    - See submit_job.job and submit_job_sub.job
    - Use only forward sequences. Numbered or not (or the defline at all) doesn't matter as only the md5sum is used

2. Make a list of hashes that were searched and do not appear in the tsv results, so we can add them to the DB (mark as searched, which versions of IPS member dbs)
   - Note: haven't done this (as of Jan 14, 2016)

3. See IPSParser.py

## Adding new proteins/annotations to the db

1. Same. IPSParser will update entries that are different
-  `ls *.tsv | parallel -j4 'python3 ~/metaproteomics/IPSParser.py -f {} -mh wl-cmadmin -mp 27018 -md DomainDB_072114 -mc DomainDB_072114'`
