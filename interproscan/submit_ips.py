"""
Created on Mon Oct 13 15:53:22 2014

@author: Greg
"""

from Bio import SeqIO
import os
import argparse
import subprocess
import shelve


def main():
    '''
    Example usage:
    Split fasta file 1.fasta into chunks of 200 sequences each, using only non-refseq forward sequences
    python submit_ips.py 1.fasta -n 200 -nr
    '''
    
    parser = argparse.ArgumentParser()
    parser.add_argument('fasta_file', help = 'Path to input fasta file', type=str)
    parser.add_argument('-n', '--numseq', help = 'Number of sequences per chunk', type = int, default = 40000)
    parser.add_argument('-c', '--chunk', help = "Make chunks of fasta files, if not set, they will be read from 'log' in out_folder", action = 'store_true')
    parser.add_argument('-s', '--submit', help = 'Submit jobs to Garibaldi queue', action = 'store_true')
    parser.add_argument('-d', '--debug', action = 'store_true')
    parser.add_argument('-o', '--out-folder', help = ' '.join(['Absolute or relative (to fasta_file) path to output files.',
    'Default: "submission" relative to fasta_file']), type = str, default = 'submission')
    parser.add_argument('-t', '--test', help = 'test submission with the first split file', action = 'store_true')
    parser.add_argument('-v', '--verify-done', help = 'Verify that all tsv files in out-folder exist', action = 'store_true')
    parser.add_argument('--non-refseq', help = 'Split forward non-refseq sequences', action = 'store_true')
    parser.add_argument('--refseq', help = 'Split forward non-refseq sequences', action = 'store_true')
    parser.add_argument('--forward-uniprot', help = 'Split forward uniprot sequences', action = 'store_true')
    args = parser.parse_args()

    if args.test:
        args.submit = True
        
    if args.numseq:
        args.chunk = True
    
    # Make fasta_file path absolute
    if not args.fasta_file.startswith(os.sep):
        args.fasta_file = os.path.join(os.getcwd(), args.fasta_file)
    if args.debug:
        print('Input fasta file: ' + args.fasta_file)
    
    # Check if given absolute or relative path for out_folder. Make absolute
    if args.out_folder.startswith(os.sep):
        new_file_path = args.out_folder
    else:
        new_file_path = os.path.join(os.path.split(args.fasta_file)[0], args.out_folder)
    if args.debug and args.chunk:
        print('Output files going in: ' + new_file_path)
    
    # Split fasta file or read paths if already split
    log_file_path = os.path.join(new_file_path, 'log')
    if args.chunk:
        if args.non_refseq:
            fasta_type = is_forward_not_refseq
        elif args.refseq:
            fasta_type = is_forward_refseq
        elif args.forward_uniprot:
            fasta_type = is_forward_uniprot
        else:
            fasta_type = lambda x: True
        fasta_filenames = create_new_files(args.fasta_file, new_file_path, args.numseq, fasta_type)
        if args.debug:
            print('Fasta files created: \n' + '\n'.join(fasta_filenames))
        # Save log of fasta files created
        d = shelve.open(log_file_path)
        d['fasta_filenames'] = fasta_filenames
        d.close()
    else:
        # read log of fasta files created
        d = shelve.open(log_file_path)
        fasta_filenames = d['fasta_filenames']
        d.close()
        if args.debug:
            print('Fasta files to use: \n' + '\n'.join(fasta_filenames))
    
    if args.submit:
        if args.test:
            fasta_filenames = [fasta_filenames[0]]
        if args.debug:
            print('Submitting jobs on: \n' + '\n'.join(fasta_filenames))
        PBS_job_ids, ips_filepaths = submit_jobs(fasta_filenames, new_file_path)
        # Save log of tsvs, so we can join them/check them later
        d = shelve.open(log_file_path)
        d['PBS_job_ids'] = PBS_job_ids
        d['ips_filepaths'] = ips_filepaths
        d.close()
    
    if args.verify_done:
        #Check if all tsv files in log are in new_file_path
        d = shelve.open(log_file_path)
        ips_filepaths = d['ips_filepaths']
        d.close()
        list_dir = os.listdir(new_file_path)
        list_dir = [os.path.join(new_file_path, f) for f in list_dir] #full path
        files_not_done = [x for x in ips_filepaths if x not in list_dir]
        if files_not_done is not None:
            print(str(len(files_not_done)) + ' files not found: ' + ', '.join(files_not_done))
            return False
        else:
            print('All files done')            
            return True
    
def submit_jobs(fasta_filenames, new_file_path):
    job_filepaths = [os.path.join(new_file_path, fname.replace('.fasta','.job')) for fname in fasta_filenames]
    ips_filepaths = [os.path.join(new_file_path, fname.replace('.fasta','.tsv')) for fname in fasta_filenames]
    #oe_filepaths = [os.path.join(new_file_path, fname.replace('.fasta','.oe')) for fname in fasta_filenames]
    
    num_cores = 4
    mem_gb = 6
    walltime_hours = 48
    job_boilerplate =   ['#!/bin/bash',
                        '#PBS -l nodes=1:ppn={}'.format(num_cores),
                        '#PBS -l walltime={}:00:00'.format(walltime_hours),
                        '#PBS -l mem={}gb'.format(mem_gb)
                        ]
    
    run_ips_command = 'time ~/interproscan/interproscan.sh -f tsv --goterms --iprlookup -pa -i {} -o {}'
    # missing: -i ~/ips_files/1_1.fasta -o ~/ips_files/1_1.tsv
    # will be added in loop below
    
    PBS_job_ids = []
    job_num = 1
    for job_file, ips_file, fasta_file in zip(job_filepaths, ips_filepaths, fasta_filenames):
        with open(job_file,'w') as f:
            for line in job_boilerplate:
                f.write(line + '\n')
            f.write('#PBS -N ips{}\n'.format(job_num))
            f.write('#PBS -e {}\n'.format(os.path.join(new_file_path, str(job_num) + '.e')))
            f.write('#PBS -o {}\n'.format(os.path.join(new_file_path, str(job_num) + '.o')))
            #f.write('cd {}\n'.format(new_file_path))
            f.write(run_ips_command.format(fasta_file, ips_file) + '\n')
        job_num += 1
        process = subprocess.Popen(['qsub',job_file], stdout = subprocess.PIPE)
        stdout = process.communicate()[0].decode('utf-8')
        PBS_job_id = stdout.split('.')[0]
        print("Submitted search job: {}".format(PBS_job_id))
        PBS_job_ids.append(PBS_job_id)
    
    return PBS_job_ids, ips_filepaths
    
def random_access_fasta():
    # Demo on how to open a large fasta file and return sequences by id #
    # unused for now
    def get_id(title):
        return title.split('|', 1)[0]
    
    fasta_file =r"C:\Users\Greg\Documents\python\metaproteomics\1.fasta"
    record = SeqIO.index(fasta_file, "fasta", key_function=get_id)
    
def create_new_files(fasta_file, new_file_path, n_seq, fasta_type):
    '''
    Split (fasta_file) into chunks of (n_seq) each. Put chunks in (new_file_path)
    
    '''
    fasta_parser = SeqIO.parse(fasta_file, "fasta")
    
    if not os.path.isdir(new_file_path):
        os.makedirs(new_file_path)
    
    chunk_num = 1
    f_out_files = []
    f_out_file = os.path.join(new_file_path, str(chunk_num) + '.fasta')
    f_out_files.append(f_out_file)
    
    while write_next_sequences(fasta_parser, f_out_file, n_seq, fasta_type):
        chunk_num += 1
        f_out_file = os.path.join(new_file_path, str(chunk_num) + '.fasta')
        f_out_files.append(f_out_file)
    
    return f_out_files
    
def write_next_sequences(fasta_parser, f_out_file, n_seq, fasta_type):
    '''
    Write n_seq number of fasta sequences to a file.
    
    fasta_parser (generator): generator of fasta sequences. Ex: SeqIO.parse(fasta_file, "fasta")
    f_out_file (str): path to output file
    n_seq (int): number of fasta sequences to write
    fasta_type (function): returns true or false if we will write the sequence
    
    Return False if reached the end of the input fasta file (fasta_parser)
    '''
    f_out_handle = open(f_out_file, 'a')
    while n_seq > 0:
        try:
            f = next(fasta_parser)
            if fasta_type(f.name):
                SeqIO.write(f, f_out_handle, 'fasta')
                n_seq -= 1
        except StopIteration:
            f_out_handle.close()
            return False
    f_out_handle.close()
    return True

def is_forward_refseq(name):
    #accepts a fasta_defline
    return name.count('|ref|') and not name.split('||')[1].startswith('Reverse')

def is_forward_not_refseq(name):
    return not name.count('|ref|') and not name.split('||')[1].startswith('Reverse')
        
def is_forward_uniprot(name):
    return name.lower().count('uniprot') and not name.split('||')[1].startswith('Reverse')

if __name__ == '__main__':
    main()
