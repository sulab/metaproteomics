#!/bin/bash
# Usage: ./fasta_fromto.sh FROM TO path_to_fasta
# Get the `from`-th to the `to`-th fasta entry 
# Numbering starts at 1 but follows python slice notation otherwise [from:to)
# from 1 to 3 returns 2 entries
a=$1
b=$2
c=$((b+a))
FASTA=$3
awk -v f=$a -v t=$b -v RS='>' -v ORS='>' 'BEGIN{printf ">"} NR>t{exit} NR>f && NR<=t' $FASTA | head -n-1
