"""
Created on Wed Oct  8 15:28:18 2014

@author: Greg

Parse tsv file outputed by InterProScan into json docs and insert into mongodb

Example usage:
python3 ~/metaproteomics/IPSParser.py -f 1.tsv -mh localhost -mp 27017 -md test -mc test -fastdb

Shard:
mongos> sh.enableSharding("DomainDB_072114")
mongos> sh.shardCollection("DomainDB_072114.DomainDB_072114", {"_id":1})

Parallel:
ls *.tsv | parallel -j4 'python3 ~/metaproteomics/IPSParser.py -f {} -mh wl-cmadmin -mp 27018 -md DomainDB_072114 -mc DomainDB_072114'
Four jobs:
ls *.tsv | parallel -j4 'python3 ~/metaproteomics/IPSParser.py -appl "SUPERFAMILY,ProSiteProfiles,Hamap,PIRSF" -f {} -mh wl-cmadmin -mp 27018 -md DomainDB_072114 -mc DomainDB_072114'


DomainDB_072114

###################
  *** Schema ***
###################
Domain Documents:
{
    '_id': 'md5sum'
    'd': 
        {
           'memberdb_name' : [ ...domain...],
           'memberdb_name2': [ ...domain...]
        }
    'v': 
        {
           'memberdb_name' : 'version', 
           'memberdb_name2' : 'version'
        }
}
Member DBs: [Coils,Gene3D,Hamap,PANTHER,Pfam,PIRSF,PRINTS,ProSitePatterns,ProSiteProfiles,SMART,SUPERFAMILY,TIGRFAM,ProDom]
Member db versions: http://www.ebi.ac.uk/interpro/release_notes_48.0.html
v48 = {'TIGRFAM': '13.0', 'ProDom': '2006.1', 'SMART': '6.2', 'Hamap': '201311.27', 
'ProSitePatterns': '20.97', 'ProSiteProfiles': '20.97', 'SUPERFAMILY': '1.75', 
'PRINTS': '42.0', 'PANTHER': '9.0', 'Gene3D': '3.5.0', 'PIRSF': '2.84', 
'Pfam': '27.0', 'Coils': '2.2'}

where 'domain' is:
{
	"g" : ["GO:#######",...],                     # list of go terms
    "p" : {                                       # pathways
		"Reactome" : [ "REACT_###", ...	],
		"KEGG" : ["#####", ... ],
		"MetaCyc" : ["PWY-###", ... ],
		"UniPathway" : ["UPA#####", ... ]
	      }
	"ipd" : "Interpro domain description",
	"sd" : "memberdb description",
	"ipa" : "interpro accession",
	"st" : start pos,
	"sa" : "memberdb accession",
	"sc" : score,
	"a" : "memberdb",
	"sp" : stop pos
}
Not required: "g", "p", "ipd", "ipa"


Additional thoughts/ things to add:
 * IPR superfamilies may not have X-refs (to go terms, etc.) whereas their children might. For example:
   IPR001547 has go terms mapped but its parent does not. Proteins with domains annotated to the parent (001aecc550866e4f27fb51c32f5cbf08) lack xrefs
   Download IPS family mapping: ftp://ftp.ebi.ac.uk/pub/databases/interpro/ParentChildTreeFile.txt and propagate matches up/down the tree



"""

from pymongo import MongoClient
import argparse
import glob
from collections import defaultdict
from itertools import groupby

def get_version_dict(version):
    v = {'TIGRFAM': '13.0', 'ProDom': '2006.1', 'SMART': '6.2', 'Hamap': '201311.27', 'ProSitePatterns': '20.97', 'ProSiteProfiles': '20.97', 'SUPERFAMILY': '1.75', 
         'PRINTS': '42.0', 'PANTHER': '9.0', 'Gene3D': '3.5.0', 'PIRSF': '2.84', 'Pfam': '27.0', 'Coils': '2.2'}    
    if version == 48:
        return v
    elif version == 50:
        v.update({'ProSitePatterns': '20.105', 'ProSiteProfiles': '20.105', 'PIRSF': '3.01' })
        return v
    elif version == 55:
        return {'TIGRFAM': '15.0', 'ProDom': '2006.1', 'SMART': '6.2', 'Hamap': '201511.02', 'ProSitePatterns': '20.113', 
        'ProSiteProfiles': '20.113', 'SUPERFAMILY': '1.75', 'PRINTS': '42.0', 'PANTHER': '10.0', 'Gene3D': '3.5.0', 
        'PIRSF': '3.01', 'Pfam': '28.0', 'Coils': 'undefined'}
    else:
        raise ValueError('Add version information for version ' + str(version))
        

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--tsv-file', help = 'Path to input tsv file. If not given, run on all tsv files in cur dir', type = str)
    parser.add_argument('-mh', '--host', help = 'mongodb host', type = str)
    parser.add_argument('-mp', '--port', help = 'mongodb port', type = int)
    parser.add_argument('-md', '--database', help = 'mongodb db', type = str)
    parser.add_argument('-mc', '--collection', help = 'mongodb coll', type = str)
    parser.add_argument('-iv', '--ips-version', help = 'Interproscan version', type = int, default = 48)
    parser.add_argument('-appl', help = 'Analysis run', type = str, default = 'Coils,Gene3D,Hamap,PANTHER,Pfam,PIRSF,PRINTS,ProSitePatterns,ProSiteProfiles,SMART,SUPERFAMILY,TIGRFAM,ProDom')
    parser.add_argument('-fastdb', help = 'specify -appl as "Coils,Gene3D,PANTHER,Pfam,PRINTS,ProSitePatterns,SMART,TIGRFAM"', action='store_true')
    args = parser.parse_args()
    
    # List of member DBs that were searched
    appl = args.appl.split(',') if not args.fastdb else "Coils,Gene3D,PANTHER,Pfam,PRINTS,ProSitePatterns,SMART,TIGRFAM".split(',')
    # Filter version dict to only include these
    version = get_version_dict(version = args.ips_version)
    version = {key: value for key, value in version.items() if key in appl}

    p = IPSParser(args.host, args.port, args.database, args.collection)

    p.parse_tsv(args.tsv_file, version)   

class IPSParser():
    def __init__(self, host='localhost', port=27017, mongo_db = 'domainDB', mongo_coll = 'domainDB'):

        client = MongoClient(host, port)
        self.db = client[mongo_db][mongo_coll]

    def parse_tsv(self, in_file, version = None):
        '''
        Interpro tsv parser.
        See format here: https://code.google.com/p/interproscan/wiki/OutputFormats
        Parses the entire file, then performs a bulk update. Best to parallelize using multiple files.
        
        '''
        print(in_file)
            
        line_keys = ['accession','md5','length','analysis','sig_accession','sig_description','start','stop','score','status','date','ipr_accession','ipr_description','GO','pathway']
        line_type = [str, str, int, str, str, str, int, int, float, str, str, str, str, str, str]
        line_type_ProSitePatterns = [str, str, int, str, str, str, int, int, str, str, str, str, str, str, str]
        analysis_keys = ['analysis','sig_accession','sig_description','start','stop','score','ipr_accession','ipr_description','GO','pathway']
    
        bulk = self.db.initialize_unordered_bulk_op()
        with open(in_file) as f:
            iter_hash = groupby(f, lambda x: x.split('\t')[1])
            for hash, hash_lines in iter_hash:
                analysis_records = []
                for line in hash_lines:
                    line = line.strip().split('\t')
                    if line[3] == 'ProSitePatterns' or line[3] == 'Coils':
                        line = [x(y) for x, y in zip(line_type_ProSitePatterns, line)]
                    else:
                        line = [x(y) for x, y in zip(line_type, line)]
                    record = dict(zip(line_keys, line)) #dictionary from one line in interpro tsv file
                    analysis_record = {k: record[k] for k in analysis_keys if k in record} #record with only analysis_keys
                    analysis_record = abbrev(analysis_record) # record with keys abbreviated
                    # Remove blank keys
                    analysis_record = dict([(key, analysis_record[key]) for key in analysis_record.keys() if analysis_record[key] != ''])
                    if 'p' in analysis_record.keys():
                        analysis_record['p'] = parse_pathways(analysis_record['p'])
                    if 'g' in analysis_record.keys() and analysis_record['g'] != '':
                        analysis_record['g'] = analysis_record['g'].split('|')
                    analysis_records.append(analysis_record)
                
                # For each hash
                record = {'d': group_by_db(analysis_records)}
                record.update({'v': version})
                record = dot_dict(record)
                bulk.find({'_id': hash}).upsert().update({'$set':record})
        
        bulk.execute()
        
    def clear_db(self):
        print("Are you sure you want to clear db? [y/n]")
        print(self.db)
        yes = {'yes', 'y', 'ye'}
        no = {'no', 'n'}
        choice = input().lower()
        if choice in yes:
            self.db.remove()
            print('done')
        elif choice in no:
            return
        else:
            print("Please respond with 'yes' or 'no'")
            
def group_by_db(analysis_records):
    new_records = defaultdict(list)
    for d in analysis_records:
        new_records[d['a']].append(d)
    return new_records


def dot_dict(d):
    '''
    Convert {'z': {'a':1, 'b':2}, 'x': {'a':23}}
    to: {'z.a': 1, 'z.b': 2, 'x.a': 23}
    For mongodb updates
    '''
    d = {key:value for key, value in d.items() if key is not '_id'}
    d_dot = dict()
    for pkey in d.keys():    
        for key in d[pkey].keys():
            d_dot[pkey + '.' + key] = d[pkey][key]
    return d_dot

def abbrev(d):
    key_lookup = dict(zip(['analysis','sig_accession','sig_description','start','stop','score','ipr_accession','ipr_description','GO','pathway', 'id'], 
                          ['a','sa','sd','st','sp','sc','ipa','ipd','g','p', 'id']))
    for key in list(d.keys()):
        d[key_lookup[key]] = d.pop(key)
    return d

def unabbrev(d):
    key_lookup = dict(zip(['a','sa','sd','st','sp','sc','ipa','ipd','g','p', 'id'],
                          ['analysis','sig_accession','sig_description','start','stop','score','ipr_accession','ipr_description','GO','pathway', 'id']))
    for key in list(d.keys()):
        d[key_lookup[key]] = d.pop(key)
    return d

def parse_pathways(txt):
    # txt = "KEGG: 00020+1.1.1.42|KEGG: 00480+1.1.1.42|KEGG: 00720+1.1.1.42|MetaCyc: PWY-5913|MetaCyc: PWY-6549|MetaCyc: PWY-6728|MetaCyc: PWY-6969|MetaCyc: PWY-7124|MetaCyc: PWY-7254|MetaCyc: PWY-7268"
    # return: {'KEGG': ['00020+1.1.1.42', '00480+1.1.1.42', '00720+1.1.1.42'], 'MetaCyc': ['PWY-5913',  'PWY-6549',  'PWY-6728',  'PWY-6969',  'PWY-7124',  'PWY-7254',  'PWY-7268']}    
    dd = defaultdict(list)
    for entry in txt.split('|'):
        entry_split = entry.split(':')
        dd[entry_split[0].strip()].append(entry_split[1].strip())
    return dict(dd)

if __name__ == '__main__':
    main()
    #pass
