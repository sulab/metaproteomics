# -*- coding: utf-8 -*-
"""
>Cluster 0
0	36805aa, >5618922||gi|78188592|ref|YP_378930.1|... *

to

>Cluster 0
0	36805aa, >5618922... *

"""
import sys
for line in sys.stdin:
    line = line.rstrip()
    if "||" not in line:
        print(line)
        continue
    bline = line[:line.index("||")]
    eline = line[line.rfind("..."):]
    line = bline+eline
    print(line)
