# -*- coding: utf-8 -*-
"""
mmseq
"""
#clstr_file = "/home/gstupp/eggNOG/eggnogv4.proteins.valid.bactNOG.out.fa"
def cluster_chunk(clstr_file):
    lines = []
    for line in open(clstr_file):
        line = line.strip()
        if line[0] == '>':
            if lines:
                yield lines
                lines = []
        else:
            lines.append(line)
