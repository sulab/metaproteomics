"""
Started to replicate cd-hit-v4.6.1-2012-08-27/clstr_merge.pl
Does not actually do the merge. Just checks to see if the file parses correctly
"""

f1 = """>Cluster 0
0	36805aa, >5618922... *
1	36805aa, >23125518... at 100.00%
2	36805aa, >74186607... at 100.00%
>Cluster 1
0	34350aa, >61524516... at 74.16%
1	27051aa, >61524517... at 86.06%
2	33423aa, >61524518... at 75.59%
3	35991aa, >61524519... *
4	34484aa, >61524520... at 73.07%
>Cluster 2
0	29409aa, >61538870... *
1	29361aa, >61538871... at 89.21%
""".split("\n")
f2 = """>Cluster 0
0	36805aa, >5618922... *
>Cluster 1
0	35991aa, >61524519... *
1	33467aa, >165672414... at 70.32%
2	33467aa, >165675367... at 70.32%
>Cluster 2
0	29409aa, >61538870... *
""".split("\n")
f1 = iter(f1)
f2 = iter(f2)

def get_next_chunk(f):
    chunk = []
    for line in f:
        if line.startswith('>Cluster'):
            if chunk:
                chunk = [x.split(None,1)[1] for x in chunk] #remove numbering
                yield chunk_num, chunk
                chunk = []
            chunk_num = int(line.split(' ')[-1])
        else:
            chunk.append(line)
    if chunk:            
        chunk = [x.split(None,1)[1] for x in chunk if x] #remove numbering
        yield chunk_num, chunk

def do_merge_chunk(chunk1, chunk2):
    chunk2_master = [x for x in chunk2 if '*' in x][0]
    if chunk2_master in chunk1:
        merge_chunk(chunk1, chunk2)
    else:
        print("Error:" + str(chunk2))

def merge_chunk(chunk1, chunk2):
    pass
    
#%%
import os
os.chdir("/home/gstupp/metaproteomics/cluster_compil")
f1 = open("071414_ComPIL_forwardonly_cluster10_0.7s0.5.clstr")
f2 = open("month-new.clstr")

chunker1 = get_next_chunk(f1)
chunker2 = get_next_chunk(f2)
#((chunk_num1, chunk1),(chunk_num2, chunk2)) = next(zip(chunker1, chunker2))
#%%
for ((chunk_num1, chunk1),(chunk_num2, chunk2)) in zip(chunker1, chunker2):
    if chunk_num1 == chunk_num2:
        do_merge_chunk(chunk1, chunk2)
    else:
        print("Error: " + chunk_num1 + ", " + chunk_num2)
    if chunk_num1%100000 == 0:
        print(chunk_num1)

    