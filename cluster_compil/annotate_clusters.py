
# multiprocess version of code to annotate protein clusters

from pymongo import MongoClient
from itertools import chain
from multiprocessing import Pool
HOST = 'wl-cmadmin'

#%% Annotate each cluster
client = MongoClient(HOST, 27018)
hashDB = client.HashDB_072114.HashDB_072114
domainDB = client.DomainDB_072114.DomainDB_072114

def update_cluster_annotations(doc, verbose = False):
    hashes = [x['_id'] for x in hashDB.find({'pID':{'$in': doc['pID']}}, {'_id': True })]
    domain_list = [x['d'] for x in domainDB.find({'_id':{'$in': hashes}})]    
    db_domain_list = [{key:value for key,value in x.items()} for x in domain_list]
    # Collapse db : domains dict into list of domains
    domain_list = list(chain(*[list(chain(*x.values())) for x in db_domain_list]))
    ipa = set([domain['ipa'] for domain in domain_list if 'ipa' in domain])
    go = set(chain(*[domain['g'] for domain in domain_list if 'g' in domain]))
    sa = set([domain['sa'] for domain in domain_list if 'sa' in domain])
    if ipa:
        doc['ipa'] = list(ipa)
    if go:
        doc['go'] = list(go)
    if sa:
        doc['sa'] = list(sa)
    doc['annotated'] = True
    clusterDB.update({'_id': doc['_id']}, doc)

#%% Example
#cluster = {'_id': 73289068, 'pID': [73289068, 5021269, 11033336, 32921006, 73274855, 4766684, 32925152, 32239377, 20980547, 33434698, 33434818, 33436710, 73278695, 33438249, 33434817, 33436711, 19126449, 73283356, 32124280, 34625332, 73272078, 34620431, 29699230]}

#%% Parse clusterDB
from pymongo import MongoClient
name = "071414_ComPIL_forwardonly_0_7"
clusterDB = MongoClient('wl-cmadmin')[name][name]

def generate_docs(clusterDB):
    for doc in clusterDB.find({'annotated': {'$exists': False}}):
        yield doc

parser = generate_docs(clusterDB)

pool = Pool()
p = pool.imap_unordered(update_cluster_annotations, parser, chunksize=5000)

pool.close()
pool.join()

    