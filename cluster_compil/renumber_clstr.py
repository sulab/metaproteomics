"""
renumber a cd-hit clstr file
"""
import shutil
import sys
import tempfile
in_file = sys.argv[1]
t = tempfile.NamedTemporaryFile(encoding = 'utf-8', mode='w',delete=False)
counter = 0
for line in open(in_file, 'rb'):
    line = line.decode('utf-8').rstrip()
    if line.startswith(">"):
        print(">Cluster {}".format(counter), file = t)
        counter += 1
    else:
        print(line, file = t)
t.close()
shutil.move(t.name, in_file)