import os
import sys
import json
import argparse
from metaproteomics.cluster.submit_blazmass import set_params, make_blazmass_params, rewrite_job_files

def main():
    
    parser = argparse.ArgumentParser(description = """This script will update 
        a single parameter for a given params file. If no filenames are given, 
        updates will be applied to all param files in the directory.  Options 
        allow for updating of a files generated from the params file""")
    parser.add_argument('key', type = str)    
    parser.add_argument('val', type = str) 
    parser.add_argument('files', nargs='*', type = str,
                        default=[f for f in os.listdir() if f.endswith('.json')])
    parser.add_argument('-b', action='store_true', 
                        help = 'Regenerate blazmass.params from new param values')
    parser.add_argument('-j', action='store_true',
                        help = 'Regenerate all .job files using updated params')
    args = parser.parse_args()

    
    for file in args.files:
        params = update_params(file, args) 
    
    if args.b:
        regen_blazmass(params)

    if args.j:
        regen_jobs(params)
    
    sys.exit(0)        

def update_params(file, args):
    with open(file, 'r') as fp:
        params = json.load(fp)
    
    if args.val in ['False', 'false']:
        args.val = False
    
    # Check if key exists and maintain type    
    key_type = str    
    if args.key in params.keys():
        key_type = type(params[args.key])
    params[args.key] = key_type(args.val)

    with open(file, 'w') as fp:
        json.dump(params, fp, indent=2, sort_keys=True)
    return params

def regen_blazmass(params):
    params = set_params(argparse.Namespace(**params), '')
    make_blazmass_params(params)

def regen_jobs(params):
    params = set_params(argparse.Namespace(**params), '')
    rewrite_job_files(params)


if __name__ == '__main__':
    main()
