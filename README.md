# Microbiome Metaproteomics
==============

Sandip Chatterjee / Greg Stupp
-----------------

## Collection of scripts written for analysis of microbiome metaproteomics data

### build_compil

#### create_compil (bash)
- for creating ComPIL databases ProtDB, MassDB, SeqDB
- requires Python, Java, and a few other dependencies

### cluster

#### submit_blazmass.py (Python3)
- for dividing up 1 or more MS2 files into smaller MS2 files and submitting jobs on the Garibaldi cluster

### file_processing

#### ms2/mass\_correct.py (Python3)
- For correcting mass drift in MS2 files
- Corrects all ions or precursor ions only

