"""
hash_group_to_json.py

Input: renumbered_071414_indexDB_forward.fasta.hash.sort.group
"hash,comma-separated list of protDB_IDs"
Example: 
000002722cb70da7f3426704b59d7039,40911822
000002aa15832b94a71e3c7de643c267,29110293,67374782

Output: json doc
Example:
{'_id':'000002722cb70da7f3426704b59d7039', 'pID': [40911822]}
{'_id':'000002aa15832b94a71e3c7de643c267', 'pID': [29110293, 67374782]}

"""

import sys
import json
for line in sys.stdin:
    line_split = line.rstrip().split(',')
    print(json.dumps({'_id':line_split[0], 'pID': [int(x) for x in line_split[1:]]}))
  
