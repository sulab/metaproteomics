## Steps to generate non-redundant protein db (HashDB)
##### Used as a cross-reference between hashes and ProtDB IDs, and to quickly compile list of identical protein sequences
Format: `{ "_id" : hash (str), "pID" : list of ProtDB IDs (list of ints)}`

1. Generate fasta file containing only forward sequences: remove_reverse_fasta_records.py
    - Usage: `cat fasta_file.fasta | python remove_reverse_fasta_records.py > forward_fasta_file.fasta`
    - Parallelized Example: `cat renumbered_071414_indexDB_reversed.fasta | parallel -j4 --block 500K --pipe python ~/metaproteomics/make_nrdb/remove_reverse_fasta_records.py > renumbered_071414_indexDB_forward.fasta`

2. Hash all sequences. Generates text file with "protDB_ID, hash", one per line
    - Usage: `cat forward_fasta_file.fasta | python3 hashing.py --hash > forward_fasta_file.fasta.hash`
    - Parallelized Example: `cat renumbered_071414_indexDB_forward.fasta | parallel --recstart '>' --recend '\n' --pipe python3 hashing.py --hash > renumbered_071414_indexDB_forward.fasta.hash`

3. Sort hashes (sort file from step2 by 2nd column)
    - Example: `time sort -t, -k2 renumbered_071414_indexDB_forward.fasta.hash > renumbered_071414_indexDB_forward.fasta.hash.sort`

4. Group sorted hashes (don't parallelize this). Output flat text file with "hash,comma-separated list of protDB_IDs". Example: `00000c0f48bb052c92d78a3408f6dd91,18766308,73012192,9574399`
    - Usage: `cat forward_fasta_file.fasta.hash.sort | python3 hashing.py --group > forward_fasta_file.fasta.hash.group`

5. Import into mongodb (piped through python script that converts to json doc)
    - Usage: `cat forward_fasta_file.fasta.hash.group | python3 hash_group_to_json.py | mongoimport -d DB -c DB`
    - Parallelized Example: `cat 071414_indexDB_numbered.fasta.hash.group | python3 ~/metaproteomics/make_nrdb/hash_group_to_json.py | parallel -j6 --pipe mongoimport -d HashDB_072114 -c HashDB_072114`

6. Create index on pID
    - Example: > `mongo HashDB_072114 --eval "db.HashDB_072114.ensureIndex({'pID': true})"`

### To add new sequences to existing hashDB
1. Follow steps 1-4 above on new fasta file.

2. Create json doc.
    - Example: `cat forward_fasta_file.fasta.hash.group | python3 hash_group_to_json.py > forward_fasta_file_hash.json`

3. Update mongo hashdb:
    - Example: `nohup cat forward_fasta_file_hash.json | parallel -j8 -N1000 --pipe "python3 add_to_hash_db.py HashDB_mouse_mgm" > insert_hash.out &`
    

## Steps to generate taxonomy db (TaxDB)
##### Used to look up taxonomy information for ProtDB IDs
Format: 

    {
        "_id" : ProtDB ID,
        "exact_match" : true or false, (was the tax info in the fasta defline exactly matched to ncbi taxonomy file?)
        "matched_organism" : organism name, (entry in ncbi tax file) 
        "organism" : organism name, (entry in fasta defline) 
        "taxid" : tax_id (tax ID from NCBI taxonomy) or 'none' if not found
    }

1. Parse NCBI and uniprot taxonomy information. Writes a tax.json file (contains ncbi taxonomy info). See help in file
    - Usage: `python3 metaproteomics/taxonomy_parser.py`

2. Set up SimString for fuzzy matching (python2 only)
    - See lookup_tax.py for installation
    - Usage: `python lookup_tax.py --build --tax tax.db --json tax.json`

3. Extract all the deflines from forward fasta file
    - `grep '^>' 071414_indexDB_numbered.fasta | gzip > 071414_indexDB.defline.gz`

4. Parse deflines and lookup tax info (python2 only)
    - `zcat 071414_indexDB.defline.gz | parallel -j8 --block 1000K --pipe python lookup_tax.py --tax tax.db --json tax.json > tax_result`

5. Mongo Import
    - `cat tax_result | parallel -j8 --pipe mongoimport -d TaxDB_072114 -c TaxDB_072114`
