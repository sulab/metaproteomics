#!/usr/bin/python
'''
Parse and Lookup Taxonomy ID from fasta defline

Used SimString for fuzzy matching (python 2.7)
http://www.chokkan.org/software/simstring/
http://www.chokkan.org/software/simstring/swig/
Had compilation erros, follow steps below: to fix
https://github.com/chokkan/simstring/issues/9
include/simstring/memory_mapped_file_posix.h:43 +#include <unistd.h>
swig/python/export_wrap.cpp:10 +#include <stddef.h>

'''
from __future__ import print_function
import json
import sys
try:
    import simstring
except ImportError:
    sys.stderr.write("No simstring available, no fuzzy matching\n")
    fuzzy = False

import re
import sys
import argparse

class TaxonomyLookup(object):
    def __init__(self, taxdb_path, json_file):
        if fuzzy:
            self.db = simstring.reader(taxdb_path)
        
        f = open(json_file)
        self.sci_tax = dict()
        self.uni_tax = dict()
        for line in f:
            x = json.loads(line)
            self.sci_tax[x['scientific_name'].lower()] = x['taxid']
            if 'uniprot_name' in x:
                self.uni_tax[x['uniprot_name'].lower()] = x['taxid']
                
    def lookup(self, organism):
        result = self.sci_tax.get(organism.lower(), None)
        org_result = organism
        exact_match = True
        if result == None:
            result = self.uni_tax.get(organism.lower(), None)
        if result == None and fuzzy:
            simstring_result = self.db.retrieve(organism)
            if simstring_result:
                org_result = simstring_result[0]
                result = self.sci_tax[org_result]
                exact_match = False
        return (result, org_result, exact_match)
        #self.db.retrieve('trichoderma sp.')

def fasta_to_organism_uniprot(fasta_defline):
    g = re.search('OS=(.*?)( [A-Z]{2}=|\|UniProt)', fasta_defline)
    # Matches everything after 'OS=' until either : (any two uppercase letters, = ) or (|UniProt)
    if g:
        return g.groups()[0]
    else:
        #logger.warn('Unmatched organism' + fasta_defline)
        return None


def fasta_to_organism_refseq(fasta_defline):
    '''    
    refseq defline are retarded and have no standardized way of noting organism with square brackets
    testers:
    string = "[GSEE] tandem repeats [Invertebrate iridescent virus 30]"
    string = "coat protein [Euphorbia mosaic virus - A [Mexico:Yucatan:2004]]" #pID: 745
    string = "[citrate [pro-3S]-lyase] ligase [Vibrio cholerae]"
    '''
    # If there are 8 pipes (coming from fasta file)
    if fasta_defline.count('|') == 8:
        # Split defline by '|'. Take the 6th
        txt = fasta_defline.split('|')[6].strip()
    elif fasta_defline.count('|') == 4:   
        txt = fasta_defline.split('|')[-1].strip()
    else:
        txt = fasta_defline
    
    try:
        if txt.count('[') != txt.count(']'):
            # logger.warn('Malformed defline: ' + fasta_defline)
             # Just return whatever is between the last '[' and the last ']'   
            txt_flip = txt[::-1]
            organism = txt_flip[txt_flip.find(']')+1:txt_flip.find('[')][::-1]
            return organism
            
    
        brackets = list(parse_brackets(txt))
        # if there are nested brackets
        if max(list(zip(*brackets))[0]) > 0:
            # take the string at level 0
            organism = [x[1] for x in brackets if x[0] == 0]
            return organism[-1]
        else:
            # there are no nested brackets
            # take the last brackets
            return brackets[-1][1]
    except Exception:
        return None
        
def parse_brackets(string):
    """Generate parenthesized contents in string as pairs (level, contents).
    http://stackoverflow.com/questions/4284991/parsing-nested-parentheses-in-python-grab-content-by-level
    """
    stack = []
    for i, c in enumerate(string):
        if c == '[':
            stack.append(i)
        elif c == ']' and stack:
            start = stack.pop()
            yield (len(stack), string[start + 1: i])

def fasta_to_organism(fasta_defline, source = None):
    if source and source.lower() == "refseq":
        return fasta_to_organism_refseq(fasta_defline)
    elif source and source.lower() == "uniprot":
        return fasta_to_organism_uniprot(fasta_defline)
    # if passing a dictionary from ProtDB
    if type(fasta_defline) is dict:
        if 'r' in fasta_defline:
            if fasta_defline['r'].lower().count('refseq') or fasta_defline['r'].lower().count('hmp_reference_genomes'):
                return fasta_to_organism_refseq(fasta_defline['d'])
            elif fasta_defline['r'].lower().count('uniprot'):
                return fasta_to_organism_uniprot(fasta_defline['d'])
        #print('No db info: ' + str(fasta_defline), file = sys.stderr)
    # If passing a string from a fasta file
    else:
        linesplit = fasta_defline.split('|')
        repository = linesplit[-2].lower()
        if repository == 'refseq' or repository == 'hmp_reference_genomes':
            return fasta_to_organism_refseq(linesplit[-3])
        elif repository.count('uniprot'):
            return fasta_to_organism_uniprot(fasta_defline)
        print('No db info: ' + fasta_defline, file = sys.stderr)
    return None

def build_simstring(json_file, taxdb_path):
    f = open(json_file)
    db = simstring.writer(taxdb_path)
    for line in f:
        x = json.loads(line)
        db.insert(str(x['scientific_name'].lower()))
    db.close()
    f.close()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--tax', help='path to simstring db', type = str, default = '/home/gstupp/taxonomy/tax.db')
    parser.add_argument('--json', help = 'path to tax json file', type = str, default = '/home/gstupp/taxonomy/tax.json')
    parser.add_argument('--source', help = 'one of ["refseq","uniprot"]')
    parser.add_argument('--build', help = 'build simstring db', action = 'store_true')
    args = parser.parse_args()
    
    taxdb_path = args.tax
    json_file = args.json

    if args.build:
        build_simstring(json_file, taxdb_path)
        return
    
    looker = TaxonomyLookup(taxdb_path, json_file)
    for line in sys.stdin:
        protDB = int(line.split('|')[0][1:].strip())
        organism = fasta_to_organism(line, source = args.source)
        if organism == None:
            result = {'taxid': None, '_id': protDB}
        else:
            (taxID, org_result, exact_match) = looker.lookup(organism)
            result = {'taxid': taxID, 'organism': organism, 'matched_organism': org_result, 'exact_match': exact_match, '_id': protDB}
        print(json.dumps(result))
    
    if fuzzy:
        looker.db.close()
###

if __name__ == '__main__':
    main()
