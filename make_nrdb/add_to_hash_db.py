"""
add_to_hash_db.py
#==============================================================================
# Add json file to existing HASH db
#==============================================================================
Runs find and update with upsert. The update command runs $addToSet

Example usage:
nohup cat test.json | parallel -j8 -N1000 --pipe "python3 add_to_hash_db.py HashDB_20151009" > insert_hash.out &

example doc:
{"pID": [167688981, 168069570], "_id": "000fa8ba4cc46f2be7730fdc6960e804"}

"""
from pymongo import MongoClient
import json
import sys
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('db_name', help='db and coll name (ex: HashDB_20151009)', type=str)
    parser.add_argument('--host', help='mongo host', type=str, default="wl-cmadmin")
    parser.add_argument('--port', help='mongo port', type=int, default=27018)
    
    args = parser.parse_args()
    db_name = args.db_name
    coll_name = db_name
    client = MongoClient(args.host, args.port)
    db_coll = client[db_name][coll_name]
    bulk = db_coll.initialize_unordered_bulk_op()
        
    for count,line in enumerate(sys.stdin):
        doc = json.loads(line)
        bulk.find({'_id': doc['_id']}).upsert().update_one({'$addToSet': {"pID": {"$each": doc["pID"]}}})

    print(bulk.execute())
