# -*- coding: utf-8 -*-
"""
Created on Wed Oct 15 10:15:54 2014

@author: Greg

#Usage: --hash
#Parse fasta file of format: 
#    >209||gi|430025802|ref|YP_007195275.1| VP2 [Cebus albifrons polyomavirus 1]
#    DNFJDKFJSADFJSDGF...
#Output:
#    209,00000083b5cacb61de38fc94dffce1b9


Usage --group
# Parse file of format:
#    209,00000083b5cacb61de38fc94dffce1b9
#    249,00000083b5cacb61de38fc94dffce1b9
#    555,31636b7f94950bfa4d272098c7bd0a66
#Output:
#    00000083b5cacb61de38fc94dffce1b9,209,249
#    31636b7f94950bfa4d272098c7bd0a66,555
"""


import hashlib
import argparse
import sys
from Bio import SeqIO
from itertools import groupby

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--hash', help='hash (md5sum) fasta file', action='store_true')
    parser.add_argument('--group', help = 'Group sorted hashes', action='store_true')
    args = parser.parse_args()
    
    if args.hash:
        hash_fasta()
        
    if args.group:
        group_sorted_hashes()

def hash_fasta():    
    fasta_parser = SeqIO.parse(sys.stdin, "fasta")
    for seq in fasta_parser:
        hash = hashlib.md5(str(seq.seq).encode('utf-8')).hexdigest()
        print(seq.description.split('||')[0] + ',' + hash)
        
def group_sorted_hashes():
    grouper = groupby(sys.stdin, lambda x: x.split(',')[1])   
    for hash, p in grouper:
        print(hash.strip() + ',' + ','.join([x.split(',')[0] for x in p]))        

if __name__ == '__main__':
    main()

