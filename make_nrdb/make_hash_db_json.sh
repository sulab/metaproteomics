#==================
## make_hash_db_json.sh
#==================
# Generate a json file with hashes or protein sequences.
# For making a new db, just import the json using mongoimport
# For updating a hashdb, add through add_to_hash_db.py


# this is a numbered fasta file. If its from the build_compil pipeline, it has reverse in it too
FASTA=$1

F_FASTA=${FASTA/".fasta"/"_for.fasta"}
cat $FASTA | parallel --recstart '>' --recend '\n' --pipe "python3 remove_reverse_fasta_records.py" > "$F_FASTA"

HASH=${F_FASTA/"_for.fasta"/".hash"}
cat $F_FASTA | parallel --recstart '>' --recend '\n' --pipe "python3 hashing.py --hash" > "$HASH"

SORT=${HASH}.sort
sort -t, -k2 $HASH > $SORT

GROUP=${SORT/".sort"/".group"}
cat $SORT | python3 hashing.py --group > $GROUP

JSON=${GROUP}.json
cat $GROUP | parallel --pipe "python3 hash_group_to_json.py" > "$JSON"
