#!/usr/bin/env python3

# submit_blazmass.py
#
# For splitting an input MS2 file and submitting to the Garibaldi cluster
# (need to load Python3 module using 'module load python/3.3.2')
# 
# Sandip Chatterjee
# March 6, 2015
#
# usage: submit_blazmass.py [-h] [-s] [-c] [-p SPLITPREFIX] MS2_input split_n

# positional arguments:
#   MS2_input             Path to input MS2 file
#   split_n               Split input file into n subfiles

# optional arguments:
#   -h, --help            show this help message and exit
#   -s, --submit          Submit jobs to Garibaldi queue
#   -c, --cleanup         Run final cleanup job after all search jobs complete
#   -p SPLITPREFIX, --splitprefix SPLITPREFIX
#                         Prefix for split MS2 files (default is input filename)

# Some typical arguments for indexDB search on MicroCloud sharded MongoDB:
# python3 submit_blazmass.py -s -n 4 -m 12 all 25
# "-n 4"  -- requests 4 cores (since mongos is being started, runs Blazmass with n-1 = 3 search threads)
# "-m 12" -- requests 12GB of RAM (allocating 4GB per search thread -- probably necessary for indexDB search since all candidates are scored and stored in RAM)
# "all"   -- MS2 filename to split and submit. "all" means submit all MS2 files in directory (anything matching *.ms2)
# "25"    -- splits each input MS2 file into 25 chunks with roughly equal numbers of scans


import os
import sys
import glob
import socket
import argparse
import subprocess

def main():

    new_file_directory = 'dummy'

    parser = argparse.ArgumentParser()
    parser.add_argument('MS2_input', help='Path to input MS2 file (use "all" for all .ms2 files in current directory)')
    parser.add_argument('split_n', help='Split each input file into n subfiles', type=int)
    parser.add_argument('-s', '--submit', help='Submit jobs to Garibaldi queue', action='store_true')
    parser.add_argument('-c', '--cleanup', help='Run final cleanup job after all search jobs complete', action='store_true')
    parser.add_argument('-n', '--numcores', help='Number of cores per job', type=int)
    parser.add_argument('-t', '--numthreads', help='Number of threads per search job', type=int)    
    parser.add_argument('-m', '--memgb', help='Amount of available RAM per job (in GB)', type=int)
    parser.add_argument('-w', '--walltime', help='Amount of walltime to request per job (in hours)', default=24, type=int)
    parser.add_argument('-p', '--splitprefix', help='Prefix for split MS2 files (default is input filename)')
    parser.add_argument('--nomongos', help='Don\'t use nearby cluster mongos servers', action='store_true')

    args = parser.parse_args()

    MS2_file = args.MS2_input

    if MS2_file == 'all':
        MS2_files = glob.glob('*.ms2')
        for MS2f in MS2_files:
            success = split_and_submit_one_MS2(MS2f, new_file_directory, args)
            if success:
                print('Jobs submitted successfully for MS2 file {}\n'.format(MS2f))
            else:
                print('!! Error submitting jobs for MS2 file {}'.format(MS2f))
    else:
        success = split_and_submit_one_MS2(MS2_file, new_file_directory, args)
        if success:
            print('Jobs submitted successfully for MS2 file {}\n'.format(MS2_file))
        else:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'+len(MS2_file)*'!')
            print('!! Error submitting jobs for MS2 file {} !!'.format(MS2_file))
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'+len(MS2_file)*'!')

def split_and_submit_one_MS2(MS2_file, new_file_directory, args):

    with open(MS2_file) as MS2_input:
        MS2_scans = create_MS2_chunks(MS2_input)

    if not MS2_scans:
        print('No scans in MS2 file {} -- skipping'.format(MS2_file))
        return False

    if args.splitprefix:
        filename_prefix = args.splitprefix
    else:
        filename_prefix = MS2_file.replace('.ms2','')

    n = args.split_n

    MS2_scan_blocks = split_MS2_scans(MS2_scans, n)

    new_MS2_filenames = create_MS2_files(MS2_scan_blocks, filename_prefix, new_file_directory)

    num_jobs = len(new_MS2_filenames)

    if args.submit:
        if 'garibaldi' in socket.gethostname() or 'ims' in socket.gethostname():
            submit_jobs(new_MS2_filenames, new_file_directory, MS2_file, args)
            return True
        else:
            print("Jobs need to be submitted from garibaldi or ims nodes")
            return False
    else:
        return True

def ask_about_submitting_jobs(num_jobs):

    ''' prompt user about submitting jobs to garibaldi cluster '''

    answer = input("Submit "+str(num_jobs)+" jobs to cluster? (y/n) ")

    if  answer == 'y' or answer == 'yes':
        submit_flag = True
    else:
        submit_flag = False

    return submit_flag

def extract_H_lines(MS2_input):

    ''' extracts the first set of H lines from input and returns as a list, 1 item per line '''

    H_lines = []

    while True:
        line = MS2_input.readline()
        if line[:2] == 'H\t':
            H_lines.append(line)
        if not line:
            break
        if H_lines and line[:2] != 'H\t':
            break

    return H_lines

def create_MS2_chunks(MS2_input):

    ''' returns a list of "chunks" of the MS2 file, with one chunk per scan '''

    ms2_scan_chunks = []
    current_chunk = []

    for line in MS2_input:
        if line[:2] != 'H\t':
            if line[:2] == 'S\t' and current_chunk:
                ms2_scan_chunks.append(current_chunk)
                # do something with scan data block (current_chunk) here!

                current_chunk = []  ## reset current block
                current_chunk.append(line)
            else:
                current_chunk.append(line)
    else:
        ms2_scan_chunks.append(current_chunk) ## add final block to chunk after for loop ends

    print("Parsed", len(ms2_scan_chunks), "scans from MS2 input")

    return ms2_scan_chunks

def split_MS2_scans(MS2_scans, n):

    ''' splits a list of MS2 chunks into n sublists '''
    ''' (generator function) '''

    return (MS2_scans[i::n] for i in range(n if n < len(MS2_scans) else len(MS2_scans)))

def create_MS2_files(MS2_scan_blocks, filename_prefix, new_file_directory, H_lines=[]):

    ''' creates new MS2 files based on filename prefix '''

    if not os.path.isdir(new_file_directory):
        os.makedirs(new_file_directory)

    new_MS2_filenames = []

    for block_num, MS2_block in enumerate(MS2_scan_blocks):
        new_file_name = filename_prefix+'_'+str(block_num+1)+'.ms2'
        with open(new_file_directory+'/'+new_file_name, 'w') as f:
            for chunk in MS2_block:
                f.write(''.join(H_lines))
                f.write(''.join(chunk))
            print(len(MS2_block), "scans written to", new_file_name)
        new_MS2_filenames.append(new_file_name)

    print("Created", len(new_MS2_filenames) ,"MS2 files")

    return new_MS2_filenames

def submit_jobs(MS2_filenames, new_file_directory, MS2_file, args):

    ''' creates .job files and submits jobs to Garibaldi cluster (PBS scheduler) '''

    if not os.path.isdir(new_file_directory+'/resubmitted'):
        os.makedirs(new_file_directory+'/resubmitted')

    job_filepaths = [new_file_directory+'/'+fname.replace('.ms2','.job') for fname in MS2_filenames]
    sqt_output = [new_file_directory+'/'+fname.replace('.ms2','.sqt') for fname in MS2_filenames]

    if 'garibaldi' in socket.gethostname():
        cluster = 'garibaldi'
        mongos_node_prefix = 'node'
        home_directory = '/gpfs/home/sandip'
    elif 'ims' in socket.gethostname():
        cluster = 'ims'
        mongos_node_prefix = 'imsb'
        home_directory = '/gpfs/ims/home/sandip'
    else:
        print('Unrecognized cluster... exiting') # shouldn't ever get here...
        sys.exit(1)

    blazmass_jar = '{}/blazmass_search/blazmass/blazmass.jar'.format(home_directory)

    # num_cores is the number of cores requested in the PBS job script
    if args.numcores:
        num_cores = args.numcores
    else:
        num_cores = 9

    # num_threads is the number of threads to run Blazmass using (should be <= num_cores)
    if args.numthreads:
        num_threads = args.numthreads
        if num_threads > num_cores:    # don't want 18 threads running on 16 cores, for example
            num_threads = num_cores
    else:
        # if args.nomongos:
        #     num_threads = num_cores
        # else:
        #     num_threads = num_cores - 1 # allowing an extra core to handle mongos CPU usage
        num_threads = num_cores
    if num_threads < 1:
        num_threads = 1

    if args.memgb:
        mem_gb = args.memgb
    else:
        mem_gb = num_threads * 4 # allowing 4GB RAM per thread

    walltime_hours = args.walltime

    cputime_hours = walltime_hours * num_threads

    job_boilerplate =             ['#!/bin/bash',
                                '#PBS -l nodes=1:ppn={}'.format(num_cores),
                                '#PBS -l cput={}:00:00'.format(cputime_hours),
                                '#PBS -l walltime={}:00:00'.format(walltime_hours),
                                # '#PBS -e localhost:/dev/null',
                                # '#PBS -o localhost:/dev/null',
                                '#PBS -j oe',
                                '#PBS -l mem={}gb'.format(mem_gb)
                                ]

    ##########################

    # mongos startup/shutdown is the least reliable portion of the whole search workflow.
    #
    # for one proteomics dataset, ~500 jobs spawned, which means ~500 mongos processes 
    # are (attempted to be) started up / shutdown.

    # seems like ~5-10% of these fail, which means jobs must be resubmitted, and 
    # 'dangling' mongos processes must be periodically killed across the garibaldi cluster.

    # instead, try out having a number of 'persistent' mongos processes running across the cluster.
    # if there is one running mongos per switch, any search jobs started on nodes on that switch 
    # can connect to their 'neighborhood' mongos process, which should limit client-mongos traffic
    # to that portion of the cluster

    # one simple way to implement this is by grepping a number of files containing garibaldi 
    # hostnames for the current node's hostname and connecting to the appropriate switch:
    # 
    # contents of file 'node01234' -- which is a list of nodes connected to the same switch
    # (each filename is a mongos host)
    #
    #     node01234
    #     node01235
    #     node01236
    #     node01237
    #     node01238
    #     node01239
    #     node01240
    # 
    # So when a search job starts, run a command like this:
    #
    #
    # cd $PBS_O_WORKDIR
    # for i in `ls /gpfs/home/sandip/mongosnodes/node*`; do
    #     if grep -q `hostname` $i; then 
    #         MONGOHOST=`head -1 $i`;
    #        break;
    #    fi
    # done
    #
    # if [ -z ${MONGOHOST+x} ]; then 
    #     MONGOHOST="wl-cmadmin";
    #    echo "! mongos host not found. Using default mongos host $MONGOHOST";
    # else
    #    echo "Using mongos host $MONGOHOST";
    # fi
    #
    # function test_mongo_connect { 
    #    mongo --host $MONGOHOST --port 27018 --eval "sh.status()"; 
    # }
    #
    # if test_mongo_connect>/dev/null; then 
    #    echo "mongos on $MONGOHOST appears to be OK"; 
    # else 
    #    echo "could not connect to $MONGOHOST";
    #     if [ $MONGOHOST == "wl-cmadmin" ]; then 
    #        echo "error - no mongos host to connect to. exiting.";
    #        exit 1
    #    fi
    # fi
    #
    # PARAMS_FILE=$PBS_JOBID"_blazmass.params"
    # cat blazmass.params > sed s/MONGOS_HOSTNAME/$MONGOHOST/g > $PARAMS_FILE
    # 
    # java -jar /gpfs/home/sandip/blazmass_search/blazmass/blazmass.jar . {} $PARAMS_FILE {} 1> /dev/null

    # tested mongos selection script:

    if args.nomongos:
        base_job_file = '''#!/bin/bash
#PBS -l nodes=1:ppn={num_cores}
#PBS -l cput={cputime_hours}:00:00
#PBS -l walltime={walltime_hours}:00:00
#PBS -j oe
#PBS -l mem={mem_gb}gb
#PBS -N "blazmass_mongo{job_num}"

set -e
echo "################################################################################"
echo "Processing MS2 file: {ms2_file}"
echo "PBS job script file: {job_file}"
echo "Running on node: `hostname`"
echo "################################################################################"

module load java/1.7.0_21

cd $PBS_O_WORKDIR

function remove_job_running_file {{
    if ! [ -z ${{JOB_RUNNING_FILE+x}} ] && [ -f $JOB_RUNNING_FILE ]; then 
        rm -rf $JOB_RUNNING_FILE;
    fi
}}

PARAMS_FILE=$PBS_JOBID"_blazmass.params"
cat blazmass.params > $PARAMS_FILE

JOB_RUNNING_FILE=/scratch/`whoami`/`echo $PBS_JOBID | cut -d '.' -f 1`"_job_running"
touch $JOB_RUNNING_FILE
java -jar {blazmass_jar} . {ms2_file} $PARAMS_FILE {num_threads} 1> /dev/null
remove_job_running_file

echo "Finished"
'''
    else:
        base_job_file = '''#!/bin/bash
#PBS -l nodes=1:ppn={num_cores}
#PBS -l cput={cputime_hours}:00:00
#PBS -l walltime={walltime_hours}:00:00
#PBS -j oe
#PBS -l mem={mem_gb}gb
#PBS -N "blazmass_mongo{job_num}"

set -e
echo "################################################################################"
echo "Processing MS2 file: {ms2_file}"
echo "PBS job script file: {job_file}"
echo "Running on node: `hostname`"
echo "################################################################################"

module load java/1.7.0_21

MONGOSNODES_PATH={home_directory}/mongosnodes

if ! test -n "$(find $MONGOSNODES_PATH -maxdepth 1 -name '{mongos_node_prefix}*' -print -quit)"; then
    echo "No mongos node information found at $MONGOSNODES_PATH -- exiting"
    exit 1
fi

# mongo database and collection names for testing mongos connection
DBNAME="SeqDB_072114"
COLLNAME="SeqDB_072114"

cd $PBS_O_WORKDIR

function remove_job_running_file {{
    if ! [ -z ${{JOB_RUNNING_FILE+x}} ] && [ -f $JOB_RUNNING_FILE ]; then 
        rm -rf $JOB_RUNNING_FILE;
    fi
}}

function pick_nearby_mongos {{
    for i in `ls $MONGOSNODES_PATH/{mongos_node_prefix}* | shuf`; do
        if grep -q `hostname` $i; then
            MONGOHOST=`head -1 $i`;
            break;
        fi;
    done
}}

function pick_any_mongos {{
    RANDOM_MONGOS=`ls $MONGOSNODES_PATH/{{imsb,node}}* | shuf | head -1`;
    MONGOHOST=`head -1 $RANDOM_MONGOS`;
}}

echo
echo "Trying to connect to nearby Garibaldi mongos"
j="1"
while [ $j -lt 4 ]
do
    echo -n "Attempt #$j of 3"
    pick_nearby_mongos

    # test if MONGOHOST is set, and then test if a successful connection can be made to mongos on MONGOHOST
    # (if connection fails, unset MONGOHOST and run pick_nearby_mongos again)
    if [ ! -z ${{MONGOHOST+x}} ] && mongo $DBNAME --quiet --host $MONGOHOST --port 27018 --eval "db.$COLLNAME.findOne()" 2> /dev/null 1> /dev/null; then
        echo "... successfully connected to mongos on "$MONGOHOST;
        break;
    else
        echo "... failed"
        unset MONGOHOST
    fi
    j=$[$j+1]
done

# if unable to connect to a MONGOHOST from nearby nodes, try to pick any Garibaldi mongos...
if [ -z ${{MONGOHOST+x}} ]; then
    echo
    echo "Could not connect to nearby mongos. Trying to connect to any available Garibaldi mongos..."
    j="1"
    while [ $j -lt 4 ]; do
        echo -n "Attempt #$j of 3"
        pick_any_mongos
        
        # test if MONGOHOST is set, and then test if a successful connection can be made to mongos on MONGOHOST
        # (if connection fails, unset MONGOHOST and run pick_any_mongos again)
        if [ ! -z ${{MONGOHOST+x}} ] && mongo $DBNAME --quiet --host $MONGOHOST --port 27018 --eval "db.$COLLNAME.findOne()" 2> /dev/null 1> /dev/null; then
            echo "... successfully connected to mongos on "$MONGOHOST;
            break;
        else
            echo "... failed"
            unset MONGOHOST
        fi
        j=$[$j+1]
    done
fi

# if still unable to connect to a MONGOHOST, exit...
if [ -z ${{MONGOHOST+x}} ]; then
    echo "Unable to connect to mongos. Exiting";
    exit 1;
fi

mongo --host $MONGOHOST --port 27018 --eval "sh.status()"

PARAMS_FILE=$PBS_JOBID"_blazmass.params"
cat blazmass.params | sed s/MONGOS_HOSTNAME/$MONGOHOST/g > $PARAMS_FILE

JOB_RUNNING_FILE=/scratch/`whoami`/`echo $PBS_JOBID | cut -d '.' -f 1`"_job_running"
touch $JOB_RUNNING_FILE
java -jar {blazmass_jar} . {ms2_file} $PARAMS_FILE {num_threads} 1> /dev/null
remove_job_running_file

echo "Finished"
'''

    customization_dict = {
                    'num_cores':num_cores,
                    'num_threads':num_threads,
                    'cputime_hours':cputime_hours,
                    'walltime_hours':walltime_hours,
                    'mem_gb':mem_gb,
                    'blazmass_jar':blazmass_jar,
                    'mongos_node_prefix':mongos_node_prefix,
                    'home_directory':home_directory
                    }



    ###########################

    # start mongos only if 'mongos' process not running
    #
    # should help when multiple jobs are started on a single node, but will cause problems if the current job (job #1)
    # starts mongos, another search job (job #2) begins on the same node, and job #1 finishes 
    # mongo_start = 'PID_FILE=/scratch/`whoami`/mongos.pid\nSTARTED_MONGOS=false\nif [ ! -f $PID_FILE ] && ! `ps -e | grep -q mongos`; then echo "mongos not running. starting mongos..."; mongos --configdb wl-cm01:27019,wl-cm02:27019,wl-cm03:27019 --port 27018 --fork --logpath /dev/null --pidfilepath $PID_FILE; STARTED_MONGOS=true; fi'

    mongo_start = '\n'.join([
                            'PID_FILE=/scratch/`whoami`/mongos.pid',
                            'STARTED_MONGOS=false',
                            'if `ps -e | grep -q mongos`;',
                            'then MONGOS_PID=`ps -ef | grep \`whoami\` | awk \'/[m]ongos/{print $2}\'`;',
                            'if (( $(grep -c . <<<"$MONGOS_PID") > 1 )); then echo "! Multiple mongos processes running... exiting with error"; exit 1;fi', ## if there are multiple mongos processes running, $MONGOS_PID will have multiple lines (multiple PIDs) -- not sure how to handle this yet
                            'echo "mongos already running with PID $MONGOS_PID -- not starting additional mongos";',
                            'echo $MONGOS_PID > $PID_FILE',
                            'else echo "mongos not running. starting mongos...";',
                            'mongos --configdb wl-cm01:27019,wl-cm02:27019,wl-cm03:27019 --port 27018 --fork --logpath /dev/null --pidfilepath $PID_FILE;',
                            'STARTED_MONGOS=true;',
                            'MONGOS_PID=`cat $PID_FILE`',
                            'fi'
                            ])

    # blazmass_generic_invocation = 'java -jar /gpfs/home/sandip/blazmass_search/blazmass/blazmass.jar . $MS2FILE blazmass.params {} 1> /dev/null'.format(num_threads)

    cleanup_mongo = '\n\t'.join([
                                'function cleanup_mongo {',
                                # 'if [ "$STARTED_MONGOS" = false ]; then return; fi',
                                'if test -n "$(find /scratch/`whoami` -maxdepth 1 -name \'*_job_running\' -print -quit)"; then echo "search jobs still running on `hostname`, not killing mongos process"; return; fi',
                                'echo "killing mongos process..."',
                                'kill $MONGOS_PID',
                                'rm -rf $PID_FILE',
                                'if ! ps -e | grep -q mongos; then echo "mongos process no longer running"; else echo "mongos process still running!"; echo $(ps -e | grep mongos); kill -9 $MONGOS_PID; fi\n}'
                                ])

    remove_job_running_file = '\n\t'.join([
                                'function remove_job_running_file {',
                                'if ! [ -z ${JOB_RUNNING_FILE+x} ] && [ -f $JOB_RUNNING_FILE ]; then rm -rf $JOB_RUNNING_FILE;fi\n}'
                                ])

    trap_sigterm = 'trap \'echo "! Received SIGTERM"; remove_job_running_file; cleanup_mongo; exit\' TERM'
    trap_err = 'trap \'echo "! An error occurred"; remove_job_running_file; cleanup_mongo; exit\' ERR'
    trap_exit = 'trap \'remove_job_running_file;\' EXIT'

    if not os.path.exists('blazmass.params'):
        print("Need a blazmass.params file...")
        sys.exit(1)

    if not os.path.exists(new_file_directory+'/blazmass.params'):
        # create symlink to blazmass.params file in parent directory
        subprocess.Popen(['ln','-s','../blazmass.params','.'],cwd=new_file_directory)

    PBS_job_ids = []
    job_num = 1
    for job_file, ms2_file in zip(job_filepaths, MS2_filenames):
        with open(job_file,'w') as f:
            customization_dict['job_num'] = job_num
            customization_dict['job_file'] = job_file
            customization_dict['ms2_file'] = ms2_file
            f.write(base_job_file.format(**customization_dict))

        job_num += 1
        PBS_job_id = submit_job_file(job_file.split('/')[1], new_file_directory, job_label='search')
        PBS_job_ids.append(PBS_job_id)

    if args.cleanup:
        cleanup_job_id, cleanup_job_output_filename = submit_cleanup_job(PBS_job_ids, sqt_output, new_file_directory, MS2_file, args)
        cat_job_id = submit_cat_job(MS2_file, cleanup_job_id, cleanup_job_output_filename, new_file_directory)

def submit_cleanup_job(PBS_job_ids, sqt_output, new_file_directory, MS2_file, args):

    ''' submits a "cleanup" job that runs only after all work jobs have successfully completed '''

    first_job_id = PBS_job_ids[0]
    last_job_id = PBS_job_ids[-1]
    job_file_name = 'cleanup{}_{}.job'.format(first_job_id, last_job_id)
    cleanup_jobfile = new_file_directory+'/'+job_file_name
    cleanup_job_output_filename = 'cleanup_summary_{}_{}.out'.format(first_job_id, last_job_id)

    cleanup_boilerplate =         ['#!/bin/bash',
                                '#PBS -l nodes=1:ppn=1',
                                '#PBS -l walltime=1:00:00',
                                '#PBS -j oe',
                                '#PBS -o {}'.format(cleanup_job_output_filename),
                                '#PBS -l mem=1gb'
                                ]

    tests_for_output = []
    # if not args.nomongos:
    # tests_for_output.append('grep -q "mongos process no longer running" $k')
    tests_for_output.append('! grep -q "Error" $k')
    tests_for_output.append('! grep -q "error" $k')
    tests_for_output.append('! grep -q "Received SIGTERM" $k')
    # tests_for_output.append('grep -q "child process started successfully, parent exiting" $k')

    with open(cleanup_jobfile,'w') as f:
        f.write('\n'.join(cleanup_boilerplate)+'\n')
        f.write('#PBS -N "cleanup{}_{}"\n'.format(first_job_id, last_job_id))
        f.write('#PBS -W depend=afterany:'+':'.join(PBS_job_ids)+'\n')
        f.write('cd $PBS_O_WORKDIR\n')
        f.write('cd ..\n')
        f.write('echo\n')
        f.write('pwd\n')
        f.write('echo\n')
        f.write('for k in `ls dummy/blazmass_mongo*.o{{{}}}`; do\n'.format(','.join(PBS_job_ids)))
        # f.write('\tTHEOUTPUT=`tail -1 $k`;\n')
        f.write('\tif ! grep -q "Done processing MS2:" $k; then\n')
        f.write('\t\techo "$k -- "`echo $k | cut -d "." -f 2 | cut -d "o" -f 2` -- error running Blazmass;\n')
        f.write('\t\techo -n "Resubmitting PBS job script ";\n')
        f.write('\t\tJOB_FILE=`grep "PBS job script file:" $k | cut -d \'/\' -f 2`;\n')
        f.write('\t\techo -ne $JOB_FILE"\t";\n')
        f.write('\t\tcd dummy; NEW_JOB_ID=$(qsub $JOB_FILE | cut -d \'.\' -f 1); cd ..;\n')    ## resubmit failed job (once)
        f.write('\t\techo "New Job ID $NEW_JOB_ID";\n')
        f.write('\t\tmv $k {}/resubmitted/;\n'.format(new_file_directory)) ## move resubmitted (old) job output file to directory dummy/resubmitted/
        f.write('\t\techo;\n')
        # if not args.nomongos:
        f.write('\telse\n')
        f.write('\t\tif '+' && '.join(tests_for_output)+'; then\n')
        f.write('\t\t\techo -n "$k -- finished searching ";\n')
        f.write('\t\t\tgrep "Done processing MS2:" $k | cut -d ":" -f 3 | rev | cut -d "/" -f 1 | rev;\n')
        f.write('\t\t\techo\n')
        f.write('\t\telse\n')
        f.write('\t\t\techo "Problem shutting down mongos -- see output file $k"\n')
        f.write('\t\t\tgrep "PBS job script file:" $k\n')
        f.write('\t\t\techo\n')
        f.write('\t\tfi;\n')
        f.write('\tfi;\n')
        f.write('done\n')

    cleanup_job_id = submit_job_file(job_file_name, new_file_directory, job_label='cleanup')
    return cleanup_job_id, cleanup_job_output_filename

def submit_cat_job(MS2_file, cleanup_job_id, cleanup_job_output_filename, new_file_directory):

    ''' creates and submits a "concatenation" job that runs only after cleanup job is completed '''

    job_file_name = 'cat_{}.job'.format(cleanup_job_id)
    cat_jobfile = new_file_directory+'/'+job_file_name
    MS2_base_filename = MS2_file.replace('.ms2', '')

    cat_boilerplate =         ['#!/bin/bash',
                                '#PBS -l nodes=1:ppn=1',
                                '#PBS -l walltime=1:00:00',
                                '#PBS -j oe',
                                '#PBS -o "cat_{}.out"'.format(cleanup_job_id),
                                '#PBS -l mem=1gb',
                                '#PBS -N "cat_{}"'.format(cleanup_job_id),
                                '#PBS -W depend=afterok:{}'.format(cleanup_job_id)
                                ]

    with open(cat_jobfile,'w') as f:
        f.write('\n'.join(cat_boilerplate)+'\n')
        f.write('TIMESECONDS=`date +"%s"`\n')
        f.write('cd $PBS_O_WORKDIR\n')                                                                                                    ## this is the dummy directory
        f.write('if grep -q \'error\' {}; then echo "Error in some job, exiting"; exit 1; fi\n'.format(cleanup_job_output_filename))     ## exit if error found in cleanup job output
        f.write('echo "Combining SQT parts for {}.sqt..."\n'.format(MS2_base_filename))                        
        f.write('grep \'^H\' {}"_1.sqt" > $TIMESECONDS"_H_lines"\n'.format(MS2_base_filename))                                            ## save H lines from a single file
        f.write('grep -vh \'^H\' {}"_"*.sqt > $TIMESECONDS"_all_SQT_output"\n'.format(MS2_base_filename))                                ## concatenate rest of output into a new file
        f.write('cat $TIMESECONDS"_H_lines" $TIMESECONDS"_all_SQT_output" > "../"{}".sqt"\n'.format(MS2_base_filename))                    ## concatenate H lines and rest of output into SQT file
        f.write('rm -rf $TIMESECONDS"_H_lines"\n')
        f.write('rm -rf $TIMESECONDS"_all_SQT_output"\n')
        f.write('echo "...done"\n')

    cat_job_id = submit_job_file(job_file_name, new_file_directory, job_label='concatenation')
    return cat_job_id

def submit_job_file(job_file_name, new_file_directory, job_label=None):

    ''' submit a PBS job file named job_file_name from directory new_file_directory '''

    process = subprocess.Popen(['qsub', job_file_name],stdout=subprocess.PIPE,cwd=new_file_directory)
    job_id = process.communicate()[0].decode('utf-8').split('.')[0]
    if job_label:
        print("Submitted job {}{}".format(job_id, ' ('+job_label+') '))
    else:
        print("Submitted job {}".format(job_id))

    return job_id

if __name__ == '__main__':
    main()