"""
Celery worker for submitting and monitoring job files

To start on a compute node:
run or submit submit_blazmass_worker.job

'''
#!/bin/sh
#PBS -l walltime=999:00:00 
#PBS -l mem=4gb 
#PBS -l nodes=1:ppn=4
cd /gpfs/home/gstupp/metaproteomics/cluster
celery -A submit_blazmass_worker worker --loglevel=info --queue submit_bm --concurrency 4
'''

"""
import os
import sys
import time
import traceback
import subprocess
from random import randint
import pymongo
from textwrap import dedent

from pymongo import MongoClient
from celery import Celery
import datetime
from celery.exceptions import MaxRetriesExceededError
# note moved imports for spur and drmaa to the functions that actually need them

ex_path = os.path.split(os.path.abspath(__file__))[0]  # the directory in which this script lives
MONGO_HOST = "wl-cmadmin.scripps.edu"
MONGO_PORT = 27017
# Where celery task info is stored
mongo_client = MongoClient(MONGO_HOST, MONGO_PORT)

app = Celery('submit_blazmass_worker')
app.config_from_object('celeryconfig')

ERRORS = {1: "sqt output doesn't exist", 
          2: "sqt output is empty",
          3: "There are no scans in the sqt file",
          4: "Log file doesn't contain 'Done processing'",
          5: "Number of ms2 scans does not match number read in log file",
          6: "Garibaldi marked as failed",
          7: "sqt file contains write errors"}

# for maintaining SSH connections to garibaldi
shell = dict()

def setup_ssh(user, host):
    import spur
    # This should keep the ssh connection open
    ssh_key = os.path.expanduser("~/.ssh/blazmass/{user}.priv".format(user=user))
    shell_name = user + "@" + host
    shell[shell_name] = spur.SshShell(host, user, private_key_file=ssh_key)

def submit_job_as_user_spur(job_path, user):
    if job_path.startswith("/gpfs/ims/"):
        host = "ims"
    elif job_path.startswith("/gpfs/home/"):
        host = "garibaldi.scripps.edu"
    else:
        print("Unknown host for job path: " + job_path)
        raise ValueError
    shell_name = user + "@" + host
    if shell_name not in shell or shell[shell_name]._closed:
        setup_ssh(user, host)
    try:
        cmd = shell[shell_name].run(["qsub", job_path])
    except ConnectionError:
        setup_ssh(user, host)
        cmd = shell[shell_name].run(["qsub", job_path])
    return cmd.output.decode('utf-8').strip()

@app.task()
def after_blazmass_job(dir_path, user, queue):
    """make a job file that just runs submit_blazmass --resume as the specified user"""
    job_boilerplate = '\n'.join(['#!/bin/bash',
                                 '#PBS -q {}'.format(queue),
                                 '#PBS -l nodes=1:ppn=2',
                                 '#PBS -l walltime=4:00:00',
                                 '#PBS -j oe',
                                 '#PBS -l mem=4gb',
                                 '#PBS -N "{}"'.format(dir_path),
                                 '#PBS -o {}'.format(dir_path+'/resume.$PBS_JOBID')])
    base_job_file = dedent("""
                    echo "################################################################################"
                    echo "Folder: {dir_path}"
                    echo "Running on node: `hostname`"
                    echo "################################################################################"
                    module load python/3.5.1
                    cd {dir_path}
                    submit_blazmass --resume
                    """).format(dir_path=dir_path)
    
    job_file_path = os.path.join(dir_path,"resume.job")           
    with open(job_file_path, 'w') as f:
        print('Writing job file: ' + job_file_path)
        f.write(job_boilerplate + '\n' + base_job_file)
    
    job_id = submit_job_as_user_spur(job_file_path, user)
    print("Submitted job {job_id}. job file: {job_file}".format(job_id=job_id, job_file=os.path.basename(job_file_path)))

@app.task(bind=True, max_retries=2, rate_limit="60/m", name="submit_blazmass_worker.submit_and_check_job")
def submit_and_check_job(self, job_path, user, sample_name, job_id=None, verbose=True):
    
    import drmaa

    def resubmit(task_status):
        db = mongo_client["celery"][sample_name]
        db.update_one({'job_file': job_path}, 
                      {'$set': {'status': "fail_retry", "reason": task_status}, 
                      '$unset': {'job_id': ""}})
        try:
            raise self.retry((job_path, user, sample_name), {'job_id': None}, countdown=randint(1, 20))
        except MaxRetriesExceededError:
            db.update_one({'job_file': job_path}, {'$set': {'status': "fail", "reason": task_status}, '$unset': {'job_id': ""}})
            # send_job_failed_email(job_path, self.request.retries)
            print('Job failed exceded max_retry')

    if job_id is None:
        # check if already submitted or already done
        time.sleep(randint(4, 8))  # wait to make sure mongo is updated if this has just been called
        db = mongo_client["celery"][sample_name]
        doc = db.find_one({'job_file': job_path})
        if doc['status'] == 'queued':
            print("Job already exists!!!: " + job_path)
            return
        if "job_id" in doc:
            print("Job already queued!!!: " + job_path)
            return
        print(doc)

        job_id = submit_job_as_user_spur(job_path, user)
        print("Submitted job {job_id}. job file: {job_file}".format(job_id=job_id, job_file=os.path.basename(job_path)))
        db.update_one({'job_file': job_path}, {'$set': {'job_id': job_id, 'status': "queued"}})
        print(db.find_one({'job_file': job_path}))

    print('retries: ' + str(self.request.retries))
    
    try:
        with drmaa.Session() as s:
            job_status = s.jobStatus(job_id)
    except drmaa.errors.InternalException:
        print("Failed to get drmaa job status. Retrying indefinitely")
        job_status = "unknown"

    if verbose: print(job_status + ': ' + str(job_id))
    sqt_file = job_path.replace('.job', '.sqt')

    """Something it still not right with blazmass and/or garibalid and/or job file. 
        Sometimes job_status is failed but the job completed succesfully. why??"""
    if job_status not in ['done', 'failed']:
        # it is either queued, active, or on hold. Check again in 90-180 seconds
        # checking jobs, nothing really failed decrement number of retries
        self.request.retries = self.request.retries - 1
        raise self.retry((job_path, user, sample_name), {'job_id': job_id}, countdown=randint(90, 180))
        # TODO: Once job is started, and an sqt file exists. Check if the file doesn't change size in an hour -> kill it
    else:  # job status: 'done' or 'failed'
        task_status,task_status_info = check_job(job_path, job_id)
        if task_status == 0:
            print("output exists & passes checks: " + sqt_file)
            db = mongo_client["celery"][sample_name]
            db.update_one({'job_file': job_path}, {'$set': {'status': "done"}})
        else:
            print("Error!! : " + ERRORS[task_status] + ". " + sqt_file)
            # log the error somewhere
            db_errors = mongo_client["celery_errors"][sample_name]
            db_errors.insert({'job_path':job_path, 'date': datetime.datetime.utcnow(), 'job_id': job_id, 'error': task_status, 'error_info': task_status_info})
            resubmit(task_status)


@app.task
def check_mongos_status(host, port, db, coll):
    """ check if the mongos process on host:port is running and is a mongos process
    get the list of shards. Connect to each and make sure they are up and accepting connections"""
    try:
        client = MongoClient(host, port)
        shard_info = client.admin.command('listShards')
        print(shard_info)
        for shard in shard_info['shards']:
            shard_client = MongoClient(shard['host'])
            doc = shard_client[db][coll].find_one()
            print("Success: " + shard['host'])
            shard_client.close()
        print("mongos status ok")
    except pymongo.errors.ConnectionFailure as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        send_email("stuppie@gmail.com", "mongos down", traceback.format_exc())
        print(traceback.format_exc())
        raise e


@app.task()
def combine_sqt_parts(MS2_input, temp="dummy"):
    return subprocess.check_call([os.path.join(ex_path, 'combine_sqt_parts.sh'), temp], cwd=MS2_input)


@app.task(name="submit_blazmass_worker.add")
def add(x, y):
    return x + y


@app.task(name="submit_blazmass_worker.echo")
def echo():
    print("Hi!")
    return "Hi!"


def validate_sqt(sqt_file):
    # Very basic check of .sqt file to ensure each row has the correct
    # number of columns.  Will result in re-run of job if 1 error found
    # TODO: Future versions could have smarter checks and store line number
    # of error
    with open(sqt_file, 'r') as f:
        for line_num, line in enumerate(f):
            if line.startswith('M'):
                assert line.count('\t') == 10, str(line_num) + ':' + line
            if line.startswith('S'):
                assert line.count('\t') == 9, str(line_num) + ':' + line
            if line.startswith('L'):
                assert line.count('\t') == 3, str(line_num) + ':' + line


def check_job(job_path, job_id):
    # check if ms2 file exists and is non-zero 
    # and has at least one Scan in it 
    # and the SGE job log file has "done processing" in it
    # and the number of ms2 scans in the log file is correct
    # returns a tuple containing: an error code >=1 if failed. 0 = success 
    # and extra information as a string or ""
    sqt_file = job_path.replace('.job', '.sqt')
    ms2_file = job_path.replace('.job', '.ms2')
    # garibaldi's #####.garibaldi01-adm.cluster.net file
    job_log_file = job_path.replace('.job', '.' + job_id)
    # blazmass log file
    log_file = job_path.replace('.job', '.LOG')
    if not os.path.isfile(sqt_file):
        return 1,""
    if not os.path.getsize(sqt_file) > 0:
        return 2,""
    """ # Removing this because its possible the file is succesfully searched and has no scans in it
    if not any([line.startswith('S\t') for line in open(sqt_file)]):
        return 3,""
    """
    if not "INFO: Done processing MS2:" in open(job_log_file).read():
        return 4,""
    try:
        validate_sqt(sqt_file)
    except AssertionError as e:
        return 7,str(e)
    scans_log = len([line for line in open(log_file) if line.startswith("INFO:")])
    scans_ms2 = len([line for line in open(ms2_file) if line.startswith("S")])
    if scans_log != scans_ms2:
        return 5,"Found {scans_log} scans, expected {scans_ms2}."
    
    return 0,""


def send_email(to, subject, message):
    import smtplib
    from email.mime.text import MIMEText
    msg = MIMEText(message)
    msg['Subject'] = subject
    msg['From'] = 'garibaldi@scripps.edu'
    msg['To'] = to
    with smtplib.SMTP('localhost') as s:
        s.sendmail(msg['From'], [to], msg.as_string())
    print('email sent')
    
    
def is_done_searching(name):
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery[name]
    done_count = db.find({'status': 'done'}).count()
    all_count = db.find().count()
    return True if done_count == all_count else False
    
@app.task(bind=True, max_retries=24*3*60, default_retry_delay = 60)
def check_search_status(self, user, name, queue, submit_queue):
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery[name]
    search_loc = os.path.dirname(db.find_one()['job_file'])
    print("checking search: {}".format(search_loc))
    if is_done_searching(name):
        print("done searching: {}".format(search_loc))
        # continue the rest of the search process
        after_blazmass_job.apply_async((os.path.dirname(search_loc), user, queue),
                                       queue=submit_queue)
        send_email(user + '@scripps.edu', name + ': Jobs Finished', 
                   'All jobs for search ' + name + ' are done!\n\n' +
                   'Search Location:\n' + search_loc + 
                   '\n\nPlease tend to jobs to contine search.')
    else:
        try:         
            self.retry((user, name, queue, submit_queue))
        except MaxRetriesExceededError:
            print("search still running: {}".format(search_loc))
            send_email(user + '@scripps.edu', name + ': Search still running', 
                       'Jobs for search ' + name + ' have been queued ' + 
                       'for an unusually long time.\n\nSearch Location:\n' + 
                       search_loc + '\n\nPlease check status.')
    
# This doesn't seem to be working
"""
# Run every hour for one week
@app.task(bind=True, max_retries=24 * 7, default_retry_delay = 60 * 60, name='submit_blazmass_worker.monitor_status_and_alert')
def monitor_status_and_alert(self, user, name):
    print('Email will be sent to: ' + user + '@scripps.edu')
    db = MongoClient('wl-cmadmin.scripps.edu', 27017).celery[name]
    docs = db.find()
    statuses = [x['status'] for x in docs]
    search_loc = os.path.dirname(db.find_one()['job_file'])
    if statuses.count('done') == len(statuses):
        send_email(user + '@scripps.edu', name + ': Jobs Finished', 
                   'All jobs for search ' + name + ' are done!\n\n' +
                   'Search Location:\n' + search_loc + 
                   '\n\nPlease tend to jobs to contine search.')
    elif statuses.count('init') + statuses.count('queued') == 0:
        send_email(user + '@scripps.edu', name + ': Jobs Stalled', 
                   'Jobs for search ' + name + ' have stalled.\n\n' +
                   'Search Location:\n' + search_loc + 
                   '\n\nPlease tend to jobs to contine search.')
    else:
        try:         
            self.retry((user, name))
        except MaxRetriesExceededError:
            send_email(user + '@scripps.edu', name + ': Search still running', 
                       'Jobs for search ' + name + ' have been queued ' + 
                       'for an unusually long time.\n\nSearch Location:\n' + 
                       search_loc + '\n\nPlease check status.')

"""