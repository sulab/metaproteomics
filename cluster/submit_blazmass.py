#!/usr/bin/env python3
"""
Functions for preparing to submit blazmass searches 
and submitting with celery worker

External dependencies:
'blazmass.params.template', 'combine_sqt_parts.sh', 'make_filtered_database.py' in the same folder as this script

To run:
python3 ~/metaproteomics/cluster/submit_blazmass.py params.json

"""
import os
import re
import pwd
import sys
import json
import argparse
import datetime
import subprocess
from glob import glob
from textwrap import dedent
from itertools import chain
from socket import gethostname
from argparse import Namespace
from pymongo import MongoClient
import make_filtered_database

ex_path = os.path.split(os.path.abspath(__file__))[0]  # the directory in which this script lives
MONGO_HOST = "wl-cmadmin.scripps.edu"
MONGO_PORT = 27017


def split_ms2_file(ms2_file_path, split_n, temp_folder, verbose=True):
    """ Split an ms2 file"""
    if verbose: print('Splitting: ' + ms2_file_path)
    dir_name = os.path.dirname(ms2_file_path)
    base_name = os.path.basename(ms2_file_path)
    out_path = os.path.join(dir_name, temp_folder, base_name.replace('.ms2', '_{#}.ms2'))

    num_scans = sum([True for line in open(ms2_file_path) if line.startswith('S\t')])
    block_size = round(num_scans / split_n) + 1

    command = "cat {ms2_file_path} | parallel --pipe -N {block_size} --block-size 20M --recstart 'S\\t' \"cat > {out_path} && echo {out_path}\"".format(
        **{'ms2_file_path': ms2_file_path, 'block_size': block_size, 'out_path': out_path})

    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if p.returncode != 0:
        raise ValueError(stderr)
    return stdout.decode('utf-8').splitlines()


def make_job_file(ms2_file, args):
    """ create .job file
    """
    dir_name = os.path.dirname(ms2_file)
    base_name = os.path.basename(ms2_file)
    file_name = base_name.split('.')[0]
    job_file_path = ms2_file.replace('.ms2', '.job')

    job_boilerplate = '\n'.join(['#!/bin/bash',
                                 '#PBS -q {}'.format(args.queue),
                                 '#PBS -l nodes=1:ppn={}'.format(args.numcores),
                                 '#PBS -l cput={}:00:00'.format(args.cputime),
                                 '#PBS -l walltime={}:00:00'.format(args.walltime),
                                 '#PBS -j oe',
                                 '#PBS -l mem={}gb'.format(args.memgb),
                                 '#PBS -N "BM_{}"'.format(file_name),
                                 '#PBS -o {}'.format(ms2_file.replace('.ms2', '.$PBS_JOBID')),
                                 '#PBS -m n'  # don't send me emails!!!
                                 ])
    # special case for Ana
    user = pwd.getpwuid(os.getuid()).pw_name
    if user == "wolan":
        job_boilerplate += "\n#PBS -M anawang@scripps.edu"

    customization_dict = {
        'num_threads': args.numthreads,
        'blazmass_jar': args.blazmass_jar,
        'ms2_file': ms2_file,
        'base_name': base_name,
        'job_file': job_file_path,
        'ex_path': args.ex_path,
        'dir_name': dir_name,
        'job_log_file': ms2_file.replace('.ms2', '.$PBS_JOBID')}

    
    # TODO: Will have to change the module load settings once JC is done migrating everything
    base_job_file_not_sharded = dedent("""
                    echo "################################################################################"
                    echo "Processing MS2 file: {ms2_file}"
                    echo "PBS job script file: {job_file}"
                    echo "Running on node: `hostname`"
                    echo "################################################################################"
                    
                    module load java/1.7.0_21
                    module load java/1.8.0_65
                    chmod a+r {job_log_file}

                    cd {dir_name}
                    java -jar {blazmass_jar} . {base_name} blazmass.params {num_threads}
                    STATUS=$?
                    if [ $STATUS -ne 0 ]; then
                      echo "Blazmass failed"; exit $STATUS
                    else
                      echo "Finished Succesfully!"
                    fi

                    """).format(**customization_dict)

    with open(job_file_path, 'w') as f:
        print('Writing job file: ' + job_file_path)
        f.write(job_boilerplate + '\n' + base_job_file_not_sharded)

    return job_file_path


def set_params(params, tag):
    
    params.ex_path = ex_path
    
    #####
    ## Required params
    #####
    if params.MS2_input == '':
        params.MS2_input = os.getcwd()

    if params.numthreads:
        # number of threads to run Blazmass using (should be <= num_cores)
        if params.numthreads > params.numcores:
            params.numthreads = params.numcores
    else:
        params.numthreads = params.numcores

    params.cputime = params.walltime * params.numthreads

    if params.server_preset == 'garibaldi':
        if not ("mongos_uri" in params and len(params.mongos_uri) > 1):
            params.mongos_uri = """mongodb://wl-cm01.scripps.edu:27018,
                wl-cm02.scripps.edu:27018,wl-cm03.scripps.edu:27018,
                wl-cm04.scripps.edu:27018,wl-cm05.scripps.edu:27018,
                wl-cm06.scripps.edu:27018,wl-cm07.scripps.edu:27018,
                wl-cm08.scripps.edu:27018""".replace(' ','').replace('\n','')
        if not ("submit_queue" in params and len(params.submit_queue) > 1):
            params.submit_queue = 'submit_bm' #celery queue
    elif params.server_preset == 'ims':
        if not ("submit_queue" in params and len(params.submit_queue) > 1):
            params.submit_queue = 'ims'
        if not ("mongos_uri" in params and len(params.mongos_uri) > 1):
            params.mongos_uri = "mongodb://imsb0501:27018,imsb0515:27018,imsb0601:27018,imsb0615:27018"
    else:
        raise ValueError('Undefined preset server configuration: {}\n(options: garibaldi, ims)'.format(params.server_preset))

    params.use_ProtDB = 1 if params.use_ProtDB in [True, '1', 1] else 0
    params.use_SeqDB = 1 if params.use_SeqDB in [True, '1', 1] else 0
    params.ppm_fragment_ion_tolerance = float(params.ppm_fragment_ion_tolerance)
    params.ppm_peptide_mass_tolerance = float(params.ppm_peptide_mass_tolerance)

    #####
    ## Optional params that depend on other parameters
    #####

    if "name" in params:
        params.name = params.name + "_" + tag
    else:
        params.name = "noname_" + tag

    if "massdbcoll" not in params:
        params.massdbcoll = params.mass_db
    if "protdbcoll" not in params:
        params.protdbcoll = params.prot_db
    if "seqdbcoll" not in params:
        params.seqdbcoll = params.seq_db
    
    if "memgb" not in params:
        params.memgb = params.numthreads * 1.5  # 1.5GB RAM per thread
    params.memgb = round(float(params.memgb))

    #####
    ## Optional params with defaults
    #####

    DEFAULTS = {"add_static_mod": "",
                "job_spacing_init": 0,
                "job_spacing": 1/60,
                # warning, if you change the default dta_params for some reason, make sure you change it in the next section too
                "dta_params": "--quiet --brief --trypstat --modstat -y 2 -DM 10 --extra --dm --sfp 0.01 -p 2",
                "temp": "dummy",
                "diff_search_options": "",
                "add_C_terminus": 0,
                "add_N_terminus": 0,
                "queue": "workq", # garibaldi queue to submit to
                "isotopes": 1, # 0=search only monoisotopic peak, 1=search isotopes
                "count_scans": True, # count the total # of ms2 scans?
                }
    
    for key,value in DEFAULTS.items():
        if key not in params:
            params.__dict__[key] = value
    
    #####
    ## Warnings, changes, errors
    #####    
    
    # If --sfp or -p is set in the params file, then make sure we apply it
    if ("sfp" in params and params.sfp):
        if "--pfp" in params.dta_params or "--fp" in params.dta_params:
            raise ValueError("Conflicting dta_params. Please check 'dta_params' and 'sfp'")
        print("Warning: overriding with sfp from params file: " + str(params.sfp))
        params.dta_params = params.dta_params.replace("--sfp 0.01", "--sfp {}".format(params.sfp))
    if ("ppp" in params and params.ppp):
        print("Warning: overriding with ppp from params file: " + str(params.ppp))
        params.dta_params = params.dta_params.replace("-p 2", "-p {}".format(params.ppp))
    
    if (("diff_search_Nterm" in params and params.diff_search_Nterm) or 
    ("diff_search_Cterm" in params and params.diff_search_Cterm)):
        raise ValueError("Sorry, can't do N/C term diff_search")

    if ("reverse_peptides" in params and params.reverse_peptides in [True, '1', 1]):
        raise ValueError("Sorry, can't do reverse_peptides")
    
    if ("ppm_fragment_ion_tolerance_high" in params and params.ppm_fragment_ion_tolerance_high in [True, '1', 1]):
        raise ValueError("Sorry, can't use ppm_fragment_ion_tolerance_high")
        
    if ("mongoshost" in params and len(params.mongoshost) > 0):
        print("warning: overiding mongos_uri")

    if ("mongosport" in params and len(params.mongosport) > 0):
        print("warning: overiding mongos_uri")

    return params

def make_blazmass_params(params):
    bm_params_template = os.path.join(ex_path, 'blazmass.params.template')
    bm_params_out = os.path.join(params.MS2_input, 'blazmass.params')
    bm_params_text = open(bm_params_template).read().format(**params.__dict__)
    with open(bm_params_out, 'w') as f:
        f.write(bm_params_text)
    return bm_params_out


def split_ms2_make_jobs(MS2_input, params):
    """
    Main function that sets up a folder for job submission
    Check that all necessary files are present
    Splits ms2 file
    makes job files
    return list of path to job files that should be submitted
    """

    if params.verbose: print(params)

    # Check for ms2 files
    MS2_files = glob(os.path.join(params.MS2_input, '*.ms2'))
    if len(MS2_files) == 0:
        raise ValueError('No ms2 files found in ' + params.MS2_input)
    elif params.verbose:
        print('ms2 files found in ' + params.MS2_input)

    bmp = make_blazmass_params(params)
    if params.verbose: print('made blazmass.params in: ' + bmp)

    # Check for blazmass.params in the same folder as ms2 files
    if not os.path.exists(os.path.join(params.MS2_input, 'blazmass.params')):
        raise ValueError("Needs a blazmass.params file in the same directory as MS2 files")
    elif params.verbose:
        print('Found blazmass.params in ' + params.MS2_input)

    # Make symlink to blazmass.params in dummy folder
    dummy_path = os.path.join(params.MS2_input, params.temp)
    if not os.path.exists(dummy_path):
        os.mkdir(dummy_path)
    elif params.verbose:
        print('temp folder exists already: ' + dummy_path)
    if not os.path.exists(os.path.join(dummy_path, 'blazmass.params')):
        if params.verbose: print('Making symlink: ' + os.path.join(dummy_path, 'blazmass.params'))
        subprocess.check_call(['ln', '-s', '../blazmass.params'], cwd=dummy_path)

    # Simlink split .ms2 files to dummy folder if option given
    if not params.use_split_ms2:
        # Do splitting from this folder
        split_ms2_files = []
        for ms2_file in MS2_files:
            split_ms2_files.append(split_ms2_file(ms2_file, params.split_n, params.temp, params.verbose))
        if params.verbose:
            print('Created ms2 files in: ' + dummy_path)
        split_ms2_files = list(chain(*split_ms2_files))
    else:
        globbed_ms2_files = os.path.join(os.path.abspath(os.path.expanduser(params.use_split_ms2)),"*.ms2")
        split_ms2_files = glob(globbed_ms2_files)
        if len(split_ms2_files) == 0:
            raise ValueError('No ms2 files found in ' + os.path.abspath(params.use_split_ms2))        
        for ms2_file in split_ms2_files:
            subprocess.check_call(['ln', '-s', ms2_file, "."], cwd=dummy_path)
        split_ms2_files = glob(os.path.join(dummy_path,'*.ms2'))
        if params.verbose: 
            print('Symlinked ms2 files from: {a} \nto: {b}'.format(a=globbed_ms2_files, b=dummy_path))

    # Create job files
    job_files = []
    for file in split_ms2_files:
        job_files.append(make_job_file(file, params))

    return job_files


def check_mongo(params):
    client = MongoClient(params.mongohost, params.mongoport)
    assert (client[params.mass_db][params.massdbcoll].count() > 10)
    if client[params.seq_db][params.seqdbcoll].count() < 10:
        print("WARNING: SeqDB {} appears to be empty".format(params.seq_db + ":" + params.seqdbcoll))
    if client[params.prot_db][params.protdbcoll].count() < 10:
        print("WARNING: MassDB {} appears to be empty".format(params.mass_db + ":" + params.massdbcoll))


def kill_tasks(name):
    """
    Kill all celery tasks
    Kill all garibaldi jobs

    :param name: the mongo collection name that contains the jobs to kill
    """
    from celery import Celery
    app = Celery('submit_blazmass_worker')
    app.config_from_object('celeryconfig')
    
    from celery.task.control import revoke
    db = MongoClient(MONGO_HOST, MONGO_PORT)['celery'][name]

    for count, doc in enumerate(db.find(), 1):
        if doc.get('status', False) == 'done':
            continue
        revoke(doc['celery_id'], terminate=True)
        if 'job_id' in doc:
            subprocess.call('qdel ' + doc['job_id'], shell=True)
        db.update_one({'job_file': doc['job_file']}, {'$set': {'status': 'revoked'}, '$unset': {'job_id': ""}})
    print(str(count) + " tasks killed")
    
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery["status"]
    db.update_one({'_id':params.name}, {'$set':{'status':'KILLED'}})
    task_id = db.find_one({'_id':params.name})['check_search_status_task_id']
    revoke(task_id, terminate=True)

def get_ms2_scan_count(d):
    cd = os.getcwd()
    os.chdir(d)
    count = int(subprocess.getoutput("cat *.ms2 | grep -c '^S'"))
    os.chdir(cd)
    return count

def check_progress_full(name, temp, ms2_scan_count):
    cd = os.getcwd()
    os.chdir(temp)
    ms2_scan_done_count = int(subprocess.getoutput("cat *.LOG | grep -c '^INFO'"))
    print("MS2 Scans Completed: " + str(ms2_scan_done_count) + "/" + str(ms2_scan_count))
    os.chdir(cd)
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery[name]
    done_count = 0
    count = 0
    print("done tasks")
    for done_count, doc in enumerate(db.find({'status': 'done'}), 1):
        print(doc)
    print("not done tasks")
    for count, doc in enumerate(db.find({'status': {'$ne': 'done'}}), 1):
        print(doc)
    print("Tasks completed: " + str(done_count) + ' / ' + str(done_count + count))

def check_progress_brief(name, temp):
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery[name]
    done_count = db.find({'status': 'done'}).count()
    all_count = db.find().count()
    print("Tasks completed: " + str(done_count) + ' / ' + str(all_count))

def after_blazmass(params):
    subprocess.check_call([os.path.join(params.ex_path, 'combine_sqt_parts.sh'), params.temp], cwd=params.MS2_input)
    run_make_filtered_db(params)
    run_dta_select(params)

def run_make_filtered_db(params):
    make_filtered_database.main(params.MS2_input, **vars(params))

def run_dta_select(params):
    dta_params = params.dta_params
    
    if "wl-cmadmin" in gethostname():
        host = "wl"
        dta_path = "/mongoa/DTASelect/DTASelect2"
    else:
        host = "garibaldi"
        dta_path = "/gpfs/home/gstupp/DTASelect2"
        qsub_command = "qsub -q {queue} -d {cwd} -l walltime=24:00:00 -l nodes=1:ppn=8 -l mem=24gb".format(cwd=params.MS2_input, 
                                                                                        queue=params.queue)
    
    dta_command = "java -Xmx24G -cp {dta_path} DTASelect {dta_params} > dtaselect.out".format(
            dta_params=dta_params, dta_path=dta_path)
    
    # replace protIDs with defline, or not
    if not params.use_ProtDB:
        prot_command = os.path.join(ex_path, "dta_select_prot.py") + " DTASelect-filter.txt " \
          "--protdb {prot_db} --protdbcoll {protdbcoll} --host {mongohost} --port {mongoport}".format(**params.__dict__)
        full_command = dta_command + " && " + prot_command
    else:
        full_command = dta_command
    if host == "garibaldi":
        full_command = "module load python/3.3.2 && module load python/3.5.1 && " + full_command
    print(full_command)
    if host == "garibaldi":
        os.system("echo \"" + full_command + "\" | " + qsub_command)
    else:
        subprocess.call(full_command, shell=True)

def rewrite_job_files(params):
    split_ms2_files = glob(os.path.join(params.MS2_input, params.temp,'*.ms2'))
    job_files = [make_job_file(file, params) for file in split_ms2_files]
    if params.verbose:
        print("Wrote {} job files".format(len(job_files)))
    
def gettimestamp(thestring):
    m = re.compile("^params_(\d*_\d*)").search(thestring)
    return datetime.datetime.strptime(m.groups()[0], timeformat)


def staggered_countdown(n, init=10, job_spacing=20):
    """ Generate a list of delays (in seconds) for `n` jobs. Where `init` # jobs will start immediately,
    and then each sequential job will start `job_spacing` minutes later """
    if init > n:
        print("No countdown delay")
        return list(range(n))
    delays = [x + 5 for x in list(range(init))]
    for count in range(1, n - init + 1):
        delays.append(int(count * (job_spacing * 60)))
    return delays


def jumpstart(job_file_paths, params, user, spacing=None):
    """ Immediately start these tasks.
    Details:    Given a list of job_file_paths, look up celery task ids, revoke those celery tasks
    Create new jobs with no countdown
    If spacing, start with spacing (in min)
    """
    from celery import Celery
    app = Celery('submit_blazmass_worker')
    app.config_from_object('celeryconfig')
    
    from celery.task.control import revoke
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery[params.name]
    for n, doc in enumerate(db.find({'job_file': {'$in': job_file_paths}})):
        revoke(doc['celery_id'], terminate=True)
        countdown = n*60*spacing if spacing else n
        job_task = submit_blazmass_worker.submit_and_check_job.apply_async((doc['job_file'], user, params.name),
                                                                           queue=params.submit_queue, countdown=countdown)
        db.update_one({'job_file': doc['job_file']}, {'$set':{'celery_id': job_task.id, 'countdown': countdown, 'status': "init"}})
        print("Started: " + doc['job_file'])


def submit(job_files, params):
    print("Submitting {n} job{s}".format(n=len(job_files), s='s' if len(job_files) > 1 else ''))
    countdowns = staggered_countdown(len(job_files), init=params.job_spacing_init, job_spacing=params.job_spacing)

    job_tasks = [submit_blazmass_worker.submit_and_check_job.apply_async((job_file, user, params.name),
                                                                         queue=params.submit_queue, countdown=countdown)
                 for (job_file, countdown) in zip(job_files, countdowns)]

    # write job_tasks ids to mongo
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery[params.name]
    for i, job_task in enumerate(job_tasks):
        db.update_one({'job_file': job_files[i]}, {'$set':{'celery_id': job_task.id, 'countdown': countdowns[i],
                                                        'status': "init"}}, upsert=True)
    #submit_blazmass_worker.monitor_status_and_alert.apply_async((user, params.name), queue=params.submit_queue)
    print("submitting checker")
    task = submit_blazmass_worker.check_search_status.apply_async((user, params.name, params.queue, params.submit_queue),
                                                                  queue=params.submit_queue, countdown=10)
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery["status"]
    db.update_one({'_id':params.name}, {'$set':{'status':'RUNNING'}}, upsert=True)
    db.update_one({'_id':params.name}, {'$set':{'check_search_status_task_id':task.id}})


def reset(params):
    """
    Recover from "worker lost" error where celery worker crashes and jobs are lost.
    Reset all tasks where status is not "done", but garibaldi does not have the job (queued or running)
    A completed task: {'celery_id': 'f78cd749-9516-47b3-b9be-80a0a3582f28', 
    'job_id': '9187322.garibaldi01-adm.cluster.net', 
    'job_file': '/gpfs/home/cmoon/proteomics/2015_11_CM1E2d56col/dummy/20151129_CM1E2d56_RTcoh_unenr2_6_9.job', 
    'status': 'done', '_id': ObjectId('568b4839a7d05712f510c67d'), 'countdown': 1320}
    A "in-limbo" task
    {'celery_id': '6c1ded95-a45c-435a-87df-c65331b7f4ef', 
    'job_id': '9186709.garibaldi01-adm.cluster.net', 
    'job_file': '/gpfs/home/cmoon/proteomics/2015_11_CM1E2d56col/dummy/20151127_CM1E2d56_WTcoh_unenr2_5_6.job', 
    'status': 'init', '_id': ObjectId('568b4838a7d05712f510c421'), 'countdown': 14}
    """
    db = MongoClient(MONGO_HOST, MONGO_PORT).celery[params.name]
    for count, doc in enumerate(db.find({'status': {'$ne': 'done'}}), 1):
        db.update_one({'job_file': doc['job_file']}, {'$set': {'status': 'reset'}, '$unset': {'job_id': ""}})
    print("Reset {} tasks".format(count))
    
# %% To run

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('params', nargs='?', help='Path to params.json file', type=str, default="")
    parser.add_argument('--progress', help='show progress', action='store_true')
    parser.add_argument('--progress-full', help='show detailed progress', action='store_true')
    parser.add_argument('--submit-only', help="submit all job files in temp folder", action='store_true')
    parser.add_argument('--kill', help='kill jobs', action='store_true')
    parser.add_argument('--resume',
                        help="resume workflow assuming all ms2 searches have completed succefully (doesn't check!)",
                        action='store_true')
    parser.add_argument('--combine-sqt', help="Only re-combine sqt files", action='store_true')    
    parser.add_argument('--resume-search', help="start all jobs that are not 'done'", action='store_true')
    parser.add_argument('--resubmit', help='Reads full paths to job files to submit from stdin, one per line',
                        action='store_true')
    parser.add_argument('--jump-start', help='Start the next `n` jobs', type=int)
    parser.add_argument('--jump-start-delay', help='Start the next `n` jobs with spacing (read from params)', type=int)
    parser.add_argument('--dry-run', help='Like running with no args, but do not actually submit celery tasks', action='store_true')
    parser.add_argument('--use-split-ms2', help='Path to previously split .ms2 files', type=str, default=None)
    parser.add_argument('--make-db', help='Only run make_filtered_db', action='store_true')
    parser.add_argument('--dta-select', help='Only run run_dta_select', action='store_true')
    parser.add_argument('--reset', help='Recover from "worker lost" error where celery jobs are lost.\n' + 
                                        'Will reset all tasks where status is not "DONE"', action='store_true')
    parser.add_argument('--rewrite-job-files', help="rewrite all job files in temp folder", action='store_true')
 
    args = parser.parse_args()
    
    timeformat = "%Y%m%d_%H%M%S"
    tag = datetime.datetime.now().strftime(timeformat)
    user = pwd.getpwuid(os.getuid()).pw_name
    
    # not: dry-run, use-split-ms2 or setting params only
    use_existing_params = (args.progress or args.progress_full or args.submit_only or args.kill or args.resume or
                            args.resume_search or args.resubmit or args.jump_start or args.jump_start_delay or 
                            args.make_db or args.dta_select or args.reset or args.rewrite_job_files or args.combine_sqt)

    if use_existing_params:
        if args.params == "":
            args.params = sorted(glob('params_' + '[0-9]'*8 + '_' + '[0-9]'*6 + '.json'), key=gettimestamp, reverse=True)[0]
            print("params not given. using: " + args.params)
    else:
        if args.params == "":
            print("params not given. using: 'params.json'")
            assert os.path.exists('params.json'), "'params.json' not found"
            args.params = "params.json"
    with open(args.params) as f:
        params = Namespace(**json.load(f))
    
    # If, use-split-ms2, store them in params
    params.use_split_ms2 = args.use_split_ms2

    if args.combine_sqt:
        subprocess.check_call([os.path.join(params.ex_path, 'combine_sqt_parts.sh'), params.temp], cwd=params.MS2_input)
        sys.exit(0)

    if args.reset:
        reset(params)
        sys.exit(0)

    if args.kill:
        kill_tasks(params.name)
        sys.exit(0)

    if args.progress:
        check_progress_brief(params.name, params.temp)
        sys.exit(0)
        
    if args.progress_full:
        check_progress_full(params.name, params.temp, params.ms2_scan_count)
        sys.exit(0)
    
    if args.resume:
        after_blazmass(params)
        sys.exit(0)
        
    if args.make_db:
        run_make_filtered_db(params)
        sys.exit(0)
        
    if args.dta_select:
        run_dta_select(params)
        sys.exit(0)
    
    if args.rewrite_job_files:
        make_blazmass_params(params)
        rewrite_job_files(params)
        sys.exit(0)        
        
    # Done with options that don't cause tasks to be submitted. Now import worker    
    cd = os.getcwd()
    os.chdir(ex_path)  # needs to be in this folder. Else, the namespace is wrong. Look into this...
    import submit_blazmass_worker
    os.chdir(cd)

    if args.resume_search:
        db = MongoClient(MONGO_HOST, MONGO_PORT).celery[params.name]
        docs = list(db.find({'status': {'$ne': "done"}}))
        job_files = [x['job_file'] for x in docs]
        submit(job_files, params)
        sys.exit(0)

    if args.resubmit:
        job_files = [x.strip() for x in sys.stdin.readlines()]
        submit(job_files, params)
        sys.exit(0)

    if args.submit_only:
        job_files = glob(os.path.join(params.MS2_input, params.temp, '*.job'))
        submit(job_files, params)
        sys.exit(0)

    if args.jump_start or args.jump_start_delay:
        db = MongoClient(MONGO_HOST, MONGO_PORT).celery[params.name]
        docs = list(db.find({'$or': [{'job_id': {'$exists': False}}, {'status': 'revoked'}]}))
        if args.jump_start:
            docs_to_start = sorted(docs, key=lambda x: x['countdown'])[:args.jump_start]
            docs = docs[args.jump_start:]
            print("Jump starting: ")
            print(docs_to_start)
            jumpstart([x['job_file'] for x in docs_to_start], params, user)
        if args.jump_start_delay:
            docs_to_start = sorted(docs, key=lambda x: x['countdown'])[:args.jump_start_delay]
            print("Jump starting with delay ({n} min): ".format(n=params.job_spacing))
            print(docs_to_start)
            jumpstart([x['job_file'] for x in docs_to_start], params, user, spacing=params.job_spacing)
        sys.exit(0)

    # No special options. Submit new set of tasks
    params = set_params(params, tag)
    check_mongo(params)
    
    #### Start actual work ####
    # Split ms2 files
    job_files = split_ms2_make_jobs(params.MS2_input, params)
    # Store ms2 scan count
    if params.count_scans:
        print("Counting number of scans")
        params.ms2_scan_count = get_ms2_scan_count(params.MS2_input)
    else:
        params.ms2_scan_count = 0
    # write args in ms2_input folder (for reference purposes)
    tag = datetime.datetime.now().strftime(timeformat)
    out_params = os.path.join(params.MS2_input, 'params_' + tag + '.json')
    with open(out_params, 'w') as f:
        json.dump(vars(params), f, indent=2, sort_keys=True)
        f.write('\n')
    # Submit celery tasks
    if not args.dry_run:
        submit(job_files, params)
    
    
