[BLAZMASS]
version = 0.998
split_scan_size = 2000
database_name = foo
ppm_peptide_mass_tolerance = {ppm_peptide_mass_tolerance}
isotopes = {isotopes}                  ; 0=search only monoisotopic peak, 1=search isotopes
create_output_files = 1                ; 0=no, 1=yes
ion_series = 0 1 1 1.0 1.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0
ppm_fragment_ion_tolerance = {ppm_fragment_ion_tolerance}
ppm_fragment_ion_tolerance_high = 0
num_output_lines = 10                   ; # peptide results to show
num_description_lines = 3              ; # full protein descriptions to show for top N peptides
show_fragment_ions = 0                 ; 0=no, 1=yes
print_duplicate_references = 1         ; 0=no, 1=yes
enzyme_name = trypsin
enzyme_residues = KR
enzyme_cut = c
miscleavage = 2
use_index = 1   ; 0= search with no index, 1=search using index
index_type = 1   ; 1= faster for small and medium db, 2= for large db
index_inmemory = 0 ;=0 for normal file, =1 for loading existing index in memory
enzyme_nocut_residues = P
xcorr_mode = 0                         ; 0 regular Xcorr(default), 1 - EE, 2 - ET, 3 - TT

[MONGODB]
#
# Use existing MongoDB database for peptide database (MassDB)
# example URI: mongodb://wl-cmadmin:27018 or "mongodb://imsb0501:27018,imsb0515:27018,imsb0601:27018,imsb0615:27018"
use_mongodb = 1
mongoDB_URI = {mongos_uri}

#
# MassDB MongoDB database info (overrides database_name above)
# MassDB is a mass-indexed MongoDB database of all possible tryptic peptides
# Connection and DB information must be present if use_mongodb is enabled above


MassDB_dbname = {mass_db}
MassDB_collection = {mass_db}

#
# SeqDB is a peptide sequence-indexed MongoDB database of all possible tryptic peptides
# Used for looking up parent protein information for identified peptides (Protein ID numbers only)
# Used to fill in L lines in SQT output file
#
use_SeqDB = {use_SeqDB}
SeqDB_dbname = {seq_db}
SeqDB_collection = {seq_db}

#
# ProtDB is a protein-indexed MongoDB database of all proteins in a FASTA file
# Used for looking up parent protein information for identified proteins
# Used to fill in additional information for L lines in SQT output file
#
use_ProtDB = {use_ProtDB}
ProtDB_dbname = {prot_db}
ProtDB_collection = {prot_db}

# Diff search options
diff_search_options = {diff_search_options}

# new parameters

max_num_differential_AA_per_mod = 2    ; max # of modified AA per diff. mod in a peptide
nucleotide_reading_frame = 0           ; 0=proteinDB, 1-6, 7=forward three, 8=reverse three, 9=all six
mass_type_parent = 1                   ; 0=average masses, 1=monoisotopic masses
remove_precursor_peak = 0              ; 0=no, 1=yes
mass_type_fragment = 1                 ; 0=average masses, 1=monoisotopic masses
ion_cutoff_percentage = 0.0            ; prelim. score cutoff % as a decimal number i.e. 0.30 for 30%
match_peak_count = 0                   ; number of auto-detected peaks to try matching (max 5)
match_peak_allowed_error = 1           ; number of allowed errors in matching auto-detected peaks
match_peak_tolerance = 1.0             ; mass tolerance for matching auto-detected peaks
max_num_internal_cleavage_sites = 3    ; maximum value is 5; for enzyme search

#
# partial sequence info ... overrides entries in .dta files
#   up to 10 partial sequences ... each must appear in peptides
#      analyzed in the forward or reverse directions
partial_sequence =

# protein mass & mass tolerance value i.e. 80000 10%
# or protein min & max value i.e. 72000 88000  (0 for both = unused)
protein_mass_filter = 0 0

# For sequence_header_filter, enter up to five (5) strings where any one must
# be in the header of a sequence entry for that entry to be searched.
# Strings are space separated and '~' substitutes for a space within a string.
# Example:  sequence_header_filter = human homo~sapien trypsin
sequence_header_filter =

add_C_terminus = {add_C_terminus}                ; added to C-terminus (peptide mass & all Y-ions)
add_N_terminus = {add_N_terminus}                ; added to N-terminus (B-ions)
add_static_mod = {add_static_mod}

