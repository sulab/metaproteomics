"""
On garibaldi. Assume you have the following in a folder (ms2_path):
    ms2 files

# To start the worker on gb:
> celery -A submit_blazmass worker --loglevel=info --queue submit_bm --concurrency 24

# Start worker on wl
> celery -A submit_blazmass worker --loglevel=info --queue dtaselect --concurrency 24


External dependencies:
params.json should be supplied from elsewhere, such as a web form, or edited manually
a copy of params.json will be written in ms2_path
"""
# broker, queue, etc info: http://abhishek-tiwari.com/post/amqp-rabbitmq-and-celery-a-visual-guide-for-dummies
#%% to run
import os
import json
from argparse import Namespace
from random import randint
submitter_path = "/local/home/gstupp/metaproteomics/cluster/" # where on wl-cmadmin are the script located?
submitter_path = "/home/gstupp/metaproteomics/cluster/" # where on laptop
os.chdir(submitter_path)

import submit_blazmass # needs to be in this folder. Else, the namespace is wrong. Look into this...
from celery import group

#ms2_path = "/gpfs/home/gstupp/01_2015_mass_spec/06152015_LC_Optimization/2_fewer_salt_steps_2015_06_16_10_32866/indexDB_search_10ppm_50ppmfrag"
#ms2_path = "/gpfs/home/gstupp/test/ms2search"
#ms2_path = "/gpfs/home/gstupp/blazmass_test/rs_rshigh/runSearchHigh"

# get params
json_file = "/home/gstupp/metaproteomics/cluster/params_new_compil.json"
#json_file = "/home/gstupp/blazmass030714/test/rs_rshigh/params.json"
with open(json_file) as f:
    params = Namespace(**json.load(f))

#%% run all
res = submit_blazmass.split_ms2_and_gen_jobs.apply_async((ms2_path,params), queue = 'submit_bm')
job_files = res.get(timeout = 600)

# submit all jobs
job_tasks = group(submit_blazmass.check_job.s(job_file).set(queue='submit_bm', 
                  countdown=randint(1,len(job_files))) for job_file in job_files)()

job_tasks.join(propagate=False)

if job_tasks.completed_count() == len(job_tasks):
    submit_blazmass.combine_sqt_parts.apply_async((ms2_path,), queue = 'submit_bm')

#%% run test
job_files = ['/gpfs/home/gstupp/test/ms2search/dummy/06162015_lysed_AWandH32_11_1.job','/gpfs/home/gstupp/test/ms2search/dummy/06162015_lysed_AWandH32_11_2.job']
job_tasks = group(submit_blazmass.check_job.s(job_file).set(queue='submit_bm', countdown=randint(1,len(job_files))) for job_file in job_files)()

#%% Copy sqt files over
# requires ssh keys set up,
user = 'gstupp'
basename = os.path.basename(ms2_path)
target = "/mongoc/{user}/proteomics/{basename}/".format(user=user, basename=basename)
mkdir = "ssh wl-cmadmin mkdir -p {target}".format(target=target)
os.system(mkdir)
cmd = "ssh -A garibaldi rsync -av --exclude='dummy' --exclude='/*/*' {ms2_path}/ wl-cmadmin:{target}".format(ms2_path=ms2_path, target=target)
os.system(cmd)

#%% DTA Select - make fasta db
result_fasta = submit_blazmass.make_filtered_database.apply_async((target,), vars(params), queue = 'dtaselect')
result_dta = submit_blazmass.run_dta_select.apply_async((target,), {'sfp': params.sfp, 'p': params.ppp}, queue = 'dtaselect')


#%% delete alll tasks
from celery.task.control import revoke
for task in job_tasks.children:
    revoke(task.id, terminate=True)


