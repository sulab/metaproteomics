from time import sleep

from IPython import get_ipython
from pymongo import MongoClient
from pymongo.errors import *

ipython = get_ipython()

uri = "wl-cmadmin:27018,node0097:27018,node0129:27018,node0401:27018,node0441:27018,node0481:27018,node0521:27018,node0561:27018,node0601:27018,node0617:27018,node0649:27018,node0953:27018,node0985:27018,node1001:27018,nodea1331:27018,nodea1431:27018,node0113:27018,node0145:27018,node0411:27018,node0421:27018,node0431:27018,node0451:27018,node0461:27018,node0471:27018,node0491:27018,node0501:27018,node0511:27018,node0531:27018,node0541:27018,node0551:27018,node0571:27018,node0581:27018,node0591:27018,node0633:27018,node0665:27018,node0681:27018,node0921:27018,node0937:27018,node0969:27018,nodea1301:27018,nodea1401:27018"

for node in uri.split(","):
    try:
        client = MongoClient("mongodb://" + node, socketTimeoutMS=10 * 1000, connectTimeoutMS=10 * 1000,
                             serverSelectionTimeoutMS=10 * 1000)
        db = client["MassDB_072114"]["MassDB_072114"]
        print("testing " + node)
        ipython.magic("time x=list(db.find({'$and':[{'_id':{'$gte':1000000}}, {'_id':{'$lte':1000500}}]}))")
    except (ServerSelectionTimeoutError, ExecutionTimeout):
        print("Timeout")
    except ConnectionFailure:
        print("connect failure")

uri_client = MongoClient(uri)
uri_db = uri_client["MassDB_072114"]["MassDB_072114"]
sleep(5)
print("testing uri client")
ipython.magic("time x=list(uri_db.find({'$and':[{'_id':{'$gte':1000000}}, {'_id':{'$lte':1000500}}]}))")
