#!/bin/bash
# combines SQT parts

set -e

TEMP=$1

for ms2file in `ls *.ms2`; do
        MS2BASEFILENAME=${ms2file/.ms2/}
        echo "Combining SQT parts for "$MS2BASEFILENAME

        cd $TEMP
        grep '^H' $MS2BASEFILENAME"_1.sqt" > H_lines
        grep -vh '^H' $MS2BASEFILENAME"_"*.sqt > all_SQT_output
        cat H_lines all_SQT_output > "../"$MS2BASEFILENAME".sqt"
        rm H_lines
        rm all_SQT_output
        cd ..
done
