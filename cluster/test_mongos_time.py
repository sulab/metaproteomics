"""
Run using ipython for time to work

ipython3 test_mongos_time.py

"""
from time import sleep
from IPython import get_ipython
ipython = get_ipython()
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError, ExecutionTimeout, ConnectionFailure

mongos_hosts = """imsb0501
imsb0515
imsb0601
imsb0615
node0097
node0113
node0129
node0145
node0401
node0411
node0421
node0431
node0441
node0451
node0461
node0471
node0481
node0491
node0501
node0511
node0521
node0531
node0541
node0551
node0561
node0571
node0581
node0591
node0601
node0617
node0633
node0649
node0665
node0681
node0922
node0937
node0953
node0969
node0985
node1001
nodea1301
nodea1331
nodea1401
nodea1431""".split('\n')
mongos_hosts = [host + ':27018' for host in mongos_hosts]

for node in mongos_hosts:
    try:
        client = MongoClient("mongodb://" + node.strip(), socketTimeoutMS = 10*1000, connectTimeoutMS = 10*1000, serverSelectionTimeoutMS = 10*1000)
        db = client["MassDB_072114"]["MassDB_072114"]
        print("testing " + node)
        ipython.magic("time x=list(db.find({'$and':[{'_id':{'$gte':1000000}}, {'_id':{'$lte':1000500}}]}))")
    except (ServerSelectionTimeoutError, ExecutionTimeout):
        print("Timeout")
    except ConnectionFailure:
        print("connect failure")

all_uri = "mongodb://" + ','.join(mongos_hosts)
uri_client = MongoClient(all_uri)
uri_db = uri_client["MassDB_072114"]["MassDB_072114"]
sleep(5)
print("testing uri client")
ipython.magic("time x=list(uri_db.find({'$and':[{'_id':{'$gte':1000000}}, {'_id':{'$lte':1000500}}]}))")
