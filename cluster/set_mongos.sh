
#MONGOSNODES_PATH=/gpfs/home/sandip/mongosnodes
MONGOSNODES_PATH=$1
#mongos_node_prefix=node
mongos_node_prefix=$2

# mongo database and collection names for testing mongos connection
DBNAME="SeqDB_072114"
COLLNAME="SeqDB_072114"

if ! test -n "$(find $MONGOSNODES_PATH -maxdepth 1 -name 'node*' -print -quit)"; then
    echo "No mongos node information found at $MONGOSNODES_PATH -- exiting"
    exit 1
fi

function pick_nearby_mongos {
    for i in `ls $MONGOSNODES_PATH/${mongos_node_prefix}* | shuf`; do
        if grep -q `hostname` $i; then
            MONGOHOST=`head -1 $i`;
            break;
        fi;
    done
}

function pick_any_mongos {
    RANDOM_MONGOS=`ls $MONGOSNODES_PATH/{imsb,node}* | shuf | head -1`;
    MONGOHOST=`head -1 $RANDOM_MONGOS`;
}

echo
echo "Trying to connect to nearby Garibaldi mongos"
j="1"
while [ $j -lt 4 ]
do
    echo -n "Attempt #$j of 3"
    pick_nearby_mongos

    # test if MONGOHOST is set, and then test if a successful connection can be made to mongos on MONGOHOST
    # (if connection fails, unset MONGOHOST and run pick_nearby_mongos again)
    if [ ! -z ${MONGOHOST+x} ] && mongo $DBNAME --quiet --host $MONGOHOST --port 27018 --eval "db.$COLLNAME.findOne()" 2> /dev/null 1> /dev/null; then
        echo "... successfully connected to mongos on "$MONGOHOST;
        break;
    else
        echo "... $MONGOHOST failed"
        unset MONGOHOST
    fi
    j=$[$j+1]
done

# if unable to connect to a MONGOHOST from nearby nodes, try to pick any Garibaldi mongos...
if [ -z ${MONGOHOST+x} ]; then
    echo
    echo "Could not connect to nearby mongos. Trying to connect to any available Garibaldi mongos..."
    j="1"
    while [ $j -lt 4 ]; do
        echo -n "Attempt #$j of 3"
        pick_any_mongos
        
        # test if MONGOHOST is set, and then test if a successful connection can be made to mongos on MONGOHOST
        # (if connection fails, unset MONGOHOST and run pick_any_mongos again)
        if [ ! -z ${MONGOHOST+x} ] && mongo $DBNAME --quiet --host $MONGOHOST --port 27018 --eval "db.$COLLNAME.findOne()" 2> /dev/null 1> /dev/null; then
            echo "... successfully connected to mongos on "$MONGOHOST;
            break;
        else
            echo "... failed"
            unset MONGOHOST
        fi
        j=$[$j+1]
    done
fi

# if still unable to connect to a MONGOHOST, exit...
if [ -z ${MONGOHOST+x} ]; then
    echo "Unable to connect to mongos. Exiting";
    return 1;
fi

export $MONGOHOST
