import subprocess
from celery import Celery
import os
import time 
import random
import drmaa
import sys

#%% testing.... submit and wait for jobs

# Set this up first
# http://www.linuxjournal.com/content/speed-multiple-ssh-connections-same-server
""" ~/.ssh/config
Host gb
    ControlMaster auto
    ControlPath ~/.ssh/master-%r@%h:%p
    User gstupp
    HostName garibaldi.scripps.edu
"""

# then run: ssh -MNn gb

#or, I have this kind of set up
'''
in ~/ssh_master/run : 
    #!/bin/sh
    ssh -MNn gb
'''
# > nohup ~/bin/daemontools/command/supervise ~/ssh_master/ &

app = Celery('submitter', broker='redis://wl-cmadmin:6379/0', backend='redis://wl-cmadmin:6379')

#%% Given: path to ms2 files on garibaldi. Split ms2 files and generate job scripts

@app.task(bind=True)
def split_ms2_and_gen_jobs(self, ms2_path, ex_path):
    # TODO: The options for submit_blazmass.py need to be accesible from this function's arguments
    submit_blazmass_py = os.path.join(ex_path, 'submit_blazmass.py')
    script = """
    set -e
    python3 {submit_blazmass_py} {ms2_path} 20 -n 4 -m 8 -w 8
    """.format(**{'ms2_path': ms2_path, 'submit_blazmass_py': submit_blazmass_py})
    try:
        job_id = submit_job_script(script, working_dir = ms2_path, num_cores = 1, wall_time_hrs = 1, mem_gb = 4, name = "split")
        job_status = wait_for_job(job_id, delay = 5, verbose = True)
    except subprocess.CalledProcessError as exc:
        print('Failed to submit job')
        raise self.retry(exc=exc)
    if job_status != 'done':
        self.retry(max_retries = 4, countdown=10)
        
    #get list of job files
    output = subprocess.getoutput("ssh gb 'ls {}'".format(os.path.join(ms2_path, 'dummy', '*.job'))).splitlines()
    
    # insert checks here for number of job files (any having any, really)

    return output

@app.task(bind=True, default_retry_delay = 10)
def submit_job_file(self, job_path):
    """submit a PBS job file
    """
    sqt_file = job_path.replace('.job','.sqt')
    try:
        job_id = subprocess.check_output("qsub {}".format(job_path), shell = True).decode('utf-8').strip()
    except Exception as exc:
        print('Failed to submit job {}'.format(job_path))
        raise self.retry(max_retries = 4, exc=exc, countdown = random.randint(1,10))
        
    print("Submitted job {}".format(job_id))
    
    job_status = wait_for_job(job_id, delay = 10, verbose = True)
    
    if job_status != 'done':
        self.retry(max_retries = 4)
    
    #check if ms2 file exists
    output = subprocess.getoutput("[ ! -f {} ] && echo ERROR".format(sqt_file))
    if output == 'ERROR':
        print("{} does not exist or couldn't be checked".format(sqt_file))
        self.retry(max_retries = 4)

def wait_for_job(job_id, delay = 10, verbose = False):
    """ not using drmaa, wait for job forever (done or failed). return status"""
    while True:
        time.sleep(delay) #seconds
        with drmaa.Session() as s:
            job_status = s.jobStatus(job_id)

        if verbose: print(job_status)
        if job_status in ['done', 'failed']:
            return job_status

def submit_job_script(script, working_dir = None, num_cores = 4, wall_time_hrs = 8, mem_gb = 4, name = "job", out_file = "job_$PBS_JOBID.o"):
    
    job_boilerplate = '\n'.join(['#!/bin/bash',
                            '#PBS -d "{}"'.format(working_dir if working_dir else '~'),
                            '#PBS -l nodes=1:ppn={}'.format(num_cores),
                            '#PBS -l walltime={}:00:00'.format(wall_time_hrs),
                            '#PBS -j oe',
                            '#PBS -l mem={}gb'.format(mem_gb),
                            '#PBS -N "{}"'.format(name),
                            '#PBS -o "{}"'.format(out_file),                            
                            ])
    
    job_string = job_boilerplate + '\n' + script
    qsub = subprocess.Popen("qsub -", stdin = subprocess.PIPE, stdout = subprocess.PIPE, shell = True)
    qsub.stdin.write(job_string.encode('utf-8'))
    stdout, stderr = qsub.communicate()
    job_id = stdout.decode('utf-8').strip()
    return job_id
    


