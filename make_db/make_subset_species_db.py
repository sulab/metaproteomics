#!/usr/bin/env python3

# make_subset_species_fasta.py
# uses MongoDB databases 'taxonomy', 'taxDB', and 'ProtDB_072114' to 
# construct a FASTA file composed of 'well-annotated' proteins
# (proteins with at least 'species-level' annotation)
#
# usage: python3 make_subset_species_fasta.py [output_filename.fasta]
#
# Sandip Chatterjee
# 12/10/14

import sys
from pymongo import MongoClient

def main():

    # mongod, mongos connection information

    mongod = MongoClient('localhost', 27017)
    mongos = MongoClient('localhost', 27018)

    taxonomyDB = mongod['taxonomy']
    taxonomyDBColl = taxonomyDB['taxonomy']

    taxDB = mongod['taxDB']
    taxDBColl = taxDB['taxDB']

    protDB = mongos['ProtDB_072114']
    protDBColl = protDB['ProtDB_072114']

    # From Greg's taxonomy_parser.py notes:

    # 'rank':             one of: ['superkingdom', 'kingdom', 'subkingdom', 'superphylum', 'phylum', 'subphylum',
    #                     'superclass', 'class', 'subclass', 'infraclass', 'superorder', 'order', 'suborder',
    #                     'infraorder', 'parvorder', 'superfamily', 'family', 'subfamily', 'tribe', 'subtribe', 'genus', 
    #                     'subgenus', 'species group', 'species subgroup', 'species', 'subspecies', 'varietas', 'forma', 'no rank']

    ranks_of_interest = ['species', 'subspecies', 'varietas', 'forma']
    taxIDs = get_taxIDs(taxonomyDBColl, ranks_of_interest)
    protDB_IDs = get_protDB_IDs(taxDBColl, taxIDs)
    protDB_records = get_protDB_records(protDBColl, protDB_IDs)
    if len(sys.argv) > 1:
        output_fasta(protDB_records, filename=sys.argv[1])
    else:
        output_fasta(protDB_records)

    print('Finished')

def get_taxIDs(taxonomyDBColl, ranks):

    '''
    Gets taxIDs by querying 'taxonomy' for documents matching 'rank' values in ranks
    '''

    taxIDs = []
    for rank in ranks:
        query = taxonomyDBColl.find({'rank':rank})
        if query:
            for document in query:
                taxIDs.append(document['tax_id'])
        else:
            print('Query "rank" : "{}" returned no results'.format(rank))

    print('Retrieved {} taxIDs'.format(len(taxIDs)))

    return taxIDs

def get_protDB_IDs(taxDBColl, taxIDs):
    
    '''
    Returns all associated ProtDB IDs for a list of input taxIDs
    '''

    protDB_IDs = []
    for taxID in taxIDs:
        query = taxDBColl.find({'taxID':taxID})
        if query:
            for document in query:
                protDB_IDs.append(document['_id'])
        else:
            print('No tax ID "{}" found'.format(taxID))

    return protDB_IDs

def get_protDB_records(protDBColl, protDB_IDs):
    
    '''
    Returns a list of ProtDB records (protein records).
    (generator function)
    '''

    for protDB_ID in protDB_IDs:
        query = protDBColl.find_one({'_id':protDB_ID})
        if query:
            yield query
        else:
            print('ProtDB ID {} not found!'.format(protDB_ID))

def output_fasta(protDB_records, filename='output.fasta'):
    
    '''
    Writes ProtDB records to FASTA file
    '''

    with open(filename, 'w') as f:
        for record in protDB_records:
            f.write(format_record(record))

def format_record(record):
    
    '''
    Returns a formatted record (string) suitable for writing to a FASTA file
    '''

    defline, sequence = record['d'], record['s']

    formatted_record = ''.join(['>', str(record['_id']), '||', defline, '\n', '\n'.join(split_string_by_n(sequence, 80)), '\n'])

    return formatted_record

def split_string_by_n(long_string,n):

    '''
    Returns a list created by splitting a long string into n pieces
    '''

    while long_string:
        yield long_string[:n]
        long_string = long_string[n:]

if __name__ == '__main__':
    main()