#! /usr/bin/env python3
"""
Runs QC on all ms2 files in current folder (or takes first arg as path if given). Outputs plot images in same folder.
To properly parse the LC step of a file it expects the LC-step to be in the filename (before the extension)
as the last number delimeted by underscores. Example: 06162015_lysed_AWandH32_2.ms2, where 2 is the lc-step

The name of the dataset is the filename without the LC step.

If there are multiple possible names, the name of the folder will be used.


"""
import subprocess
import numpy as np
import glob
import os
import re
import sys
from itertools import chain
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

#%% Intensity of PrecursorInt
def log_precursor_int(ms2_file):
    grep = subprocess.Popen(['grep', 'PrecursorInt', ms2_file], stdout=subprocess.PIPE)
    cut = subprocess.Popen('cut -f3'.split(), stdin = grep.stdout, stdout = subprocess.PIPE)
    PrecursorInt = list(map(float,cut.communicate()[0].split()))
    PrecursorInt = [x if x else 1 for x in PrecursorInt]
    LogPrecursorInt = list(map(np.log10, PrecursorInt))
    return LogPrecursorInt
    
def get_fragment_ions(ms2_file):
    # returns a list of lists
    # each outer list contains list of fragment ion intensities for that scan
    groups = []
    group = []
    numbers = set(list(map(str,range(10))))
    with open(ms2_file) as f:
        for line in f:
            if line[0] in numbers:
                group.append(line.split()[1])
            else:
                if group:
                    groups.append(list(map(np.log10,map(float,group))))
                group = []
        if group:
            groups.append(list(map(np.log10,map(float,group))))
    return groups

                  

def get_lcstep(filename):
    # Parse lc step out of filename
    # failure to parse -> returns -1
    
    lcstep = re.split('_|-', filename.split('.')[0])[-1]    
    if len(lcstep) > 3:    
        lcstep = re.split('_|-', filename.split('.')[0])[-2]
    if lcstep[0] == 's' or lcstep[0] == 'S':
        lcstep = lcstep[1:]
    try:
        lcstep = int(lcstep)
        return lcstep
    except ValueError:
        return -1

        
# Number of scans per ms2 file
def num_scans_per_ms2(ms2_list):
    num_scans_per_file = {get_lcstep(ms2_file): int(subprocess.check_output(['grep', '-c', '^S', ms2_file])) for ms2_file in ms2_list}
    x,y = zip(*sorted(num_scans_per_file.items(), key = lambda x:x[0]))
    return x,y

#%% 
def plot_bar(x, y, title, xlabel, ylabel, save=None):
    # save: folder to save plot in
    plt.figure()
    plt.bar(x,y)
    plt.title(title)
    ax = plt.gca()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if save:
        plt.savefig(os.path.join(save, title.replace("\n","_") + ".png"), orientation='landscape', dpi=300)
        plt.savefig(os.path.join(save, title.replace("\n","_") + ".pdf"), orientation='landscape')
    
def plot_hist(data, title, xlabel, ylabel, save=None):
    # save: folder to save plot in
    plt.figure()     
    n,bins,patches = plt.hist(data, bins=50)
    plt.title(title)
    data_median = np.median(data)
    plt.plot([data_median, data_median], [min(n),max(n)], color="r")
    plt.text(x=data_median, y=max(n)*.9, s="<- median: {0:.2f}".format(data_median))
    ax = plt.gca()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if save:
        plt.savefig(os.path.join(save, title.replace("\n","_") + ".png"), orientation='landscape', dpi=300)
        plt.savefig(os.path.join(save, title.replace("\n","_") + ".pdf"), orientation='landscape')
#%%
if len(sys.argv) == 2:
    ms2_path = os.path.expanduser(sys.argv[1])
else:
    ms2_path = os.getcwd()

if __name__ == "__main__":
    ms2_list = glob.glob(os.path.join(ms2_path,'*.ms2'))
    print(ms2_list)
    names = set(filename.rsplit('.')[0].rsplit('_',1)[0] for filename in ms2_list)
    if len(names) == 1:
        name = os.path.basename(list(names)[0])
    else:
        name = os.path.basename(ms2_path)
        
    LogPrecursorInt = list(chain(*[log_precursor_int(ms2_file) for ms2_file in ms2_list]))
    fragment_ions = list(chain(*[get_fragment_ions(ms2_file) for ms2_file in ms2_list]))
    fragment_count = list(map(len,fragment_ions))
    median_int = list(map(np.median,fragment_ions))
    max_int = list(map(np.max,fragment_ions))
    step,num_scans = num_scans_per_ms2(ms2_list)
    
    try:
        plot_hist(LogPrecursorInt, "Intensity of Precursor Ions\n{}".format(name), 'Log10 Intensity', 'Count', ms2_path)
    except ValueError:
        print('WARNING!! - No Precursor Ion Intensitites!   Moving to next plot')
    plot_hist(fragment_count, 'Number of Fragment Ions per Scan\n{}'.format(name), 'Number of Ions', 'Count', ms2_path)
    plot_hist(median_int, 'Median Fragment Ion Intensity per Scan\n{}'.format(name), 'Log10 Intensity', 'Count', ms2_path)
    plot_bar(step, num_scans, "Number of Scans per LC-Step\n{}".format(name), "LC-Step", "Number of Scans", ms2_path)
