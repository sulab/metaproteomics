#! /usr/bin/env python3
"""
Submit job to gariabldi doing the specified QC
"""

import os
import argparse
from textwrap import dedent

def make_job(qcs, dir_path, queue):
    
    run_commands = ""
    if "ms2" in qcs:
        run_commands+="\n"+"ms2_QC.py"
    if "sqt" in qcs:
        run_commands+="\n"+"xcorr_deltcn_plot.py"
    if "dta" in qcs:
        run_commands+="\n"+"dtaselect_QC.py"
        
    job_boilerplate = '\n'.join(['#!/bin/bash',
                                 '#PBS -q {}'.format(queue),
                                 '#PBS -l nodes=1:ppn=2',
                                 '#PBS -l walltime=4:00:00',
                                 '#PBS -j oe',
                                 '#PBS -l mem=4gb',
                                 '#PBS -N "{}"'.format(dir_path),
                                 '#PBS -o "{}"'.format(dir_path+'/QC.$PBS_JOBID')])
    base_job_file = dedent("""
                    echo "################################################################################"
                    echo "Folder: {dir_path}"
                    echo "Running on node: `hostname`"
                    echo "################################################################################"
                    module load python/3.5.1
                    #PYTHONPATH=~/lib:$PYTHONPATH
                    #source /gpfs/home/gstupp/metaproteomics/venv3.5/bin/activate
                    cd {dir_path}
                    {run_commands}
                    """).format(dir_path=dir_path, run_commands=run_commands)
    
    job_file_path = os.path.join(dir_path,"QC.job")           
    with open(job_file_path, 'w') as f:
        print('Writing job file: ' + job_file_path)
        f.write(job_boilerplate + '\n' + base_job_file)
    
    os.system("qsub {}".format(job_file_path))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(epilog="If no options given, will run all")
    parser.add_argument('--ms2', help="Run ms2_QC", action='store_true')
    parser.add_argument('--sqt', help="Run sqt QC", action='store_true')
    parser.add_argument('--dta', help="run dtaselect QC", action='store_true')
    args = parser.parse_args()
    qcs = []
    
    dir_path = os.getcwd()
    if not (args.ms2 or args.sqt or args.dta):
        qcs = ['ms2','sqt','dta']
    else:            
        if args.ms2:
            qcs.append("ms2")
        if args.sqt:
            qcs.append("sqt")
        if args.dta:
            qcs.append("dta")
    make_job(qcs, dir_path, "workq")
